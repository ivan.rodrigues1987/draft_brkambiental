#language: pt
@Features @Cadastro @Cadastro_Rubrica
Funcionalidade:  Cadastrar Rubrica no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Rubrica" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | IDFATTIPOCALCULOCATSIMPLES |
      | 76       | 5                          |
      | 77       | 5                          |
      | 78       | 5                          |
      | 79       | 5                          |
      | 80       | 5                          |

    Dado a tela "Cadastro de Rubrica" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | TAMANHOMAXRUBRICA |
      | 76       | 22                |
      | 77       | 22                |
      | 78       | 22                |
      | 79       | 22                |
      | 80       | 22                |

  @Cadastro_Rubrica_CT001
  Esquema do Cenario: CT001 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = GERAL - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica   | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | <tipoRubrica> | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E selecionar a aba "Taxa de Atraso"
    E clicar na tela de Cadastro Rubrica o botao direito do mouse clicar em "Adicionar"
    E selecionar a Taxa de Atraso "<taxaAtraso>"
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | COFINS        | COFINS                | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO ACIMA]"
    Entao o resultado da pesquisa "[RUBRICA CADASTRADO ACIMA]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | classContabil | tipoRubrica | grupoReceita | tipoIntegracaoFinanceira | taxaAtraso | exiberubrica |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | 0016          | GERAL       | O.A. PARA    | MULTAS/JUROS             | Multa      | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Rubrica_CT002
  Esquema do Cenario: CT002 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = SERVICOS - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT003
  Esquema do Cenario: CT003 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = PARCELAMENTO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PARCELAMENTO | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |

  @Cadastro_Rubrica_CT004
  Esquema do Cenario: CT004 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = ENCARGOS - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | ENCARGOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT005
  Esquema do Cenario: CT005 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = PRODUTOS - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | S          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PRODUTOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT006
  Esquema do Cenario: CT006 - Cadastrar Rubrica com sucesso - Rubrica de desconto parcelamento - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | S          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PRODUTOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       | S                             |             |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT007
  Esquema do Cenario: CT007 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = OUTROS - CRED - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica   | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | S          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | OUTROS - CRED | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT008
  Esquema do Cenario: CT008 - Cadastrar Rubrica com sucesso - Aplicar como DEBITO e Tipo Rubrica = PAGTO DUPLIC - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | S          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PAGTO DUPLIC | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT009
  Esquema do Cenario: CT009 - Cadastrar Rubrica com sucesso - Com 3 Taxa de Atraso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | N     | S          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PAGTO DUPLIC | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E selecionar a aba "Taxa de Atraso"
    E clicar na tela de Cadastro Rubrica o botao direito do mouse clicar em "Adicionar"
    E selecionar a Taxa de Atraso "<taxaAtraso>"
    E clicar na tela de Cadastro Rubrica o botao direito do mouse clicar em "Adicionar"
    E selecionar a Taxa de Atraso "Juros"
    E clicar na tela de Cadastro Rubrica o botao direito do mouse clicar em "Adicionar"
    E selecionar a Taxa de Atraso "Correção Monetária"
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | taxaAtraso | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | Multa      | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Rubrica_CT010
  Esquema do Cenario: CT010 - Cadastrar Rubrica com sucesso - Sem Taxa de Atraso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT011
  Esquema do Cenario: CT011 - Cadastrar Rubrica com sucesso - ATIVA = Nao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 15/10/2019 | N     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PAGTO DUPLIC | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT012
  Esquema do Cenario: CT012 - Cadastrar Rubrica sem sucesso - Campo Tipo Rúbrica não informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 15/10/2019 | N     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] |             | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Tipo Rúbrica obrigatório!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT013
  Esquema do Cenario: CT013 - Cadastrar Rubrica sem sucesso - Campo "Aplicar como" nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 15/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PAGTO DUPLIC | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               |             |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | S                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Aplicar Como é obrigatório."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT014
  Esquema do Cenario: CT014 - Cadastrar Rubrica sem sucesso - Campo Classif Fiscal nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 15/10/2019 | N     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      |               |                       | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Classif. Fiscal obrigatória"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT015
  Esquema do Cenario: CT015 - Cadastrar Rubrica sem sucesso - Campo Codigo Auxiliar nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 14/10/2019 | N     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> |                |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O Campo Codigo Auxiliar e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT016
  Esquema do Cenario: CT016 - Cadastrar Rubrica sem sucesso - Campo Grupo Arrecadacao nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 14/10/2019 | N     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      |                  |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O Campo Grupo Arrecadacao e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT017
  Esquema do Cenario: CT017 - Cadastrar Rubrica sem sucesso - Campo Grupo Exibicao de Rubrica nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      |                          | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O Campo Grupo Exibe e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | taxaAtraso |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | Multa      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | taxaAtraso |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | MULTA      |

  @Cadastro_Rubrica_CT018
  Esquema do Cenario: CT018 - Cadastrar Rubrica sem sucesso - Nao informar as Aliquotas de Impostos aproximados - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  |             |                |             |              |              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O(s) Campo(s) Aliquota(s) deve(m) ter valor maior ou igual a zero!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT019
  Esquema do Cenario: CT019 - Alterar Cadastro de Rubrica com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "<codigo>"
    E selecionar no resultado da pesquisa "<codigo>" e clicar em "OK"
    E alterar a combo "Classificação Contábil" para "<classContabil>"
    E alterar a combo "Aplicar como" para "DEBITO"
    E clicar nos botoes "Salvar" e "Sim"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | codigo |
      | ARAGUAIA | REDENÇÃO | 0016          | 8067   |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | codigo |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 313107        | 27     |


  @Cadastro_Rubrica_CT020
  Esquema do Cenario: CT020 - Remover Taxa de Atraso da Rubrica com sucesso  - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO]"
    E selecionar no resultado da pesquisa "[RUBRICA CADASTRADO]" e clicar em "OK"
    E selecionar a aba "Taxa de Atraso"
    E clicar com o botao direito do mouse e clicar em "Remover" e selecionar em Taxa de Atraso "<taxaAtraso>"
    E clicar nos botoes "Salvar" e "Sim"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | taxaAtraso | codigo |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | Multa      | 3006   |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | taxaAtraso | codigo |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | MULTA      | 99     |

  @Cadastro_Rubrica_CT021
  Esquema do Cenario: CT021 - Alterar Cadastro de Rubrica para INATIVO  - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO]"
    E selecionar no resultado da pesquisa "[RUBRICA CADASTRADO]" e clicar em "OK"
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           |                |                           |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E alterar o cadastro de Rubrica para o status "INATIVO"
    E clicar nos botoes "Salvar" e "Sim"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | exiberubrica |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | JUROS        |

  @Cadastro_Rubrica_CT022
  Esquema do Cenario: CT022 - Cadastrar uma Rubrica e Alterar para um novo grupo ExibeRubrica - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | SERVICOS    | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | DESCONTO PARCELAMENTO          |                       |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | N              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO ESGOTO   |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Sair"
    E navegar pelo menu "Cadastro" e "Cadastro Grupo Exibe Rubríca"
    E preencher os seguintes campos em Cadastro de Grupo Exibe Rubrica:
      | cidade   | descricaoSimples    | ordemExibicao | exibePeriodoDeReferencia | embutirImposto |
      | <cidade> | [Descrição Simples] | 1             | S                        | S              |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Sair"
    E navegar pelo menu "Cadastro" e "Cadastro Rubrica"
    E clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO ACIMA]"
    E selecionar no resultado da pesquisa "[RUBRICA CADASTRADO ACIMA]" e clicar em "OK"
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica  | exibirNaFatura | considerarNoTotalDaFatura |
      | [RUBRICA EXIBE DESCRICAO] |                |                           |
    E alterar a combo "Aplicar como" para "DEBITO"
    E clicar nos botoes "Salvar" e "Sim"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT023
  Esquema do Cenario: CT023 - Cadastrar Rubrica com sucesso - Criar uma Rubrica (A) para Parcelamento do Parcelamento, abrir o cadastro de uma rubrica (B) de Parcelamento e definir a rubrica (A) como rubricaDeParcelamentoAssociado da (B)  - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PARCELAMENTO | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | PARCELAMENTO                   | S                     |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO AGUA     |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Novo"
    E preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PARCELAMENTO | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | [RUBRICA DESCRICAO]            | S                     |                               | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO AGUA     |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |

  @Cadastro_Rubrica_CT024
  Esquema do Cenario: CT024 - Cadastrar e Excluir Rubrica com sucesso- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando preencher os seguintes campos em Cadastro de Rubrica:
      | cidade   | codigo   | revisao   | vigencia   | ativa | contratual | grupoDeRubricas | classificacaoContabil | valor   | fatorReducao    | descricaoCompleta    | descricaoSimples    | tipoRubrica  | grupoReceita   | tipoIntegracaoFinanceira   | codigoAuxiliar    | SituacaoFaturar | rubricaDeParcelamentoAssociado | rubricaDeParcelamento | rubricaDeDescontoParcelamento | aplicarComo |
      | <cidade> | [codigo] | [revisao] | 13/10/2019 | S     | N          | GERAL           | <classContabil>       | [valor] | [Fator Redução] | [Descrição Completa] | [Descrição Simples] | PARCELAMENTO | <grupoReceita> | <tipoIntegracaoFinanceira> | [Código Auxiliar] |                 | PARCELAMENTO                   | N                     | N                             | DEBITO      |
    E na aba Aba Fatura preencher os seguintes campos:
      | grupoDeExibicaoDaRubrica | exibirNaFatura | considerarNoTotalDaFatura |
      | <exiberubrica>           | S              | N                         |
    E na aba Aba Arrecadacao preencher os seguintes campos:
      | grupoarrecadacao |
      | SERVICO AGUA     |
    E na aba Aba Fiscal preencher os seguintes campos:
      | classifFiscal | classifFiscalIndireta | aliquotaPIS | aliquotaCOFINS | aliquotaISS | aliquotaICMS | aliquotaINSS |
      | CSLL          | CSLL                  | 0           | 0              | 0           | 0            | 0            |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO ACIMA]"
    E selecionar no resultado da pesquisa "[RUBRICA CADASTRADO ACIMA]" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    Entao sera exibida a mensagem em tela: "Excluído com Sucesso"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | ARAGUAIA | REDENÇÃO | 0016          | O.A. PARA    | MULTAS/JUROS             | COFINS       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | classContabil | grupoReceita | tipoIntegracaoFinanceira | exiberubrica |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | 112101        | GERAL        | VALOR AGUA               | JUROS        |


  @Cadastro_Rubrica_CT025
  Esquema do Cenario: CT025 - Excluir a Rubrica sem sucesso - Registro filho localizado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Atendimento" e "Central de Atendimento"
    E pesquisar a ligacao "<ligacao>" em Central de Atendimento
    E clicar nos botoes "Vendas" e "Nova Venda"
    E clicar no botao "Novo"
    E selecionar a Rubrica "<descricaoRubrica>" em Composicao e clicar em "Confirmar"
    E preencher em Entrada o valor "10"
    E preencher em Parcela o valor "2"
    E selecionar em Periodo o valor "07/2019"
    E clicar nos botoes "Salvar" e "Ok"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Cadastro Rubrica"
    E clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "<codigoRubrica>"
    E selecionar no resultado da pesquisa "<codigoRubrica>" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "<mensagem>"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | ligacao | mensagem                                                                                          | descricaoRubrica  | codigoRubrica |
      | ARAGUAIA | REDENÇÃO | 1021071 | ORA-02292: integrity constraint (PARAGLOB.FK_FATURADETALHE_RUBRICA) violated - child record found | ANALISE CLOROFILA | 1410          |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | cidade                  | mensagem         | ligacao | descricaoRubrica | codigoRubrica |
      | [IGNORAR] | CACHOEIRO DE ITAPEMIRIM | Tester e cambiar | 4210827 | Completa WLWMQ   | 84761         |


  @Cadastro_Rubrica_CT026
  Esquema do Cenario: CT026 - Pesquisar Rubrica - Consulta Simples - Campo "Codigo"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "codigo" e o valor "[RUBRICA CADASTRADO]"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Rubrica_CT027
  Esquema do Cenario: CT027 - Pesquisar Rubrica - Consulta Simples - Campo "idcidade"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "[cidade]", o campo "idcidade" e o valor "<valor>"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 76    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Rubrica_CT028
  Esquema do Cenario: CT028 - Pesquisar Rubrica - Consulta Simples - Campo "rubrica"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "[cidade]", o campo "rubrica" e o valor "[RUBRICA NOME]"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Rubrica_CT029
  Esquema do Cenario: CT029 - Pesquisar Rubrica - Consulta Completa - Campo "codigo" e OPERADOR "Maior que"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar no campo "codigo", em operador "Maior que" e no valor "30"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Rubrica_CT030
  Esquema do Cenario: CT030 - Pesquisar Rubrica - Consulta Completa - Campo "idclasscontabil" e OPERADOR "Igual a"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar no campo "idclasscontabil", em operador "Igual a" e no valor "<codigo>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | codigo |
      | ARAGUAIA | 15     |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | codigo |
      | CACHOEIRO DE ITAPEMIRIM | 18     |


  @Cadastro_Rubrica_CT031
  Esquema do Cenario: CT031 - Pesquisar Rubrica - Consulta Completa - Com 3 critério de Pesquisa - "E"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar no campo "idcidade", em operador "Igual a" e no valor "<valor>"
    E clicar no botao "E"
    E selecionar no campo "codigo", em operador "Menor que" e em valor "50"
    E clicar no botao "E"
    E selecionar no campo "vigencia", em operador "Menor que" e em valor "60"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 76    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 2     |

  @Cadastro_Rubrica_CT032
  Esquema do Cenario: CT032 - Pesquisar Rubrica - Consulta Completa - Com 3 criterio de Pesquisa - "OU"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar no campo "idcidade", em operador "Igual a" e no valor "<valor>"
    E clicar no botao "OU"
    E selecionar no campo "codigo", em operador "Menor que" e em valor "50"
    E clicar no botao "OU"
    E selecionar no campo "vigencia", em operador "Menor que" e em valor "60"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 76    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 2     |


  @Cadastro_Rubrica_CT033
  Esquema do Cenario: CT033 - Pesquisa de Rubrica - Consulta Completa - Validar que todos os criterios de pesquisa estao sendo apresentados - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" de Pesquisa Rapida
    Entao validar todas as opcoes do campo Campo
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Rubrica_CT034
  Esquema do Cenario: CT034 - Sair da tela de Cadastro Rubrica pelo botao "Sair"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro Rubrica"
    Quando clicar no botao "Sair"
    Entao a tela "Cadastro de Rubrica" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |