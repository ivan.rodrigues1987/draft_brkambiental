#language: pt
@Features @Cadastro @Cadastro_AnaliseAgua
Funcionalidade: Cadastrar Analise Agua no SAN

  # O Grid de parametros sao extraidos da tabela "ANALISEAGUAPARAMETROS", cadastrados por fonte de abastecimento.
  # Os parametros podem variar entre as unidades.
  # Esta feature não possui parametros de configuracoes diferenciados relevantes ao teste.

  @Cadastro_AnaliseAgua_CT001
  Esquema do Cenario: CT001 - Cadastrar Analise de Agua para o periodo - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "07/2019", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Análise da Água"
    E preencher os seguintes campos em Analise de Agua:
      | cidade           | periodo | fonteDeAbastecimento     | periodoRef |
      | [Cidade Unidade] | 07/2019 | [Fonte de Abastecimento] | 06/2019    |
    E preencher em todas as linhas do Grid de parametros
      | valMedio | valMinDetectado | valMaxDetectado | amostrasRealizadas | amostraForaPadrao | totaldeAmostras | percAmostrasConfAposRec |
      | 0,56     | 0,14            | 3,71            | 48                 | 48                | 96              |                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_AnaliseAgua_CT002
  Esquema do Cenario: CT002 - Cadastrar Analise de Agua para o periodo - periodoRef nao prenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "07/2019", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Análise da Água"
    E preencher os seguintes campos em Analise de Agua:
      | cidade           | periodo | fonteDeAbastecimento   | periodoRef |
      | [Cidade Unidade] | 08/2019 | <fonteDeAbastecimento> |            |
    E preencher em todas as linhas do Grid de parametros
      | valMedio | valMinDetectado | valMaxDetectado | amostrasRealizadas | amostraForaPadrao | totaldeAmostras | percAmostrasConfAposRec |
      | 0,56     | 0,14            | 3,71            | 48                 | 40                | 88              |                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o Período Ref"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | fonteDeAbastecimento |
      | ARAGUAIA | UTS 01               |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | fonteDeAbastecimento |
      | CACHOEIRO DE ITAPEMIRIM | ETA - BURARAMA       |


  @Cadastro_AnaliseAgua_CT003
  Esquema do Cenario: CT003 - Cadastrar Analise de Agua para o periodo - Parametros da Analise da Agua nao preenchidos - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "07/2019", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Análise da Água"
    E preencher os seguintes campos em Analise de Agua:
      | cidade           | periodo | fonteDeAbastecimento   | periodoRef |
      | [Cidade Unidade] | 08/2019 | <fonteDeAbastecimento> | 07/2019    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Preencha todos os campos."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | fonteDeAbastecimento |
      | ARAGUAIA | UTS 01               |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | fonteDeAbastecimento |
      | CACHOEIRO DE ITAPEMIRIM | ETA - BURARAMA       |


  @Cadastro_AnaliseAgua_CT004
  Esquema do Cenario: CT004 - Cadastrar Analise de Agua para o periodo - periodo ja Separado  - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade " [Cidade Unidade]", periodo "[Periodo Separado]", Grupo "[Grupo]" e validar que a Fase "1" esta "Executada"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Análise da Água"
    E preencher os seguintes campos em Analise de Agua:
      | cidade           | periodo            | fonteDeAbastecimento     | periodoRef         |
      | [Cidade Unidade] | [Periodo Separado] | [Fonte de Abastecimento] | [Periodo Separado] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao foi possivel salvar os dados. Ja foi efetuada a separacao do roteiro para o periodo selecionado"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_AnaliseAgua_CT005
  Esquema do Cenario: CT005 - Sair da tela de Cadastro de Analise de Agua - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Análise da Água"
    Quando fechar Tela
    Entao a tela "Análise da Água" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |
