#language: pt
@Features @Cadastro @Cadastro_DebitoAutomatico
Funcionalidade: Cadastrar Debito Automatico no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Debito Automatico" esta conforme os seguintes parametros da entidade "ARRECADACAOPARAMETRO":
      | IDUNIDADEOPERACIONAL | METODOGERARREGIDSTROE | CAMINHOARQENVIODEBAUTO | ENVDEBAUTOPORCIDADE | PREFIXOTIPOE | TAMANHONSATIPOE | TAMANHONUMBANCOTIPOE | SUFIXOTIPOE | EXTENSAOTIPOE | ORDENACAONOMETIPOE  | PREFIXOTIPOC | TAMANHONSATIPOC | TAMANHONUMBANCOTIPOC | SUFIXOTIPOC | EXTENSAOTIPOC | ORDENACAONOMETIPOC  |
      | 21                   | 1                     | /mnt/para/enviar/      | N                   | null         | 6               | 3                    | null        | TXT           | BANCO;DATAATUAL;NSA | null         | 6               | 3                    | _C          | TXT           | BANCO;DATAATUAL;NSA |

    Dado a tela "Debito Automatico" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | IDFATTIPOCALCULOCATSIMPLES | TIPOCODBARRAS | ENVIODAFATURACRITICADA |
      | 76       | 5                          | 5             | N                      |
      | 77       | 5                          | 5             | N                      |
      | 78       | 5                          | 5             | N                      |
      | 79       | 5                          | 5             | N                      |
      | 80       | 5                          | 5             | N                      |


  @Cadastro_DebitoAutomatico_CT001
  Esquema do Cenario: CT001 - Cadastrar debito automatico para ligacao com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao buscar uma ligacao "sem" Debito Ativo em Debito Automatico
    E preencher o campo ligacao "[ligacao]" "sem" Debito Ativo em Debito Automatico
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar nos botoes "Salvar" e "Ok"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    Entao clicar no botao "NOVO"
    E preencher o campo "Ligação" com "[Ligação]" e pressionar ENTER em Débito Automatico
    Entao validar o campo "Ligação" na tela de Debito Automático
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT002
  Esquema do Cenario: CT002 -Cadastrar Debito Automatico para ligacao com sucesso - Arrecadador Banco do Brasil - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao buscar uma ligacao "sem" Debito Ativo em Debito Automatico
    E preencher o campo ligacao "[ligacao]" "sem" Debito Ativo em Debito Automatico
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador | agencia | conta |
      | 16/08/2018 | PRISMA      | 00      | 0000  |
    E clicar nos botoes "Salvar" e "Ok"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Arrecadador tem Versao de Layout do Debito Automatico diferente de '05'." em Debito Automatico
    E fechar tela em Debito Automatico
    Entao clicar no botao "NOVO"
    E preencher o campo "Ligação" com "[Ligação]" e pressionar ENTER em Débito Automatico
    Entao validar o campo "Ligação" na tela de Debito Automático
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT003
  Esquema do Cenario: CT003 - Cadastrar Debito Automatico para ligacao com sucesso - Arrecadador Santander - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao buscar uma ligacao "sem" Debito Ativo em Debito Automatico
    E preencher o campo ligacao "[ligacao]" "sem" Debito Ativo em Debito Automatico
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador | agencia | conta |
      | 16/08/2018 | BRADESCO    | 00      | 00    |
    E clicar nos botoes "Salvar" e "Ok"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Arrecadador tem Versao de Layout do Debito Automatico diferente de '05'." em Debito Automatico
    E fechar tela em Debito Automatico
    Entao clicar no botao "NOVO"
    E preencher o campo "Ligação" com "[Ligação]" e pressionar ENTER em Débito Automatico
    Entao validar o campo "Ligação" na tela de Debito Automático
    @ARAGUAIA @21
    Exemplos: #este CT será ignorado para a execução na esteira do dia 28/08/2019
      | unidade   |
      | [IGNORAR] |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |

  @Cadastro_DebitoAutomatico_CT004
  Esquema do Cenario: CT004 - Cadastrar de Debito Automatico para ligacao inexistente - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador | agencia | conta |
      | 16/08/2018 | PRISMA      | 00      | 0000  |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Ligacao nao encontrada: 0"

    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: # Apresenta Comportamento diferente de Araguaia
      | unidade |
      | IGNORAR |

  @Cadastro_DebitoAutomatico_CT005
  Esquema do Cenario:  CT005 - Cadastrar Debito Automatico para ligacao e nao preencher os demais campos obrigatorios - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E preencher o campo ligacao "[ligacao]" "sem" Debito Ativo em Debito Automatico
    E clicar nos botoes "Salvar" e "Ok"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Can not set int field logic.nsgc.cadastro.vo.ArrecadadorVO.id to null value"
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT006
  Esquema do Cenario: CT006 - Cadastrar Debito Automatico para ligacao que esta com Debito Automatico Ativo em outro Arrecadador - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E preencher o campo ligacao "[ligacao]" "com" Debito Ativo em Debito Automatico
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar nos botoes "Salvar" e "Ok"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "A ligacao ja possui um debito automatico ativo. Inative este antes de salvar."

    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT007
  Esquema do Cenario: CT007 - Pesquisar cadastro de Debito Automatico por ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao clicar no botao "NOVO"
    E em "Ligação" Pesquisar com o Filtro "Cod. Ligacao (CDC)" por "<ligacao>" em Débito Automático
    Entao validar o dado "16/08/2018" no campo "Cadastro DA" na tela de Debito Automático

    @ARAGUAIA @21
    Exemplos:
      | unidade  | ligacao |
      | ARAGUAIA | 1005101 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | ligacao |
      | CACHOEIRO DE ITAPEMIRIM | 7536    |

  @Cadastro_DebitoAutomatico_CT008
  Esquema do Cenario: CT008 - Pesquisar Ligacao no cadastro de Debito Automatico - Consulta Personalizada por Nome do Inquilino - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao clicar no botao "NOVO"
    E em "Ligação" Pesquisar com o Filtro "Nome do Inquilino" por "<nome>" em Débito Automático
    Entao validar o dado "16/08/2018" no campo "Cadastro DA" na tela de Debito Automático

    @ARAGUAIA @21
    Exemplos:
      | unidade  | nome                        |
      | ARAGUAIA | ROSANIA RODRIGUES DE AQUINO |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | nome                     |
      | CACHOEIRO DE ITAPEMIRIM | ARISTOTELES MORAES FILHO |

  @Cadastro_DebitoAutomatico_CT009
  Esquema do Cenario: CT009 - Pesquisar Ligacao no cadastro de Debito Automatico - Consulta Personalizada por Cod Ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    Entao clicar no botao "NOVO"
    E em "Ligação" Pesquisar com o Filtro "Cod. Ligacao (CDC)" por "<ligacao>" em Débito Automático
    Entao validar o dado "16/08/2018" no campo "Cadastro DA" na tela de Debito Automático

    @ARAGUAIA @21
    Exemplos:
      | unidade  | ligacao |
      | ARAGUAIA | 1005101 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | ligacao |
      | CACHOEIRO DE ITAPEMIRIM | 7536    |

  @Cadastro_DebitoAutomatico_CT010
  Esquema do Cenario: CT010 - Inativar o cadastro de Debito Automatico para ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E clicar no botao "Novo"
    E preencher o campo ligacao "[ligacao]" "com" Debito Ativo em Debito Automatico
    E preencher o campo "Ligação" com "[Ligação]" e pressionar ENTER em Débito Automatico
    E clicar no botao "Inativar"
    Entao  a janela de mensagem "Atenção" sera: "Registro inativado com sucesso."

    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT011
  Esquema do Cenário: CT011 - Validar a ajuda do Campo "Agencia"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E clicar no botao ajuda em "Agência" em Débito Automático
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Agência de débito do cliente. Regras de preenchimento:"
  #-Somente números
  #-Sem dígito verificador
  #-Tamanho máximo de quatro caracteres
  #-Tamanho máximo de 2 caracteres.
  #(Cada banco/arrecadador deverá ter a qtde. total de dígitos definida no cadastro de Arrecadador)


    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT012
  Esquema do Cenário: CT012 - Validar a ajuda do Campo "Agencia DV"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E clicar no botao ajuda em "Agência DV" em Débito Automático
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Não preencher"


    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT013
  Esquema do Cenário: CT013 - Validar a ajuda do Campo "Conta"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E clicar no botao ajuda em "Conta" em Débito Automático
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Conta corrente para débito do cliente. Regras de preenchimento:"
                                                        #-Informe todos os valores contidos na conta assim como o digito, não preencher com caracteres especiais como traço, ponto (.), vírgula (,).
                                                        #-Conta Corrente deve possuir somente números
                                                        #-Deve ser informado o dígito verificador, composto por um caracter: Pode ser letra ou número.
                                                        #-Tamanho máximo de 14 caracteres:
                                                        #conta corrente + dígito verificador
                                                        #Exemplos: 538221, 6706111P
                                                        #Para o arrecadador Caixa Econômica deve ser informada a operação, e a conta com zeros a esquerda:
                                                        #operação (3 posições) + conta corrente (8 posições) + dígito verificador (1 posição)
                                                        #Exemplos: 001000007680, 003000004492
                                                        #- Tamanho máximo de 14 caracteres. (Cada banco/arrecadador deverá ter a qtde. total de dígitos definida no cadastro de Arrecadador)"

    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT014
  Esquema do Cenário: CT014 - Validar a ajuda do Campo "Conta DV"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Débito Automático"
    E clicar no botao ajuda em "Conta DV" em Débito Automático
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Não preencher"


    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_DebitoAutomatico_CT015
  Esquema do Cenário: CT015 - Cadastrar Arrecadador e em seguida cadastrar debito automatico para a ligacao com o novo arrecadador
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador | diasCarencia | nrBanco        | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | Teste2      | 11           | [Numero Banco] | Ativo    | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 04              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | N                     | 0          | 0          | 0     | 0              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Débito Automático"
    Entao buscar uma ligacao "sem" Debito Ativo em Debito Automatico
    E preencher o campo ligacao "[ligacao]" "sem" Debito Ativo em Debito Automatico
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar nos botoes "Salvar" e "Ok"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    Entao clicar no botao "NOVO"
    E preencher o campo "Ligação" com "[Ligação]" e pressionar ENTER em Débito Automatico
    Entao validar o campo "Ligação" na tela de Debito Automático

    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |
