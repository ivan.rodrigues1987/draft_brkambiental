#language: pt
@Features @Cadastro @Cadastro_InstalacaoMedidor
Funcionalidade: Cadastrar Instalacao Medidor no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Instalacao de Medidor" esta conforme os seguintes parametros da entidade "SITUACAOLIGACAO":
      | ID | SITUACAOLIGACAO | ACEITAMEDIDOR | ENVIACOLETOR |
      | 1  | ATIVA           | S             | S            |
      | 3  | INATIVA         | N             | N            |
      | 4  | PROVISORIA      | S             | N            |
      | 6  | POTENCIAL       | N             | N            |

  @Cadastro_InstalacaoMedidor_CT001
  Esquema do Cenario: CT001 - Instalar Medidor para ligacao que nao possui medidor instalado - Motivo da Troca:  INSTALACAO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                | 2                 | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |

#Se for necessario adicionar periodo leitura anterior e leitura na retirada adicionar no seguinte formato:
#    ---
#    E preencher os seguintes campos em Instalacao de Medidor:
#      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor         | motivoDaTroca | programaDeTroca | lacre | observacao |
#      | 01/2018                | 2                 | [Data Atual] | [Medidor Disponivel] | 3       | WAGNER CATANHEDE | INSTALAÇÃO    | INSTALAÇÃO      | 2     |            |

  @Cadastro_InstalacaoMedidor_CT002
  Esquema do Cenario: Cenario: CT002 - Instalar Medidor para ligação que não possui medidor instalado - Executor "CALL CENTER-0800"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 100     | <executor> | <motivoDaTroca> | <programaDeTroca> | 321654 | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: # Unidade não posuui 0800 no combo
      | unidade   |
      | [IGNORAR] |

  @Cadastro_InstalacaoMedidor_CT003
  Esquema do Cenario: Cenario: CT003 - Instalar Medidor para ligação que possui medidor instalado - Motivo da Troca:  Parado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao     |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 1000    | <executor> | <motivoDaTroca> | <programaDeTroca> | 654321 | Medidor Parado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | Parado        | GERAL           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Motivo da Troca Inexistente para esta unidade
      | unidade   |
      | [IGNORAR] |


  @Cadastro_InstalacaoMedidor_CT004
  Esquema do Cenário: Cenario: CT004 - Instalar Medidor para ligação que possui medidor instalado - Motivo da Troca:  Preventiva
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao     |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 1000    | <executor> | <motivoDaTroca> | <programaDeTroca> | 654321 | Medidor Parado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | Preventiva    | GERAL           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Motivo da Troca Inexistente para esta unidade
      | unidade                 | executor       | motivoDaTroca    | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADRIANA SANTOS | TROCA PREVENTIVA | GERAL           |

  @Cadastro_InstalacaoMedidor_CT005
  Esquema do Cenário: Cenario: CT005 - Remover um medidor de uma ligação e instalar o mesmo em outra
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando  preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao com medidor ativo]"
    E clicar nos botoes " Remover Medidor  " e "Ok"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao     |
      | 10/2018                | 2                 | 01/11/2018 | [Medidor Disponivel] | 1000    | <executor> | <motivoDaTroca> | <programaDeTroca> | 654321 | Medidor Parado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | Preventiva    | GERAL           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Não Permitido para ligação Inativa - Levantar Regra.
      | unidade |
      | IGNORAR |

  @Cadastro_InstalacaoMedidor_CT006
  Esquema do Cenário:  CT006 - Cadastrar um medidor novo e efetuar a instalação em uma ligação nova
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    Quando clicar no botao "Novo"
    E preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | N         |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Ligação"
    E no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao   | bacia   | zonaDeMacro   | quadra | lote | inscricao   | nrImovelAntigo |
      | <zonaDePressao> | <bacia> | <zonaDeMacro> | 32     | 12   | [Inscricao] | 123            |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> | 1       | 3      | 23       | 15     | 2          |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> | 1       | 0         | 23       | 15          | 0            |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | 06/08/2018         | 06/08/2018           |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento   | tipoDeRede   | padraoDeInstalacao   | posicaoDaRede   | volPiscina | piscina | tipoDeCavalete   | situacaoDoCavalete   | tipoDeEsgotamento   | fonteDeAbastecimento   | capacidadeReservatorio   | poco | reservatorio   | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp   | bloqOsClienteCallCenter | posicaoRedeDeEsgoto   | orgaoPublico   | distanciaRamalAgua   | distanciaRamalEsgoto   |
      | 2                 | <tipoDePavimento> | <tipoDeRede> | <padraoDeInstalacao> | <posicaoDaRede> | 20000      | S       | <tipoDeCavalete> | <situacaoDoCavalete> | <tipoDeEsgotamento> | <fonteDeAbastecimento> | <capacidadeReservatorio> | S    | <reservatorio> | 2         | 200              | 40        | <ligacaoTemp> | S                       | <posicaoRedeDeEsgoto> | <orgaoPublico> | <distanciaRamalAgua> | <distanciaRamalEsgoto> |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    Quando fechar a tela de Cadastro de Ligacao
    E navegar pelo menu "Cadastro" e "Instalação de Medidor"
    E preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca | lacre  | observacao         |
      | 03/2016                | 2                 | 01/01/2019 | [Medidor Disponivel] | 100     | <executor> | <motivoDaTroca> | GERAL           | 123456 | Teste Automatizado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | inquilino                  | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade | executor         | motivoDaTroca |
      | ARAGUAIA | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | BOM ESTADO | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | [CODIGO CNPJ SEM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 | CALL CENTER-0800 | Preventiva    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo | aquisicao | situacao | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade | executor     | motivoDaTroca    |
      | CACHOEIRO DE ITAPEMIRIM | ALFA   | Doado     | NOVO     | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     | ANA DA SILVA | TROCA PREVENTIVA |

  @Cadastro_InstalacaoMedidor_CT007
  Esquema do Cenario: Cenario: CT007 - Período de leitura anterior menor que 3 períodos do atual
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 03/2016                | 2                 | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_InstalacaoMedidor_CT008
  Esquema do Cenario: Cenario: CT008 - Leitura com mais dígitos do que o numeror de dígitos do Medidor retirada ou instalação
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao | medidor              | leitura          | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao         |
      | 01/2018                | 2                 | 07/05/2018 | [Medidor Disponivel] | 9999999999999999 | <executor> | <motivoDaTroca> | <programaDeTroca> | 878589 | Teste Automatizado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |

  @Cadastro_InstalacaoMedidor_CT009
  Esquema do Cenário: CT009 - Validar se é possível remoção de Medidor em ligação de grupo em processo de leitura
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "<periodo>", Grupo "<grupo>" e validar que a Fase "2" esta "Executada"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Instalação de Medidor"
    E preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E clicar no botao "Remover Medidor" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor removido com sucesso."

  @ARAGUAIA @21
    Exemplos:  # [Ligacao com medidor ativo] está trazendo inativo
      | unidade  | periodo | grupo   |
      | ARAGUAIA | 05/2018 | GRUPO 7 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | periodo | grupo        |
      | CACHOEIRO DE ITAPEMIRIM | 03/2006 | G1 - SETOR 5 |


  @Cadastro_InstalacaoMedidor_CT010
  Esquema do Cenario: CT010 - Aplicar período de leitura anterior maior que período de leitura atual
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 12/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao e permitido selecionar um periodo futuro."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_InstalacaoMedidor_CT011
  Esquema do Cenário: Cenario: CT011 - Aplicar leitura de retirada menor que a última leitura
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | medidor              | instalacao   | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao         |
      | 01/2018                | 5400              | [Medidor Disponivel] | [Data Atual] | 5400    | <executor> | <motivoDaTroca> | <programaDeTroca> | 878589 | Teste Automatizado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_InstalacaoMedidor_CT012
  Esquema do Cenario: CT012 - Selecionar Medidor já instalado em outra ligação
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Ligacao" e preencher em valor "0"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao ha dados que satisfacam esta pesquisa!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_InstalacaoMedidor_CT013
  Esquema do Cenario: Cenario: CT013 - Instalar Medidor - campo "Instalacao" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao |
      | 01/2019                | 2                 |            | [Medidor Disponivel] | 100     | <executor> | <motivoDaTroca> | <programaDeTroca> | 321654 | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data de instalação!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | INSTALAÇÃO    | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor       | motivoDaTroca | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADRIANA SANTOS | GERAL         | COMERCIAL       |

  @Cadastro_InstalacaoMedidor_CT014
  Esquema do Cenário: Cenario: CT014 - Instalar Medidor - campo "Medidor" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao     |
      | 01/2019                | 2                 | [Data Atual] |         | 1000    | <executor> | <motivoDaTroca> | <programaDeTroca> | 654321 | Medidor Parado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o medidor!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | Preventiva    | GERAL           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor       | motivoDaTroca    | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADRIANA SANTOS | TROCA PREVENTIVA | GERAL           |


  @Cadastro_InstalacaoMedidor_CT015
  Esquema do Cenario: Cenario: CT015 - Instalar Medidor - campo "Executor" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao     |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 1000    | <executor> | <motivoDaTroca> | <programaDeTroca> | 654321 | Medidor Parado |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe um Executor!"


  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor | motivoDaTroca | programaDeTroca |
      | ARAGUAIA |          | Preventiva    | GERAL           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Motivo da Troca Inexistente para esta unidade
      | unidade                 | executor | motivoDaTroca    | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM |          | TROCA PREVENTIVA | GERAL           |


  @Cadastro_InstalacaoMedidor_CT016
  Esquema do Cenario: CT016 - Instalar Medidor - campo "Motivo da Troca" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 100     | <executor> | <motivoDaTroca> | <programaDeTroca> | 321654 | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o motivo da troca do hidrômetro."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 |               | INSTALAÇÃO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor       | motivoDaTroca | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADRIANA SANTOS |               | COMERCIAL       |


  @Cadastro_InstalacaoMedidor_CT017
  Esquema do Cenario: Cenario: CT017 - Instalar Medidor - campo "Programa de Troca" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E preencher os seguintes campos em Instalacao de Medidor:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre  | observacao |
      | 01/2019                | 2                 | [Data Atual] | [Medidor Disponivel] | 100     | <executor> | <motivoDaTroca> | <programaDeTroca> | 321654 | Medidor    |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o programa da troca do hidrômetro."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | executor         | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | CALL CENTER-0800 | Preventiva    |                 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | executor       | motivoDaTroca    | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM | ADRIANA SANTOS | TROCA PREVENTIVA |                 |


  @Cadastro_InstalacaoMedidor_CT018
  Esquema do Cenario: Cenario: CT018 - Remover Medidor da ligação
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando preencher a combo de Cidade "[Cidade Unidade]" em Instalacao de Medidor
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao com medidor ativo]"
    E clicar no botao "Remover Medidor" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor removido com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_InstalacaoMedidor_CT019
  Esquema do Cenário: Cenario: CT019 - Validar a Pesquisa Instalação Medidor - Consulta Simples - Campo Cod Ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Ligacao" e preencher em valor "<ligacao>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  | ligacao |
      | ARAGUAIA | 1011639 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | ligacao |
      | CACHOEIRO DE ITAPEMIRIM | 4210800 |


  @Cadastro_InstalacaoMedidor_CT020
  Esquema do Cenario: Cenario: CT020 - Validar a Pesquisa Instalação Medidor - Consulta Simples - Campo Cod Medidor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Medidor" e preencher em valor "<medidor>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela


  @ARAGUAIA @21
    Exemplos:
      | unidade  | medidor |
      | ARAGUAIA | 10      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | medidor |
      | CACHOEIRO DE ITAPEMIRIM | 20      |

  @Cadastro_InstalacaoMedidor_CT021
  Esquema do Cenário:  CT021 - Validar a Pesquisa de instalção Medidor - Consulta Completa - Com 3 critérios de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Cod. Ligacao", em operador "Diferente de" e em valor "0"
    E clicar no botao "E"
    E selecionar em campo "Cod. Medidor", em operador "Igual a" e em valor "<medidor>"
    E clicar no botao "E"
    E selecionar em campo "dtinstalacao", em operador "Contém" e em valor "<ano>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  | medidor | ano  |
      | ARAGUAIA | 6242    | 2013 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | medidor | ano  |
      | CACHOEIRO DE ITAPEMIRIM | 44173   | 2006 |


  @Cadastro_InstalacaoMedidor_CT022
  Esquema do Cenario: CT022 - Validar a Pesquisa Instalação Medidor - Consulta Completa - Com 3 critério de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Cod. Ligacao", em operador "Diferente de" e em valor "0"
    E clicar no botao "OU"
    E selecionar em campo "Cod. Medidor", em operador "Igual a" e em valor "<medidor>"
    E clicar no botao "OU"
    E selecionar em campo "dtinstalacao", em operador "Contém" e em valor "<ano>"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | medidor | ano  |
      | ARAGUAIA | 6242    | 2013 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | medidor | ano  |
      | CACHOEIRO DE ITAPEMIRIM | 44173   | 2006 |


  @Cadastro_InstalacaoMedidor_CT023
  Esquema do Cenario: Cenario: CT023 - Sair da tela de Cadastro Instalação Medido pelo botao "Sair"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Instalação de Medidor"
    Quando  preencher a combo de Cidade "[Cidade Unidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E clicar no botao "Sair"
    Entao a tela "Instalação de Medidor" estara fechada

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |
