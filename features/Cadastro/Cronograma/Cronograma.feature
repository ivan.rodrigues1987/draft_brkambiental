#language: pt
@Features @Cadastro @Cadastro_Cronograma
Funcionalidade: Cadastrar Cronograma no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro Cronograma" esta conforme os seguintes parametros da entidade "FASE":
      | ID | FASE               | SEQEXEC | TOLANTES | TOLDEPOIS | APENASNOPRAZO | EXIGEINICIO |
      | 1  | Separacao Coletor  | 1       | 0        | 0         | N             | N           |
      | 2  | Envio para Coletor | 2       | 1        | 1         | N             | S           |
      | 3  | Retorno Coletor    | 3       | 0        | 0         | N             | S           |
      | 4  | Repasse            | 4       | 0        | 0         | N             | S           |
      | 5  | Retorno Repasse    | 5       | 0        | 0         | N             | S           |
      | 6  | Calculo Faturas    | 6       | 0        | 0         | N             | N           |
      | 7  | Impressao          | 7       | 0        | 0         | N             | S           |

    Dado a tela "Cadastro Cronograma" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | DIASENTREGAVENCTO |
      | 76       | 5                 |
      | 77       | 5                 |
      | 78       | 5                 |
      | 79       | 5                 |
      | 80       | 5                 |

    Dado a tela "Cadastro Cronograma" esta conforme os seguintes parametros da entidade "PARAMETROSCRONOGRAMA":
      | IDCIDADE | INTERVSEPARACAOELEIT | INTERVENVIOCOLETORELEIT | INTERVRETORNOELEIT | INTERVREPASSEELEIT | INTERVRETREPASSEELEIT | INTERVCALCULOELEIT | INTERVIMPRESSAOELEIT | INTERVENTREGAFATEVENC | NRMINDIASCONS | NRMAXDIASCONS | NRMINDIASVENC | NRMAXDIASVENC | NRMINDIASSEPIMP | NRMAXDIASSEPIMP | QTDIASMINVENCIMENTO | QTDIASMAXVENCIMENTO | QTDIASMINLEITURA | QTDIASMAXLEITURA |
      | 76       | null                 | null                    | null               | null               | null                  | null               | null                 | null                  | null          | null          | null          | 10            | 0               | 6               | 5                   | 20                  | 29               | 34               |
      | 77       | null                 | null                    | null               | null               | null                  | null               | null                 | null                  | null          | null          | null          | 10            | 0               | 6               | 5                   | 20                  | 29               | 34               |
      | 78       | null                 | null                    | null               | null               | null                  | null               | null                 | null                  | null          | null          | null          | 10            | 0               | 6               | 5                   | 20                  | 29               | 34               |
      | 79       | null                 | null                    | null               | null               | null                  | null               | null                 | null                  | null          | null          | null          | 10            | 0               | 6               | 5                   | 20                  | 29               | 34               |
      | 80       | null                 | null                    | null               | null               | null                  | null               | null                 | null                  | null          | null          | null          | 10            | 0               | 6               | 5                   | 20                  | 29               | 34               |

    Dado a tela "Cadastro Cronograma" esta conforme os seguintes parametros da entidade "PARAMETROEVENTO":
      | ID | PARAMETRO           | VALOR |
      | 21 | AUTOMACAO_SEPARACAO | N     |

  @Cadastro_Cronograma_CT001
  Esquema do Cenario: CT001 - Cadastrar Cronograma com sucesso para um Grupo Novo sem cronograma para o periodo anterior
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cronograma_CT002
  Esquema do Cenario: CT002 - Cadastrar Cronograma com sucesso para um Grupo que possui cronograma para o periodo anterior
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT003
  Esquema do Cenario: CT003 - Alterar Cronograma com sucesso - Todos os campos
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT004
  Esquema do Cenario: CT004 - Cadastrar Cronograma com sucesso - Opção de Coletar Dados Localização - SIM
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT005
  Esquema do Cenario: CT005 - Cadastrar Cronograma -  Data prevista da fase Separacao Coletor nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | 55/55/5555                   | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Separacao Coletor."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT006
  Esquema do Cenario: CT006 - Cadastrar Cronograma -  Data prevista da fase Envio para Coletor nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | 55/55/5555                   | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Envio para Coletor."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cronograma_CT007
  Esquema do Cenario: CT007 - Cadastrar Cronograma -  Data prevista da fase Retorno Coletor nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | 55/55/5555                 | [Data]              | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Retorno Coletor."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cronograma_CT008
  Esquema do Cenario: CT008 -  Cadastrar Cronograma -  Data prevista da fase Repasse nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | 55/55/5555          | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Repasse."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cronograma_CT009
  Esquema do Cenario: CT009 - Cadastrar Cronograma -  Data prevista da fase Retorno Repasse nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | 55/55/5555                 | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Retorno Repasse."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cronograma_CT010
  Esquema do Cenario: CT010 - Cadastrar Cronograma -  Data prevista da fase Calculo Faturas nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | 55/55/5555                | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Calculo Faturas."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT011
  Esquema do Cenario: CT011 - Cadastrar Cronograma -  Data prevista da fase Impressao nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | 55/55/5555            | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a data prevista da fase: Impressao."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT012
  Esquema do Cenario: CT012 - Cadastrar Cronograma -  Data de Entrega nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | S                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega    | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | 55/55/5555 | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe todas as datas."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT013
  Esquema do Cenario: CT013 - Cadastrar Cronograma com as datas das Fases de Separação e Impressão fora do Intervalo - Maior que 6 dias
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | 03                           | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | 10                    | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data]     | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Fases de Separação e Impressão estão fora do Intervalo permitido, entre:  0 até 6 dias"
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cronograma_CT014
  Esquema do Cenario: CT014 - Cadastrar Cronograma com a diferença de dias entre o vencimento e a entrega menor que o número mínimo permitido - 5 dias
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento      | entrega      | leitAtual | proximaLeit | infDeVendaInicio |
      | <diaVencimento> | <diaEntrega> | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "A diferença de dias entre o vencimento e a entrega do grupo é menor que o número mínimo de dias"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | diaVencimento | diaEntrega |
      | Araguaia | 11            | 14         |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | diaVencimento | diaEntrega |
      | CACHOEIRO DE ITAPEMIRIM | 11            | 14         |

  @Cadastro_Cronograma_CT015
  Esquema do Cenario: CT015 - Cadastrar Cronograma a Data do vencimento fora do intervalo permitido - entre 5 e 20 dias da leitura atual
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma prencher os seguintes campos
      | dataPrevistaSeparacaoColetor | dataPrevistaEnvioParaColetor | dataPrevistaRetornoColetor | dataPrevistaRepasse | dataPrevistaRetornoRepasse | dataPrevistaCalculoFatura | dataPrevistaImpressao | coletarDadosLocalizacao |
      | [Data]                       | [Data]                       | [Data]                     | [Data]              | [Data]                     | [Data]                    | [Data]                | N                       |
    E em Dados Localizacao preencher os seguintes campos
      | vencimento   | entrega | leitAtual | proximaLeit | infDeVendaInicio |
      | [Data Acima] | [Data]  | [Data]    | [Data]      | [Data]           |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "<mensagem>"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | mensagem                                                                                 |
      | Araguaia | Data do vencimento esta fora do intervalo permitido, entre: 5 e 20 dias da leitura atual |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | mensagem                                                                                  |
      | CACHOEIRO DE ITAPEMIRIM | Data do vencimento esta fora do intervalo permitido, entre: 10 e 40 dias da leitura atual |

  @Cadastro_Cronograma_CT016
  Esquema do Cenario: CT016 - Replicar Cronograma Pendente para outra cidade-Grupo que possui cronograma criado  - Fase de calculo faturas não iniciada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    Quando preencher Cidade "<cidade unidade replicada>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E validar os campos da coluna Data Prevista
    E em Cadastro de Cronograma clicar no botao "Replicar Cronograma" e selecionar a opcao "<cidade unidade principal>" e Salvar
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "[Grupo]"
    Entao confirmar que os dados do cronograma foram atualizados com os dados do cronograma replicado
    @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade unidade principal | cidade unidade replicada |
      | Araguaia | SAO DOMINGOS DO ARAGUAI  | BOM JESUS DO TOCANTINS   |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT16 - Eh necessario duas ou mais cidades para executar a replica, o que nao se aplica a CACHOEIRADEITAPEMIRIM
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Cronograma_CT017
  Esquema do Cenario: CT017 - Replicar Cronograma Pendente para outra cidade-Grupo que não possui cronograma criado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    Quando preencher Cidade "<cidade unidade replicada>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E validar os campos da coluna Data Prevista
    E em Cadastro de Cronograma clicar no botao "Replicar Cronograma" e selecionar a opcao "<cidade unidade principal>" e Salvar
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "[Grupo]"
    Entao confirmar que os dados do cronograma foram atualizados com os dados do cronograma replicado
    @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade unidade principal | cidade unidade replicada |
      | Araguaia | SAO DOMINGOS DO ARAGUAI  | BOM JESUS DO TOCANTINS   |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT17 - Eh necessario duas ou mais cidades para executar a replica, o que nao se aplica a CACHOEIRADEITAPEMIRIM
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Cronograma_CT018
  Esquema do Cenario: CT018 - Replicar Cronograma Executado para outra cidade que possui cronograma criado e que a Fase de calculo faturas não foi iniciada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "<grupo principal>" e validar que a Fase "1" esta "Pendente"
    Quando preencher Cidade "<cidade unidade replicada>", periodo "[Periodo Atual]", Grupo "<grupo secundario>" e validar que a Fase "1" esta "Pendente"
    E validar os campos da coluna Data Prevista
    E em Cadastro de Cronograma clicar no botao "Replicar Cronograma" e selecionar a opcao "<cidade unidade principal>" e Salvar
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "<grupo principal>"
    Entao confirmar que os dados do cronograma foram atualizados com os dados do cronograma replicado
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupo principal | grupo secundario | cidade unidade principal | cidade unidade replicada |
      | Araguaia | GRUPO 4         | GRUPO 4          | SAO DOMINGOS DO ARAGUAI  | BOM JESUS DO TOCANTINS   |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT18 - Eh necessario duas ou mais cidades para executar a replica, o que nao se aplica a CACHOEIRADEITAPEMIRIM
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Cronograma_CT019
  Esquema do Cenario: CT019 - Replicar Cronograma para outra cidade/Grupo - Fase de calculo faturas iniciada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "12/2018", Grupo "GRUPO 21" e validar que a Fase "1" esta "Executada"
    E validar os campos da coluna Data Prevista
    E em Cadastro de Cronograma clicar no botao "Replicar Cronograma" e selecionar a opcao "REDENÇÃO" e Salvar
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O cronograma deste grupo nao pode ser alterado neste periodo,  pois a fase de calculo faturas ja foi iniciada."
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | Araguaia |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT19 - Eh necessario duas ou mais cidades para executar a replica, o que nao se aplica a CACHOEIRADEITAPEMIRIM
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Cronograma_CT020
  Esquema do Cenario: CT020 - Replicar Cronograma Executado para outra cidade-Grupo que não possui cronograma criado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "<cidade unidade principal>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    Quando preencher Cidade "<cidade unidade replicada>", periodo "[Periodo Atual]", Grupo "[Grupo]" e validar que a Fase "1" esta "Pendente"
    E validar os campos da coluna Data Prevista
    E em Cadastro de Cronograma clicar no botao "Replicar Cronograma" e selecionar a opcao "<cidade unidade principal>" e Salvar
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    Entao confirmar que os dados do cronograma foram atualizados com os dados do cronograma replicado
    @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade unidade principal | cidade unidade replicada |
      | Araguaia | SAO DOMINGOS DO ARAGUAI  | BOM JESUS DO TOCANTINS   |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT20 - Eh necessario duas ou mais cidades para executar a replica, o que nao se aplica a CACHOEIRADEITAPEMIRIM
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Cronograma_CT021
  Esquema do Cenario: CT021 - Alterar data de vencimento no Cronograma com sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "[Periodo Atual]", Grupo "<grupo>" e validar que a Fase "1" esta "Pendente"
    E em Fases do Cronograma alterar o campo "Vencimento" com "[Data]" em Dados Localizacao
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupo   |
      | Araguaia | GRUPO 4 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupo        |
      | CACHOEIRO DE ITAPEMIRIM | G1 - SETOR 3 |


  @Cadastro_Cronograma_CT022
  Esquema do Cenario: CT022 - Alterar data de vencimento do Cronograma sem sucesso - Fase de calculo faturas Executada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cronograma"
    Quando preencher Cidade "[Cidade Unidade]", periodo "12/2018", Grupo "<grupo>" e validar que a Fase "1" esta "Executada"
    E em Fases do Cronograma alterar o campo "Vencimento" com "[Data]" em Dados Localizacao
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O cronograma deste grupo nao pode ser alterado neste periodo,  pois a fase de calculo faturas ja foi iniciada."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupo   |
      | Araguaia | GRUPO 4 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupo        |
      | CACHOEIRO DE ITAPEMIRIM | G1 - SETOR 3 |
