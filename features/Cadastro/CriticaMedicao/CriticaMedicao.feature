#language: pt
@Features @Cadastro @Cadastro_CriticaMedicao
Funcionalidade:  Cadastrar Critica Medicao no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Critica de Medicao" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | TAMANHOMAXRUBRICA |
      | 76       | 22                |
      | 77       | 22                |
      | 78       | 22                |
      | 79       | 22                |
      | 80       | 22                |

  @Cadastro_CriticaMedicao_CT001
  Esquema do Cenario: CT001 - Cadastro de Critica de Medicao completo com sucesso- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando eu preencher os seguintes campos de cadastro de critica de medicao
      | cidade   | sigla | descricao             | codigocoletor    | ativo | volumede   | volumeate   | percentualvariacao | percentualate   | consumozero | ligacaonova | leituramenor | leituranaoefetuada | inversaohm | residuo | limiteinferior | analisecritica | exibir | confirmarleitura | repassesimultanea | repasse | variacaodeconsumo | avisoretencao           | mensagemdispositivo                                  |
      | <cidade> | 907   | LEITURA NAO REALIZADA | [CODIGO COLETOR] | S     | [VOLUMEDE] | [VOLUMEATE] | [PERCENTUALV]      | [PERCENTUALATE] | S           | S           | S            | S                  | S          | S       | S              | S              | S      | S                | S                 | S       | S                 | teste de campo retencao | teste de campo Mensagem dispositivo variacao consumo |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_CriticaMedicao_CT002
  Esquema do Cenario: CT002 - Cadastro de Critica de Medicao - validar campo obrigatorio Cidade - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando eu preencher os seguintes campos de cadastro de critica de medicao
      | cidade   | sigla | descricao          | codigocoletor    | ativo | volumede   | volumeate   | percentualvariacao | percentualate   | consumozero | ligacaonova | leituramenor | leituranaoefetuada | inversaohm | residuo | limiteinferior | analisecritica | exibir | confirmarleitura | repassesimultanea | repasse | variacaodeconsumo | avisoretencao           | mensagemdispositivo                                  |
      | <cidade> | 905   | LEITURA = ANTERIOR | [CODIGO COLETOR] | S     | [VOLUMEDE] | [VOLUMEATE] | [PERCENTUALV]      | [PERCENTUALATE] | S           | S           | S            | S                  | S          | S       | S              | S              | S      | S                | S                 | S       | S                 | teste de campo retencao | teste de campo Mensagem dispositivo variacao consumo |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Cidade Não Informada"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade |
      | ARAGUAIA | Cadastro de Crítica de Medição |        |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao |        |

  @Cadastro_CriticaMedicao_CT003
  Esquema do Cenario: CT003 - Cadastro de Critica de Medicao - validar campo obrigatorio Sigla- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando eu preencher os seguintes campos de cadastro de critica de medicao
      | cidade   | sigla | descricao             | codigocoletor    | ativo | volumede   | volumeate   | percentualvariacao | percentualate   | consumozero | ligacaonova | leituramenor | leituranaoefetuada | inversaohm | residuo | limiteinferior | analisecritica | exibir | confirmarleitura | repassesimultanea | repasse | variacaodeconsumo | avisoretencao           | mensagemdispositivo                                  |
      | <cidade> |       | LEITURA NAO REALIZADA | [CODIGO COLETOR] | S     | [VOLUMEDE] | [VOLUMEATE] | [PERCENTUALV]      | [PERCENTUALATE] | S           | S           | S            | S                  | S          | S       | S              | S              | S      | S                | S                 | S       | S                 | teste de campo retencao | teste de campo Mensagem dispositivo variacao consumo |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Sigla Não Informada"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_CriticaMedicao_CT004
  Esquema do Cenario: CT004 - Cadastro de Critica de Medicao - validar campo obrigatorio Descricao- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando eu preencher os seguintes campos de cadastro de critica de medicao
      | cidade   | sigla | descricao | codigocoletor    | ativo | volumede   | volumeate   | percentualvariacao | percentualate   | consumozero | ligacaonova | leituramenor | leituranaoefetuada | inversaohm | residuo | limiteinferior | analisecritica | exibir | confirmarleitura | repassesimultanea | repasse | variacaodeconsumo | avisoretencao           | mensagemdispositivo                                  |
      | <cidade> | 907   |           | [CODIGO COLETOR] | S     | [VOLUMEDE] | [VOLUMEATE] | [PERCENTUALV]      | [PERCENTUALATE] | S           | S           | S            | S                  | S          | S       | S              | S              | S      | S                | S                 | S       | S                 | teste de campo retencao | teste de campo Mensagem dispositivo variacao consumo |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Descrição Não Informada"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_CriticaMedicao_CT005
  Esquema do Cenario: CT005 - Cadastro de Critica de Medicao - Pesquisa simples- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "id" e o valor "900" para Cadastro de Critica
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_CriticaMedicao_CT006
  Esquema do Cenario: CT006 - Pesquisa de Cadastro de Critica de Medicao - Pesquisa simples- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "idcidade" e o valor "<valor>" para Cadastro de Critica
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   | valor |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO | 80    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:#nao tem idcidade para Cachoeiro
      | unidade   |
      | [IGNORAR] |

  @Cadastro_CriticaMedicao_CT007
  Esquema do Cenario: CT007 - Pesquisa de Cadastro de Critica de Medicao - Pesquisa simples- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "criticamedicao" e o valor "QUEDA ACIMA" para Cadastro de Critica
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_CriticaMedicao_CT008
  Esquema do Cenario: CT008 - Pesquisa de Cadastro de Critica de Medicao - Consulta Completa - Com 3 criterios de Pesquisa - "E" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "analisecritica", em operador "Igual a" e em valor "N"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Maior ou igual a" e em valor "20"
    E clicar no botao "E"
    E selecionar em campo "criticamedicao", em operador "Contém" e em valor "normal"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       |
      | ARAGUAIA | Cadastro de Crítica de Medição |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao |

  @Cadastro_CriticaMedicao_CT009
  Esquema do Cenario: CT009 - Pesquisa de Cadastro de Critica de Medicao - Consulta Completa - Com 3 criterios de Pesquisa - "OU" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "analisecritica", em operador "Igual a" e em valor "N"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Maior ou igual a" e em valor "20"
    E clicar no botao "OU"
    E selecionar em campo "criticamedicao", em operador "Contém" e em valor "normal"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       |
      | ARAGUAIA | Cadastro de Crítica de Medição |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao |

  @Cadastro_CriticaMedicao_CT010
  Esquema do Cenario: CT010 - Sair da tela de Cadastro de Critica pelo botao "Sair"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando clicar no botao "Sair"
    Entao a tela "Cadastro de Crítica de Medição" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       |
      | ARAGUAIA | Cadastro de Crítica de Medição |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao |

  @Cadastro_CriticaMedicao_CT011
  Esquema do Cenario: CT011 - Cadastro de Critica de Medicao - Preenchimento dos campos obrigatorios- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<submenu2>"
    Quando eu preencher os seguintes campos de cadastro de critica de medicao
      | cidade   | sigla | descricao          | codigocoletor | ativo | volumede | volumeate | percentualvariacao | percentualate | consumozero | ligacaonova | leituramenor | leituranaoefetuada | inversaohm | residuo | limiteinferior | analisecritica | exibir | confirmarleitura | repassesimultanea | repasse | variacaodeconsumo | avisoretencao | mensagemdispositivo |
      | <cidade> | 907   | LEITURA = ANTERIOR |               |       |          |           |                    |               |             |             |              |                    |            |         |                |                |        |                  |                   |         |                   |               |                     |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | submenu2                       | cidade   |
      | ARAGUAIA | Cadastro de Crítica de Medição | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | submenu2                       | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Critica de Medicao | CACHOEIRO DE ITAPEMIRIM |