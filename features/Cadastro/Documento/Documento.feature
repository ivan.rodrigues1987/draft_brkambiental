#language: pt
@Features @Cadastro @Cadastro_Documento
Funcionalidade: Cadastrar Documento no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Documentos" esta conforme os seguintes parametros da entidade "PARAMETROSIMAGEMDOCUMENTO":
      | IDDOCUMENTOTIPOLIGACAO | ALTURA | LARGURA |
      | 1                      | 480    | 640     |
      | 3                      | 480    | 640     |

  @Cadastro_Documento_CT001
  Esquema do Cenario: Cenario: CT001 - Cadastrar Documento do Tipo PDF com o Motivo Cadastro
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc | motivo   | Obs        |
      | PDF     | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "PDF" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | motivo   |
      | ARAGUAIA | CADASTRO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | RECADASTRAMENTO |


  @Cadastro_Documento_CT002
  Esquema do Cenario: CT002 - Cadastrar Documento do Tipo PDF com o Motivo Suspeita de Fraude
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc | motivo             | Obs        |
      | PDF     | SUSPEITA DE FRAUDE | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "PDF" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Documento_CT003
  Esquema do Cenario: CT003 - Cadastrar Documento do Tipo PDF com o Motivo Vistoria
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc | motivo   | Obs        |
      | PDF     | VISTORIA | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "PDF" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Documento_CT004
  Esquema do Cenario: Cenario: CT004 - Cadastrar Documento do Tipo PDF com o Motivo Retorno Coletor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc | motivo          | Obs        |
      | PDF     | RETORNO COLETOR | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "PDF" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Documento_CT005
  Esquema do Cenario: CT005 - Cadastrar Documento do Tipo IMAGEM com o Motivo Cadastro -  Arquivo PNG
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-png.png" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |


  @Cadastro_Documento_CT006
  Esquema do Cenario: CT006 - Cadastrar Documento do Tipo IMAGEM com o Motivo Suspeita de Fraude - Arquiivo JPEG
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo             | Obs        |
      | <tipodoc> | SUSPEITA DE FRAUDE | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |


  @Cadastro_Documento_CT007
  Esquema do Cenario: CT007 - Cadastrar Documento do Tipo IMAGEM com o Motivo Vistoria
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | VISTORIA | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |


  @Cadastro_Documento_CT008
  Esquema do Cenario: CT008 - Cadastrar Documento do Tipo IMAGEM com o Motivo Retorno Coletor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo          | Obs        |
      | <tipodoc> | RETORNO COLETOR | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |

  @Cadastro_Documento_CT009
  Esquema do Cenario:  CT009 - Cadastrar Documento do Tipo IMAGEM com o Motivo FOTO ENVIO COLETOR
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo             | Obs        |
      | <tipodoc> | FOTO ENVIO COLETOR | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |


  @Cadastro_Documento_CT010
  Esquema do Cenario: CT010 - Cadastrar Documento do Tipo IMAGEM e sem informar observação
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo             | Obs |
      | <tipodoc> | FOTO ENVIO COLETOR |     |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |

  @Cadastro_Documento_CT011
  Esquema do Cenario: CT011 - Cadastrar Documento - Cancelar o envio do Arquivo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar nos botoes "Buscar" e "Cancelar"
    E clicar no botao "Adicionar"
    Entao  a janela de mensagem "Mensagem do Sistema" contera: "Informe o arquivo do documento."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |

  @Cadastro_Documento_CT012
  Esquema do Cenario: Cenario: CT012 - Cadastrar Documento do Tipo IMAGEM sem informar o Motivo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo | Obs        |
      | <tipodoc> |        | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    Entao  a janela de mensagem "Mensagem do Sistema" contera: "Informe o motivo do documento."

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc |
      | ARAGUAIA | IMAGEM  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc |
      | CACHOEIRO DE ITAPEMIRIM | Foto    |


  @Cadastro_Documento_CT013
  Esquema do Cenario: CT013 - Remover Documento do Tipo IMAGEM
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    E em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"
    Então  a janela de mensagem "Atenção" contera: "Registro excluído com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |


  @Cadastro_Documento_CT014
  Esquema do Cenario: Cenario: CT014 - Remover Documento do Tipo PDF
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    E em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"
    Então  a janela de mensagem "Atenção" contera: "Registro excluído com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | PDF     | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | PDF     | RECADASTRAMENTO |

  @Cadastro_Documento_CT015
  Esquema do Cenario: CT015 - Visualizar Documento do Tipo IMAGEM
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    E em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Visualizar"
    E fechar Tela "brk-jpeg.jpg"
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |


  @Cadastro_Documento_CT016
  Esquema do Cenario: Cenario: CT016 - Download de Documento do Tipo IMAGEM
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    E em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Download"
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |

  @Cadastro_Documento_CT017
  Esquema do Cenario: CT017 - Download de Documento do Tipo PDF
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "CadastroDeDocumentos.pdf" e clicar em "Ok"
    E clicar no botao "Adicionar"
    E  a janela de mensagem "Atenção" contera: "Registro salvo com sucesso."
    E em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Download"
    Entao em Documentos, clicar com o botao direito do mouse no registro do Tipo "<tipodoc>" e clicar em "Remover"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | PDF     | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | PDF     | RECADASTRAMENTO |

  @Cadastro_Documento_CT018
  Esquema do Cenario: CT018 - Limpar dados da Tela de Documento
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    Quando preencher a combo de Cidade "[Cidade]"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E preencher os seguintes campos do Cadastro de Documentos:
      | tipoDoc   | motivo   | Obs        |
      | <tipodoc> | <motivo> | Observação |
    E clicar em "Buscar", selecionar o arquivo "brk-jpeg.jpg" e clicar em "Ok"
    E clicar no botao "Novo"
    Entao validar que a combo "Motivo" foi alterada para "<motivo>"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | tipodoc | motivo   |
      | ARAGUAIA | IMAGEM  | CADASTRO |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | tipodoc | motivo          |
      | CACHOEIRO DE ITAPEMIRIM | Foto    | RECADASTRAMENTO |

  @Cadastro_Documento_CT019
  Esquema do Cenario: CT019 - SAIR do Menu Documento
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Documento"
    E clicar no botao "Sair"
    Entao a tela "Documento" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


