#language: pt
@Features @Cadastro @Cadastro_Arrecadador
Funcionalidade: Cadastrar Arrecadador no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Arrecadador" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | INQUILINONORMALIZADO |
      | 76       | S                    |
      | 77       | S                    |
      | 78       | S                    |
      | 79       | S                    |
      | 80       | S                    |

  @Cadastro_Arrecadador_CT001
  Esquema do Cenario: CT001 - Cadastrar Arrecadador - Tipo de layout 04 - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador   | diasCarencia | nrBanco        | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | [Arrecadador] | 11           | [Numero Banco] | Ativo    | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 04              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | N                     | 0          | 0          | 0     | 0              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado acima]"
    Entao o resultado da pesquisa "[Arrecadador cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT002
  Esquema do Cenario: CT002 - Cadastrar Arrecadador - Tipo de layout 05 - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador   | diasCarencia | nrBanco        | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | [Arrecadador] | 11           | [Numero Banco] | Ativo    | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 05              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | N                     | 0          | 0          | 0     | 0              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado acima]"
    Entao o resultado da pesquisa "[Arrecadador cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT003
  Esquema do Cenario: CT003 - Cadastrar Arrecadador - Com integracao financeira - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador   | diasCarencia | nrBanco        | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | [Arrecadador] | 11           | [Numero Banco] | Ativo    | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 05              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | S                     | 2          | 0          | 2     | 2              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado acima]"
    Entao o resultado da pesquisa "[Arrecadador cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT004
  Esquema do Cenario: CT004 - Cadastrar Arrecadador - Situacao INATIVO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador   | diasCarencia | nrBanco        | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | [Arrecadador] | 11           | [Numero Banco] | Inativo  | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 05              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | S                     | 2          | 0          | 2     | 2              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado acima]"
    Entao o resultado da pesquisa "[Arrecadador cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT005
  Esquema do Cenario: CT005 - Cadastrar Arrecadador - Duplicidade de cadastro - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando preencher os seguintes campos em Arrecadador:
      | arrecadador   | diasCarencia | nrBanco                   | situacao | nrConvArrecadacao | nsaArrecadacao | tarifaArrecadacao | camaraCompensacao | nrConvDA | nsaEnvioDA | nsaRetornoDA | tarifaSobreDA | nsaBaixaMan | versaoArquivoDA | preencherCPFCNPJ | qtdDigitosAg | agComDv | preencherAgZeroEsq | qtdDigitosCC | ccComDv | preencherCCZeroEsq | validaNSAImport | utilizarDvLig | prefixCodLig | qtdDigCodLig | qtdDigDVLig | creditoRegZ | bandeira   | valorTarifa | utilizarIntegracaoFin | codReceita | codDespesa | codDA | codArrecadacao |
      | [Arrecadador] | 11           | [Numero Banco cadastrado] | Ativo    | 13                | 14             | 15                | 16                | 17       | 18         | 19           | 20            | 21          | 05              | Nunca            | 2            | Não     | Não                | 2            | Não     | Sim                | Não             | Não           | 16           | 7            | 3           | Não         | [Bandeira] | 1010        | S                     | 2          | 0          | 2     | 2              |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Ja existe um arrecadador cadastrado com este numero de banco."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT006
  Esquema do Cenario: CT006 - Pesquisar Arrecadador - Consulta simples Arrecadador nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E clicar no botao "Pesquisar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Favor informar um dado para pesquisar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT007
  Esquema do Cenario: CT007 - Pesquisar Arrecadador - Consulta simples por Arrecadador - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado]"
    Entao o resultado da pesquisa "[Arrecadador cadastrado]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT008
  Esquema do Cenario: CT008 - Pesquisar Arrecadador - Consulta simples Numero Banco nao informado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "<campoNumeroBanco>" e preencher em valor ""
    E clicar no botao "Pesquisar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Favor informar um dado para pesquisar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoNumeroBanco |
      | ARAGUAIA | Número do Banco  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoNumeroBanco |
      | CACHOEIRO DE ITAPEMIRIM | Numero do Banco  |

  @Cadastro_Arrecadador_CT009
  Esquema do Cenario: CT009 - Pesquisar Arrecadador - Consulta simples por Numero Banco - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "<campoNumeroBanco>" e preencher em valor "[Numero Banco cadastrado]"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "[Numero Banco cadastrado]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoNumeroBanco |
      | ARAGUAIA | Número do Banco  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoNumeroBanco |
      | CACHOEIRO DE ITAPEMIRIM | Numero do Banco  |


  @Cadastro_Arrecadador_CT010
  Esquema do Cenario: CT010 - Pesquisar Arrecadador - Consulta simples por ATIVO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "<campoAtivo>" e preencher em valor "S"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "S" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoAtivo |
      | ARAGUAIA | ATIVO      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoAtivo |
      | CACHOEIRO DE ITAPEMIRIM | Ativo      |


  @Cadastro_Arrecadador_CT011
  Esquema do Cenario: CT011 - Pesquisar Arrecadador - Consulta completa por Arrecadador igual a - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Arrecadador", em operador "Igual a" e em valor "[Arrecadador cadastrado]"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa "[Arrecadador cadastrado]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT012
  Esquema do Cenario: CT012 - Pesquisar Arrecadador - Consulta completa por Codigo menor que - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Arrecadador", em operador "Menor que" e em valor "50"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT013
  Esquema do Cenario: CT013 - Pesquisar Arrecadador - Consulta completa com 2 criterios de pesquisa E - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Arrecadador", em operador "Igual a" e em valor "[Arrecadador cadastrado]"
    E clicar no botao "E"
    E selecionar em campo "<campoNumeroBanco>", em operador "Igual a" e em valor "[Numero Banco cadastrado]"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa "[Arrecadador cadastrado]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoNumeroBanco |
      | ARAGUAIA | Número do Banco  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoNumeroBanco |
      | CACHOEIRO DE ITAPEMIRIM | Numero do Banco  |


  @Cadastro_Arrecadador_CT014
  Esquema do Cenario: CT014 - Pesquisar Arrecadador - Consulta completa com 2 criterios de pesquisa OU - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Arrecadador", em operador "Igual a" e em valor "[Arrecadador cadastrado]"
    E clicar no botao "OU"
    E selecionar em campo "<campoNumeroBanco>", em operador "Igual a" e em valor "[Numero Banco cadastrado]"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa "[Arrecadador cadastrado]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoNumeroBanco |
      | ARAGUAIA | Número do Banco  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoNumeroBanco |
      | CACHOEIRO DE ITAPEMIRIM | Numero do Banco  |


  @Cadastro_Arrecadador_CT015
  Esquema do Cenario: CT015 - Alterar Arrecadador de ATIVO para INATIVO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "<campoNumeroBanco>" e preencher em valor "[Numero Banco cadastrado]"
    E clicar no botao "Pesquisar"
    E selecionar no resultado da pesquisa "[Numero Banco cadastrado]" e clicar em "OK"
    E selecionar alterar a situacao para "Inativo" e clicar em "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campoNumeroBanco |
      | ARAGUAIA | Número do Banco  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campoNumeroBanco |
      | CACHOEIRO DE ITAPEMIRIM | Numero do Banco  |


  @Cadastro_Arrecadador_CT016
  Esquema do Cenario: CT016 - Alterar Arrecadador de INATIVO para ATIVO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Arrecadador" e preencher em valor "[Arrecadador INATIVO]"
    E clicar no botao "Pesquisar"
    E selecionar no resultado da pesquisa "[Arrecadador INATIVO]" e clicar em "OK"
    E selecionar alterar a situacao para "Ativo" e clicar em "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT017
  Esquema do Cenario: CT017 - Excluir Arrecadador "ATIVO" com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Arrecadador cadastrado]"
    E selecionar no resultado da pesquisa "[Numero Banco cadastrado]" e clicar em "OK"
    E clicar no botao "Excluir"
    Entao a janela de mensagem "Atenção" sera: "Tem certeza que deseja excluir esse registro?"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Arrecadador_CT018
  Esquema do Cenario: CT018 - Excluir Arrecadador que esta em uso exclusao nao permitida - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Arrecadador"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "<arrecadador>"
    E selecionar no resultado da pesquisa "<numeroBancoArrecadador>" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Exclusao nao permitida, pois ja existe movimento para esse arrecadador"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | arrecadador | numeroBancoArrecadador |
      | ARAGUAIA | BRADESCO    | 237                    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | arrecadador | numeroBancoArrecadador |
      | CACHOEIRO DE ITAPEMIRIM | PRISMA      | 111                    |