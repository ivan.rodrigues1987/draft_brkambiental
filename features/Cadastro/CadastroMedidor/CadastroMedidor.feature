#language: pt
@Features @Cadastro @Cadastro_Medidor
Funcionalidade: Cadastrar Medidores no SAN

  # Esta feature nao possui parametros de configuracoes diferenciados

  @Cadastro_Medidor_CT001
  Esquema do Cenario: CT001 - Cadastrar Medidor com sucesso - Situacao BOM ESTADO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | N         |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   |
      | ARAGUAIA | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | BOM ESTADO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo         | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | WATERFLUX 463C | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT002
  Esquema do Cenario: CT002 - Cadastrar Medidor com sucesso - Situacao INDEFINIDO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | N         |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha esse item na combo
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Medidor_CT003
  Esquema do Cenario: CT003 - Cadastrar Medidor com sucesso - INSTALADO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   |
      | ARAGUAIA | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo         | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | WATERFLUX 463C | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT004
  Esquema do Cenario: CT004 - Cadastrar Medidor com sucesso - <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   |
      | ARAGUAIA | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo         | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | WATERFLUX 463C | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT005
  Esquema do Cenario: CT005 - Cadastrar Medidor com sucesso - <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                | aquisicao | situacao   |
      | ARAGUAIA | FAE - CLASSE "B" -QMAX 1,5M³/H-4P-UNI | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo                   | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | SIEMENS SITRANS MAG5100W | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT006
  Esquema do Cenario: CT006 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"



  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo     | aquisicao | situacao   |
      | ARAGUAIA | INDEFINIDO | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo                | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | INDEFINIDO - MIGRACAO | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT007
  Esquema do Cenario: CT007 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   |
      | ARAGUAIA | ITRON CLASSE B - 3/4" 1,5 M3/H- 4P - UNI | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo                                 | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | ITRON - UNIMAG TUIV - Q=1,5 - CLASSE B | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT008
  Esquema do Cenario: CT008 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo   | aquisicao | situacao   |
      | ARAGUAIA | MODELO A | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo        | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | MULTIMAG-TM3A | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT009
  Esquema do Cenario: CT009 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo   | aquisicao | situacao   |
      | ARAGUAIA | MODELO B | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo        | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | MULTIMAG-TM3B | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT010
  Esquema do Cenario: CT010 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo   | aquisicao | situacao   |
      | ARAGUAIA | MODELO E | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo           | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | MULTIMAG-TMII-3C | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT011
  Esquema do Cenario: CT011 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo   | aquisicao | situacao   |
      | ARAGUAIA | MODELO Y | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo         | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | MULTIMAG CYBLE | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT012
  Esquema do Cenario: CT012 - Cadastrar Medidor com sucesso - Modelo: <modelo>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | Sim       |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                               | aquisicao | situacao   |
      | ARAGUAIA | SAGA MULTI QMAX=3,0M³/H PTO = 4 CL B | COMPRADO  | INDEFINIDO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo                                 | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | LAMON MED. VAZ. ELETROMAG ML2500/MS255 | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT013
  Esquema do Cenario: CT013 - Cadastrar Medidor com sucesso pelo botao novo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | N         |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | modelo                                   | aquisicao | situacao   |
      | ARAGUAIA | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | BOM ESTADO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | modelo         | aquisicao | situacao |
      | CACHOEIRO DE ITAPEMIRIM | WATERFLUX 463C | Patriom.  | NOVO     |

  @Cadastro_Medidor_CT014
  Esquema do Cenario: CT014 - Cadastrar Medidor sem sucesso - Medidor ja cadastrado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    Entao preencher os seguintes campos em Cadastro de Medidor
      | numeroDoMedidor   | modelo   | dataAquisicao | anoFabricacao | situacao   | cidade           | formaAquisicao | empresa                                        | instalado |
      | [numeroDoMedidor] | <modelo> | 10/01/2018    | 2017          | <situacao> | [Cidade Unidade] | <aquisicao>    | Brookfield Ambiental ¿ Araguaia Saneamento S/A | N         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Existe outro medidor cadastrado com o numero:"
  @ARAGUAIA @21
    Exemplos: # Bug aguardando Chamado
      | unidade   | modelo                                   | aquisicao | situacao   | numeroDoMedidor |
      | [IGNORAR] | ELSTER CLASSE B - 3/4" 1,5 M3/H 4P - UNI | COMPRADO  | BOM ESTADO | 011429          |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: # Bug Aguardando Chamado ser finalizado
      | unidade   | modelo | aquisicao | aquisicao | numeroDoMedidor |
      | {IGNORAR] | ALFA   | Patriom.  | NOVO      | 83686           |

  @Cadastro_Medidor_CT015
  Esquema do Cenario: CT015 - Cadastrar Medidor sem sucesso - Campos nao preenchidos
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Novo"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo(s) nao foram preenchidos:"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT016
  Esquema do Cenario: CT016 - Alterar Medidor com sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "1" e clicar no resultado
    E alterar o campo "Número do Medidor" para "[numeroDoMedidor]"
    E limpar a combo "Forma Aquisição"
    E preencher a combo "Forma Aquisição" com "<aquisicao>"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | aquisicao |
      | ARAGUAIA | COMPRADO  |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | aquisicao |
      | CACHOEIRO DE ITAPEMIRIM | Patriom.  |


  @Cadastro_Medidor_CT017
  Esquema do Cenario:  CT017 - Pesquisar Medidor - Consulta Simples - Campo "Numero do Medidor"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Numero do Medidor" e preencher em valor "<numeroDoMedidor>"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | numeroDoMedidor |
      | ARAGUAIA | 336             |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | numeroDoMedidor |
      | CACHOEIRO DE ITAPEMIRIM | 83686           |

  @Cadastro_Medidor_CT018
  Esquema do Cenario:  CT018 - Pesquisar Medidor - Consulta Simples - Campo "Codigo"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "<codigo>"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | codigo |
      | ARAGUAIA | 1      |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | codigo |
      | CACHOEIRO DE ITAPEMIRIM | 1      |

  @Cadastro_Medidor_CT019
  Esquema do Cenario:  CT019 - Pesquisar Medidor - Consulta Completa - Campo "anofabricacao" e OPERADOR "Igual a"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" de Pesquisa Rapida
    E na aba "Consulta Completa" selecionar em campo "anofabricacao", em operador "Igual a" e em valor "<anofabricacao>"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | anofabricacao |
      | ARAGUAIA | 2016          |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | anofabricacao |
      | CACHOEIRO DE ITAPEMIRIM | 2016          |

  @Cadastro_Medidor_CT020
  Esquema do Cenario:  CT020 - Pesquisar Medidor - Consulta Completa - Campo "Codigo" e OPERADOR "Menor que"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Codigo", em operador "Menor que" e em valor "1000"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT021
  Esquema do Cenario:  CT021 - Pesquisar Medidor - Consulta Completa - Campo "dataaquisicao" e OPERADOR "Diferente de"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "dataaquisicao", em operador "Diferente de" e em valor "10/04/2018"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT022
  Esquema do Cenario:  CT022 - Pesquisar Medidor - Consulta Completa - Campo "idmodelomedidor" e OPERADOR "Maior que"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idmodelomedidor", em operador "Maior que" e em valor "50"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT023
  Esquema do Cenario:  CT023 - Pesquisar Medidor - Consulta Completa - Campo "idsitmedidor" e OPERADOR "Diferente de"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idsitmedidor", em operador "Diferente de" e em valor "1"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT024
  Esquema do Cenario:  CT024 - Pesquisar Medidor - Consulta Completa - Campo "instalado" e OPERADOR "Igual a"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "instalado", em operador "Igual a" e em valor "S"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Medidor_CT025
  Esquema do Cenario:  CT025 - Pesquisar Medidor - Consulta Completa - Campo "Numero do Medidor" e OPERADOR "Igual a"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Numero do Medidor", em operador "Igual a" e em valor "<numeroDoMedidor>"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | numeroDoMedidor |
      | ARAGUAIA | 336             |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | numeroDoMedidor |
      | CACHOEIRO DE ITAPEMIRIM | 83686           |

  @Cadastro_Medidor_CT026
  Esquema do Cenario:  CT026 - Pesquisar Medidor - Consulta Completa - Com 3 criterio de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Numero do Medidor", em operador "Igual a" e em valor "<numeroDoMedidor>"
    E clicar no botao "E"
    E selecionar em campo "dataaquisicao", em operador "Diferente de" e em valor "11/01/2018"
    E clicar no botao "E"
    E selecionar em campo "Codigo", em operador "Maior que" e em valor "50"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | numeroDoMedidor |
      | ARAGUAIA | 88442           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | numeroDoMedidor |
      | CACHOEIRO DE ITAPEMIRIM | 83686           |

  @Cadastro_Medidor_CT027
  Esquema do Cenario:  CT027 - Pesquisar Medidor - Consulta Completa - Com 3 criterio de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Numero do Medidor", em operador "Igual a" e em valor "<numeroDoMedidor>"
    E clicar no botao "OU"
    E selecionar em campo "idmodelomedidor", em operador "Igual a" e em valor "50"
    E clicar no botao "OU"
    E selecionar em campo "Codigo", em operador "Igual a" e em valor "1000"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | numeroDoMedidor |
      | ARAGUAIA | 29582           |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | numeroDoMedidor |
      | CACHOEIRO DE ITAPEMIRIM | 83686           |

  @Cadastro_Medidor_CT028
  Esquema do Cenario: CT028 - Sair da tela de Pesquisar Medidor pelo botao "Sair"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Medidor"
    E clicar nos botoes "Novo" e "Sair"
    Entao a tela "Cadastro Medidores" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |