#language: pt
@Features @Cadastro @Cadastro_MensagemFatura
Funcionalidade: Cadastrar Mensagens de Fatura no SAN

  @Cadastro_Mensagem_Faturas_CT001
  Esquema do Cenario: CT001 - Cadastrar Mensagem em Fatura
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Mensagens Fatura"
    Quando incluir dados
      | cidade   | dataInicio       | dataFinal    | tipoImpressao | prioridade | mensagem   |
      | [cidade] | [data de inicio] | [data final] | Convencional  | 1          | [Mensagem] |
    E clicar no botao "Adicionar"
    Entao a mensagem criada e incluida no combo conforme esperado e listada em data decrescente
  @ARAGUAIA @21
    Exemplos: #Este cenário será ignorado para a execução do dia 29/08/2019
      | unidade   |
      | [IGNORAR] |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |
