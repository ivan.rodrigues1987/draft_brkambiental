#language: pt
@Features @Cadastro @Cadastro_RetencaoForcadaFatura
Funcionalidade: Retencao Forcada Fatura no SAN

  # Esta feature nao possui parametros de configuracoes diferenciados

  @Cadastro_RetencaoForcadaFatura_CT001
  Esquema do Cenario: CT001 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO SEM MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT002
  Esquema do Cenario: CT002 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso pelo - BOTAO NOVO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando no campo "Ligação" clicar em pesquisar e filtrar por "Cod. Ligacao (CDC)" e pesquisar "[LIGACAO SEM MENSAGEM CADASTRADA]"
    E clicar no botao "Novo"
    E preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO SEM MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT003
  Esquema do Cenario: CT003 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso - NAO ATIVO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO SEM MENSAGEM CADASTRADA] | N     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT004
  Esquema do Cenario: CT004 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso - Ligacao nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] |         | S     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Ligação não informada!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT005
  Esquema do Cenario: CT005 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso - Periodo Inicial nao informado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO SEM MENSAGEM CADASTRADA] | S     |                | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Selecione o período Inicial."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

    #Mensagem inesperada ocorrendo
  @Cadastro_RetencaoForcadaFatura_CT006
  Esquema do Cenario: CT006 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso - Periodo Final nao informado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem   |
      |        | [LIGACAO SEM MENSAGEM CADASTRADA] | S     | 06/2019        |              | [Mensagem] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "java.lang.NullPointerException"
    #Entao a janela de mensagem "Mensagem do Sistema" sera: "Selecione o período Final."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT007
  Esquema do Cenario: CT007 - Cadastrar Retencao Forcada de Fatura para a Ligacao com Sucesso - Mensagem nao informada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem |
      | [Cidade Unidade] | [LIGACAO SEM MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      |          |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Por favor informe uma mensagem!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT008
  Esquema do Cenario: CT008 - Cadastrar Retencao Forcada de Fatura - Mensagem vigente para o periodo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                       | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "possui uma mensagem de retencao forcada de fatura vigente para o periodo inicial informado!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT009
  Esquema do Cenario: CT009 - Alterar Retencao Forcada de Fatura com sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "[ID MENSAGEM CADASTRADA]"
    E selecionar no resultado da pesquisa "[ID MENSAGEM CADASTRADA]" e clicar em "OK"
    E preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem          |
      |        | [LIGACAO SEM MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      | Mensagem alterada |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT010
  Esquema do Cenario: CT010 - Alterar Retencao Forcada de Fatura com sucesso - Ativo NAO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "[ID MENSAGEM CADASTRADA]"
    E selecionar no resultado da pesquisa "[ID MENSAGEM CADASTRADA]" e clicar em "OK"
    E preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade | ligacao                           | ativo | periodoInicial | periodoFinal | mensagem          |
      |        | [LIGACAO SEM MENSAGEM CADASTRADA] | N     | 06/2019        | 06/2019      | Mensagem alterada |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT011
  Esquema do Cenario: CT011 - Alterar Retencao Forcada de Fatura com sucesso SEM sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "[ID MENSAGEM CADASTRADA]"
    E selecionar no resultado da pesquisa "[ID MENSAGEM CADASTRADA]" e clicar em "OK"
    E preencher os seguintes campos em Retencao Forcada de Fatura:
      | cidade           | ligacao                       | ativo | periodoInicial | periodoFinal | mensagem   |
      | [Cidade Unidade] | [LIGACAO MENSAGEM CADASTRADA] | S     | 06/2019        | 06/2019      | [Mensagem] |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "possui uma mensagem de retencao forcada de fatura vigente para o periodo inicial informado!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT012
  Esquema do Cenario: CT012 - Excluir Retencao Forcada de Fatura com sucesso
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "[ID MENSAGEM CADASTRADA]"
    E selecionar no resultado da pesquisa "[ID MENSAGEM CADASTRADA]" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    Entao sera exibida a mensagem de sucesso em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT013
  Esquema do Cenario: CT013 - Pesquisar Retencao Forcada de Fatura - Consulta Simples por ID
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "[ID MENSAGEM CADASTRADA]"
    Entao o resultado da pesquisa "[ID MENSAGEM CADASTRADA]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT014
  Esquema do Cenario: CT014 - Pesquisar Retencao Forcada de Fatura - Consulta Simples por mensagem
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "mensagem" e preencher em valor "[MENSAGEM CADASTRADA]"
    Entao o resultado da pesquisa "[MENSAGEM CADASTRADA]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Arrecadador_CT015
  Esquema do Cenario: CT015 - Pesquisar Retencao Forcada de Fatura - Consulta Completa - Campo "ID" e OPERADOR "Menor que"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "200"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Arrecadador_CT016
  Esquema do Cenario: CT016 - Pesquisar Retencao Forcada de Fatura - Consulta Completa - Com 3 criterios de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "200"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Diferente de" e em valor "5"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Maior que" e em valor "0"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Arrecadador_CT017
  Esquema do Cenario: CT017 - Pesquisar Retencao Forcada de Fatura - Consulta Completa - Com 2 criterios de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "200"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Diferente de" e em valor "100"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Arrecadador_CT018
  Esquema do Cenario: CT018 - Pesquisar Retencao Forcada de Fatura - Consulta Completa - Com 3 criterios de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "200"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Igual a" e em valor "5"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Igual a" e em valor "1"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT019
  Esquema do Cenario: CT019 - Pesquisar Retencao Forcada de Fatura - Consulta Personalizada - Cod Ligacao CDC
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando no campo "Ligação" clicar em pesquisar e filtrar por "Cod. Ligacao (CDC)" e pesquisar "[LIGACAO MENSAGEM CADASTRADA]"
    Entao serao carregados os dados da Mensagem cadastrada para a ligacao
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_RetencaoForcadaFatura_CT020
  Esquema do Cenario: CT020 - Pesquisar Retencao Forcada de Fatura - Consulta Personalizada - Cod Ligacao Inexistente
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando no campo "Ligação" clicar no icone lupa
    E pressionar "ENTER"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Favor informar um dado para pesquisar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_AnaliseAgua_CT021
  Esquema do Cenario: CT021 - Sair da tela de Retencao Forcada de Fatura - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Retenção Forçada de Fatura"
    Quando fechar Tela
    Entao a tela "Retenção Forçada de Fatura" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


