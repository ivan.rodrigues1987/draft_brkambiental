#language: pt
@Features @Cadastro @Cadastro_CodigoLeitura
Funcionalidade: Cadastrar Codigo de Leitura no SAN

  #Esta feature nao possui parametros de configuracoes diferenciados

  @Cadastro_CodigoLeitura_CT001
  Esquema do Cenario: CT001 -  Cadastrar de Codigo de Leitura com sucesso - Prevencao de Acidentes: "CAO SOLTO" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | <cidade> | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaoColetor] | ATENÇÃO: CAO SOLTO   | S             | S                   | S                  | S                 | S             | S                | S       | S        | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento            | descricaoDF   | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR MEDIA FATURADO COM CREDITO | [descricaoDF] | S                       | S                         | S                   | S                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | Menu                          |
      | ARAGUAIA | REDENÇÃO | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #EXEMPLO IGNORADO - Nao possui os itens de prevencao de acidentes
      | unidade   |
      | [IGNORAR] |

  @Cadastro_CodigoLeitura_CT002
  Esquema do Cenario: CT002 -  Cadastrar de Codigo de Leitura com sucesso - Prevencao de Acidentes: "OCORRENCIA DE CAO SOLTO" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | <cidade> | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes                       | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaoColetor] | ATENÇÃO IMÓVEL COM OCORRÊNCIA DE CÃO SOLTO | S             | S                   | S                  | S                 | S             | S                | S       | S        | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | LEITURA NORMAL         | [descricao] | S                       | S                         | S                   | S                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   | Menu                          |
      | ARAGUAIA | REDENÇÃO | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #EXEMPLO IGNORADO - Nao possui os itens de prevencao de acidentes
      | unidade   |
      | [IGNORAR] |


  @Cadastro_CodigoLeitura_CT003
  Esquema do Cenario: CT003 - Cadastrar de Codigo de Leitura com sucesso - Criterios de Faturamento "ADOTAR MEDIA FATURADO COM CREDITO" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaoColetor] | <prevencaoDeAcidentes> | S             | S                   | S                  | S                 | S             | S                | S       | S        | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento            | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR MEDIA FATURADO COM CREDITO | [descricao] | S                       | S                         | S                   | S                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT004
  Esquema do Cenario: CT004 - Cadastrar de Codigo de Leitura com sucesso - Criterios de Faturamento "ADOTAR MEDIA FATURADO SEM CREDITO" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaoColetor] | <prevencaoDeAcidentes> | S             | S                   | S                  | S                 | S             | S                | S       | S        | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento            | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR MEDIA FATURADO SEM CREDITO | [descricao] | S                       | S                         | S                   | S                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT005
  Esquema do Cenario: CT005 - Cadastrar de Codigo de Leitura com sucesso - Criterios de Faturamento "ADOTAR VOLUME MINIMO SEM CREDITO" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaoColetor] | <prevencaoDeAcidentes> | S             | S                   | S                  | S                 | S             | S                | S       | S        | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento           | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR VOLUME MINIMO SEM CREDITO | [descricao] | S                       | S                         | S                   | S                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT006
  Esquema do Cenario: CT006 - Cadastrar Codigo de Leitura sem Mensagem de Prevencao de Acidentes - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaocoletor] |                      | S             |                     |                    | S                 |               |                  |         |          | S                      |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento           | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR VOLUME MINIMO SEM CREDITO | [descricao] |                         | s                         | s                   | s                               |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT007
  Esquema do Cenario: CT007 - Cadastrar Codigo de Leitura - Campo "Descricao" nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao | padrao |
      | [cidade] |           |        |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaocoletor] | <prevencaoDeAcidentes> |               |                     |                    |                   |               |                  |         |          |                        |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento           | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR VOLUME MINIMO SEM CREDITO | [descricao] |                         |                           |                     |                                 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Descrição do Código de Leitura Não Informado"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT008
  Esquema do Cenario: CT008 - Cadastrar Codigo de Leitura - Campo "Descricao Coletor" nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      |                  | <prevencaoDeAcidentes> |               |                     |                    |                   |               |                  |         |          |                        |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento           | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR VOLUME MINIMO SEM CREDITO | [descricao] |                         |                           |                     |                                 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Descrição do Coletor Não Informado"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |

  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT009
  Esquema do Cenario: CT009 - Cadastrar Codigo de Leitura - Campo "Cidade" nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade | descricao   | padrao |
      |        | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaocoletor] |                      |               |                     |                    |                   |               |                  |         |          |                        |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento           | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      | ADOTAR VOLUME MINIMO SEM CREDITO | [descricao] |                         |                           |                     |                                 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Cidade Não Informada"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT010
  Esquema do Cenario: CT010 - Cadastrar Codigo de Leitura completo - Campo "Criterio de Faturamento" nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando eu preencher os seguintes campos em Codigo de Leitura:
      | cidade   | descricao   | padrao |
      | [cidade] | [descricao] | S      |
    E em dados medicao preencher os seguintes campos
      | descricaoColetor   | prevencaodeAcidentes   | aceitaLeitura | leituraHMNaoConfere | emiteAvisoRetencao | repasseSimultanea | enviarColetor | alterarCategoria | repasse | estimada | alterarnumeroEconomias |
      | [descricaocoletor] | <prevencaoDeAcidentes> |               |                     |                    |                   |               |                  |         |          |                        |
    E em dados faturamento preencher os seguintes campos
      | criteriosDeFaturamento | DescricaoDF | criterioFaturamentoFixo | padraoRecalculoSimultanea | considerarParaMedia | imprimirAvisoImpedimentoleitura |
      |                        | [descricao] |                         |                           |                     |                                 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Critério de Faturamento Não Informado"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | prevencaoDeAcidentes | Menu                          |
      | ARAGUAIA | ATENÇÃO: CÃO SOLTO   | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | prevencaoDeAcidentes | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM |                      | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT011
  Esquema do Cenario: CT011 - Pesquisar Codigo de Leitura - Consulta Simples - Campo "id" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "2"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT012
  Esquema do Cenario: CT012 - Pesquisar Codigo de Leitura - Consulta Simples - Campo "idcidade" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Código de Leitura"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "idcidade" e preencher em valor "<idCidade>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | idCidade |
      | ARAGUAIA | 78       |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #EXEMPLO IGNORADO - Nao possui idCidade para pesquisa
      | unidade   |
      | [IGNORAR] |

  @Cadastro_CodigoLeitura_CT013
  Esquema do Cenario: CT013 - Pesquisar Codigo de Leitura - Consulta Simples - Campo "codigoleitura" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "codigoleitura" e preencher em valor "<codigoLeitura>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | codigoLeitura  | Menu                          |
      | ARAGUAIA | LEITURA NORMAL | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | codigoLeitura          | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | CONFIRMACAO DE LEITURA | Cadastro de Codigo de Leitura |

  @Cadastro_CodigoLeitura_CT014
  Esquema do Cenario: CT014 - Pesquisar Codigo de Leitura - Consulta Completa - Com 3 critério de Pesquisa - "E" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "aceitaleitura", em operador "Igual a" e em valor "S"
    E clicar no botao "E"
    E na aba "Consulta Completa" selecionar em campo "codigoleitura", em operador "Diferente de" e em valor "RECALCULO SIMULTANEA MINIMO"
    E clicar no botao "E"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "10"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |

  @Cadastro_CodigoLeitura_CT015
  Esquema do Cenario: CT015 - Pesquisar Codigo de Leitura - Consulta Completa - Com 3 critério de Pesquisa - "OU" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idcriteriofat", em operador "Igual a" e em valor "1000"
    E clicar no botao "OU"
    E na aba "Consulta Completa" selecionar em campo "codigoleitura", em operador "Contém" e em valor "RECALCULO SIMULTANEA MINIMO"
    E clicar no botao "OU"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Menor que" e em valor "10"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |


  @Cadastro_CodigoLeitura_CT016
  Esquema do Cenario: CT016 - Sair da tela de Cadastro de Código de Leitura pelo botao "Sair" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "<Menu>"
    Quando clicar no botao "Sair"
    Entao a tela "Cadastro de Código de Leitura" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  | Menu                          |
      | ARAGUAIA | Cadastro de Código de Leitura |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | Menu                          |
      | CACHOEIRO DE ITAPEMIRIM | Cadastro de Codigo de Leitura |