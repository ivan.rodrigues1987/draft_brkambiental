#language: pt
@Cadastro @Cadastro_Ligacao @Cadastro_Ligacao_Industrial
Funcionalidade: Cadastrar Ligacao Categoria INDUSTRIAL no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | ENTALTERNATIVAMESMACIDADE | VALIDADEBITOALTERACAO | CHECADEBITOSORGAOPUBLICO | UTILIZASOLEIRANEGATIVA | TIPOESGOTAMENTOOBRIGATORIO | CPFCNPJOBRIGATORIO | VALIDARCPFCNPJ | INQUILINONORMALIZADO | CHECADEBITOSCLIENTEATUAL | CHECADEBITOSCLIENTEFUTURO | CHECADEBITOSINQUILINOATUAL | CHECADEBITOSINQUILINOFUTURO | CALCULARVOLUMEESTIMADOLIGACAO | UTILIZAIDENTIFICACAO | UTILIZACONSOLIDACAOCONSUMO | NRMESESALTERACAOVENCIMENTO |
      | 76       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 77       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 78       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 79       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 80       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSMEDICAO":
      | IDCIDADE | IDTIPOCALCULOCONSUMO | IDTIPOCALCULOVOLUMEESTIMADO | NRDIASPADRAOCALCVOLESTIMADO |
      | 76       | 3                    | 2                           | 30                          |
      | 77       | 3                    | 2                           | 30                          |
      | 78       | 3                    | 2                           | 30                          |
      | 79       | 3                    | 2                           | 30                          |
      | 80       | 3                    | 2                           | 30                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | LIMITEMAXENTREGASVENCTO | DIASENTREGAVENCTO | DIASMAXENTREGASVENCTO | TIPODVLIGACAO |
      | 76       | N                       | 5                 | null                  | 1             |
      | 77       | N                       | 5                 | null                  | 1             |
      | 78       | N                       | 5                 | null                  | 1             |
      | 79       | N                       | 5                 | null                  | 1             |
      | 80       | N                       | 5                 | null                  | 1             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCOBRANCA":
      | IDCIDADE | UTILIZARNEGATIVACAOPORCLIENTE |
      | 76       | N                             |
      | 77       | N                             |
      | 78       | N                             |
      | 79       | N                             |
      | 80       | N                             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCONTROLESERVICO":
      | IDCIDADE | TIPOCADASTROLIGACAO |
      | 76       | 2                   |
      | 77       | 2                   |
      | 78       | 2                   |
      | 79       | 2                   |
      | 80       | 2                   |

  #Considerar volumeEstimado = qtdEconomias * valorMensuracao * nrDiasPadraoCalcVolEstimado * baseCalc / 1000
  #Parametro parametrosMedicao.nrDiasPadraoCalcVolEstimado = 30

  @Cadastro_Ligacao_Industrial_CT001
  Esquema do Cenario: CT001 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             | 32     | 12   |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> | 1       | 3      | 23       | 15     |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
#    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
#      | logradouro   | complemento   | numero   | cep   | bairro   |
#      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |


  @Cadastro_Ligacao_Industrial_CT002
  Esquema do Cenario: CT002 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia - Campos obrigatorios - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |


  @Cadastro_Ligacao_Industrial_CT003
  Esquema do Cenario: CT003 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 2 Economia - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |


  @Cadastro_Ligacao_Industrial_CT004
  Esquema do Cenario: CT004 - Cadastrar Ligacao para PESSOA JURIDICA, cliente igual a inquilino - Tipo de Entrega "NO LOCAL" e Economia Mista - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria    | economias | valorMensuracao    |
      | INDUSTRIAL | <subCategoria>  | 1         | <valorMensuracao>  |
      | COMERCIAL  | <subCategoria2> | 1         | <valorMensuracao2> |
#    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
#      | unidadeDeMensuracao             | BaseDeCalculo             | volumeEstimado            |
#      | [Unidade de Mensuracao linha 1] | [Base de Calculo linha 1] | [Volume Estimado linha 1] |
#      | [Unidade de Mensuracao linha 2] | [Base de Calculo linha 2] | [Volume Estimado linha 2] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 9                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | subCategoria2             | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | ESCOLAS - SEMI-INTERNATOS | 23,5            | 2                | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | subCategoria2 | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS | CONSTRUCAO    |                 |                  | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


#  #Verificar - Aguardando retorno - Mensagem de erro ao gravar
#  Cenario: CT005 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "ENTREGA ALTERNATIVA"
#    Dado Eu acesse o TSOne unidade "ARAGUAIA" e navegue pelo menu "Cadastro" e "Ligação"
#    Quando clicar em Pesquisar "Cliente" e no campo Filtrar Por "cnpj", Pesquisar "16718278000178" e "OK"
#    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
#    E na Aba "Localização", em Instalacao preencher os seguintes campos
#      | cidade   | distrito | bairro | subbairro | logradouro | complemento | cep      | numero | zonaDePressao      | bacia   | zonaDeMacro      | quadra | lote | inscricao   | nrImovelAntigo |
#      | REDENÇÃO | REDENÇÃO | CENTRO |           | AV. BRASIL | AP 5 BL C   | 68165000 | 8      | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | 32     | 12   | 12188888229 | 5198           |
#    E na Aba "Localização", em Identificacao preencher os seguintes campos
#      | grupoID | setorID | rotaID | quadraID | loteID | cavaleteID |
#      | GRUPO 5 | 1       | 3      | 23       | 15     | 2          |
#    E na Aba "Localização", em Roteirizacao de Leitura preencher os seguintes campos
#      | grupoRL | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
#      | GRUPO 2 | 1       | 0         | 23       | 15          | 0            |
#    E na Aba Localização, em Roteirização de Entrega, clicar em Tipo Entrega selecionar "ENTREGA ALTERNATIVA"
#    E na Aba Localizacao, em Roteirizacao de Entrega, clicar em Pesquisar Ligacao, em Filtrar Por "Cod. Ligacao (CDC)", Pesquisar "1020749" e "OK"
#    E validar o preenchimento automatico dos demais campos de Roteirização de Entrega:
#      | ligacaoRE | logradouroRE | complementoRE | numeroRE | cepRE | bairroRE | sub-bairroRE |
#      |           | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |              |
#    E na Aba "Dados Comerciais" preencher os seguintes campos
#      | tipoFaturamento | tipoCobranca | situacaoDaLigacao | situacaoDeCobranca | classConsumo         | irregularidade | situacaoImovel | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa |
#      | AGUA E ESGOTO   |              | PROVISORIA        | NORMAL             | CLASSE CONSUMO GERAL |                | CONSTRUÇÃO     | 06/08/2018         | 06/08/2018           |                       | CHAFARIZ         |
#    E na Aba "Dados Comerciais", em Categoria preencher os seguintes campos
#      | categoria | subCategoria | economias | valorMensuracao |
#      | INDUSTRIAL   | PRACA        | 1         | 11              |
#    E validar que os campos abaixo de Categoria foram preenchidos corretamente:
#      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
#      | [Unidade de mensuração] | [Base de Cálculo] | [Volume Estimado] |
#    E na Aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
#    E na Aba "Complemento" validar que os campos abaixo do Inquilino estão preenchidos com os mesmos dados do Cliente:
#      | codInquilino     | nomeInquilino | dataNascimentoInquilino | dddTelefoneInquilino | telefoneInquilino | DDDTelefoneMovelInquilino | telefoneMovelInquilino | emailInquilino | rgInquilino | dataExpRGInquilino | cpfInquilino | cnpjInquilino |
#      | [cód do cliente] | [Nome]        |                         |                      | [Telefone]        |                           | [Telefone Movel]       | [E-mail]       |             |                    |              | [CNPJ]        |
#    E preencher os seguintes campos na Aba Complemento
#      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | poco | reservatorio | diametroRamalAgua | diametroRamalEsgoto | nrQuartos | areaConstruidam2 | tipoDeDespejo | classificacaoDeUso | latitudeGPS | longitudeGPS | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | dataAlteracaoSituacaoLigacao |
#      | 4                 | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | 20000      | S       | BUGATI         | PAREDE             | FOSSA             | SISTEMA INTEGRADO    | 30000                  | S    | 35           |                   |                     | 2         | 200              |               |                    |             |              | 40        | S           | S                       | DIREITA             | ESTADUAL     | 15                 | 10                   |                              |
#    E clicar em "Salvar" em Cadastro de Ligacao
#    E no pop de "Alteracao de Titularidade" clicar em "Sim"
#    Entao a mensagem na tela de Cadastro de Ligacao sera "Dados Salvos com Sucesso!"
#
#  #Verificar - Aguardando retorno
#  Cenario:CT006 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "CORREIO ELETRONICO"
#    Dado Eu acesse o TSOne unidade "ARAGUAIA" e navegue pelo menu "Cadastro" e "Ligação"
#    Quando clicar em Pesquisar "Cliente" e no campo Filtrar Por "cnpj", Pesquisar "16718278000178" e "OK"
#    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
#    E na Aba "Localização", em Instalacao preencher os seguintes campos
#      | cidade   | distrito | bairro | subbairro | logradouro | complemento | cep      | numero | zonaDePressao      | bacia   | zonaDeMacro      | quadra | lote | inscricao   | nrImovelAntigo |
#      | REDENÇÃO | REDENÇÃO | CENTRO |           | AV. BRASIL | AP 5 BL C   | 68165000 | 8      | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | 32     | 12   | 12188888229 | 5198           |
#    E na Aba "Localização", em Identificacao preencher os seguintes campos
#      | grupoID | setorID | rotaID | quadraID | loteID | cavaleteID |
#      | GRUPO 5 | 1       | 3      | 23       | 15     | 2          |
#    E na Aba "Localização", em Roteirizacao de Leitura preencher os seguintes campos
#      | grupoRL | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
#      | GRUPO 2 | 1       | 0         | 23       | 15          | 0            |
#    E na Aba Localização, em Roteirização de Entrega, clicar em Tipo Entrega selecionar "CORREIO ELETRONICO"
#    E validar o preenchimento automatico dos demais campos de Roteirização de Entrega:
#      | ligacaoRE | logradouroRE | complementoRE | numeroRE | cepRE | bairroRE | sub-bairroRE |
#      |           | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |              |
#    E na Aba "Dados Comerciais" preencher os seguintes campos
#      | tipoFaturamento | tipoCobranca | situacaoDaLigacao | situacaoDeCobranca | classConsumo         | irregularidade | situacaoImovel | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa |
#      | AGUA E ESGOTO   |              | PROVISORIA        | NORMAL             | CLASSE CONSUMO GERAL |                | CONSTRUÇÃO     | 06/08/2018         | 06/08/2018           |                       | CHAFARIZ         |
#    E na Aba "Dados Comerciais", em Categoria preencher os seguintes campos
#      | categoria | subCategoria                          | economias | valorMensuracao |
#      | INDUSTRIAL   | TERMINAIS DE PASSAGEIROS E AEROPORTOS | 1         | 12              |
#    E validar que os campos abaixo de Categoria foram preenchidos corretamente:
#      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
#      | [Unidade de mensuração] | [Base de Cálculo] | [Volume Estimado] |
#    E na Aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
#    E na Aba "Complemento" validar que os campos abaixo do Inquilino estão preenchidos com os mesmos dados do Cliente:
#      | codInquilino     | nomeInquilino | dataNascimentoInquilino | dddTelefoneInquilino | telefoneInquilino | DDDTelefoneMovelInquilino | telefoneMovelInquilino | emailInquilino | rgInquilino | dataExpRGInquilino | cpfInquilino | cnpjInquilino |
#      | [cód do cliente] | [Nome]        |                         |                      | [Telefone]        |                           | [Telefone Movel]       | [E-mail]       |             |                    |              | [CNPJ]        |
#    E preencher os seguintes campos na Aba Complemento
#      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | poco | reservatorio | diametroRamalAgua | diametroRamalEsgoto | nrQuartos | areaConstruidam2 | tipoDeDespejo | classificacaoDeUso | latitudeGPS | longitudeGPS | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | dataAlteracaoSituacaoLigacao |
#      | 7                 | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | 20000      | S       | BUGATI         | PAREDE             | FOSSA             | SISTEMA INTEGRADO    | 30000                  | S    | 35           |                   |                     | 2         | 200              |               |                    |             |              | 40        | S           | S                       | DIREITA             | ESTADUAL     | 15                 | 10                   |                              |
#    E clicar em "Salvar" em Cadastro de Ligacao
#    E no pop de "Alteracao de Titularidade" clicar em "Sim"
#    Entao a mensagem na tela de Cadastro de Ligacao sera "Dados Salvos com Sucesso!"


  @Cadastro_Ligacao_Industrial_CT007
  Esquema do Cenario: CT007 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - com Tipo Faturamento  <tipoFaturamento> - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais // Cachoeiro nao tem TipoFaturamento Agua
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A               | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |


  @Cadastro_Ligacao_Industrial_CT008
  Esquema do Cenario: CT008 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - com Tipo Faturamento  <tipoFaturamento> - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais // Cachoeiro nao tem TipoFaturamento ESGOTO
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | ESGOTO          | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | CE              | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |

  @Cadastro_Ligacao_Industrial_CT009
  Esquema do Cenario: CT009 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO ISENTO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO ISENTO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |

  @Cadastro_Ligacao_Industrial_CT010
  Esquema do Cenario: CT010 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |

  @Cadastro_Ligacao_Industrial_CT011
  Esquema do Cenario: CT011 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Situacao da Ligacao ATIVA - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | ATIVA             | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | ATIVA             | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

  @Cadastro_Ligacao_Industrial_CT012
  Esquema do Cenario: CT012 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Situacao da Ligacao INATIVA - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | INATIVA           | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | INATIVA           | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

  @Cadastro_Ligacao_Industrial_CT013
  Esquema do Cenario: CT013 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Endereco do Cliente diferente do Endereco da instalacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_Ligacao_Industrial_CT014
  Esquema do Cenario: CT014 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Inclusao de Debito Automatico, DA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Inclui DA
    Quando aba "Dados Comerciais" clicar no botao "Hist. D.A."
    E na tela de DA preencher o campo ligacao com "[Ligacao cadastrada acima]"
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar no botao "Salvar" na segunda tela
    E clicar no botao "Ok"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_Ligacao_Industrial_CT015
  Esquema do Cenario: CT015 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Ligacao Consolidadora
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao Consolidadora preencher os seguintes campos:
      | ligacaoConsolidadora | economiasLigacaoConsolidadora | observacaoLigacaoConsolidadora |
      | S                    | 1                             | Sem observações                |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois os campos de ligacao consolidadora nao estao habilitados em cachoeiro
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

  @Cadastro_Ligacao_Industrial_CT016
  Esquema do Cenario: CT016 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Consolidar a outra ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao a Consolidar, pesquisar a ligacao "[Ligacao cadastrada da mesma cidade]"
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "Sim"
    E clicar no botao "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

  @Cadastro_Ligacao_Industrial_CT017
  Esquema do Cenario: CT017 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao   | bacia   | zonaDeMacro   | quadra | lote | inscricao   | nrImovelAntigo |
      | <zonaDePressao> | <bacia> | <zonaDeMacro> | 32     | 12   | [Inscricao] | 123            |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> | 1       | 3      | 23       | 15     | 2          |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> | 1       | 0         | 23       | 15          | 0            |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | 06/08/2018         | 06/08/2018           |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento   | tipoDeRede   | padraoDeInstalacao   | posicaoDaRede   | volPiscina | piscina | tipoDeCavalete   | situacaoDoCavalete   | tipoDeEsgotamento   | fonteDeAbastecimento   | capacidadeReservatorio   | poco | reservatorio   | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp   | bloqOsClienteCallCenter | posicaoRedeDeEsgoto   | orgaoPublico   | distanciaRamalAgua   | distanciaRamalEsgoto   |
      | 2                 | <tipoDePavimento> | <tipoDeRede> | <padraoDeInstalacao> | <posicaoDaRede> | 20000      | S       | <tipoDeCavalete> | <situacaoDoCavalete> | <tipoDeEsgotamento> | <fonteDeAbastecimento> | <capacidadeReservatorio> | S    | <reservatorio> | 2         | 200              | 40        | <ligacaoTemp> | S                       | <posicaoRedeDeEsgoto> | <orgaoPublico> | <distanciaRamalAgua> | <distanciaRamalEsgoto> |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF SEM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |

  @Cadastro_Ligacao_Industrial_CT018
  Esquema do Cenario: CT018 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente diferente de Inquilino
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                  | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CNPJ COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |

  @Cadastro_Ligacao_Industrial_CT019
  Esquema do Cenario: CT019 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Inquilino Pessoa Fisica
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_Ligacao_Industrial_CT020
  Esquema do Cenario: CT020 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA - Tipo de Ligacao Consumo Fixo e associar um medidor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [numero] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando selecionar a aba "Medição"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    E fechar Tela "Instalação de Medidor"
    E na aba "Medição", validar que a combo "Tipo de Ligação" foi alterada para "HIDROMETRADO"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |

  @Cadastro_Ligacao_Industrial_CT021
  Esquema do Cenario: CT021 - Cadastrar Ligacao para o Cliente PESSOA FISICA com debito
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria  | subCategoria   | economias | valorMensuracao   |
      | INDUSTRIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "possui debitos nas ligacoes"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | FABRICAS DE BEBIDAS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha mensagem de debito em Cachoeiro
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria        | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | POLIMENTO DE ROCHAS |                 | ETA - SEDE           |              |                     |

