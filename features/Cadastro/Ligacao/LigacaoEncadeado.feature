#language: pt
@Cadastro @Cadastro_Ligacao @Cadastro_LigacaoEncadeado
Funcionalidade: Cadastrar Ligacao Categoria ENCADEADO no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Ligacao" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | ENTALTERNATIVAMESMACIDADE | VALIDADEBITOALTERACAO | CHECADEBITOSORGAOPUBLICO | UTILIZASOLEIRANEGATIVA | TIPOESGOTAMENTOOBRIGATORIO | CPFCNPJOBRIGATORIO | VALIDARCPFCNPJ | INQUILINONORMALIZADO | CHECADEBITOSCLIENTEATUAL | CHECADEBITOSCLIENTEFUTURO | CHECADEBITOSINQUILINOATUAL | CHECADEBITOSINQUILINOFUTURO | CALCULARVOLUMEESTIMADOLIGACAO | UTILIZAIDENTIFICACAO | UTILIZACONSOLIDACAOCONSUMO | NRMESESALTERACAOVENCIMENTO |
      | 76       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 77       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 78       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 79       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 80       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |

    Dado a tela "Cadastro de Ligacao" esta conforme os seguintes parametros da entidade "PARAMETROSMEDICAO":
      | IDCIDADE | IDTIPOCALCULOCONSUMO | IDTIPOCALCULOVOLUMEESTIMADO | NRDIASPADRAOCALCVOLESTIMADO |
      | 76       | 3                    | 2                           | 30                          |
      | 77       | 3                    | 2                           | 30                          |
      | 78       | 3                    | 2                           | 30                          |
      | 79       | 3                    | 2                           | 30                          |
      | 80       | 3                    | 2                           | 30                          |

    Dado a tela "Cadastro de Ligacao" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | LIMITEMAXENTREGASVENCTO | DIASENTREGAVENCTO | DIASMAXENTREGASVENCTO | TIPODVLIGACAO |
      | 76       | N                       | 5                 | null                  | 1             |
      | 77       | N                       | 5                 | null                  | 1             |
      | 78       | N                       | 5                 | null                  | 1             |
      | 79       | N                       | 5                 | null                  | 1             |
      | 80       | N                       | 5                 | null                  | 1             |

    Dado a tela "Cadastro de Ligacao" esta conforme os seguintes parametros da entidade "PARAMETROSCOBRANCA":
      | IDCIDADE | UTILIZARNEGATIVACAOPORCLIENTE |
      | 76       | N                             |
      | 77       | N                             |
      | 78       | N                             |
      | 79       | N                             |
      | 80       | N                             |

    Dado a tela "Cadastro de Ligacao" esta conforme os seguintes parametros da entidade "PARAMETROSCONTROLESERVICO":
      | IDCIDADE | TIPOCADASTROLIGACAO |
      | 76       | 2                   |
      | 77       | 2                   |
      | 78       | 2                   |
      | 79       | 2                   |
      | 80       | 2                   |

  #Considerar volumeEstimado = qtdEconomias * valorMensuracao * nrDiasPadraoCalcVolEstimado * baseCalc / 1000
  #Parametro parametrosMedicao.nrDiasPadraoCalcVolEstimado = 30

  @Cadastro_Ligacao_Encadeado_CT001
  Esquema do Cenario: CT001 - Cadastrar Bairro, Logradouro, Grupo e SubCategoria e cadastrar nova ligacao selecionando os dados cadastrados  - <unidade>
    #Cadastro Bairro
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Bairro"
    Quando preencher os seguintes campos e salvar na tela de Cadastro de bairro
      | cidade   | distrito   | nome     |
      | [Cidade] | [Distrito] | [Bairro] |
    E clicar nos botoes "Salvar" e "Ok"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Bairro" e preencher em valor "[BAIRRO CADASTRADO]"
    E clicar no botao "Pesquisar"
    E o resultado da pesquisa "[BAIRRO CADASTRADO]" sera exibido em tela
    E clicar no botao "Cancelar"
    E clicar no botao "Sair"
    #Cadastro Logradouro
    E navegar pelo menu "Cadastro" e "Logradouro"
    E prencher os seguintes campos no cadastro de Logradouro
      | cidade   | tipo   | titulo   | nome   | inicio   | fin   | referencia   | bairro              |
      | [Cidade] | [Tipo] | [Titulo] | [Nome] | [Inicio] | [Fin] | [Referencia] | [BAIRRO CADASTRADO] |
    E clicar nos botoes "Salvar" e "Ok"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Descricao" e preencher em valor "[LOGRADOURO CADASTRADO]"
    E clicar no botao "Pesquisar"
    E o resultado da pesquisa "[LOGRADOURO CADASTRADO]" sera exibido em tela
    E clicar no botao "Cancelar"
    E clicar no botao "Sair"
    #Cadastro Grupo - Pronto
    E navegar pelo menu "Cadastro" e "Grupo"
    E prencher os seguintes campos no cadastro de Grupo
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | [Cidade] | [Grupo] | [Ordem] | 6          | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          |                       |
    E em Vencimentos Alternativos selecionar o dia do Vencimento Alternativo "29"
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Sair" em Grupo
    #Cadastro SubCategoria
    E navegar pelo menu "Cadastro" e "Cadastro Subcategoria"
    E prencher os seguintes campos no cadastro de SubCategoria
      | cidade   | imovelDesabitado | baixaRenda | possuiEsgotoDif   | aplicarDesconto   | sigla   | descricao   | unidadeDeMensuracao | baseDeCalculo |
      | [Cidade] | S                | S          | <possuiEsgotoDif> | <aplicarDesconto> | [Sigla] | [Descricao] | POR VOL(M3)         | 40            |
    E na tela de Cadastro de SubCategoria clicar o botao direito do mouse clicar em "Adicionar Categoria"
    E adicionar os valores "RESIDENCIAL" para Cadastro de Grupo em Ligacaoes
    E clicar nos botoes "Salvar" e "Ok"
    E clicar no botao "Sair"
    #Cadastro Ligação com dados anteriormente cadastrados
    E navegar pelo menu "Cadastro" e "Ligação"
    E no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro              | logradouro              | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [BAIRRO CADASTRADO] | [LOGRADOURO CADASTRADO] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao   | bacia   | zonaDeMacro   | quadra | lote | inscricao   | nrImovelAntigo |
      | <zonaDePressao> | <bacia> | <zonaDeMacro> | 32     | 12   | [Inscricao] | 123            |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID            | setorID | rotaID | quadraID | loteID | cavaleteID |
      | [GRUPO CADASTRADO] | 1       | 3      | 23       | 15     | 2          |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL            | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | [GRUPO CADASTRADO] | 1       | 0         | 23       | 15          | 0            |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro              | complemento   | numero   | cep   | bairro              |
      | [LOGRADOURO CADASTRADO] | [Complemento] | [Número] | [CEP] | [BAIRRO CADASTRADO] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | 06/08/2018         | 06/08/2018           |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria              | economias | valorMensuracao   |
      | RESIDENCIAL | [SUBCATEGORIA CADASTRADO] | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento   | tipoDeRede   | padraoDeInstalacao   | posicaoDaRede   | volPiscina | piscina | tipoDeCavalete   | situacaoDoCavalete   | tipoDeEsgotamento   | fonteDeAbastecimento   | capacidadeReservatorio   | poco | reservatorio   | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp   | bloqOsClienteCallCenter | posicaoRedeDeEsgoto   | orgaoPublico   | distanciaRamalAgua   | distanciaRamalEsgoto   |
      | 2                 | <tipoDePavimento> | <tipoDeRede> | <padraoDeInstalacao> | <posicaoDaRede> | 20000      | S       | <tipoDeCavalete> | <situacaoDoCavalete> | <tipoDeEsgotamento> | <fonteDeAbastecimento> | <capacidadeReservatorio> | S    | <reservatorio> | 2         | 200              | 40        | <ligacaoTemp> | S                       | <posicaoRedeDeEsgoto> | <orgaoPublico> | <distanciaRamalAgua> | <distanciaRamalEsgoto> |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | aplicarDesconto | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | possuiEsgotoDif | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL |                 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | S               | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF SEM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | aplicarDesconto | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | possuiEsgotoDif | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |                 | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                 |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |
