#language: pt
@Cadastro @Cadastro_Ligacao @Cadastro_LigacaoComercial
Funcionalidade: Cadastrar Ligacao Categoria COMERCIAL no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | ENTALTERNATIVAMESMACIDADE | VALIDADEBITOALTERACAO | CHECADEBITOSORGAOPUBLICO | UTILIZASOLEIRANEGATIVA | TIPOESGOTAMENTOOBRIGATORIO | CPFCNPJOBRIGATORIO | VALIDARCPFCNPJ | INQUILINONORMALIZADO | CHECADEBITOSCLIENTEATUAL | CHECADEBITOSCLIENTEFUTURO | CHECADEBITOSINQUILINOATUAL | CHECADEBITOSINQUILINOFUTURO | CALCULARVOLUMEESTIMADOLIGACAO | UTILIZAIDENTIFICACAO | UTILIZACONSOLIDACAOCONSUMO | NRMESESALTERACAOVENCIMENTO |
      | 76       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 77       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 78       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 79       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 80       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSMEDICAO":
      | IDCIDADE | IDTIPOCALCULOCONSUMO | IDTIPOCALCULOVOLUMEESTIMADO | NRDIASPADRAOCALCVOLESTIMADO |
      | 76       | 3                    | 2                           | 30                          |
      | 77       | 3                    | 2                           | 30                          |
      | 78       | 3                    | 2                           | 30                          |
      | 79       | 3                    | 2                           | 30                          |
      | 80       | 3                    | 2                           | 30                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | LIMITEMAXENTREGASVENCTO | DIASENTREGAVENCTO | DIASMAXENTREGASVENCTO | TIPODVLIGACAO |
      | 76       | N                       | 5                 | null                  | 1             |
      | 77       | N                       | 5                 | null                  | 1             |
      | 78       | N                       | 5                 | null                  | 1             |
      | 79       | N                       | 5                 | null                  | 1             |
      | 80       | N                       | 5                 | null                  | 1             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCOBRANCA":
      | IDCIDADE | UTILIZARNEGATIVACAOPORCLIENTE |
      | 76       | N                             |
      | 77       | N                             |
      | 78       | N                             |
      | 79       | N                             |
      | 80       | N                             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCONTROLESERVICO":
      | IDCIDADE | TIPOCADASTROLIGACAO |
      | 76       | 2                   |
      | 77       | 2                   |
      | 78       | 2                   |
      | 79       | 2                   |
      | 80       | 2                   |

  #Considerar volumeEstimado = qtdEconomias * valorMensuracao * nrDiasPadraoCalcVolEstimado * baseCalc / 1000
  #Parametro parametrosMedicao.nrDiasPadraoCalcVolEstimado = 30

  @Cadastro_LigacaoComercial_CT001
  Esquema do Cenario: CT001 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao   | bacia   | zonaDeMacro   | quadra | lote | inscricao   | nrImovelAntigo |
      | <zonaDePressao> | <bacia> | <zonaDeMacro> | 32     | 12   | [Inscricao] | 123            |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> | 1       | 3      | 23       | 15     | 2          |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> | 1       | 0         | 23       | 15          | 0            |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | 06/08/2018         | 06/08/2018           |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento   | tipoDeRede   | padraoDeInstalacao   | posicaoDaRede   | volPiscina | piscina | tipoDeCavalete   | situacaoDoCavalete   | tipoDeEsgotamento   | fonteDeAbastecimento   | capacidadeReservatorio   | poco | reservatorio   | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp   | bloqOsClienteCallCenter | posicaoRedeDeEsgoto   | orgaoPublico   | distanciaRamalAgua   | distanciaRamalEsgoto   |
      | 2                 | <tipoDePavimento> | <tipoDeRede> | <padraoDeInstalacao> | <posicaoDaRede> | 20000      | S       | <tipoDeCavalete> | <situacaoDoCavalete> | <tipoDeEsgotamento> | <fonteDeAbastecimento> | <capacidadeReservatorio> | S    | <reservatorio> | 2         | 200              | 40        | <ligacaoTemp> | S                       | <posicaoRedeDeEsgoto> | <orgaoPublico> | <distanciaRamalAgua> | <distanciaRamalEsgoto> |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF SEM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoComercial_CT002
  Esquema do Cenario: CT002 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia - Apenas Campos Obrigatorios
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | [IGNORAR] | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoComercial_CT003
  Esquema do Cenario: CT003 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 2 Economias
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT004
  Esquema do Cenario: CT004 - Cadastrar Ligacao para PESSOA JURIDICA, cliente igual a inquilino - Tipo de Entrega "NO LOCAL" e Economia Mista
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CNPJ SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria    | economias | valorMensuracao    |
      | RESIDENCIAL | <subCategoria>  | 1         | <valorMensuracao>  |
      | COMERCIAL   | <subCategoria2> | 1         | <valorMensuracao2> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao             | BaseDeCalculo             | volumeEstimado            |
      | [Unidade de Mensuracao linha 1] | [Base de Calculo linha 1] | [Volume Estimado linha 1] |
      | [Unidade de Mensuracao linha 2] | [Base de Calculo linha 2] | [Volume Estimado linha 2] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 9                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | subCategoria2             | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | ESCOLAS - SEMI-INTERNATOS | 23,5            | 2                | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | subCategoria2 | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   | CONSTRUCAO    |                 |                  | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT005
  Esquema do Cenario: CT005 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "ENTREGA ALTERNATIVA"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "ENTREGA ALTERNATIVA"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha esse item na combo
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

   #Ignorado pois esse item de combo nao esta funcional
  @Cadastro_LigacaoComercial_CT006
  Esquema do Cenario: CT006 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "CORREIO ELETRONICO"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[LIGACAO CODIGO CNPJ SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "ENTREGA ALTERNATIVA"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT007
  Esquema do Cenario: CT007 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo Faturamento <tipoFaturamento>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar [LIGACAO CODIGO CNPJ SEM LIGACOES]
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A               | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT008
  Esquema do Cenario: CT008 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo Faturamento <tipoFaturamento>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | ESGOTO          | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | CE              | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT009
  Esquema do Cenario: CT009 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO ISENTO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO ISENTO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 02/2019                |                   | 14/08/2019 | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade   | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | [IGNORAR] | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | CONSTRUCAO   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoComercial_CT010
  Esquema do Cenario: CT010 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | CONSTRUCAO   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoComercial_CT011
  Esquema do Cenario: CT011 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Situacao da Ligacao ATIVA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | ATIVA             | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | ATIVA             | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT012
  Esquema do Cenario: CT012 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Situacao da Ligacao INATIVA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | INATIVA           | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | INATIVA           | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT013
  Esquema do Cenario: CT013 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Endereco do Cliente diferente do Endereco da instalacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT014
  Esquema do Cenario: CT014 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Inclusao de Debito Automatico, DA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Inclui DA
    Quando aba "Dados Comerciais" clicar no botao "Hist. D.A."
    E na tela de DA preencher o campo ligacao com "[Ligacao cadastrada acima]"
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar no botao "Salvar" na segunda tela
    E clicar no botao "Ok"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT015
  Esquema do Cenario: CT015 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Ligacao Consolidadora
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao Consolidadora preencher os seguintes campos:
      | ligacaoConsolidadora | economiasLigacaoConsolidadora | observacaoLigacaoConsolidadora |
      | S                    | 1                             | Sem observações                |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois os campos de ligacao consolidadora nao estao habilitados em cachoeiro
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT016
  Esquema do Cenario: CT016 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Consolidar a outra ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao a Consolidar, pesquisar a ligacao "[Ligacao cadastrada da mesma cidade]"
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "Sim"
    E clicar no botao "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoComercial_CT017
  Esquema do Cenario: CT017 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CNPJ SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoComercial_CT018
  Esquema do Cenario: CT018 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente diferente de Inquilino
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[LIGACAO CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoComercial_CT019
  Esquema do Cenario: CT019 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Inquilino Pessoa Juridica
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Nome" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                  | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CNPJ COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | JURIDICA    | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoComercial_CT020
  Esquema do Cenario: CT020 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Tipo de Ligacao Consumo Fixo e associar um medidor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando selecionar a aba "Medição"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    E fechar Tela "Instalação de Medidor"
    E na aba "Medição", validar que a combo "Tipo de Ligação" foi alterada para "HIDROMETRADO"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | ARAGUAIA | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | CONSTRUCAO   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoComercial_CT021
  Esquema do Cenario: CT021 - Cadastrar Ligacao para o Cliente PESSOA FISICA com debito
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria | subCategoria   | economias | valorMensuracao   |
      | COMERCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "possui debitos nas ligacoes"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria              | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 2 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | ESCOLAS - SEMI-INTERNATOS | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha mensagem de debito em Cachoeiro
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | CONSTRUCAO   |                 | ETA - SEDE           |              |                     |

