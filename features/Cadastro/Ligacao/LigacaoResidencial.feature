#language: pt
@Cadastro @Cadastro_Ligacao @Cadastro_LigacaoResidencial
Funcionalidade: Cadastrar Ligacao Categoria RESIDENCIAL no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | ENTALTERNATIVAMESMACIDADE | VALIDADEBITOALTERACAO | CHECADEBITOSORGAOPUBLICO | UTILIZASOLEIRANEGATIVA | TIPOESGOTAMENTOOBRIGATORIO | CPFCNPJOBRIGATORIO | VALIDARCPFCNPJ | INQUILINONORMALIZADO | CHECADEBITOSCLIENTEATUAL | CHECADEBITOSCLIENTEFUTURO | CHECADEBITOSINQUILINOATUAL | CHECADEBITOSINQUILINOFUTURO | CALCULARVOLUMEESTIMADOLIGACAO | UTILIZAIDENTIFICACAO | UTILIZACONSOLIDACAOCONSUMO | NRMESESALTERACAOVENCIMENTO |
      | 76       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 77       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 78       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 79       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |
      | 80       | S                         | S                     | N                        | N                      | N                          | S                  | S              | S                    | N                        | S                         | N                          | S                           | S                             | S                    | S                          | 3                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSMEDICAO":
      | IDCIDADE | IDTIPOCALCULOCONSUMO | IDTIPOCALCULOVOLUMEESTIMADO | NRDIASPADRAOCALCVOLESTIMADO |
      | 76       | 3                    | 2                           | 30                          |
      | 77       | 3                    | 2                           | 30                          |
      | 78       | 3                    | 2                           | 30                          |
      | 79       | 3                    | 2                           | 30                          |
      | 80       | 3                    | 2                           | 30                          |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSFATURAMENTO":
      | IDCIDADE | LIMITEMAXENTREGASVENCTO | DIASENTREGAVENCTO | DIASMAXENTREGASVENCTO | TIPODVLIGACAO |
      | 76       | N                       | 5                 | null                  | 1             |
      | 77       | N                       | 5                 | null                  | 1             |
      | 78       | N                       | 5                 | null                  | 1             |
      | 79       | N                       | 5                 | null                  | 1             |
      | 80       | N                       | 5                 | null                  | 1             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCOBRANCA":
      | IDCIDADE | UTILIZARNEGATIVACAOPORCLIENTE |
      | 76       | N                             |
      | 77       | N                             |
      | 78       | N                             |
      | 79       | N                             |
      | 80       | N                             |

    Dado a tela "Cadastro de Ligacoes" esta conforme os seguintes parametros da entidade "PARAMETROSCONTROLESERVICO":
      | IDCIDADE | TIPOCADASTROLIGACAO |
      | 76       | 2                   |
      | 77       | 2                   |
      | 78       | 2                   |
      | 79       | 2                   |
      | 80       | 2                   |

  #Considerar volumeEstimado = qtdEconomias * valorMensuracao * nrDiasPadraoCalcVolEstimado * baseCalc / 1000
  #Parametro parametrosMedicao.nrDiasPadraoCalcVolEstimado = 30

  @Cadastro_LigacaoResidencial_CT001
  Esquema do Cenario: CT001 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao   | bacia   | zonaDeMacro   | quadra | lote | inscricao   | nrImovelAntigo |
      | <zonaDePressao> | <bacia> | <zonaDeMacro> | 32     | 12   | [Inscricao] | 123            |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> | 1       | 3      | 23       | 15     | 2          |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> | 1       | 0         | 23       | 15          | 0            |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | 06/08/2018         | 06/08/2018           |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento   | tipoDeRede   | padraoDeInstalacao   | posicaoDaRede   | volPiscina | piscina | tipoDeCavalete   | situacaoDoCavalete   | tipoDeEsgotamento   | fonteDeAbastecimento   | capacidadeReservatorio   | poco | reservatorio   | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp   | bloqOsClienteCallCenter | posicaoRedeDeEsgoto   | orgaoPublico   | distanciaRamalAgua   | distanciaRamalEsgoto   |
      | 2                 | <tipoDePavimento> | <tipoDeRede> | <padraoDeInstalacao> | <posicaoDaRede> | 20000      | S       | <tipoDeCavalete> | <situacaoDoCavalete> | <tipoDeEsgotamento> | <fonteDeAbastecimento> | <capacidadeReservatorio> | S    | <reservatorio> | 2         | 200              | 40        | <ligacaoTemp> | S                       | <posicaoRedeDeEsgoto> | <orgaoPublico> | <distanciaRamalAgua> | <distanciaRamalEsgoto> |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoResidencial_CT002
  Esquema do Cenario: CT002 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia - Apenas Campos Obrigatorios
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT003
  Esquema do Cenario: CT003 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 2 Economias
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT004
  Esquema do Cenario: CT004 - Cadastrar Ligacao para PESSOA FISICA, cliente igual a inquilino - Tipo de Entrega "NO LOCAL" e Economia Mista
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria    | economias | valorMensuracao    |
      | RESIDENCIAL | <subCategoria>  | 1         | <valorMensuracao>  |
      | COMERCIAL   | <subCategoria2> | 1         | <valorMensuracao2> |
#    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
#      | unidadeDeMensuracao             | BaseDeCalculo             | volumeEstimado            |
#      | [Unidade de Mensuracao linha 1] | [Base de Calculo linha 1] | [Volume Estimado linha 1] |
#      | [Unidade de Mensuracao linha 2] | [Base de Calculo linha 2] | [Volume Estimado linha 2] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 9                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | subCategoria2             | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | ESCOLAS - SEMI-INTERNATOS | 23,5            | 2                | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | subCategoria2 | valorMensuracao | valorMensuracao2 | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   | CONSTRUCAO    |                 |                  | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT005
  Esquema do Cenario: CT005 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "ENTREGA ALTERNATIVA"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "ENTREGA ALTERNATIVA"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"

    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha esse item na combo
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |

   #Ignorado pois esse item de combo nao esta funcional
  @Cadastro_LigacaoResidencial_CT006
  Esquema do Cenario: CT006 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Tipo de Entrega "CORREIO ELETRONICO"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "ENTREGA ALTERNATIVA"
    E na consulta personalizada "Ligação" clicar em pesquisar e pesquisar "[Ligacao cadastrada da mesma cidade]"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | [IGNORAR] | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT007
  Esquema do Cenario: CT007 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo Faturamento <tipoFaturamento>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A               | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT008
  Esquema do Cenario: CT008 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo Faturamento <tipoFaturamento>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | ESGOTO          | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | CE              | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT009
  Esquema do Cenario: CT009 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO ISENTO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO ISENTO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade   | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | [IGNORAR] | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | RESIDENCIA   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoResidencial_CT010
  Esquema do Cenario: CT010 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - com Tipo de Ligacao HIDROMETRADO
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    @ARAGUAIA @21
    Exemplos:
      | unidade   | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | [IGNORAR] | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | RESIDENCIA   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoResidencial_CT011
  Esquema do Cenario: CT011 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Situacao da Ligacao ATIVA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | ATIVA             | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | ATIVA             | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT012
  Esquema do Cenario: CT012 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Situacao da Ligacao INATIVA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | INATIVA           | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | INATIVA           | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT013
  Esquema do Cenario: CT013 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Endereco do Cliente diferente do Endereco da instalacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT014
  Esquema do Cenario: CT014 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Inclusao de Debito Automatico, DA
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Inclui DA
    Quando aba "Dados Comerciais" clicar no botao "Hist. D.A."
    E na tela de DA preencher o campo ligacao com "[Ligacao cadastrada acima]"
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar no botao "Salvar" na segunda tela
    E clicar no botao "Ok"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT015
  Esquema do Cenario: CT015 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Ligacao Consolidadora
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao Consolidadora preencher os seguintes campos:
      | ligacaoConsolidadora | economiasLigacaoConsolidadora | observacaoLigacaoConsolidadora |
      | S                    | 1                             | Sem observações                |
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois os campos de ligacao consolidadora nao estao habilitados em cachoeiro
      | unidade   | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | [IGNORAR] | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT016
  Esquema do Cenario: CT016 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente igual a Inquilino - Consolidar a outra ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao     | BaseDeCalculo     | volumeEstimado    |
      | [Unidade de Mensuracao] | [Base de Calculo] | [Volume Estimado] |
    E na aba Dados Comerciais, em Ligacao a Consolidar, pesquisar a ligacao "[Ligacao cadastrada da mesma cidade]"
    #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
     #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 5                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "Sim"
    E clicar no botao "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino        | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |              |                     |


  @Cadastro_LigacaoResidencial_CT017
  Esquema do Cenario: CT017 - Cadastrar Ligacao para o Cliente PESSOA JURIDICA, Cliente igual a Inquilino - Tipo de Entrega "NO LOCAL" e com 1 Economia - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CNPJ COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT018
  Esquema do Cenario: CT018 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Cliente diferente de Inquilino
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                 | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CPF COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | FISICA      | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoResidencial_CT019
  Esquema do Cenario: CT019 - Cadastrar Ligacao para o Cliente PESSOA FISICA, Inquilino Pessoa Juridica
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Não"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "<inquilino>"
    E na aba Complemento preencher os seguintes campos de Inquilino:
      | tipoCliente   | nome   | dataNascimento   | telefoneFixo   | telefoneMovel   | email   | rg   | dataExpRg   | cpf   | cnpj   |
      | <tipoCliente> | <nome> | <dataNascimento> | <telefoneFixo> | <telefoneMovel> | <email> | <rg> | <dataExpRg> | <cpf> | <cnpj> |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | zonaDePressao      | bacia   | zonaDeMacro      | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino                  | tipoCliente | nome | dataNascimento | telefoneFixo | telefoneMovel | email | rg | dataExpRg | cpf | cnpj | alterarTituralidade |
      | ARAGUAIA | ZONA PRESSAO GERAL | PTP 001 | ZONA MACRO GERAL | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | ASFALTO         | PVC        | NOVO PADRAO        | DIREITA       | BUGATI         | PAREDE             | FOSSA             | UTS 01               | 30000                  | 15000 LITROS | S           | DIREITA             | ESTADUAL     | 15                 | 10                   | [CODIGO CNPJ COM LIGACOES] |             |      |                |              |               |       |    |           |     |      | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | zonaDePressao | bacia | zonaDeMacro | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | reservatorio | ligacaoTemp | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto | inquilino | tipoCliente | nome   | dataNascimento       | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg        | cpf   | cnpj   | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM | Geral         | GERAL | GERAL       |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | GERAL           | GERAL      | GERAL              | GERAL         | GERAL          | GERAL              | GERAL             | ETA - SEDE           |                        | AEROPORTO    |             | GERAL               |              |                    |                      |           | JURIDICA    | [Nome] | [Data de Nascimento] | [Tel Fixo]   | [Tel Movel]   | [E-mail] | [RG] | [Data Expedicao] | [CPF] | [CNPJ] |                     |


  @Cadastro_LigacaoResidencial_CT020
  Esquema do Cenario: CT020 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Tipo de Ligacao Consumo Fixo e associar um medidor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua   | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> | <dataInstalacaoAgua> |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 2         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 4                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    #Vincula medidor
    Quando selecionar a aba "Medição"
    E clicar no botao Medidor e preencher os seguintes campos:
      | periodoLeituraAnterior | leituraNaRetirada | instalacao   | medidor              | leitura | executor   | motivoDaTroca   | programaDeTroca   | lacre | observacao |
      | 01/2018                |                   | [Data Atual] | [Medidor Disponivel] | 3       | <executor> | <motivoDaTroca> | <programaDeTroca> | 2     | Medidor    |
    E clicar no botao "Salvar" em Instalacao de Medidor
    Entao a janela de mensagem "Atenção" sera: "Medidor trocado com sucesso."
    E fechar Tela "Instalação de Medidor"
    E na aba "Medição", validar que a combo "Tipo de Ligação" foi alterada para "HIDROMETRADO"
    @ARAGUAIA @21
    Exemplos:
      | unidade   | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade | executor                       | motivoDaTroca | programaDeTroca |
      | [IGNORAR] | GRUPO 5 | GRUPO 5 | AGUA            | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | 01/12/2018         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 | 003=MANUTENÇÃO AGUA-ANALIS UVC | INSTALAÇÃO    | INSTALAÇÃO      |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | dataInstalacaoAgua | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade | executor                     | motivoDaTroca  | programaDeTroca |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  |                    | RESIDENCIA   |                 | ETA - SEDE           |              |                     | ADEMILSON VIEIRA / TERCEIROS | IRREGULARIDADE | COMERCIAL       |


  @Cadastro_LigacaoResidencial_CT021
  Esquema do Cenario: CT021 - Cadastrar Ligacao para o Cliente PESSOA FISICA com debito
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "3336"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "possui debitos nas ligacoes"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha mensagem de debito em Cachoeiro
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT022
  Esquema do Cenario: CT022 - Realizar a troca de Titularidade de uma ligacao de um Inquilino com Debitos para outro Inquilino sem debitos
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Atendimento" e "Central de Atendimento"
    E pesquisar a ligacao "[LIGACAO CLIENTE C/ DEBITO]" em Central de Atendimento e validar que a "Sit. Negativação" esta igual a "NEGATIVADO"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Ligação"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Ligacao (CDC)" e preencher em valor "[LIGACAO CLIENTE C/ DEBITO]"
    E selecionar no resultado da pesquisa "[LIGACAO CLIENTE C/ DEBITO]" e clicar em "OK"
    E na aba "Complemento", no campo "Inquilino" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CLIENTE ATIVO]"
    E clicar nos botoes "Salvar" e "Sim"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | alterarTituralidade |
      | ARAGUAIA | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois nao ha mensagem de debito em Cachoeiro e campo de pesquisa de inquilino
      | unidade   | alterarTituralidade |
      | [IGNORAR] |                     |


  @Cadastro_LigacaoResidencial_CT023
  Esquema do Cenario: CT023 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Tipo de Ligacao HIDROMETRADO e nao Associar Medidor
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF SEM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "HIDROMETRADO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Tipo de Ligação Hidrometrada está sem medidor"
    E na aba Medicao clicar no botao Medidor
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Salve a nova ligação antes de registrar o medidor"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT024
  Esquema do Cenario: CT024 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "Grupo de identificacao" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID | setorID | rotaID | quadraID | loteID | cavaleteID |
      |         |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "ORA-02291: integrity constraint (PARAGLOB.FK_LIGACAOGPIDENT_GRUPO) violated - parent key not found"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA |         | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Ignorado pois essa combo nao esta habilitada
      | unidade   | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | [IGNORAR] |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT025
  Esquema do Cenario: CT025 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "Grupo de leitura" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      |         |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo grupo de leitura e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 |         | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         |         | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT026
  Esquema do Cenario: CT026 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "tipo de entrega" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo tipo de entrega e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT027
  Esquema do Cenario: CT027 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "situacao de cobranca" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> |                    | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo situacao de cobranca e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT028
  Esquema do Cenario: CT028 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "situacao do imovel" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                |                |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo situacao do imovel e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT029
  Esquema do Cenario: CT029 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "tipo ligacao" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo tipo ligacao e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT030
  Esquema do Cenario: CT030 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "fonte de abastecimento" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   |                      |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Campo fonte de abastecimento e obrigatorio."
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT031
  Esquema do Cenario: CT031 - Cadastrar Ligacao para o Cliente PESSOA FISICA - Campo "Classe de consumo" nao informado
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             |              |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |


  @Cadastro_LigacaoResidencial_CT032
  Esquema do Cenario: CT032 - Pesquisar Ligacao, Consulta Simples - Campo Cod Ligacao CDC
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Ligacao (CDC)" e preencher em valor "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "[LIGACAO CADASTRADA DA MESMA CIDADE]" sera exibido em tela
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_LigacaoResidencial_CT033
  Esquema do Cenario: CT033 - Pesquisar Ligacao - Consulta Completa - Com 3 criterios de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Cod. Ligacao (CDC)", em operador "Igual a" e em valor "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E clicar no botao "E"
    E selecionar em campo "Cod. Bairro", em operador "Diferente de" e em valor "12"
    E clicar no botao "E"
    E selecionar em campo "Cod. Cidade", em operador "Maior que" e em valor "1"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa "[LIGACAO CADASTRADA DA MESMA CIDADE]" sera exibido em tela
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_LigacaoResidencial_CT034
  Esquema do Cenario: CT034 - Pesquisar Ligacao - Consulta Completa - Com 3 criterios de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Cod. Ligacao (CDC)", em operador "Igual a" e em valor "[LIGACAO CADASTRADA DA MESMA CIDADE]"
    E clicar no botao "OU"
    E selecionar em campo "Cod. Bairro", em operador "Igual a" e em valor "0"
    E clicar no botao "OU"
    E selecionar em campo "Cod. Cidade", em operador "Menor que" e em valor "1"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa "[LIGACAO CADASTRADA DA MESMA CIDADE]" sera exibido em tela
    @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_LigacaoResidencial_CT035
  Esquema do Cenario: CT035 - Cadastrar Ligacao e vincular Debito Automatico
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Ligação"
    Quando no campo "Cliente" clicar em pesquisar e filtrar por "Codigo" e pesquisar "[CODIGO CPF COM LIGACOES]"
    E na janela "Deseja Atualizar a Informação do Inquilino ?" clicar em "Sim"
    #Aba Localizacao
    E na aba Localizacao, preencher o Endereco de Instalacao:
      | cidade   | distrito   | bairro   | logradouro   | complemento   | cep   | numero   |
      | [Cidade] | [Distrito] | [Bairro] | [Logradouro] | [Complemento] | [CEP] | [numero] |
    E na aba Localizacao, em "Instalacao" preencher os seguintes campos:
      | zonaDePressao | bacia | zonaDeMacro | quadra | lote | inscricao | nrImovelAntigo |
      |               |       |             |        |      |           |                |
    E na aba Localizacao, em "Identificacao" preencher os seguintes campos:
      | grupoID   | setorID | rotaID | quadraID | loteID | cavaleteID |
      | <grupoID> |         |        |          |        |            |
    E na aba Localizacao, em "Roteirizacao de Leitura" preencher os seguintes campos:
      | grupoRL   | setorRL | roteiroRL | quadraRL | sequenciaRL | subUnidadeRL |
      | <grupoRL> |         |           |          |             |              |
    E na aba Localizacao, em Tipo Entrega selecionar "NO LOCAL"
    E validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:
      | logradouro   | complemento   | numero   | cep   | bairro   |
      | [Logradouro] | [Complemento] | [Número] | [CEP] | [Bairro] |
    #Aba Dados Comerciais
    E na aba "Dados Comerciais" preencher os seguintes campos:
      | tipoFaturamento   | tipoCobranca | situacaoDaLigacao   | situacaoDeCobranca | classConsumo   | irregularidade | situacaoImovel   | dataInstalacaoAgua | dataInstalacaoEsgoto | caracteristicaLigacao | fonteAlternativa   |
      | <tipoFaturamento> |              | <situacaoDaLigacao> | NORMAL             | <classConsumo> |                | <situacaoImovel> |                    |                      |                       | <fonteAlternativa> |
    E na aba Dados Comerciais, em Categoria preencher os seguintes campos:
      | categoria   | subCategoria   | economias | valorMensuracao   |
      | RESIDENCIAL | <subCategoria> | 1         | <valorMensuracao> |
    E validar que os seguintes campos de Categoria foram preenchidos corretamente:
      | unidadeDeMensuracao           | BaseDeCalculo           | volumeEstimado          |
      | [Unidade de Mensuracao linha] | [Base de Calculo linha] | [Volume Estimado linha] |
     #Aba Medicao
    E na aba "Medição" preencher o campo Tipo de Ligacao com "CONSUMO FIXO"
    #Aba Complemento
    E na aba "Complemento" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:
      | codigo         | nome   | dataNascimento    | telefoneFixo | telefoneMovel | email    | rg   | dataExpRg      | cpf   | cnpj   |
      | <codInquilino> | [Nome] | [Data Nascimento] | [Telefone]   | [Telefone]    | [E-mail] | [RG] | [Data Exp. RG] | [CPF] | [CNPJ] |
    E  na aba Complemento preencher os seguintes campos:
      | numeroDeMoradores | tipoDePavimento | tipoDeRede | padraoDeInstalacao | posicaoDaRede | volPiscina | piscina | tipoDeCavalete | situacaoDoCavalete | tipoDeEsgotamento | fonteDeAbastecimento   | capacidadeReservatorio | poco | reservatorio | nrQuartos | areaConstruidam2 | areaVerde | ligacaoTemp | bloqOsClienteCallCenter | posicaoRedeDeEsgoto | orgaoPublico | distanciaRamalAgua | distanciaRamalEsgoto |
      | 2                 |                 |            |                    |               |            |         |                |                    |                   | <fonteDeAbastecimento> |                        |      |              |           |                  |           |             |                         |                     |              |                    |                      |
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    E fechar a tela de Cadastro de Ligacao
    E navegar pelo menu "Cadastro" e "Débito Automático"
    E na tela de DA preencher o campo ligacao com "[LIGACAO CADASTRADA ANTERIORMENTE]"
    E preencher em todas as linhas do Grid de parametros do debito automatico
      | cadastroDA | arrecadador               | agencia | conta |
      | 16/08/2018 | Arrecadador AtivoLayout05 | 00      | 00    |
    E clicar nos botoes "Salvar" e "Ok"
    E sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Ligação"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Cod. Ligacao (CDC)" e preencher em valor "[LIGACAO CADASTRADA ANTERIORMENTE]"
    E selecionar no resultado da pesquisa "[LIGACAO CADASTRADA ANTERIORMENTE]" e clicar em "OK"
    Entao na aba "Dados Comerciais", validar que a combo "Tipo Cobrança" foi alterada para "DEBITO AUTOMATICO"
    @ARAGUAIA @21
    Exemplos:
      | unidade  | grupoID | grupoRL  | tipoFaturamento | situacaoDaLigacao | classConsumo         | situacaoImovel | fonteAlternativa | subCategoria                                    | valorMensuracao | fonteDeAbastecimento | codInquilino        | alterarTituralidade |
      | ARAGUAIA | GRUPO 5 | GRUPO 21 | AGUA E ESGOTO   | PROVISORIA        | CLASSE CONSUMO GERAL | CONSTRUÇÃO     | CHAFARIZ         | CASAS E APART. RESIDENCIA DE ATE 100 M2 DE AREA | 23,5            | UTS 01               | [Codigo do cliente] | Sim                 |
    @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupoID | grupoRL      | tipoFaturamento | situacaoDaLigacao | classConsumo | situacaoImovel | fonteAlternativa | subCategoria | valorMensuracao | fonteDeAbastecimento | codInquilino | alterarTituralidade |
      | CACHOEIRO DE ITAPEMIRIM |         | G1 - SETOR 4 | A / CE          | PROVISÓRIA        | GERAL        | CONSTRUCAO     |                  | RESIDENCIA   |                 | ETA - SEDE           |              |                     |

