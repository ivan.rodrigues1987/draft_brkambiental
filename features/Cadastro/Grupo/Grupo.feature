#language: pt
@Features @Cadastro @Cadastro_Grupo
Funcionalidade: Cadastrar Grupo  no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Grupo" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | NRMAXDIASVENCTOALTERNATIVOAG | NRMESESALTERACAOVENCIMENTO |
      | 76       | 5                            | 3                          |
      | 77       | 5                            | 3                          |
      | 78       | 5                            | 3                          |
      | 79       | 5                            | 3                          |
      | 80       | 5                            | 3                          |

  @Cadastro_Grupo_CT001
  Esquema do Cenario: CT001 - Cadastrar Grupo - Tipo Leitura C
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 10         | C           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "15"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT002
  Esquema do Cenario: CT002 - Cadastrar Grupo - Tipo Leitura S
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 11         | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "16"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Grupo_CT003
  Esquema do Cenario: CT003 - Cadastrar Grupo apenas com o critério "Exige Crítica"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 12         | C           | S            |                 |                        |            |                     |           |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "17"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT004
  Esquema do Cenario: CT004 - Cadastrar Grupo apenas com o critério "Reaviso de Débito"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 13         | C           |              | S               |                        |            |                     |           |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "18"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Grupo_CT005
  Esquema do Cenario: CT005 - Cadastrar Grupo apenas com o critério "Grupo de Ligação Estimada"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 14         | C           |              |                 | S                      |            |                     |           |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "19"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT006
  Esquema do Cenario: CT006 - Cadastrar Grupo apenas com o critério "Corte Light"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | {Grupo] | [ordem] | 15         | C           |              |                 |                        | S          |                     |           |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Grupo_CT007
  Esquema do Cenario: CT007 - Cadastrar Grupo apenas com o critério "Fiscalização de Corte"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 16         | C           |              |                 |                        |            | S                   |           |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "21"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT008
  Esquema do Cenario: CT008 - Cadastrar Grupo apenas com o critério "Estimadas"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 17         | C           |              |                 |                        |            |                     | S         |                        |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "22"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT009
  Esquema do Cenario: CT009 - Cadastrar Grupo apenas com o critério "Ret do Coletor Automático"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 18         | C           |              |                 |                        |            |                     |           | S                      |                        |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "23"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT010
  Esquema do Cenario: CT010 - Cadastrar Grupo apenas com o critério "Cal de Consumo Automático"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 19         | C           |              |                 |                        |            |                     |           |                        | S                      |                            | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "24"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT011
  Esquema do Cenario: CT011 - Cadastrar Grupo apenas com o critério "Cal de Faturamento Automático"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 20         | C           |              |                 |                        |            |                     |           |                        |                        | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "25"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT012
  Esquema do Cenario: CT012 - Cadastrar Grupo com 3 dias de vencimento alternativo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 21         | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "10"
    E em Vencimentos Alternativos selecionar o dia "15"
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT013
  Esquema do Cenario: CT013 - Cadastrar Grupo - Campo "Cidade" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      |        | [Grupo] | [ordem] | 22         | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Informe a cidade."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT014
  Esquema do Cenario: CT014 - Cadastrar Grupo - Campo "Grupo" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo | ordem | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> |       | 263   | 23         | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Informe o Grupo."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT015
  Esquema do Cenario: CT015 - Cadastrar Grupo - Campo "Ordem" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] |       | 24         | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Informe a Ordem."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Grupo_CT016
  Esquema do Cenario: CT016 - Cadastrar Grupo - Campo "Venc Padrão" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] |            | S           | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Informe o Vencimento Padrão."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT017
  Esquema do Cenario: CT017 - Cadastrar Grupo - Campo "Tipo Leitura" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 25         |             | S            | S               | S                      | S          | S                   | S         | S                      | S                      | S                          | Teste 11/03/2019      |
    E em Vencimentos Alternativos selecionar o dia "20"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Informe o Tipo Leitura."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Grupo_CT018
  Esquema do Cenario: CT018 - Cadastrar Grupo - Campo "Venc Alternativos" não preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando preencher os seguintes campos do Cadastro de Grupo:
      | cidade   | grupo   | ordem   | vencPadrao | tipoLeitura | exigeCritica | reavisodeDebito | grupodeLigacaoEstimada | corteLight | fiscalizacaodeCorte | estimadas | retdoColetorAutomatico | caldeConsumoAutomatico | caldeFaturamentoAutomatico | mensagemCanhotoEsgoto |
      | <cidade> | [Grupo] | [ordem] | 25         | S           | s            | s               | s                      | s          | s                   | s         | s                      | s                      | s                          | Teste 11/03/2019      |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o(s) Dia(s) do Vencimento Alternativo."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade   |
      | ARAGUAIA | REDENÇÃO |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Grupo_CT019
  Esquema do Cenario: CT019 - Cadastrar Grupo - Alterar vencimento do Grupo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    E selecionar no resultado da pesquisa "<valor>" e clicar em "OK"
    E alterar a combo "Venc. Padrão" para "14"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
  @ARAGUAIA @21
    Exemplos: #Verificar o passo selecionar
      | unidade   | valor |
      | [IGNORAR] | 7     |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT019 Alteração Permitida Somente se Alterar a Ordem
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Grupo_CT020
  Esquema do Cenario: CT020 - Cadastrar Grupo - Alterar nome do Grupo para um nome já existente na cidade
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    E selecionar no resultado da pesquisa "<valor>" e clicar em "OK"
    E alterar o campo "Grupo" para "<Grupo>"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O grupo ou ordem ja existe para esta cidade"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor | Grupo    |
      | ARAGUAIA | 8     | GRUPO 13 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor | Grupo        |
      | CACHOEIRO DE ITAPEMIRIM | 8     | G1 - SETOR 1 |


  @Cadastro_Grupo_CT021
  Esquema do Cenario: CT021 - Cadastrar Grupo - Replicar vencimento alternativo para outro Grupo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    E selecionar no resultado da pesquisa "<valor>" e clicar em "OK"
    E clicar no botao "Replicar"
    E selecionar o grupo "<Grupo>" referente a cidade "<cidade>"
    E clicar no botao "Replicar" em Grupo
    Entao sera exibida a mensagem em tela: "Vencimento(s) Alternativo(s) Replicados com Sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade              | valor | Grupo   |
      | ARAGUAIA | SANTANA DO ARAGUAIA | 55    | GRUPO 1 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #CT021 Replicação não disponível em cachoeiro
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Grupo_CT022
  Esquema do Cenario: CT022 - Validar a pesquisa de Grupos - Pesquisa Simples - Campo Código
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "<valor>" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 7     |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 8     |


  @Cadastro_Grupo_CT023
  Esquema do Cenario: CT023 - Validar a pesquisa de Grupos - Pesquisa Simples - Campo Grupo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Grupo" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 11    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 1     |

  @Cadastro_Grupo_CT024
  Esquema do Cenario: CT024 - Validar a pesquisa de Grupos - Pesquisa Simples - Campo Cód Cidade
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E em pesquisa rapida selecionar em campo "Cod Cidade" e preencher em valor "<valor>"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "<valor>" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 76    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 2     |

  @Cadastro_Grupo_CT025
  Esquema do Cenario: CT025 - Validar a pesquisa de Grupos - Consulta Completa - Com 3 critério de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E na aba "Consulta Completa" selecionar em campo "Grupo", em operador "Igual a" e em valor "<grupo>"
    E clicar no botao "E"
    E selecionar em campo "Codigo", em operador "Maior ou igual a" e em valor "<codigo>"
    E clicar no botao "E"
    E selecionar em campo "Cod Cidade", em operador "Contém" e em valor "<codcidade>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa "<grupo>" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | grupo | codigo | codcidade |
      | ARAGUAIA | 7     | 1      | 76        |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupo | codigo | codcidade |
      | CACHOEIRO DE ITAPEMIRIM | 7     | 1      | 2         |

  @Cadastro_Grupo_CT026
  Esquema do Cenario: CT026 - Validar a pesquisa de Grupos - Consulta Completa - Com 3 critério de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E na aba "Consulta Completa" selecionar em campo "Grupo", em operador "Igual a" e em valor "<grupo>"
    E clicar no botao "OU"
    E selecionar em campo "Codigo", em operador "Maior ou igual a" e em valor "<codigo>"
    E clicar no botao "OU"
    E selecionar em campo "Cod Cidade", em operador "Contém" e em valor "<codcidade>"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa "<grupo>" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | grupo | codigo | codcidade |
      | ARAGUAIA | 7     | 1      | 76        |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | grupo | codigo | codcidade |
      | CACHOEIRO DE ITAPEMIRIM | 7     | 1      | 2         |

  @Cadastro_Grupo_CT027
  Esquema do Cenario: CT027 - Sair da tela de Cadastro de Grupos pelo botao "Sair"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Grupo"
    Quando clicar no botao "Pesquisa Rapida" em Grupo
    E clicar no botao "Cancelar"
    E clicar no botao "Sair" em Grupo
    Entao a tela "Cadastro de Grupo" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |