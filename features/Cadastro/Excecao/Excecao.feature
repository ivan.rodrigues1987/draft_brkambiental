#language: pt
@Features @Cadastro @Cadastro_Excecao
Funcionalidade:  Cadastrar Exceção no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Exceção" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | TAMANHOMAXRUBRICA |
      | 76       | 22                |
      | 77       | 22                |
      | 78       | 22                |
      | 79       | 22                |
      | 80       | 22                |


  @Cadastro_Excecao_CT001
  Esquema do Cenario: CT001 - Cadastrar Excecao com sucesso - Produto AGUA - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | rubrica         |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | AGUA    | PIS/PASEP - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Excecao_CT002
  Esquema do Cenario: CT002 - Cadastrar Excecao com sucesso - Produto ESGOTO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | rubrica                         |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | ESGOTO  | LIGACAO DE ESGOTO EXTERNA - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Excecao_CT003
  Esquema do Cenario: CT003 - Cadastrar Excecao Ativa com sucesso - Apenas os campos obrigatorios - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE |                |                       |                     |                 |                      |               |             |                           |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | rubrica         |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | ESGOTO  | PIS/PASEP - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | produto | rubrica     |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | CTE     | JUROS - R$0 |

  @Cadastro_Excecao_CT004
  Esquema do Cenario: CT004 - Cadastrar Excecao com sucesso - Ativa = NAO - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] | S     | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE |                |                       |                     |                 |                      |               |             |                           |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | rubrica      |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | ESGOTO  | COFINS - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | produto | rubrica     |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | C       | JUROS - R$0 |

  @Cadastro_Excecao_CT005
  Esquema do Cenario: CT005 - Cadastrar Excecao com sucesso - sucesso - Com 3 Produtos/Rubrica - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE |                |                       |                     |                 |                      |               |             |                           |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto2>" e "<rubrica2>" em Cadastrar Excecao
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto3>" e "<rubrica3>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | produto2 | produto3 | rubrica                         | rubrica2                                        | rubrica3                                       |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | ESGOTO  | AGUA     | ESGOTO   | LIGACAO DE ESGOTO EXTERNA - R$0 | RELIGACAO COM REPOSICAO DE HIDROMETRO - R$73.33 | RELIGACAO APOS SUPRESSAO - PCP - URGENTE - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | produto | produto2 | produto3 | rubrica                      | rubrica2                               | rubrica3                                    |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | C       | CTE      | CTE      | DEV PAGAMENTO INDEVIDO - R$0 | SERVICO DE RELIGACAO DE AGUA - R$16.75 | TARIFA DE COLETA ESGOTO - RESIDENCIAL - R$0 |


  @Cadastro_Excecao_CT006
  Esquema do Cenario: CT006 - Cadastrar Excecao - Pelo botao Novo - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Novo"
    E Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE |                |                       |                     |                 |                      |               |             |                           |
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Adicionar"
    E adicionar os valores "<produto>" e "<rubrica>" em Cadastrar Excecao
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  | produto | rubrica                           |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI | ESGOTO  | TARIFA COLET/TRATAMENTO ESG - R$0 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  | produto | rubrica                                   |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM | C       | TARIFA DE COLETA ESGOTO - COMERCIAL - R$0 |


  @Cadastro_Excecao_CT007
  Esquema do Cenario: CT007 - Cadastrar sem sucesso - Campo Cidade nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade         | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | --SELECIONAR-- | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo Cidade e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT008
  Esquema do Cenario: CT008 - Cadastrar sem sucesso - Campo Excecao nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> |         |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo excecao e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT009
  Esquema do Cenario: CT009 - Cadastrar sem sucesso - Campo tipo nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       |      | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo Tipo Excecao e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT010
  Esquema do Cenario: CT010 - Cadastrar sem sucesso - Campo tipo faturamento nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    |                         | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo tipo faturamento admitido e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT011
  Esquema do Cenario: CT011 - Cadastrar sem sucesso - Campo Aplicar sobre nao preenchido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA |              | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo Aplicacao Excecao e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT012
  Esquema do Cenario: CT012 - Cadastrar sem sucesso - Campo Momento Calculo - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     |                | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "o Campo momento calculo e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT013
  Esquema do Cenario: CT013 - Remover Produtos/Rubrica da Exceção com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
    E na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em "Remover"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor | cidade                  |
      | ARAGUAIA | 8001  | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | 30100 | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT014
  Esquema do Cenario: CT014 - Alterar excecao com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
    E Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     |                     |                 |                      |               |             |                           |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT015
  Esquema do Cenario: CT015 - Alterar excecao com sucesso - Ativar Nao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
    E Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] | S     | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     |                     |                 |                      |               |             |                           |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT016
  Esquema do Cenario: CT016 - Alterar excecao com sucesso - Ativar Nao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
    E Preencher os seguintes campos em Cadastro de Excecao
      | cidade   | excecao   | ativa | tipo             | aplicarsobre | momentocalcula | tipofaturamentoadmitido | motivopadrao       | perdacomercial | considerarnorecalculo | permiteligacaofilho | retencaoforzada | aplicaligacaocortada | consrecalculo | cadastravel | visualizadapelocallcenter |
      | <cidade> | [excecao] |       | LIGACAO PRECARIA | DESCONTO     | Faturamento    | SIMPLES                 | ACORDO COM CLIENTE | S              | S                     | S                   | S               | S                    | S             | S           | S                         |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT017
  Esquema do Cenario: CT017 - Excluir excecao com sucesso - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    E clicar no botao "Pesquisa Rapida"
    E clicar no botao "OK"
    Entao a janela de mensagem "Mensagem do Sistema" contera: "Nao existe Excecao com o id"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade           |
      | ARAGUAIA | Cadastro_Excecao |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |

#  @Cadastro_Excecao_CT018  CASO NAO SE PODE TESTAR DE FORMA MANUAL
#  Esquema do Cenario: CT018 - Excluir excecao sem sucesso - Registro filho localizado - <unidade>
#    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
#    Quando clicar no botao "Pesquisa Rapida"
#    E em pesquisa rapida selecionar em campo "Codigo" e preencher em valor "[EXCECAO CADASTRADO]"
#    E selecionar no resultado da pesquisa "[EXCECAO CADASTRADO]" e clicar em "OK"
#    E clicar nos botoes "Excluir" e "Sim"
#    E clicar no botao "Pesquisa Rapida"
#    E clicar no botao "OK"
#    Entao a janela de mensagem "Mensagem do Sistema" sera: "ORA-02292: restrição de integridade (PARAGLOB.FK_MEDDET_EXCECAO) violada - registro filho localizado"
#  @ARAGUAIA @21
#    Exemplos:
#      | unidade  | cidade   |
#      | ARAGUAIA | REDENÇÃO |
#  @CACHOEIRODEITAPEMIRIM @2
#    Exemplos:
#      | unidade                 | cidade                  |
#      | CACHOEIRO DE ITAPEMIRIM | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT019
  Esquema do Cenario: CT019 - Pesquisar Excecao - Consulta Simples - Campo "idcidade" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "idcidade" e o valor "80" para Cadastro de Excecao
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | cidade                  |
      | ARAGUAIA | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Excecao_CT020
  Esquema do Cenario: CT020 - Pesquisar Excecao - Consulta Simples - Campo "Excecao" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "Exceção" e o valor "acima" para Cadastro de Excecao
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor | cidade                  |
      | ARAGUAIA | 8001  | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | 30100 | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT021
  Esquema do Cenario: CT021 - Pesquisar Excecao - Consulta Simples - Campo "Codigo" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa tem que preencher na cidade "<cidade>", o campo "Codigo" e o valor "<valor>" para Cadastro de Excecao
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor | cidade                  |
      | ARAGUAIA | 8001  | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor | cidade                  |
      | CACHOEIRO DE ITAPEMIRIM | 30100 | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Excecao_CT022
  Esquema do Cenario: CT022 - Pesquisar Excecao - Consulta Completa - Validar que todos os criterios de pesquisa estao sendo apresentados - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" de Pesquisa Rapida
    Entao validar os criterios de pesquisa de Cadastro de Excecao
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT023
  Esquema do Cenario: CT023 - Pesquisar Excecao - Consulta Completa - Campo "codigo" e OPERADOR "Maior que" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Codigo", em operador "Maior que" e em valor "30"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Excecao_CT024
  Esquema do Cenario: CT024 - Pesquisar Excecao - Consulta Completa - Com 3 criterio de Pesquisa - "E" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idcidade", em operador "Igual a" e em valor "<valor>"
    E clicar no botao "E"
    E selecionar em campo "Codigo", em operador "Menor que" e em valor "20"
    E clicar no botao "E"
    E selecionar em campo "perdacomercial", em operador "Contém" e em valor "N"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 80    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 2     |

  @Cadastro_Excecao_CT025
  Esquema do Cenario: CT025 - Pesquisar Excecao - Consulta Completa - Com 3 criterio de Pesquisa - "OU" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idcidade", em operador "Igual a" e em valor "<valor>"
    E clicar no botao "OU"
    E selecionar em campo "Codigo", em operador "Menor que" e em valor "20"
    E clicar no botao "OU"
    E selecionar em campo "perdacomercial", em operador "Contém" e em valor "N"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 80    |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor |
      | CACHOEIRO DE ITAPEMIRIM | 2     |


  @Cadastro_Excecao_CT026
  Esquema do Cenario: CT026 - Sair da tela de Cadastro Excecao pelo botao "Sair"- <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Exceção"
    Quando clicar no botao "Sair"
    Entao a tela "Exceção" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |
