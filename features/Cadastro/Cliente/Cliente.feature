#language: pt
@Features @Cadastro @Cadastro_Cliente
Funcionalidade: Cadastrar Cliente no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Cadastro de Cliente" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | CPFCNPJOBRIGATORIO | VALIDARCPFCNPJ | MASCARATELEFONE | VALIDARTELEFONE | DDDINICIAL | DDDFINAL | TAMANHOFONEFIXOMAXIMO | TAMANHOFONEMOVELMINIMO | NORMALIZAENDERECOCOMPLETO |
      | 76       | S                  | S              | S               | S               | 11         | 98       | 7000                  | 7000                   | S                         |
      | 77       | S                  | S              | S               | S               | 11         | 98       | 7000                  | 7000                   | S                         |
      | 78       | S                  | S              | S               | S               | 11         | 98       | 7000                  | 7000                   | S                         |
      | 79       | S                  | S              | S               | S               | 11         | 98       | 7000                  | 7000                   | S                         |
      | 80       | S                  | S              | S               | S               | 11         | 98       | 7000                  | 7000                   | S                         |

  @Cadastro_Cliente_CT001
  Esquema do Cenario: CT001 - Cadastro com sucesso de Cliente Pessoa Juridica - CNPJ Valido e todos os campos - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg | dataExpRg | cpf | cnpj   | telefoneFixo    | telefoneMovel    | email   | nomePai | nomeMae | observacao           |
      | [Nome] | JURIDICA    | [Cidade] |      |                | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> |    |           |     | [CNPJ] | [Telefone Fixo] | [Telefone Movel] | [Email] |         |         | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Cliente cadastrado acima]"
    Entao o resultado da pesquisa "[Cliente cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT002
  Esquema do Cenario: CT002 - Cadastro com sucesso de Cliente Pessoa Juridica - CNPJ Valido e campos obrigatorios - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg | dataExpRg | cpf | cnpj   | telefoneFixo    | telefoneMovel    | email   | nomePai | nomeMae | observacao |
      | [Nome] | JURIDICA    | [Cidade] |      |                | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> |    |           |     | [CNPJ] | [Telefone Fixo] | [Telefone Movel] | [Email] |         |         |            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT003
  Esquema do Cenario: CT003 - Cadastro de novo cliente Pessoa Juridica - CNPJ Invalido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo | dataNascimento | bairro   | logradouro   | numLogradouro | complemento | cep | uf   | rg | dataExpRg | cpf | cnpj           | telefoneFixo    | telefoneMovel    | email   | nomePai | nomeMae | observacao |
      | [Nome] | JURIDICA    | [Cidade] |      | 10101980       | [Bairro] | [Logradouro] | [Numero]      |             |     | <UF> |    |           |     | 12300798000000 | [Telefone Fixo] | [Telefone Movel] | [Email] |         |         |            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Cnpj invalido"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT004
  Esquema do Cenario: CT004 - Cadastro de novo cliente Pessoa Juridica - Dados ja cadastrado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf | cnpj         | telefoneFixo    | telefoneMovel    | email   | nomePai    | nomeMae     | observacao           |
      | [Nome] | JURIDICA    | [Cidade] |      | 10101980       | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  |     | [Cadastrado] | [Telefone Fixo] | [Telefone Movel] | [Email] | Joao Silva | Clara Silva | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Ja existe um cliente registrado com este CNPJ."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT005
  Esquema do Cenario: CT005 - Cadastro de novo cliente Pessoa Fisica - CPF Valido e todos os campos - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo      | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf   | cnpj | telefoneFixo    | telefoneMovel    | email   | nomePai    | nomeMae     | observacao           |
      | [Nome] | FISICA      | [Cidade] | MASCULINO | 10101980       | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  | [CPF] |      | [Telefone Fixo] | [Telefone Movel] | [Email] | Joao Silva | Clara Silva | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E na tela de pesquisa rapida pesquisar "[Cliente cadastrado acima]"
    Entao o resultado da pesquisa "[Cliente cadastrado acima]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT006
  Esquema do Cenario: CT006 - Cadastro de novo cliente Pessoa Fisica - CPF Valido e campos obrigatorios - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo      | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf   | cnpj | telefoneFixo    | telefoneMovel    | email   | nomePai    | nomeMae     | observacao           |
      | [Nome] | FISICA      | [Cidade] | MASCULINO | 10101980       | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  | [CPF] |      | [Telefone Fixo] | [Telefone Movel] | [Email] | Joao Silva | Clara Silva | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |


  @Cadastro_Cliente_CT007
  Esquema do Cenario: CT007 - Cadastrar cliente Pessoa Fisica - CPF Invalido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo      | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf | cnpj | telefoneFixo    | telefoneMovel    | email   | nomePai    | nomeMae     | observacao           |
      | [Nome] | FISICA      | [Cidade] | MASCULINO |                | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  |     |      | [Telefone Fixo] | [Telefone Movel] | [Email] | Joao Silva | Clara Silva | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Campo CPF e obrigatorio!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos: #Nao esta fazendo a validacao de CPF Obrigatorio
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Cliente_CT008
  Esquema do Cenario: CT008 - Cadastrar cliente Pessoa Fisica - CPF ja cadastrado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo      | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf          | cnpj | telefoneFixo    | telefoneMovel    | email   | nomePai      | nomeMae   | observacao     |
      | [Nome] | FISICA      | [Cidade] | MASCULINO | 10101980       | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  | [Cadastrado] |      | [Telefone Fixo] | [Telefone Movel] | [Email] | Thiago Silva | Ana Silva | Confirmar docs |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Ja existe um cliente registrado com este CPF."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |

  @Cadastro_Cliente_CT009
  Esquema do Cenario: CT009 - Pesquisar cliente - por Nome do Cliente - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Nome do Cliente" e o valor "[NOME CPF SEM LIGACOES]"
    E clicar no botao "Pesquisar"
    Entao o resultado da pesquisa "[NOME CPF SEM LIGACOES]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT010
  Esquema do Cenario: CT010 - Pesquisar cliente - por Codigo - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CPF SEM LIGACOES]"
    Entao o resultado da pesquisa "[CODIGO CPF SEM LIGACOES]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT011
  Esquema do Cenario: CT011 - Pesquisar cliente - CNPJ valido mas nao cadastrado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "cnpj" e o valor "<valor>"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao ha dados que satisfacam esta pesquisa!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor          |
      | ARAGUAIA | 68996344000136 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Cliente_CT012
  Esquema do Cenario: CT012 - Pesquisar cliente - CPF valido mas nao cadastrado - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "cpf" e o valor "<valor>"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao ha dados que satisfacam esta pesquisa!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor       |
      | ARAGUAIA | 92126895076 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Cliente_CT013
  Esquema do Cenario: CT013 - Pesquisar cliente - por Nome do Cliente invalido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Nome do Cliente" e o valor "<valor>"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao ha dados que satisfacam esta pesquisa!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor             |
      | ARAGUAIA | BRK LINK GWEB 001 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor             |
      | CACHOEIRO DE ITAPEMIRIM | BRK LINK GWEB 001 |

  @Cadastro_Cliente_CT014
  Esquema do Cenario: CT014 - Pesquisar cliente - por Codigo invalido - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "<valor>"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Nao ha dados que satisfacam esta pesquisa!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor  |
      | ARAGUAIA | 666666 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | valor   |
      | CACHOEIRO DE ITAPEMIRIM | 7777777 |

  @Cadastro_Cliente_CT015
  Esquema do Cenario: CT015 - Pesquisar cliente - campos em branco - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "<campo2>" e o valor "<campo3>"
    E clicar no botao "Pesquisar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Favor informar um dado para pesquisar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  | campo2 | campo3 |
      | ARAGUAIA |        |        |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | campo2 | campo3 |
      | CACHOEIRO DE ITAPEMIRIM |        |        |

  @Cadastro_Cliente_CT016
  Esquema do Cenario: CT016 - Cadastro de novo cliente Pessoa Juridica - Campos Obrigatorios preenchidos - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo | dataNascimento | bairro   | logradouro   | numLogradouro | complemento | cep | uf | rg      | dataExpRg | cpf | cnpj   | telefoneFixo    | telefoneMovel    | email   | nomePai | nomeMae | observacao |
      | [Nome] | JURIDICA    | [Cidade] |      | 13101987       | [Bairro] | [Logradouro] | [Numero]      |             |     |    | 5232144 |           |     | [CNPJ] | [Telefone Fixo] | [Telefone Movel] | [Email] |         |         |            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |


  @Cadastro_Cliente_CT017
  Esquema do Cenario: CT017 - Cadastro de novo cliente Pessoa Fisica - Campos Obrigatorios preenchidos - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando preencher os seguintes campos em Cadastro de Cliente:
      | nome   | tipoCliente | cidade   | sexo     | dataNascimento | bairro   | logradouro   | numLogradouro | complemento   | cep   | uf   | rg      | dataExpRg | cpf   | cnpj | telefoneFixo    | telefoneMovel    | email   | nomePai      | nomeMae      | observacao           |
      | [Nome] | FISICA      | [Cidade] | FEMININO | 11101981       | [Bairro] | [Logradouro] | [Numero]      | [Complemento] | [Cep] | <UF> | 5232344 | 10101999  | [CPF] |      | [Telefone Fixo] | [Telefone Movel] | [Email] | Benito Silva | Leoria Silva | Confirmar Documentos |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | UF |
      | ARAGUAIA | PA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | UF |
      | CACHOEIRO DE ITAPEMIRIM | ES |

  @Cadastro_Cliente_CT018
  Esquema do Cenario: CT018 - Limpar tela de Cadastro de cliente pelo botão "Novo" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Nome do Cliente" e o valor "[NOME CPF SEM LIGACOES]"
    E selecionar no resultado da pesquisa "[NOME CPF SEM LIGACOES]" e clicar em "OK"
    E clicar no botao "NOVO"
    Entao os campos em cadastro de cliente estarao em branco
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT019
  Esquema do Cenario: CT019 - Sair da tela de Cadastro de Clientes pelo botao "Sair" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Sair"
    Entao a tela "Cadastro de Cliente" estara fechada
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT020
  Esquema do Cenario: CT020 - Excluir um cliente Pessoa Fisica pelo botao "Excluir" - Cliente NAO vinculado a ligacao  - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CPF SEM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CPF SEM LIGACOES]" e clicar em "OK"
    Entao clicar nos botoes "Excluir" e "Sim"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT021
  Esquema do Cenario: CT021 - Excluir um cliente Pessoa Fisica pelo botao "Excluir" - Cliente vinculado a uma ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CPF COM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CPF COM LIGACOES]" e clicar em "OK"
    E clicar nos botoes "Excluir" e "Sim"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "<mensagem>"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | mensagem   |
      | ARAGUAIA | ORA-02292: |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | mensagem   |
      | CACHOEIRO DE ITAPEMIRIM | ORA-02292: |


  @Cadastro_Cliente_CT022
  Esquema do Cenario: CT022 - Excluir um cliente Pessoa Juridica pelo botao "Excluir" - Cliente NAO vinculado a ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CNPJ SEM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CNPJ SEM LIGACOES]" e clicar em "OK"
    Entao clicar nos botoes "Excluir" e "Sim"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT023
  Esquema do Cenario: CT023 - Excluir um cliente Pessoa Juridica pelo botão "Excluir" - Cliente vinculado a uma ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CNPJ COM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CNPJ COM LIGACOES]" e clicar em "OK"
    Entao clicar nos botoes "Excluir" e "Sim"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "<mensagem>"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | mensagem   |
      | ARAGUAIA | ORA-02292: |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | mensagem   |
      | CACHOEIRO DE ITAPEMIRIM | ORA-02292: |

  @Cadastro_Cliente_CT024
  Esquema do Cenario: CT024 - Alterar cliente Pessoa Fisica - Cliente Não vinculado a ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "<valor>"
    E selecionar no resultado da pesquisa "<valor>" e clicar em "OK"
    E alterar o campo "Complemento" para "AP 101"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Nenhuma Ligação deverá ser alterada, pois o Cliente não possui vínculo!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 24727 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:#nao faz validacao
      | unidade   |
      | [IGNORAR] |


  @Cadastro_Cliente_CT025
  Esquema do Cenario: CT025 - Alterar cliente Pessoa Fisica - Cliente vinculado a ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CPF COM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CPF COM LIGACOES]" e clicar em "OK"
    E alterar o campo "Complemento" para "AP 101"
    E alterar o campo "Telefone Fixo" para "1125456874"
    E alterar o campo "Telefone Movel" para "11991879754"
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | alterarTituralidade | Cidade                  |
      | ARAGUAIA | Ok                  | SAO DOMINGOS DO ARAGUAI |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | alterarTituralidade | Cidade                  |
      | CACHOEIRO DE ITAPEMIRIM |                     | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT026
  Esquema do Cenario:  CT026 - Alterar cliente Pessoa Juridica - Cliente Não vinculado a ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "<valor>"
    E selecionar no resultado da pesquisa "<valor>" e clicar em "OK"
    E alterar o campo "Complemento" para "AP 101"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Nenhuma Ligação deverá ser alterada, pois o Cliente não possui vínculo!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | valor |
      | ARAGUAIA | 24727 |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:#nao faz validacao
      | unidade   |
      | [IGNORAR] |

  @Cadastro_Cliente_CT027
  Esquema do Cenario: CT027 - Alterar cliente Pessoa Juridica - Cliente vinculado a ligacao - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na tela de Pesquisa eu preencher a cidade "[cidade]", o campo "Codigo" e o valor "[CODIGO CNPJ COM LIGACOES]"
    E selecionar no resultado da pesquisa "[CODIGO CNPJ COM LIGACOES]" e clicar em "OK"
    E selecionar o combo "Cidade" para "<Cidade>"
    E alterar o campo "Complemento" para "AP 101"
    E clicar nos botoes "Salvar" e "<alterarTituralidade>"
    Entao sera exibida a mensagem em tela: "Dados Salvos com Sucesso!"
  @ARAGUAIA @21
    Exemplos:
      | unidade  | alterarTituralidade | Cidade |
      | ARAGUAIA | Ok                  |        |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | alterarTituralidade | Cidade |
      | CACHOEIRO DE ITAPEMIRIM |                     |        |

  @Cadastro_Cliente_CT028
  Esquema do Cenario: CT028 - Pesquisar cliente Pessoa Juridica - Consulta Completa - Campo "cnpj" e OPERADOR "Igual a" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "cnpj", em operador "Igual a" e em valor "[CNPJ SEM LIGACOES]"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT029
  Esquema do Cenario: CT029 - Pesquisar cliente Pessoa Fisica - Consulta Completa - Campo "cpf" e OPERADOR "Igual a" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "cpf", em operador "Igual a" e em valor "[CPF SEM LIGACOES]"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT030
  Esquema do Cenario: CT030 - Pesquisar cliente - Consulta Completa - Campo "Codigo" e OPERADOR "Menor que" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Codigo", em operador "Menor" e em valor "50"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_Cliente_CT031
  Esquema do Cenario: CT031 - Pesquisar cliente - Consulta Completa - Campo "bairro" e OPERADOR "Igual a" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "bairro", em operador "Igual a" e em valor "<bairro>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | bairro        |
      | ARAGUAIA | VILA PAULISTA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | bairro    |
      | CACHOEIRO DE ITAPEMIRIM | AEROPORTO |

  @Cadastro_Cliente_CT032
  Esquema do Cenario: CT032 - Pesquisar cliente - Consulta Completa - Com 3 criterio de Pesquisa - "E" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Codigo", em operador "Menor que" e em valor "50"
    E clicar no botao "E"
    E selecionar em campo "bairro", em operador "Igual a" e em valor "<bairro>"
    E clicar no botao "E"
    E selecionar em campo "Nome do Cliente", em operador "Contém" e em valor "<nome>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | bairro        | nome   |
      | ARAGUAIA | VILA PAULISTA | Thiago |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | bairro    | nome   |
      | CACHOEIRO DE ITAPEMIRIM | AEROPORTO | ABDALA |


  @Cadastro_Cliente_CT033
  Esquema do Cenario: CT033 - Pesquisar cliente - Consulta Completa - Com 3 criterio de Pesquisa - "OU" - <unidade>
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Cadastro de Cliente"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "Codigo", em operador "Menor que" e em valor "50"
    E clicar no botao "OU"
    E selecionar em campo "bairro", em operador "Igual a" e em valor "<bairro>"
    E clicar no botao "OU"
    E selecionar em campo "Nome do Cliente", em operador "Contém" e em valor "<nome>"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  | bairro        | nome   |
      | ARAGUAIA | VILA PAULISTA | Thiago |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | bairro    | nome   |
      | CACHOEIRO DE ITAPEMIRIM | AEROPORTO | ABDALA |