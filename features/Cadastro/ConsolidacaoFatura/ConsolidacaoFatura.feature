#language: pt
@Features @Cadastro @Cadastro_ConsolidacaoFatura
Funcionalidade: Consolidar Fatura no SAN

  #PARAMETROS ARAGUAIA
  @ARAGUAIA @21
  Cenario: PARAMETROS
    Dado a tela "Consolidacao de Faturas" esta conforme os seguintes parametros da entidade "PARAMETROSCADASTRO":
      | IDCIDADE | ENDNORMALIZADOCONSFATURA |
      | 76       | N                        |
      | 77       | N                        |
      | 78       | N                        |
      | 79       | N                        |
      | 80       | N                        |

  @Cadastro_ConsolidacaoFatura_CT001
  Esquema do Cenario: CT001 - Cadastrar Consolidacao de Faturas - Status Ativo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome               | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name 1 |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT002
  Esquema do Cenario: CT002 - Cadastrar Consolidacao de Faturas - Status Inativo
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome               | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name 2 |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT003
  Esquema do Cenario: CT003 - Cadastrar Consolidacao de Faturas - Status Ativo - COM CNPJ
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name | SIM  |     | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT004
  Esquema do Cenario: CT004 - Cadastrar Consolidacao de Faturas - Status Inativo - COM CNPJ
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name | SIM  |     | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT005
  Esquema do Cenario: CT005 - Cadastrar Consolidacao de Faturas - Status Ativo - COM CPF
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT006
  Esquema do Cenario: CT006 - Cadastrar Consolidacao de Faturas - Status Inativo - COM CPF
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT007
  Esquema do Cenario: CT007 - Cadastrar Consolidacao de Faturas - Status Ativo - COM CNPJ - Enviar SeFaz
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name | SIM  |     | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT008
  Esquema do Cenario: CT008 - Cadastrar Consolidacao de Faturas - Status Ativo - COM CPF - Enviar SeFaz
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT009
  Esquema do Cenario: CT009 - Cadastrar Consolidacao de Faturas - Status Ativo - Com 2 ligacoes
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT010
  Esquema do Cenario: CT010 - Cadastrar Consolidacao de Faturas - Status Ativo - Com 3 ligacoes
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              |             |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT011
  Esquema do Cenario: CT011 - Cadastrar Consolidacao de Faturas - Remover ligacao quando ouver mais de duas
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome                    | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | ANDERSON GOMES FERREIRA |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    E clicar com o botao direito do mouse e clicar em "Remover"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Atenção" sera: "Registro salvo com sucesso."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT012
  Esquema do Cenario: CT012 - Cadastrar Consolidacao de Faturas - Status Ativo - Efetuar Cadastro Sem Ligacao
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome                    | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | ANDERSON GOMES FERREIRA |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informa pelo menos uma ligação para consolidar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT013
  Esquema do Cenario: CT013 - Cadastrar Consolidacao de Faturas - Campo "[cidade]" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    E limpar a combo cidade na Consolidacao Fatura
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar no botao "Adicionar"
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe a cidade."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT014
  Esquema do Cenario: CT014 - Cadastrar Consolidacao de Faturas - Campo "Status" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o status da consolidação."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT015
  Esquema do Cenario: CT015 - Cadastrar Consolidacao de Faturas - Campo "Ligacao Responsavel" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informa a ligação responsável."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT016
  Esquema do Cenario: CT016 - Cadastrar Consolidacao de Faturas - Campo "Nome" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      |      |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o nome do responsável."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT017
  Esquema do Cenario: CT017 - Cadastrar Consolidacao de Faturas - Campo "Dia de vencimento" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               |                 | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o vencimento da consolidação."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT018
  Esquema do Cenario: CT018 - Cadastrar Consolidacao de Faturas - Campo "Ligacoes a consolidar" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | SIM | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informa pelo menos uma ligação para consolidar."
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT019
  Esquema do Cenario: CT019 - Cadastrar Consolidacao de Faturas - Campo "CEP" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | Sim | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     |     |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o CEP"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT020
  Esquema do Cenario: CT020 - Cadastrar Consolidacao de Faturas - Campo "Municipio" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | Sim | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA | BELA VISTA | -      | -    |           | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o Município"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT021
  Esquema do Cenario: CT021 - Cadastrar Consolidacao de Faturas - Campo "Bairro" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | Sim | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro             | numero | complemento | bairro | quadra | lote | municipio | estado | cep      |
      | AV. GUILHERMINA C/ VAZ | 0      | 111 B.VISTA |        | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o Bairro"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT022
  Esquema do Cenario: CT022 - Cadastrar Consolidacao de Faturas - Campo "Logradouro" nao preenchido
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    E clicar no botao "Novo"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Ativo"
    Entao buscar uma nova ligacao para Consolidacao Fatura
    E em "Ligação Responsável" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura
      | nome             | cnpj | cpf | inscricaoEstadual | diadeVencimento | enviarSefaz |
      | BRK Testing name |      | Sim | 000               | 10              | Sim         |
    E em Endereco preencher os seguintes campos
      | logradouro | numero | complemento | bairro     | quadra | lote | municipio | estado | cep      |
      |            | 0      | 111 B.VISTA | BELA VISTA | -      | -    | REDENÇÃO  | PA     | 68550000 |
    E em "Ligação" Pesquisar por "[ligacao]" em Consolidacao de Fatura
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "Informe o Endereço"
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT023
  Esquema do Cenario: CT023 - Pesquisar Consolidacao de Faturas - Consulta Simples - Campo "ID"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "ID" e preencher em valor "10"
    Entao o resultado da pesquisa sera exibido em tela


  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT024
  Esquema do Cenario: CT024 - Pesquisar Consolidacao de Faturas - Consulta Simples - Campo "NOMERESPONSAVEL"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "NOMERESPONSAVEL" e preencher em valor "<nomeResponsavel>"
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  | nomeResponsavel         |
      | ARAGUAIA | ANDERSON GOMES FERREIRA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 | nomeResponsavel |
      | CACHOEIRO DE ITAPEMIRIM | 1               |

  @Cadastro_ConsolidacaoFatura_CT025
  Esquema do Cenario: CT025 - Pesquisar Consolidação de Faturas - Consulta Completa - Campo "ATIVO" e OPERADOR "Igual a"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "ATIVO", em operador "Igual a" e em valor "S"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT026
  Esquema do Cenario: CT026 - Pesquisar Consolidação de Faturas - Consulta Completa - Campo "CNPJ" e OPERADOR "Diferente de"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "CNPJ", em operador "Diferente de" e em valor "S"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT027
  Esquema do Cenario: CT027 - Pesquisar Consolidação de Faturas - Consulta Completa - Caom 3 criterio de Pesquisa - "E"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "DATAINATIVACAO", em operador "Igual a" e em valor "24/01/2018"
    E clicar no botao "E"
    E selecionar em campo "DATAVENCIMENTO", em operador "Menor que" e em valor "xx/xx/xxxx"
    E clicar no botao "E"
    E selecionar em campo "IDLIGACAO", em operador "Diferente de" e em valor "120"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT028
  Esquema do Cenario: CT028 - Pesquisar Consolidação de Faturas - Consulta Completa - Com 3 criterio de Pesquisa - "OU"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "DATAINATIVACAO", em operador "Igual a" e em valor "24/01/2018"
    E clicar no botao "OU"
    E selecionar em campo "DATAVENCIMENTO", em operador "Menor que" e em valor "xx/xx/xxxx"
    E clicar no botao "OU"
    E selecionar em campo "IDLIGACAO", em operador "Diferente de" e em valor "120"
    E  clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

  @Cadastro_ConsolidacaoFatura_CT029
  Esquema do Cenario: CT029 - Sair da tela de Pesquisar Consolidacao de Faturas pelo botao "Sair"
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Cadastro" e "Consolidação de Faturas"
    Quando eu preencher em cidade "[Cidade Unidade]" e em status "Inativo"
    E clicar no botao "Sair"
    Entao a tela "Cadastro de Consolidação de Faturas" estara fechada

  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |