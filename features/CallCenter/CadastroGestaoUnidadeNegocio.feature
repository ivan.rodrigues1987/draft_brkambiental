#language: pt
@CallCenter @CallCenter_CadastroGestaoUnidadeNegocio
Funcionalidade: Validar funcionalidades Gestao Call Center Unidade Negocio

  @CallCenter_CadastroGestaoUnidadeNegocio_CT001
  Cenario: CT001 - Realizar o Cadastro Gestao CallCenter Por Unidade - Tipo de Atendimento por SOLICITACAO
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | SOLICITAÇÃO     | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "[Gestao Unidade Negocio cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao Unidade Negocio cadastrada anteriormente]" sera exibido em tela

  @CallCenter_CadastroGestaoUnidadeNegocio_CT002
  Cenario: CT002 - Realizar o Cadastro Gestao CallCenter Por Unidade - Tipo de Atendimento por INFORMACAO
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | INFORMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT003
  Cenario: CT003 - Realizar o Cadastro Gestao CallCenter Por Unidade - Tipo de Atendimento por RECLAMACAO
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT004
  Cenario: CT004 - Realizar o Cadastro Gestao CallCenter Por Unidade - Opcao "Nao Atendido pelo CallCenter"
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | S           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Técnico         | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT005
  Cenario: CT005 - Realizar o Cadastro Gestao CallCenter Por Unidade - Configuracao Emergenciais - Comercial
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Comercial       | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT006
  Cenario: CT006 - Realizar o Cadastro Gestao CallCenter Por Unidade - Configuracao Emergenciais - Comercial Tecnico
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento   | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Comercial Técnico | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT007
  Cenario: CT007 - Realizar o Cadastro Gestao CallCenter Por Unidade - Configuracao Emergenciais - Tecnico
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Técnico         | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT008
  Cenario: CT008 - Realizar o Cadastro Gestao CallCenter Por Unidade - Botao Adicionar - 1 Servico
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Técnico         | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar nos botoes "Adicionar" e "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT009
  Cenario: CT009 - Realizar o Cadastro Gestao CallCenter Por Unidade - Botao Adicionar - 2 Servicos
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Técnico         | 09:00:00               | 18:00:00            | 09:00:00                   | 13:00:00                |
    E clicar no botao "Adicionar"
    E adicionar o servico de informacao "5095"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT010
  Cenario: CT010 - Remover serviço informativo do Cadastro Gestao CallCenter Por Unidade - Botao Remover
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "Juan" e clicar no resultado
    E remover o servico de informacao "ENTREGA DE FATURA"
    E validar que o servico de informacao "ENTREGA DE FATURA" foi "REMOVIDO"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT011
  Cenario: CT011 - Adicionar serviço informativo a um Cadastro Gestao CallCenter Por Unidade - Botao Adicionar
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "Juan" e clicar no resultado
    E adicionar o servico de informacao "ENTREGA DE FATURA"
    E validar que o servico de informacao "ENTREGA DE FATURA" foi "ADICIONADO"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT012
  Cenario: CT012 - Alterar o Cadastro Gestao CallCenter Por Unidade
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "Juan" e clicar no resultado
    E alterar a combo "Tipo Atendimento" para "RECLAMAÇÃO"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"


  @CallCenter_CadastroGestaoUnidadeNegocio_CT013
  Cenario: CT013 - Realizar o Cadastro Gestao CallCenter Por Unidade - Campo ID do Servico Inicial nao informado
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO |                | INFORMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O campo ID do Serviço Inicial não foi informado!"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT014
  Cenario: CT014 - Realizar o Cadastro Gestao CallCenter Por Unidade - Tipo de Atendimento por SOLICITACAO
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | SOLICITAÇÃO     | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "[Gestao Unidade Negocio cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao Unidade Negocio cadastrada anteriormente]" sera exibido em tela

  @CallCenter_CadastroGestaoUnidadeNegocio_CT015
  Cenario: CT015 - Realizar o Cadastro Gestao CallCenter Por Unidade - Campo Tipo de Atendimento nao informado
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           |                 | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O campo Tipo de Atendimento não foi informado!"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT016
  Cenario: CT016 - CT016 -Realizar o Cadastro Gestao CallCenter Por Unidade - Configuracao Emergenciais - horarios do dia da semana nao preenchidos
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    Quando preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao              | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao Disponivel] | ARAGUAIA | REDENÇÃO | 6051           | RECLAMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E na aba Configuracao Emergenciais preencher os seguintes campos:
      | emergencia | tipoAtendimento | diaSemanaHorarioInicio | diaSemanaHorarioFim | finaldeSemanaHorarioInicio | finaldeSemanaHorarioFim |
      | S          | Comercial       |                        |                     | 09:00:00                   | 13:00:00                |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "É necessário preencher o horário do dia da semana."

  @CallCenter_CadastroGestaoUnidadeNegocio_CT017
  Cenario: CT017 - Cadastro de Gestao em Cadastro Gestao Call Center e utilizar o mesmo em Cadastro Gestao Call Center Unidade Negocio
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao      |
      | [Gestao] | [Nome Reduzido] | S     | Sem informações |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E fechar Tela
    E navegar pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao                            | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | [Gestao cadastrada anteriormente] | ARAGUAIA | REDENÇÃO | 6051           | INFORMAÇÃO      | N           | 5093              | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"


  @CallCenter_CadastroGestaoUnidadeNegocio_CT018
  Cenario: CT018 - Realizar o Cadastro Gestao CallCenter Por Unidade - Unidade com apenas uma cidade Validar que o campo cidade esta desabilitado
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao         | unidade                 | cidade | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade  | documentacao | observacoes |
      | Corte Indevido | CACHOEIRO DE ITAPEMIRIM |        | 564            | SOLICITAÇÃO     | N           | 89                | Req unidade | Doc          | Observacao  |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT019
  Cenario: CT019 - Associar o mesmo Cadastro Gestão Call Center a duas Unidades de Negocios diferentes
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao         | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade             | documentacao               | observacoes             |
      | Corte Indevido | ARAGUAIA | REDENÇÃO | 564            | SOLICITAÇÃO     | N           | 89                | Cadastro para prova 14 | Prova demo documentacao 14 | Nenhuma pelo Momento 14 |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso"
    E preencher os seguintes campos em  Cadastro Gestao Call Center Unidade Negocio:
      | gestao         | unidade  | cidade   | servicoInicial | tipoAtendimento | naoAtendido | servicoInformacao | reqUnidade             | documentacao               | observacoes             |
      | Corte Indevido | ARAGUAIA | REDENÇÃO | 564            | SOLICITAÇÃO     | N           | 89                | Cadastro para prova 14 | Prova demo documentacao 14 | Nenhuma pelo Momento 14 |
    E clicar no botao "Salvar"
    E sera exibida a mensagem em tela: "Salvo com Sucesso"

  @CallCenter_CadastroGestaoUnidadeNegocio_CT020
  Cenario: CT020 - Pesquisar Cadastro Gestão CallCenter Por Unidade - Campo id
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "44" e clicar no resultado
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroGestaoUnidadeNegocio_CT021
  Cenario: CT021 - Pesquisar Cadastro Gestão CallCenter Por Unidade - Campo Gestao CallCenter
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "Gestao CallCenter" e preencher em valor "Alteração de Dados Inquilino" e clicar no resultado
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroGestaoUnidadeNegocio_CT022
  Cenario: CT022 - Pesquisar Cadastro Gestao CallCenter Por Unidade - Criterio E
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Unidade Negócio"
    E clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "idunidadenegocio", em operador "Igual a" e em valor "21"
    E clicar no botao "E"
    E selecionar em campo "Gestao CallCenter", em operador "Contém" e em valor "Juan"
    E clicar no botao "E"
    E selecionar em campo "idunidadenegocio", em operador "Igual a" e em valor "ARAGUAIA"
    E  clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela
