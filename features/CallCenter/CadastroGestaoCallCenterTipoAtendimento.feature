#language: pt


@CallCenter @CallCenter_CadastroGestaoCallCenterTipoAtendimento
Funcionalidade: Validar funcionalidades Call Center Central de Atendimento

  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT001
  Cenario: CT001 - Atendimento ao Cliente, Abertura de Solicitacao no Call Center sem Ordem de Servico associada
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Acordo Dentro do Prazo" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Sem Serviços"
    Entao validar o protocolo gerado


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT002
  Cenario: CT002 - Atendimento ao Cliente, Abertura de OS no Call Center - Ligacao Recebida
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend       | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 002=PERSONALIZADO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Falta de Água Informada pelo Cliente" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT003
  Cenario: CT003 - Atendimento ao Cliente, Abertura de OS no Call Center - Chat
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Chat"
    E preencher os seguintes dados do solicitante
      | origemAtend   | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 003=DISO-PARÁ | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Reparo de Asfalto" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT004
  Cenario: CT004 - Atendimento ao Cliente, Abertura de OS no Call Center - E-mail
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 004=TACE    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Reparo de Asfalto" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT005
  Cenario: CT005 - Atendimento ao Cliente, Abertura de OS no Call Center - Fax
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Fax"
    E preencher os seguintes dados do solicitante
      | origemAtend         | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 005=COMERCIALIZAÇÃO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Verificação de Pressão" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT006
  Cenario: CT006 - Atendimento ao Cliente, Abertura de OS no Call Center - Ligacao Ativa
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Ativa"
    E preencher os seguintes dados do solicitante
      | origemAtend                              | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 006=CCC - CENTRAL DE CONTROLE DE CONSUMO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vazamento de Rede" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT007
  Cenario: CT007 - Atendimento ao Cliente, Abertura de OS no Call Center - Redes Sociais
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Redes Sociais"
    E preencher os seguintes dados do solicitante
      | origemAtend                            | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 007=RCP - REDUÇÃO E CONTROLE DE PERDAS | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Substituição de Registro" e a Prioridade "MEDIA"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT008
  Cenario: CT008 - Atendimento ao Cliente, Abertura de OS no Call Center - SMS
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "SMS"
    E preencher os seguintes dados do solicitante
      | origemAtend      | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 008=FALE CONOSCO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vazamento Ligação" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"

  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT009
  Cenario: CT009 - Atendimento ao Cliente, Abertura de OS no Call Center - Site/APP
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Site/APP"
    E preencher os seguintes dados do solicitante
      | origemAtend                    | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 009=USI - COBRANÇA/ARRECADAÇAO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vistoria para Vazamento Interno" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"

  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT010
  Cenario: CT010 - Atendimento ao Cliente, Abertura de OS no Call Center - Cliente com Debito
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO COM OS DEBITO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Site/APP"
    E preencher os seguintes dados do solicitante
      | origemAtend                  | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 010=USI CADASTRO/FATURAMENTO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vistoria para Vazamento Interno" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT011
  Cenario: CT011 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Ligacão Recebida
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend      | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 011=SIPSAP-MOVEL | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Explicação do valor da Conta" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E clicar no botao "Ok" em Central de Atendimento
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT012
  Cenario: CT012 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Chat
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Chat"
    E preencher os seguintes dados do solicitante
      | origemAtend          | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 012=USI FISCALIZAÇÃO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Alteração Cadastral da Conta" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT013
  Cenario: CT013 - Atendimento ao Cliente, Abertura de OS no Call Center  - OS de Encerramento automático - E-mail
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend         | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 013=USI HIDROMETRIA | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Ligação de Água / Ligação de Esgoto" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT014
  Cenario: CT014 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Fax
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Fax"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Débito Automático" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT015
  Cenario: CT015 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Ligacão Ativa
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Ativa"
    E preencher os seguintes dados do solicitante
      | origemAtend        | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 015=CHAT (ON-LINE) | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Supressão da ligação de água" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT016
  Cenario: CT016 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Redes Sociais
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Redes Sociais"
    E preencher os seguintes dados do solicitante
      | origemAtend                              | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 006=CCC - CENTRAL DE CONTROLE DE CONSUMO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informações Gerais" e a Prioridade "MEDIA"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT017
  Cenario: CT017 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - SMS
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "SMS"
    E preencher os seguintes dados do solicitante
      | origemAtend                            | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 007=RCP - REDUÇÃO E CONTROLE DE PERDAS | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Parcelamento de debitos" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT018
  Cenario: CT018 - Atendimento ao Cliente, Abertura de OS no Call Center - OS de Encerramento automático - Site/APP
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Site/APP"
    E preencher os seguintes dados do solicitante
      | origemAtend      | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 008=FALE CONOSCO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Informação de Suspensão de Corte" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E em Central de Atendimento clicar no botao "Ok"
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT019
  Cenario: CT019 - Atendimento ao Cliente, Abertura de OS no Call Center - Por Protocolo -  Protocolo de Atendimento sem Servico
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[PROTOCOLO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend                    | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 009=USI - COBRANÇA/ARRECADAÇAO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[PROTOCOLO PREENCHIDO]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Sem Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "[PROTOCOLO PREENCHIDO]"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT020
  Cenario: CT020 - Atendimento ao Cliente, Abertura de OS no Call Center - Por Protocolo - Ligacão Recebida
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Falta de Água Informada pelo Cliente" e a Prioridade "MEDIA"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT021
  Cenario: CT021 - Atendimento ao Cliente, Abertura de OS no Call Center - Por Protocolo - Chat
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Chat"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vazamento de Rede" e a Prioridade "MEDIA"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT022
  Cenario: CT022 -Atendimento ao Cliente, Abertura de OS no Call Center - Por Protocolo - E-mail
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Reparo de Asfalto" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT023
  Cenario: CT023 -Atendimento ao Cliente, Abertura de OS no Call Center - Por Protocolo - Fax
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Fax"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Verificação de Pressão" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT024
  Cenario: CT024 -Atendimento ao Cliente, Abertura de OS no Call Center - Ligacao indefinida
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e selecionar ENTER
    E preencher os seguintes campos de filtro da tela de Pesquisa de Ligacao
      | cidade   | Logradouro | nrImovel | Complemento | cep |
      | REDENÇÃO | AV. BRASIL | 8        |             |     |
    E em pesquisar por marcar a opcao "Logradouro"
    E em Central de Atendimento clicar no botao "Pesquisar"
    E selecionar um dos logradouros retornado na pesquisa e confirmar
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Fax"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vazamento de Rede" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar" e salvar
    Entao deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT025
  Cenario: CT025 -Atendimento ao Cliente - Ligacao indefinida - Solicitacao sem servico
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e selecionar ENTER
    E preencher os seguintes campos de filtro da tela de Pesquisa de Ligacao
      | cidade   | Logradouro | nrImovel | Complemento | cep |
      | REDENÇÃO | AV. BRASIL | 8        |             |     |
    E em pesquisar por marcar a opcao "Logradouro"
    E em Central de Atendimento clicar no botao "Pesquisar"
    E selecionar um dos logradouros retornado na pesquisa e confirmar
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Central de Atendimento clicar no botao "Salvar"
    Entao deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT026
  Cenario: CT026 -Atendimento ao Cliente - Ligacao indefinida - Nao clicar no botao Iniciar Atendimento
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e selecionar ENTER
    E preencher os seguintes campos de filtro da tela de Pesquisa de Ligacao
      | cidade   | Logradouro | nrImovel | Complemento | cep |
      | REDENÇÃO | AV. BRASIL | 8        |             |     |
    E em pesquisar por marcar a opcao "Logradouro"
    E em Central de Atendimento clicar no botao "Pesquisar"
    E selecionar um dos logradouros retornado na pesquisa e confirmar
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend     | solicitante   | identidade   | cpf   | tel   | referencia   |
      | CLIENTE INTERNO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vazamento de Rede" e a Prioridade "URGENTE"
    E em "Atendimento" inserir a observacao ""
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao ""
    E em Central de Atendimento clicar no botao "Salvar"
    Entao a janela de mensagem callcenter "Mensagem do Sistema" sera: "Ordem de serviço não pode ser salva, verifique: É necessário informar um serviço ou iniciar um protocolo para se abrir uma Ordem de Serviço Favor clicar no botão Iniciar Atendimento para a efetivar o cadastro da ordem de serviço"

  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT027
  Cenario: CT027 -Atendimento ao Cliente, Abertura de OS no Call Center - Forma de antendimento nao informada
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento ""
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Acordo Dentro do Prazo" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao ""
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao ""
    E em Central de Atendimento clicar no botao "Adicionar"
    E em Central de Atendimento clicar no botao "Salvar"
    Entao a janela de mensagem callcenter "Mensagem do Sistema" sera: "Ordem de serviço não pode ser salva, verifique: Informe a forma de antendimento."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT028
  Cenario: CT028 -Atendimento ao Cliente, Abertura de OS no Call Center - Dados do Solicitante nao informado
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "E-mail"
    E preencher os seguintes dados do solicitante
      | origemAtend   | solicitante | identidade   | cpf   | tel   | referencia   |
      | 003=DISO-PARÁ |             | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Acordo Dentro do Prazo" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao ""
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao ""
    E em Central de Atendimento clicar no botao "Adicionar"
    E em Central de Atendimento clicar no botao "Salvar"
    Entao a janela de mensagem callcenter "Mensagem do Sistema" sera: "Ordem de serviço não pode ser salva, verifique: Informe o nome do solicitante."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT029
  Cenario: CT029 -Atendimento ao Cliente, Abertura de OS no Call Center - Servico Standard nao informado
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Chat"
    E em Servicos, selecionar o Servico Standard "" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao ""
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao ""
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao a janela de mensagem callcenter "Mensagem do Sistema" sera: "Selecione um serviço."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT030
  Cenario: CT030 -Atendimento ao Cliente, Abertura de OS no Call Center - Alteracao do servico antes de Salvar
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Acordo Dentro do Prazo" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E em Central de Atendimento clicar no botao "Alterar"
    E em "Atendimento" inserir a observacao "Atendimento Alterado"
    E em "Comercial" inserir a observacao "Comercial Alterado"
    E em "Técnica" inserir a observacao "Técnica Alterada"
    Entao salvar e deve ser apresentado a tela de Encerramento Automatico
    E clicar no botao "Ok" em Central de Atendimento
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Encerrada"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT031
  Cenario: CT031 -Atendimento ao Cliente, Abertura de OS no Call Center - Cliente com Debito e nao continuar
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Site/APP"
    E preencher os seguintes dados do solicitante
      | origemAtend                  | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 010=USI CADASTRO/FATURAMENTO | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Vistoria para Vazamento Interno" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    Entao clicar no botao "Não" em Central de Atendimento


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT032
  Cenario: CT032 -Atendimento ao Cliente, Abertura de OS no Call Center - CPF Invalido
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E em Dados Solicitante inserir o CPF "88888888888"
    Entao a janela de mensagem callcenter sera: "CPF inválido."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT033
  Cenario: CT033 -Atendimento ao Cliente - Ligacao indefinida - Nao selecionar o Logradouro
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e selecionar ENTER
    E preencher os seguintes campos de filtro da tela de Pesquisa de Ligacao
      | cidade   | Logradouro | nrImovel | Complemento | cep |
      | REDENÇÃO | AV. BRASIL | 8        |             |     |
    E em pesquisar por marcar a opcao "Logradouro"
    E em Central de Atendimento clicar no botao "Pesquisar"
    E em Central de Atendimento clicar no botao "Confirmar"
    Entao a janela de mensagem callcenter "Mensagem do Sistema" sera: "Selecione um logradouro."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT034
  Cenario: CT034 - Atendimento ao Cliente, Abertura de OS no Call Center - Ordens de Servico duplicadas
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Substituição de Registro" e a Prioridade "RISCO DE IMAGEM"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    E validar que a solicitacao esta sendo apresentada na situacao "Aberta"
    E clicar no botao "Iniciar Atendimento"
    E clicar em "Localização" em Gestao CallCenterTipoAtendimento
    E em Dados Solicitante selecionar a Forma de Atendimento "Site/APP"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Substituição de Registro" e a Prioridade "RISCO DE IMAGEM"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Central de Atendimento clicar no botao "Salvar"
    Entao a janela de mensagem callcenter "Ordem Serviço - Duplicadas" sera: "Já existe a mesma solicitação aberta para o cliente."


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT035
  Cenario: CT035 - Atendimento ao Cliente, Abertura de OS no Call Center - Dois Servicos no mesmo Protocolo
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO SEM OS ABERTA]" em ordem de servico e teclar ENTER
    E clicar no botao "Iniciar Atendimento"
    E em Dados Solicitante selecionar a Forma de Atendimento "Ligação Recebida"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Substituição de Registro" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Adicionar"
    E clicar no botao "Sim" em Central de Atendimento
    E em Servicos, selecionar o Servico Standard "Falta de Água Informada pelo Cliente" e a Prioridade "MEDIA"
    E em "Atendimento" inserir a observacao ""
    E em "Comercial" inserir a observacao ""
    E em "Técnica" inserir a observacao "Falta de água"
    E em Central de Atendimento clicar no botao "Adicionar"
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Com Serviços"
    Entao validar que a solicitacao esta sendo apresentada na situacao "Aberta"


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT036
  Cenario: CT036 - Validar dados do tooltip de informacoes da cidade estao carregadas conforme cidade da Ligacao selecionada
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E validar que a cidade da ligacao e igual a cidade exibida em Central de Atendimento
    E em Central de Atendimento clicar no icone "Localização"
    Entao no tooltip "Localização" deve ser exibido a informacao desejada


  @CallCenter_CadastroGestaoCallCenterTipoAtendimento_CT037
  Cenario: CT037 - Validar dados do tooltip de informacoes da cidade estao carregadas conforme cidade da Ligacao selecionada
    Dado Eu acesse a unidade "CALLCENTER-ARAGUAIA" e navegue pelo menu "Call Center" e "Call Center"
    Quando preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO]" em ordem de servico e teclar ENTER
    E validar que a cidade da ligacao e igual a cidade exibida em Central de Atendimento
    E em Central de Atendimento clicar no icone "Feriado"
    Entao no tooltip "Feriado" deve ser exibido a informacao desejada


