#language: pt
@CallCenter @CallCenter_CadastroGestao
Funcionalidade: Validar o Cadastro Gestao Call Center

  @CallCenter_CadastroGestao_CT001
  Cenario: CT001 - Realizar o cadastro de uma Gestao de CallCenter - Todos os campos
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao      |
      | [Gestao] | [Nome Reduzido] | S     | Sem informações |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "gestao" e preencher em valor "[Gestao cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao cadastrada anteriormente]" sera exibido em tela

  @CallCenter_CadastroGestao_CT002
  Cenario: CT002 - Realizar o cadastro de uma Gestao de CallCenter - ATIVO = nao
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao      |
      | [Gestao] | [Nome Reduzido] | N     | Sem informações |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "gestao" e preencher em valor "[Gestao cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao cadastrada anteriormente]" sera exibido em tela

  @CallCenter_CadastroGestao_CT003
  Cenario: CT003 - Realizar o cadastro de uma Gestao de CallCenter - Sem preencher o campo INFORMACAO
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao |
      | [Gestao] | [Nome Reduzido] | S     |            |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "gestao" e preencher em valor "[Gestao cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao cadastrada anteriormente]" sera exibido em tela

  @CallCenter_CadastroGestao_CT004
  Cenario: CT004 - Realizar o cadastro de uma Gestao de CallCenter - Campo GESTAO nao informado
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao | nomeReduzido    | ativo | informacao |
      |        | [Nome Reduzido] | S     |            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O campo Gestão não foi informado!"

  @CallCenter_CadastroGestao_CT005
  Cenario: CT005 - Realizar o cadastro de uma Gestao de CallCenter - Campo GESTAO nao informado
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido | ativo | informacao |
      | [Gestao] |              | S     |            |
    E clicar no botao "Salvar"
    Entao a janela de mensagem "Mensagem do Sistema" sera: "O campo Nome Reduzido não foi informado!"

  @CallCenter_CadastroGestao_CT006
  Cenario: CT006 - Alterar o cadastro de uma Gestao de CallCenter - Todos os campos
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "264"
    E selecionar no resultado da pesquisa "264" e clicar em "OK"
    E preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao |
      | [Gestao] | [Nome Reduzido] | S     | Nova inf   |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @CallCenter_CadastroGestao_CT007
  Cenario: CT007 - Alterar o Nome Reduzido de uma Gestao de CallCenter
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "264"
    E selecionar no resultado da pesquisa "264" e clicar em "OK"
    E alterar o campo "Nome Reduzido" para "Nome reduzido novo"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @CallCenter_CadastroGestao_CT008
  Cenario: CT008 - Alterar uma Gestao de CallCenter checkbox Ativo
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "264"
    E selecionar no resultado da pesquisa "264" e clicar em "OK"
    E clicar no checkbox "Ativo"
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @CallCenter_CadastroGestao_CT009
  Cenario: CT009 - Pesquisar Gestao de CallCenter - Consulta Simples - Campo ID
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "260"
    Entao o resultado da pesquisa "260" sera exibido em tela

  @CallCenter_CadastroGestao_CT010
  Cenario: CT010 - Pesquisar Gestao de CallCenter - Consulta Simples - Campo informacao
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "informacao" e preencher em valor "informações"
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroGestao_CT011
  Cenario: CT011 - Pesquisar Gestao de CallCenter - Consulta Completa - Criterio E
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "gestao", em operador "Contém" e em valor "Religacao"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroGestao_CT012
  Cenario: CT012 - Pesquisar Gestao de CallCenter - Consulta Completa - Com 3 criterios de Pesquisa E
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "gestao", em operador "Contém" e em valor "Religacao"
    E clicar no botao "E"
    E selecionar em campo "informacao", em operador "Contém" e em valor "informações"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Maior que" e em valor "0"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroGestao_CT013
  Cenario: CT013 - Pesquisar Gestao de CallCenter - Consulta Completa - Com 3 criterios de Pesquisa OU
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "gestao", em operador "Contém" e em valor "Religacao"
    E clicar no botao "OU"
    E selecionar em campo "informacao", em operador "Contém" e em valor "informações"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Menor que" e em valor "10"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela



