#language: pt
@CallCenter @CallCenter_CadastroTipoAtendimento
Funcionalidade: Validar o Cadastro de Tipo de Atendimento

  @CallCenter_CadastroTipoAtendimento_CT001
  Cenario: CT001 - Cadastro de Tipo de Atendimento
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando preencher o campo "Tipo Atendimento" com "[Tipo Atendimento]" em Cadastro Tipo Atendimento
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @CallCenter_CadastroTipoAtendimento_CT002
  Cenario: CT002 - Pesquisar Consulta Simples - Campo Tipo de Atendimento
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "tipoatendimento" e preencher em valor "Tipo Atendimento"
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT003
  Cenario: CT003 - Pesquisar Consulta Simples - Campo ID
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "id" e preencher em valor "12"
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT004
  Cenario: CT004 - Pesquisa Completa  - Campo "ID" - Operador "E"
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "id", em operador "Igual a" e em valor "12"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT005
  Cenario: CT005 - Pesquisa Completa  -Campo "tipoatendimento" - Operador "OU"
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "tipoatendimento", em operador "Contém" e em valor "Tipo Atendimento"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT006
  Cenario: CT006 - Pesquisar Gestao de CallCenter Tipo de Atendimento - Consulta Completa - Com 3 critério de Pesquisa - "E"
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "tipoatendimento", em operador "Contém" e em valor "Tipo Atendimento"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Igual a" e em valor "12"
    E clicar no botao "E"
    E selecionar em campo "id", em operador "Maior que" e em valor "0"
    E clicar nos botoes "E" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT007
  Cenario: CT007 - Pesquisar Gestao de CallCenter Tipo de Atendimento - Consulta Completa - Com 3 critério de Pesquisa - "OU"
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E na aba "Consulta Completa" selecionar em campo "tipoatendimento", em operador "Contém" e em valor "Tipo Atendimento"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Igual a" e em valor "12"
    E clicar no botao "OU"
    E selecionar em campo "id", em operador "Igual a" e em valor "0"
    E clicar nos botoes "OU" e pesquisar
    Entao o resultado da pesquisa sera exibido em tela

  @CallCenter_CadastroTipoAtendimento_CT008
  Cenario: CT008 - Alterar registro do Tipo de Atendimento
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "tipoatendimento" e preencher em valor "Tipo Atendimento"
    E selecionar no resultado da pesquisa "12" e clicar em "OK"
    E preencher o campo "Tipo Atendimento" com "[Tipo Atendimento]" em Cadastro Tipo Atendimento
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"

  @CallCenter_CadastroTipoAtendimento_CT009
  Cenario: CT009 - Sair da tela Cadastro Gestao Call Center Tipo de Atendimento
    Dado Eu acesse a unidade "CALLCENTER" e navegue pelo menu "Cadastro" e "Cadastro Gestão Call Center Tipo Atendimento"
    Quando clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "tipoatendimento" e preencher em valor "Tipo Atendimento"
    E selecionar no resultado da pesquisa "12" e clicar em "OK"
    E fechar Tela
    Entao a tela "Cadastro Gestão Call Center Tipo Atendimento" estara fechada
