#language: pt
@Features @Atendimento @Atendimento_CadastroGestao
Funcionalidade: Validar o Cadastro Gestao Call Center

  @Atendimento_CadastroGestao_CT001
  Esquema do Cenário: CT001 - Realizar o cadastro de uma Gestao de CallCenter - Todos os campos
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Atendimento" e "Central de Atendimento"
    Quando preencher os seguintes campos em Cadastro Gestao Call Center:
      | gestao   | nomeReduzido    | ativo | informacao      |
      | [Gestao] | [Nome Reduzido] | S     | Sem informações |
    E clicar no botao "Salvar"
    Entao sera exibida a mensagem em tela: "Salvo com Sucesso!"
    E clicar no botao "Pesquisa Rapida"
    E em pesquisa rapida selecionar em campo "gestao" e preencher em valor "[Gestao cadastrada anteriormente]"
    Entao o resultado da pesquisa "[Gestao cadastrada anteriormente]" sera exibido em tela
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |
