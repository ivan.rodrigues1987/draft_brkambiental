#language: pt
@Atendimento @Atendimento_CadastroGestaoTipoAtendimento
Funcionalidade: Validar funcionalidades  Central de Atendimento

  @Atendimento_CadastroGestaoTipoAtendimento_CT001
  Esquema do Cenário: CT001 - Atendimento ao Cliente, Abertura de Solicitacao no Call Center sem Ordem de Servico associada
    Dado Eu acesse a unidade "<unidade>" e navegue pelo menu "Atendimento" e "Central de Atendimento"
    Quando Informar a ligacao "[LIGAÇÃO]" em ordem de servico e apertar ENTER
    E na aba "Solicitações" acessar o item "Ordem de Serviço"
    E preencher os seguintes dados do solicitante
      | origemAtend | solicitante   | identidade   | cpf   | tel   | referencia   |
      | 001=0800    | [Solicitante] | [Identidade] | [CPF] | [Tel] | [Referencia] |
    E em Servicos, selecionar o Servico Standard "Acordo Dentro do Prazo" e a Prioridade "NORMAL"
    E em "Atendimento" inserir a observacao "Sem observações de atendimento"
    E em "Comercial" inserir a observacao "Sem observação comercial"
    E em "Técnica" inserir a observacao "Sem observação técnica"
    E em Central de Atendimento clicar no botao "Salvar"
    E deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao
    E fechar a janela de Emissao de Protocolo
    E preencher o campo Unidade com "ARAGUAIA" e informar a ligacao "[LIGAÇÃO PREENCHIDA]" em ordem de servico e teclar ENTER
    E clicar em "Protocolos" em Gestao CallCenterTipoAtendimento
    E selecionar a Aba "Protocolos de Atendimento Sem Serviços"
    Entao validar o protocolo gerado
  @ARAGUAIA @21
    Exemplos:
      | unidade  |
      | ARAGUAIA |
  @CACHOEIRODEITAPEMIRIM @2
    Exemplos:
      | unidade                 |
      | CACHOEIRO DE ITAPEMIRIM |

