package framework;

import static framework.BrowserManager.BrowsersManager.closeDriver;
import static framework.ConfigFramework.getDriver;

/**
 * @author Renan Ferreira dos Santos
 * @since 22/02/2019
 */
public class Errors {
    /**
     * <b>Metodo para exibir erro no setup de alguma configuracao, finaliza a execucao</b>
     *
     * @param message <i>mensagem</i>
     */
    public static void setUpError(String message) {
        closeDriver(getDriver());
        System.err.println("\n[SETUP ERROR] " + message);
        System.exit(0);
    }

    /**
     * <b>Metodo para exibir alerta no setup de alguma configuracao</b>
     *
     * @param message <i>mensagem</i>
     */
    public static void setUpWarning(String message) {
        System.out.println("\n[SETUP WARNING] " + message);
    }
}
