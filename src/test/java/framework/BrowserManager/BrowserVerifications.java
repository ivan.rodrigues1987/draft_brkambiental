package framework.BrowserManager;

import main.WebElementManager.Combos.ComboInputText;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.setTextPerChar;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserActions.setText;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.ConfigFramework.getDriver;
import static framework.ConfigFramework.getActions;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.Utils.isBlank;
import static main.WebElementManager.Combos.Combos.getXPathCombo;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;

/**
 * <b>Classe para verificacoes no navegador</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public abstract class BrowserVerifications {

    /**
     * <b>Pausa a execucao da aplicacao e aguarda em segundos</b>
     *
     * @param seconds <i>segundos</i>
     */
    public static void wait(int seconds) {
        final long conversaoSegundos = 1000;
        try {
            Thread.sleep((long) seconds * conversaoSegundos);
        } catch (InterruptedException var2) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * <b>Pausa a execucao da aplicacao e aguarda em centesimos</b>
     *
     * @param hundredth <i>centesimos</i>
     */
    public static void wait(double hundredth) {
        final long conversaoSegundos = 10;
        try {
            Thread.sleep((long) hundredth * conversaoSegundos);
        } catch (InterruptedException var2) {
            Thread.currentThread().interrupt();
        }
    }


    /**
     * <b>Aguardo o tamanho do elemento existir</b>
     *
     * @param by           <i>xPath elemento</i>
     * @param expectedSize <i>quantidade de elementos com o caminho de xPath</i>
     * @param seconds      <i>segundos</i>
     */
    public static void waitElementSize(By by, int expectedSize, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return getDriver().findElements(by).size() == expectedSize;
            }
        });
    }

    /**
     * <b>Aguardo o elemento nao existir</b>
     *
     * @param by      <i>xPath elemento</i>
     * @param seconds <i>segundos</i>
     */
    public static void waitElementNotExists(By by, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return !elementExists(by);
            }
        });
    }

    /**
     * <b>Aguardo o elemento existir</b>
     *
     * @param by      <i>xPath elemento</i>
     * @param seconds <i>segundos</i>
     */
    public static void waitElementExists(By by, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return elementExists(by);
            }
        });
    }

    /**
     * <b>Aguardo o elemento ficar visivel</b>
     *
     * @param by      <i>xPath elemento</i>
     * @param seconds <i>segundos</i>
     */
    public static void waitElementToBeVisible(By by, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    /**
     * <b>Aguardo o elemento ser clicavel</b>
     *
     * @param by      <i>xPath elemento</i>
     * @param seconds <i>segundos</i>
     */
    public static void waitElementToBeClickable(By by, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     * <b>Aguarda o elemento ficar habilitado na pagina</b>
     *
     * @param nomeCampo <i>xPath inputtext combo</i>
     * @param seconds   <i>segundos</i>
     */
    public static void waitElementToBeEnable(String nomeCampo, int seconds) {
        waitElementToBeEnable(nomeCampo, 1, seconds);
    }

    /**
     * <b>Aguarda o elemento ficar habilitado na pagina</b>
     *
     * @param nomeCampo <i>xPath inputtext combo</i>
     * @param seconds   <i>segundos</i>
     */
    public static void waitElementToBeEnableBotaoCall(String nomeCampo, int seconds) {
        waitElementToBeEnableBotaoCall(nomeCampo, 1, seconds);
    }


    /**
     * <b>Aguarda o elemento ficar habilitado na pagina</b>
     *
     * @param nomeCampo <i>xPath inputtext combo</i>
     * @param indice    <i>indice do campo</i>
     * @param seconds   <i>segundos</i>
     */
    public static void waitElementToBeEnable(String nomeCampo, int indice, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                By xPath = By.xpath("(//label[text()='" + nomeCampo + ":'])[" + indice + "]/following-sibling::" +
                        "div/div[@class='GKPSPJYCOJ']");
                //Para o elemento estar habilitado o xPath com a classe GKPSPJYCOJ nao deve existir
                return !elementExists(xPath);
            }
        });
    }


    /**
     * <b>Aguarda o botao ficar habilitado na pagina de CALLCENTER</b>
     *
     * @param nomeCampo <i>xPath inputtext combo</i>
     * @param indice    <i>indice do campo</i>
     * @param seconds   <i>segundos</i>
     */
    public static void waitElementToBeEnableBotaoCall(String nomeCampo, int indice, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                By xPath = By.xpath("(//div[text()='" + nomeCampo + ":'])[" + indice + "]");
                //Para o elemento estar habilitado o xPath com a classe GKPSPJYCOJ nao deve existir
                return !elementExists(xPath);
            }
        });
    }



    /**
     * <b>Aguarda um valor dentro do elemento</b>
     *
     * @param element       <i>elemento</i>
     * @param valorEsperado <i>valor esperado</i>
     * @param seconds       <i>segundos</i>
     */
    public static void waitElementValue(String element, String valorEsperado, int seconds) {
        waitElementToBeVisible(getXPathElementTextEquals(element, valorEsperado), seconds);
    }

    /**
     * <b>Aguarda um valor dentro do elemento ser igual ao valor esperado</b>
     *
     * @param by            <i>caminho do elemento</i>
     * @param valorEsperado <i>valor esperado</i>
     * @param seconds       <i>segundos</i>
     */
    public static void waitElementValueEquals(By by, String valorEsperado, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return getText(by).trim().equals(valorEsperado);
            }
        });
    }

    /**
     * <b>Aguarda um valor dentro atributo do elemento</b>
     *
     * @param by            <i>xPath elemento</i>
     * @param attribute     <i>atributo do elemento</i>
     * @param valorEsperado <i>Valor esperado no elemento</i>
     * @param seconds       <i>Tempo de Espera em segundos</i>
     */
    public static void waitElementAttributeValue(By by, String attribute, String valorEsperado, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(ExpectedConditions.attributeContains(by, attribute, valorEsperado));
    }


    /**
     * <b>Aguarda um valor dentro do campo, caso nao esteja tenta uma nova insercao um caracter por vez</b>
     *
     * @param by            <i>xPath elemento</i>
     * @param expectedText  <i>Valor esperado no elemento</i>
     * @param seconds       <i>Tempo de Espera em segundos</i>
     */
    public static void waitPersistentTextSetPerChar(By by, String expectedText, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                getActions().click(getElement(by));

                for (int i = 0; i < expectedText.length(); i++) {
                    sendKeys(Keys.BACK_SPACE);
                }
                setTextPerChar(by, expectedText);

                return getTextAttribute(by, "value").trim().equals(expectedText);
            }
        });
    } //waitPersistentTextSet

    /**
     * <b>Aguarda um valor dentro do campo, caso nao esteja tenta uma nova insercao</b>
     *
     * @param by            <i>xPath elemento</i>
     * @param expectedText  <i>Valor esperado no elemento</i>
     * @param seconds       <i>Tempo de Espera em segundos</i>
     */
    public static void waitPersistentTextSet(By by, String expectedText, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                getActions().click(getElement(by));

                for (int i = 0; i < expectedText.length(); i++) {
                    sendKeys(Keys.BACK_SPACE);
                }
                setText(by, expectedText);

                return getTextAttribute(by, "value").trim().equals(expectedText);
            }
        });
    }

    /**
     * <b>Aguarda um valor dentro do campo, caso nao esteja tenta uma nova insercao</b>
     *
     * @param combo            <i>nome da label desse elemento</i>
     * @param valor            <i>Valor a ser inserido no elemento</i>
     * @param clearField       <i>informa se o campo precisa ser limpo previamente</i>
     * @param seconds          <i>Tempo de Espera em segundos</i>
     */
    public static void setPersistenteComboClicando(String combo, String valor, boolean clearField, int seconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                if (isBlank(valor) && clearField) {
                    clearField(ComboInputText.getXPathTextoCombo(combo));
                    sendKeys(Keys.ENTER);
                } else {
                    setComboClicando(combo, valor);
                }
                clickOnElement(getXPathCombo(combo));
                return getTextAttribute(getXPathCombo(combo), "value").trim().equals(valor);
            }
        });
    }



    /**
     * <b>Destaca um elemento web na tela</b>
     *
     * @param by <i>caminho do elemento web</i>
     */
    public static void highLightElement(By by) {
        WebElement element = getDriver().findElement(by);

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].style.border='2px dashed red'",
                new Object[]{element});
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].style.border='1,5'", new Object[]{element});
        wait(1);
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].style.border=''", new Object[]{element});
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].style.border=''", new Object[]{element});
        System.err.println("ATENÇÃO! O HIGHLIGHT ELEMENT DEVE SER USADO APENAS EM DEV MODE PARA " +
                "FACILITAR A LOCALIZACAO DOS ELEMENTOS");
    }

    /**
     * <b>Retorna o caminho do elemento clicavel</b>
     *
     * @param by <i>caminho dos elementos web</i>
     * @return WebElement - <i>elemento</i>
     */
    public static WebElement getClickableElement(By by) {
        return (new WebDriverWait(getDriver(), (long) DEFAULT_TIME_OUT)).until(
                ExpectedConditions.elementToBeClickable(by));
    }

    /**
     * <b>Retorna os elementos web em determinado caminho</b>
     *
     * @param by <i>caminho dos elementos web</i>
     * @return List WebElement - <i>lista de elementos</i>
     */
    public static List<WebElement> getElements(By by) {
        return (new WebDriverWait(getDriver(), (long) DEFAULT_TIME_OUT)).until(
                ExpectedConditions.numberOfElementsToBeMoreThan(by, 0));
    }

    /**
     * <b>Retorna os elementos web em determinado caminho</b>
     *
     * @param by               <i>caminho dos elementos web</i>
     * @param numberOfElements <i>numero de elementos</i>
     * @return List WebElement - <i>lista de elementos</i>
     */
    public static List<WebElement> getElements(By by, int numberOfElements) {
        return (new WebDriverWait(getDriver(), (long) DEFAULT_TIME_OUT)).until(
                ExpectedConditions.numberOfElementsToBeMoreThan(by, numberOfElements));
    }

    /**
     * <b>Retorna o elemento web em determinado caminho</b>
     *
     * @param by <i>caminho dos elementos web</i>
     * @return WebElement - <i>elementos</i>
     */
    public static WebElement getElement(By by) {
        return getElements(by).get(0);
    }

    /**
     * <b>Verifica se o elemento web existe na pagina</b>
     *
     * @param by <i>caminho do elemento web</i>
     * @return boolean - <i>true se o elemento existir</i>
     */
    public static boolean elementExists(By by) {
        return getDriver().findElements(by).size() != 0;
    }

    /**
     * <b>Verifica se o elemento web esta exibido na pagina</b>
     *
     * @param by <i>caminho do elemento web</i>
     * @return boolean - <i>true se o elemento estiver exibido</i>
     */
    public static boolean isElementDisplayed(By by) {
        return getDriver().findElement(by).isDisplayed();
    }

    /**
     * <b>Verifica se o elemento web esta habilitado na pagina</b>
     *
     * @param by <i>caminho do elemento web</i>
     * @return boolean - <i>true se o elemento estiver exibido</i>
     */
    public static boolean isElementEnable(By by) {
        return getElement(by).isEnabled();
    }
}
