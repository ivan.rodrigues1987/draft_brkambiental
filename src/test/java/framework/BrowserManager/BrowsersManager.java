package framework.BrowserManager;

//import framework.FileManager.PropertiesManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import static runner.Setup.Json.ParamsInfo.PARAMETROS_EXECUCAO;
import static framework.ConfigFramework.FRAMEWORK_NAME;

/**
 * <b>Classe abstrata para gerenciar o navegador</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public abstract class BrowsersManager {
    //protected PropertiesManager setupProperties;

    /**
     * <b>Carrega a aplicacao</b>
     *
     * @param driver <i>webdriver do navegador</i>
     * @param url    <i>url a ser acessada</i>
     */
    public static void loadApplication(@NotNull WebDriver driver, String url) {
        driver.get(url);
        waitPageLoad(driver, PARAMETROS_EXECUCAO.getPageLoadTimeOut());
    }

    /**
     * <b>Deleta os cookies do browser</b>
     *
     * @param driver <i>webdriver do navegador</i>
     */
    public static void deleteCookies(@NotNull WebDriver driver) {
        driver.manage().deleteAllCookies();
    }

    /**
     * <b>Aguarda em segundos a pagina carregar</b>
     *
     * @param driver  <i>webdriver do navegador</i>
     * @param seconds <i>segundos</i>
     */
    private static void waitPageLoad(@NotNull WebDriver driver, int seconds) {
        driver.manage().timeouts().pageLoadTimeout((long) seconds, TimeUnit.SECONDS);
    }

    /**
     * <b>Fecha o navegador e finaliza o WebDriver do navegador</b>
     *
     * @param driver <i>webdriver do navegador</i>
     */
    public static void closeDriver(WebDriver driver) {
        if (driver != null) {
            driver.quit();
        }
    }

    /**
     * <b>Mata determinado processo do sistema</b>
     *
     * @param processo <i>processo do sistema</i>
     */
    public static void killProcess(String processo) {
        try {
            Process p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("") && line.substring(1, line.indexOf("\"", 1)).equalsIgnoreCase(processo)) {
                    Runtime.getRuntime().exec("taskkill /F /IM " + line.substring(1, line.indexOf("\"", 1)));
                }
            }
            input.close();
        } catch (Exception var4) {
            System.out.println(FRAMEWORK_NAME + "BrowsersManager.java: " + var4.getMessage());
        }
    }
}