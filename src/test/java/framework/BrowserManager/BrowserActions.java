package framework.BrowserManager;

import org.openqa.selenium.By;


import org.openqa.selenium.WebElement;


import static framework.BrowserManager.BrowserVerifications.getClickableElement;
import static framework.BrowserManager.BrowserVerifications.getElement;
import static framework.ConfigFramework.getDriver;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getActions;
import static java.lang.String.valueOf;

/**
 * <b>Classe para acoes no browser</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class BrowserActions {

    /**
     * <b>Abre url</b>
     *
     * @param url <i>url a ser acessada</i>
     */
    public static void openURL(String url) {
        getDriver().get(url);
    }

    /**
     * <b>Clica em um elemento</b>
     *
     * @param by <i>caminho elemento web</i>
     */
    public static void clickOnElement(By by) {
        BrowserVerifications.waitElementExists(by, DEFAULT_TIME_OUT);
        BrowserVerifications.waitElementToBeVisible(by, DEFAULT_TIME_OUT);
        getClickableElement(by).click();
    }

    /**
     * <b>Clica duas vezes em um elemento</b>
     *
     * @param by <i>caminho elemento web</i>
     */
    public static void doubleClickOnElement(By by) {
        clickOnElement(by);
        clickOnElement(by);
    }


    /**
     * <b>Clica duas vezes em um elemento instavel</b>
     *
     * @param by <i>caminho elemento web</i>
     */
    public static void tryDoubleClickOnElement(By by) {
        clickOnElement(by);
        try {
            clickOnElement(by);
        } catch (Exception e) {

        }
    }

    /**
     * <b>Insere texto em um campo</b>
     *
     * @param by   <i>caminho elemento web</i>
     * @param text <i>texto a ser inserido</i>
     */
    public static void setText(By by, String text) {
        getElement(by).sendKeys(text);
    }

    /**
     * <b>Insere texto em um campo digitando um caracter por vez</b>
     *
     * @param by   <i>caminho elemento web</i>
     * @param text <i>texto a ser inserido</i>
     */
    public static void setTextPerChar(By by, String text) {
        for (int i = 0; i < text.length(); i++) {
            setText(by, valueOf(text.charAt(i)));
        }
    }

    /**
     * <b>Insere texto em um campo digitando um caracter por vez</b>
     *
     * @param text <i>texto a ser inserido</i>
     */
    public static void setTextPerChar(String text) {
        for (int i = 0; i < text.length(); i++) {
            sendKeys(valueOf(text.charAt(i)));
        } //for
    }


    /**
     * <b>Retorna o texto de um campo</b>
     *
     * @param by <i>caminho elemento web</i>
     * @return String - <i>texto</i>
     */
    public static String getText(By by) {
        return getElement(by).getText();
    }

    /**
     * <b>Retorna o texto de um campo</b>
     *
     * @param by        <i>caminho elemento web</i>
     * @param attribute <i>atributo do elemento</i>
     * @return String - <i>texto</i>
     */
    public static String getTextAttribute(By by, String attribute) {
        return getElement(by).getAttribute(attribute).trim();
    }

    /**
     * <b>Envia uma sequencia de teclas a serem acionadas no navegador</b>
     *
     * @param keys <i>teclas</i>
     */
    public static void sendKeys(CharSequence... keys) {
        getActions().sendKeys(keys).build().perform();
    }

    /**
     * <b>Limpa conteudo do elemento</b>
     *
     * @param by <i>caminho elemento web</i>
     */
    public static void clearField(By by) {
        getElement(by).clear();
    }

    /**
     * <b>Clica com o botao direito em um elemento e seleciona um item das opcoes</b>
     *
     * @param option <i>opcao selecionada</i>
     * @param by     <i>caminho elemento web</i>
     */
    public static void selectOptionRightClickOnElement(String option, By by) {
        WebElement elementToClick = getClickableElement(by);

        //SELECIONA O ELEMENTO
        getActions().moveToElement(elementToClick);

        //CLICA COM O BOTAO DIREITO
        getActions().contextClick(elementToClick).build().perform();  //Clica com o botao direito
        getClickableElement(By.linkText(option)).click(); //Seleciona item
    }
}
