package framework.DataBase;
/**
 * <b>Enum para identificar os bancos de dados</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public enum DBPlataform {
    ORACLE,
    MYSQL,
    POSTGRESQL;


    /**
     * <b>Construtor vazio da Classe DBPlataform</b>
     *
     */
    DBPlataform() {
    }
}
