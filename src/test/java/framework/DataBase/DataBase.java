//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
package framework.DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import Reports.ExtentReports;
import org.postgresql.Driver;

/**
 * <b>Classe para conexao em banco</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class DataBase {
    private String host;
    private String port;
    private String dataBase;
    private String user;
    private String pass;
    private Connection connection;

    /**
     * <b>Construtor</b>
     *
     * @param host     <i>endereco de host</i>
     * @param port     <i>porta de conexao</i>
     * @param dataBase <i>nome do banco</i>
     * @param user     <i>usuario do banco</i>
     * @param pass     <i>senha do banco</i>
     */
    public DataBase(String host, String port, String dataBase, String user, String pass) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
        this.dataBase = dataBase;
    }

    /**
     * <b>Abre a conexao</b>
     *
     * @param dbPlatform <i>plataforma de conexao</i>
     * @see DBPlataform
     */
    public void openConnection(DBPlataform dbPlatform) {
        try {
            String url;
            switch (dbPlatform) {
                case MYSQL:
                    Class.forName("com.mysql.jdbc.Driver");
                    url = "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.dataBase +
                            "?useUnicode=true&useJDBCCompliantTimezoneShift" +
                            "=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
                    this.setConnection(DriverManager.getConnection(url, this.user, this.pass));
                    break;
                case ORACLE:
                    Class.forName("oracle.jdbc.driver.OracleDriver");
                    url = "jdbc:oracle:thin:@//" + this.host + ":" + this.port + "/" + this.dataBase;
                    this.setConnection(DriverManager.getConnection(url, this.user, this.pass));
                    break;
                case POSTGRESQL:
                    DriverManager.registerDriver(new Driver());
                    Class.forName("org.postgresql.Driver");
                    url = "jdbc:postgresql://" + this.host + ":" + this.port + "/" + this.dataBase;
                    this.setConnection(DriverManager.getConnection(url, this.user, this.pass));
                default:
                    //break;
                    }
        } catch (Exception var4) {
            ExtentReports.appendToReport("Erro no metodo 'startConnection': " + var4.getMessage());
        }

    }

    /**
     * <b>Fecha a conexao</b>
     */
    public void closeConnection() {
        try {
            if (!this.getConnection().isClosed()) {
                this.getConnection().close();
            }
        } catch (SQLException var2) {
            ExtentReports.appendToReport("Erro no metodo 'closeConnection': " + var2.getMessage());
        }

    }

    /**
     * <b>Executa comando sql</b>
     *
     * @param sql <i>query sql</i>
     * @return ResultSet - <i>retorna o objeto com os resultados do comando</i>
     * @see ResultSet
     */
    public ResultSet executeSqlCommand(String sql) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = this.getConnection().prepareStatement(sql, 1);
            ps.execute();
            rs = ps.getGeneratedKeys();
        } catch (SQLException var5) {
            ExtentReports.appendToReport("Erro no metodo 'executeSqlCommand': " + var5.getMessage());
        }

        return rs;
    }

    /**
     * <b>Executa select</b>
     *
     * @param sql <i>query sql</i>
     * @return ResultSet - <i>retorna o objeto com os resultados do comando</i>
     */
    public ResultSet select(String sql) {
        ResultSet rs = null;

        try {
            PreparedStatement ps = this.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (SQLException var5) {
            ExtentReports.appendToReport("Erro no metodo 'select': " + var5.getMessage());
        }

        return rs;
    }

    /**
     * <b>Verifica se o valor existe ou nao em determinada coluna</b>
     *
     * @param rs          <i>resultado de uma query executada</i>
     * @param indexColumn <i>indice da coluna a ser verificada</i>
     * @param contains    <i>verificar se o valor existe ou nao</i>
     * @param value       <i>query sql</i>
     * @return boolean - <i>retorna se o valor existe ou nao</i>
     */
    public boolean findValueColumn(ResultSet rs, int indexColumn, boolean contains, String value) {
        while (true) {
            try {
                if (rs.next()) {
                    String nome = null;
                    nome = rs.getString(indexColumn);
                    if (contains) {
                        if (!nome.contains(value)) {
                            continue;
                        }

                        return true;
                    }

                    if (!nome.equals(value)) {
                        continue;
                    }

                    return true;
                }
            } catch (SQLException var6) {
                ExtentReports.appendToReport("Erro no metodo 'findValueColumn': " + var6.getMessage());
            }

            return false;
        }
    }

    /**
     * <b>Retorna a conexao</b>
     *
     * @return Connection - <i>retorna se o valor existe ou nao</i>
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * <b>Define a conexao</b>
     *
     * @param connection <i>conexao</i>
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
