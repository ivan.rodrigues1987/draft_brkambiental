package framework.FileManager;

import com.google.gson.Gson;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

/**
 * <b>Classe para leitura e manipulacao dos arquivos de parametro JSON</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */

public abstract class JsonManager {

    /**
     * <b>Converte as informacoes de um arquivo JSON para um objeto</b>
     *
     * @param filePath <i>caminho do arquivo</i>
     * @param auxClass <i>classe na mesma estrutura do JSON</i>
     * @param <T>      <i>tipo da Classe</i>
     * @return Classe - <i>objetos da classe preenchidos</i>
     */
    public static <T> T getJsonInfo(String filePath, Class<T> auxClass) {
        try {
            //Read Json file
            JsonReader jsonReader = Json.createReader(new InputStreamReader(new FileInputStream(new File(filePath))));
            JsonObject jobj = jsonReader.readObject();

            //Cast Json Info in class
            return new Gson().fromJson(jobj.toString(), (Type) auxClass);

        } catch (Exception e) {
            System.err.println("[JSON MANAGER ERROR]" + e.getMessage());
            return null;
        }
    }
}
