package framework.FileManager;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Classe para leitura e manipulacao de arquivos de parametro PROPERTIES
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */

public class PropertiesManager {
    private Properties props = new Properties();
    private String filePath;

    /**
     * <b>Construtor</b>
     *
     * @param filePath <i>caminho do arquivo</i>
     */
    public PropertiesManager(String filePath) {
        this.filePath = filePath;
    }

    /**
     * <b>Retorna as propriedades</b>
     *
     * @return Properties - <i>propriedades</i>
     */
    public Properties getProps() {
        try {
            File file = new File(this.filePath);
            FileInputStream fileInputStream = new FileInputStream(file);
            this.props.load(fileInputStream);
            fileInputStream.close();
        } catch (Exception var3) {
            System.out.println("PropertiesManager.java: " + var3.getMessage());
        }

        return this.props;
    }
}