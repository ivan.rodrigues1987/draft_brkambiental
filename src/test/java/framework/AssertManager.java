package framework;

import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.testng.Assert;

import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserVerifications.elementExists;
import static framework.BrowserManager.BrowserVerifications.getElements;

/**
 * <b>Classe para verificacoes no afirmacoes, se a afirmacao for falsa o teste falha</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class AssertManager extends Assert {

    /**
     * <b>Afirma se um texto é igual a um elemento web</b>
     *
     * @param by           <i>xPath do elemento</i>
     * @param expectedText <i>texto esperado</i>
     */
    public static void assertElementText(By by, String expectedText) {
        Assert.assertEquals(getText(by), expectedText);
    }

    /**
     * <b>Afirma se um texto de um elemento é igual ao texto de outro elemento web</b>
     *
     * @param byActual   <i>xPath do elemento</i>
     * @param byExpected <i>xPath do elemento a ser comparado</i>
     */
    public static void assertElementText(By byActual, By byExpected) {
        Assert.assertEquals(getText(byActual), getText(byExpected));
    }

    /**
     * <b>Afirma se um texto esta presente em um elemento web</b>
     *
     * @param by           <i>xPath do elemento</i>
     * @param expectedText <i>texto esperado</i>
     */
    public static void assertElementTextContains(By by, String expectedText) {
        String actualText = getText(by);
        boolean contains = actualText.contains(expectedText);

        //Verifica se o texto do elemento contem o texto esperado
        expectedText = (contains) ? actualText : "O TEXTO (" + expectedText + ") NAO ESTA PRESENTE NO ELEMENTO";
        Assert.assertEquals(actualText, expectedText);
    }

    /**
     * <b>Afirma que os valores dos atributos dos elementos sao iguais</b>
     *
     * @param byActual   <i>xPath do elemento</i>
     * @param byExpected <i>xPath do elemento a ser comparado</i>
     * @param attribute  <i>valor desejado</i>
     */
    public static void assertElementAttributeText(By byActual, By byExpected, String attribute) {
        Assert.assertEquals(getTextAttribute(byActual, attribute), getTextAttribute(byExpected, attribute));
    }

    /**
     * <b>Afirma se um texto é igual ao texto de um atributo de um elemento web</b>
     *
     * @param by           <i>xPath do elemento</i>
     * @param attribute    <i>atributo do elemento</i>
     * @param expectedText <i>texto esperado</i>
     */
    public static void assertElementAttributeText(By by, String attribute, String expectedText) {
        Assert.assertEquals(getTextAttribute(by, attribute), expectedText);
    }

    /**
     * <b>Afirma se um texto esta presente no texto de um atributo de um elemento web</b>
     *
     * @param by           <i>xPath do elemento</i>
     * @param attribute    <i>atributo do elemento</i>
     * @param expectedText <i>texto esperado</i>
     */
    public static void assertElementAttributeTextContains(By by, String attribute, String expectedText) {
        String actualText = getTextAttribute(by, attribute);
        boolean contains = actualText.contains(expectedText);

        //Verifica se o texto do atributo contem o texto esperado
        expectedText = (contains) ? actualText : "O TEXTO (" + expectedText + ") NAO ESTA PRESENTE NO ATRIBUTO";
        Assert.assertEquals(actualText, expectedText);
    }

    /**
     * <b>Afirma que um elemento existe</b>
     *
     * @param by <i>xPath do elemento</i>
     */
    public static void assertElementExists(By by) {
        BrowserVerifications.waitElementToBeClickable(by, ConfigFramework.DEFAULT_TIME_OUT);
    }

    /**
     * <b>Afirma que um elemento nao existe</b>
     *
     * @param by <i>xPath do elemento</i>
     */
    public static void assertElementNotExists(By by) {
        String expected = "xPath: " + by.toString();
        String actual = expected;
        if (elementExists(by)) {
            actual = "O elemento " + expected + " foi encontrado em tela.";
        }
        Assert.assertEquals(actual, expected);
    }

    /**
     * <b>Afirma no caminho especificado a uma quantidade determinada de elementos</b>
     *
     * @param by               <i>xPath dos elementos</i>
     * @param numberOfElements <i>numero de elementos</i>
     */
    public static void assertNumberOfElementsMoreThan(By by, int numberOfElements) {
        String expected = numberOfElements + " - xPath: " + by.toString();
        String actual = expected;
        int size = getElements(by, numberOfElements).size();
        if (size < numberOfElements) {
            actual = "O elemento tem tamanho " + size;
        }
        Assert.assertEquals(actual, expected);
    }

    /**
     * <b>Afirma se o elemento possui um texto</b>
     *
     * @param by        <i>xPath dos elementos</i>
     * @param attribute <i>atributo do elemento</i>
     * @param empty     <i>validar elemento vazio ou nao</i>
     */
    public static void assertElementAttributeTextEmpty(By by, String attribute, boolean empty) {
        Assert.assertEquals(getTextAttribute(by, attribute).isEmpty(), empty);
    }
}
