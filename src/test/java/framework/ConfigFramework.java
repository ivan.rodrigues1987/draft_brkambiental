package framework;

import framework.BrowserManager.BrowserActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

/**
 * <b>Classe para as configuracoes iniciais do WebDriver</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class ConfigFramework {
    private static String so;

    public static final String FRAMEWORK_NAME = "[BRK FRAMEWORK] ";
    private static final String OSLINUX = "linux";
    private static final String OSWINDOWS = "windows";

    private static WebDriver driver;
    private static Actions actions;
    private static BrowserActions browserActions;
    public static final int DEFAULT_TIME_OUT = 20;

    /**
     * <b>Inicializa o WebDriver</b>
     *
     * @param headless <i>inicializar driver em modo oculto</i>
     */
    public static void initializeDriver(boolean headless) {
        //Get operational system name
        so = System.getProperty("os.name").toLowerCase();

        if (headless) {
            initializeChrome(true);
        } else {
            initializeChrome(false);
        }
        if (driver != null) {
            actions = new Actions(driver);
        }
    }

    /**
     * <b>Inicializa o Chrome WebDriver</b>
     *
     * @param headless <i>inicializar chrome em modo oculto</i>
     */
    private static void initializeChrome(boolean headless) {
        String resourcesPath = "src" + File.separator + "test" + File.separator + "resources" + File.separator;

        if (so.contains(OSLINUX)) {
            System.setProperty("webdriver.chrome.driver", resourcesPath + "linux" + File.separator + "chromedriver");
        } else if (so.startsWith(OSWINDOWS)) {
            System.setProperty("webdriver.chrome.driver", resourcesPath +
                    "windows" + File.separator + "chromedriver.exe");
        }

        ChromeOptions options = new ChromeOptions();

        if (headless) {
            options.addArguments("start-maximized", "disable-extensions",
                    "--ignore-ssl-errors", "disable-gpu", "--headless", "--no-sandbox", "window-size=1240x780");
        } else {
            options.addArguments("start-maximized", "--ignore-ssl-errors");
        }
        driver = new ChromeDriver(options);
    }

    /**
     * <b>Retorna o WebDriver</b>
     *
     * @return WebDriver - <i>retorna o driver do navegador</i>
     */
    public static WebDriver getDriver() {
        return driver;
    }

    /**
     * <b>Retorna acoes do navegador</b>
     *
     * @return Actions - <i>retorna as acoes do navegador</i>
     */
    public static Actions getActions() {
        return actions;
    }
}
