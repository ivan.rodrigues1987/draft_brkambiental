package runner.Dados.Cadastro;

import static main.Utils.ComumScreens.Auxiliar.getCampoValidado;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * @author Renan Ferreira dos Santos
 * @since 12/02/2019
 */
public class Endereco {
    private String cidade;
    private String distrito;
    private String bairro;
    private String logradouro;
    private String estado;
    private String municipio;
    private String quadra;
    private String lote;
    //Gerados aleatoriamente
    private String numero;
    private String complemento;
    private String cep;
    private static final int APT_CEP = 3;
    private static final int CEP_PART = 5;

    /**
     * <b>Construtor para inicializar Endereço</b>
     *
     * @param cidade <i>variable a inserir</i>
     * @param distrito <i>variable a inserir</i>
     * @param bairro <i>variable a inserir</i>
     * @param logradouro <i>variable a inserir</i>
     */
    public Endereco(String cidade, String distrito, String bairro, String logradouro) {
        this.cidade = cidade;
        this.distrito = distrito;
        this.bairro = bairro;
        this.logradouro = logradouro;
        this.numero = geradorNumberRandom(APT_CEP);
        this.complemento = "APT " + geradorNumberRandom(2);
        this.cep = geradorNumberRandom(CEP_PART) + "-" + geradorNumberRandom(APT_CEP);
    }

    /**
     * <b>Metodo get Cidade</b>
     *
     * @return cidade <i>retorna valor de variable</i>
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * <b>Metodo get bairro</b>
     *
     * @return bairro <i>retorna valor de variable</i>
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * <b>Metodo get logradouro</b>
     *
     * @return logradouro <i>retorna valor de variable</i>
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * <b>Metodo get numero</b>
     *
     * @return numero <i>retorna valor de variable</i>
     */
    public String getNumero() {
        return numero;
    }

    /**
     * <b>Metodo get complemento</b>
     *
     * @return complemento <i>retorna valor de variable</i>
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * <b>Metodo get cep</b>
     *
     * @return cep <i>retorna valor de variable</i>
     */
    public String getCep() {
        return cep;
    }

    /**
     * <b>Metodo get distrito</b>
     *
     * @return distrito <i>retorna valor de variable</i>
     */
    public String getDistrito() {
        return distrito;
    }

    /**
     * <b>Metodo get estado</b>
     *
     * @return estado <i>retorna valor de variable</i>
     */
    public String getEstado() {
        return estado;
    }

    /**
     * <b>Metodo set estado</b>
     *
     * @return estado <i>retorna valor de variable</i>
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * <b>Metodo get municipio</b>
     *
     * @return municipio <i>retorna valor de variable</i>
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * <b>Metodo set municipio</b>
     *
     * @return municipio <i>retorna valor de variable</i>
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     * <b>Metodo get quadra</b>
     *
     * @return quadra <i>retorna valor de variable</i>
     */
    public String getQuadra() {
        return quadra;
    }

    /**
     * <b>Metodo set municipio</b>
     *
     * @return quadra <i>retorna valor de variavel</i>
     */
    public void setQuadra(String quadra) {
        this.quadra = quadra;
    }

    /**
     * <b>Metodo get Lote</b>
     *
     * @return Lote <i>retorna valor de variavel</i>
     */
    public String getLote() {
        return lote;
    }

    /**
     * <b>Metodo set Lote</b>
     *
     * @return Lote <i>retorna valor de variavel</i>
     */
    public void setLote(String lote) {
        this.lote = lote;
    }

    /**
     * <b>Metodo que retorna valores de endereço</b>
     *
     * @param endereco <i>inseir dados de classe</i>
     * @return endereco <i>retorna valor de classe</i>
     */
    public Endereco retornarCamposEndereco(Endereco endereco) {

        //RETORNA PARA OS REPECTIVOS CAMPOS OS VALORES DE SEUS ATRIBUTOS
        endereco.cidade = getCampoValidado(this.cidade, endereco.cidade);
        endereco.distrito = getCampoValidado(this.distrito, endereco.distrito);
        endereco.bairro = getCampoValidado(this.bairro, endereco.bairro);
        endereco.logradouro = getCampoValidado(this.logradouro, endereco.logradouro);
        endereco.estado = getCampoValidado(this.estado, endereco.estado);
        endereco.municipio = getCampoValidado(this.municipio, endereco.municipio);
        endereco.quadra = getCampoValidado(this.quadra, endereco.quadra);
        endereco.lote = getCampoValidado(this.lote, endereco.lote);
        endereco.numero = getCampoValidado(this.numero, endereco.numero);
        endereco.complemento = getCampoValidado(this.complemento, endereco.complemento);
        endereco.cep = getCampoValidado(this.cep, endereco.cep);

        return endereco;
    } //getCampoValidado*/


}
