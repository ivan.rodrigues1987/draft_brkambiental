package runner.Dados.Cadastro;


import runner.Dados.Atendimento.DadosAtendimento;
import stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua;
import stepdefinition.Cadastro.Arrecadador.Arrecadador;
import stepdefinition.Cadastro.Cliente.Cliente;
import stepdefinition.Cadastro.ConsolidacaoFatura.ConsolidacaoFatura;
import stepdefinition.Cadastro.Cronograma.Cronograma;
import stepdefinition.Cadastro.DebitoAutomatico.DebitoAutomatico;
import stepdefinition.Cadastro.Excecao.Excecao;
import stepdefinition.Cadastro.InstalacaoMedidor.InstalacaoMedidor;
import stepdefinition.Cadastro.Ligacao.Ligacao;
import stepdefinition.Cadastro.MensagemFatura.MensagemFatura;
import stepdefinition.Cadastro.RetencaoForcadaFatura.RetencaoForcadaFatura;
import stepdefinition.Cadastro.Rubrica.Rubrica;
import main.Massas.QuantidadeMassasCadastro;

/**
 * <b>Classe para armazenar os dados dinamicos em cadastro utilizados para as unidades</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 12/02/2019
 */
public class DadosCadastro extends DadosAtendimento {
    private boolean cidadeUnica;
    private Endereco endereco;
    private AnaliseAgua analiseAgua;
    private Arrecadador arrecadador;
    private Cliente cliente;
    private ConsolidacaoFatura consolidacaoFatura;
    private Cronograma cronograma;
    private DebitoAutomatico debitoAutomatico;
    private InstalacaoMedidor instalacaoMedidor;
    private Ligacao ligacao;
    private RetencaoForcadaFatura retencaoForcadaFatura;
    private Rubrica rubrica;
    private Excecao excecao;
    private MensagemFatura mensagemFatura;
    private QuantidadeMassasCadastro qtdMassas = new QuantidadeMassasCadastro();

    /**
     * <b>retorna o valor da variavel numero</b>
     *
     * @return retencaoForcadaFatura <i>retorna classe</i>
     */
    public RetencaoForcadaFatura getRetencaoForcadaFatura() {
        return retencaoForcadaFatura;
    }

    /**
     * <b>altera o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param retencaoForcadaFatura <i>retencaoForcadaFatura</i>
     *
     */
    public void setRetencaoForcadaFatura(RetencaoForcadaFatura retencaoForcadaFatura) {
        this.retencaoForcadaFatura = retencaoForcadaFatura;
    }

    /**
     * <b>Metodo boolean Cidade Unica</b>
     *
     * @return cidadeUnica <i>retorna se cidade é unica</i>
     */
    public boolean isCidadeUnica() {
        return cidadeUnica;
    }

    /**
     * <b>altera o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @return cidadeUnica <i>retorna valor de variavel</i>
     */
    public void setCidadeUnica(boolean cidadeUnica) {
        this.cidadeUnica = cidadeUnica;
    }

    /**
     * <b>Metodo get Endereço</b>
     *
     * @return endereco <i>retorna valor de classe</i>
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * <b>Metodo set Endereço</b>
     *
     * @return endereco <i>retorna valor de classe</i>
     */
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    /**
     * <b>Metodo get AnaliseAgua</b>
     *
     * @return AnaliseAgua <i>retorna valor de classe</i>
     */
    public AnaliseAgua getAnaliseAgua() {
        return analiseAgua;
    }

    /**
     * <b>Metodo set analiseAgua</b>
     *
     * @return analiseAgua <i>retorna valor de classe</i>
     */
    public void setAnaliseAgua(AnaliseAgua analiseAgua) {
        this.analiseAgua = analiseAgua;
    }

    /**
     * <b>Metodo get analiseAgua</b>
     *
     * @return analiseAgua <i>retorna valor de classe</i>
     */
    public Arrecadador getArrecadador() {
        return arrecadador;
    }

    /**
     * <b>Metodo set analiseAgua</b>
     *
     * @return arrecadador <i>retorna valor de classe</i>
     */
    public void setArrecadador(Arrecadador arrecadador) {
        this.arrecadador = arrecadador;
    }

    /**
     * <b>Metodo get Cliente</b>
     *
     * @return cliente <i>retorna valor de classe</i>
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * <b>Metodo set Cliente</b>
     *
     * @return cliente <i>retorna valor de classe</i>
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * <b>Metodo get DebitoAutomatico</b>
     *
     * @return debitoAutomatico <i>retorna valor de classe</i>
     */
    public DebitoAutomatico getDebitoAutomatico() {
        return debitoAutomatico;
    }

    /**
     * <b>Metodo get Cronograma</b>
     *
     * @return cronograma <i>retorna valor de classe</i>
     */
    public Cronograma getCronograma() {
        return cronograma;
    }

    /**
     * <b>Metodo set Cronograma</b>
     *
     * @return cronograma <i>retorna valor de classe</i>
     */
    public void setCronograma(Cronograma cronograma) {
        this.cronograma = cronograma;
    }

    /**
     * <b>Metodo set debitoAutomatico</b>
     *
     * @return debitoAutomatico <i>retorna valor de classe</i>
     */
    public void setDebitoAutomatico(DebitoAutomatico debitoAutomatico) {
        this.debitoAutomatico = debitoAutomatico;
    }

    /**
     * <b>Metodo set ConsolidacaoFatura</b>
     *
     * @return ConsolidacaoFatura <i>retorna valor de classe</i>
     */
    public void setConsolidacaoFatura(ConsolidacaoFatura consolidacaoFatura) {
        this.consolidacaoFatura = consolidacaoFatura;
    }

    /**
     * <b>Metodo get ConsolidacaoFatura</b>
     *
     * @return ConsolidacaoFatura <i>retorna valor de classe</i>
     */
    public ConsolidacaoFatura getConsolidacaoFatura() {
        return consolidacaoFatura;
    }

    /**
     * <b>Metodo get InstalacaoMedidor</b>
     *
     * @return InstalacaoMedidor <i>retorna valor de classe</i>
     */
    public InstalacaoMedidor getInstalacaoMedidor() {
        return instalacaoMedidor;
    }

    /**
     * <b>Metodo set InstalacaoMedidor</b>
     *
     * @return InstalacaoMedidor <i>retorna valor de classe</i>
     */
    public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {
        this.instalacaoMedidor = instalacaoMedidor;
    }

    /**
     * <b>Metodo get Ligacao</b>
     *
     * @return Ligacao <i>retorna valor de classe</i>
     */
    public Ligacao getLigacao() {
        return ligacao;
    }

    /**
     * <b>Metodo set Ligacao</b>
     *
     * @return Ligacao <i>retorna valor de classe</i>
     */
    public void setLigacao(Ligacao ligacao) {
        this.ligacao = ligacao;
    }

    /**
     * <b>Metodo get Rubrica</b>
     *
     * @return rubrica <i>retorna valor de classe</i>
     */
    public Rubrica getRubrica() {
        return rubrica;
    }

    /**
     * <b>Metodo set Ligacao</b>
     *
     * @return rubrica <i>retorna valor de classe</i>
     */
    public void setRubrica(Rubrica rubrica) {
        this.rubrica = rubrica;
    }

    /**
     * <b>Metodo get Excecao</b>
     *
     * @return excecao <i>retorna valor de classe</i>
     */
    public Excecao getExcecao() {
        return excecao;
    }

    /**
     * <b>Metodo set Excecao</b>
     *
     * @return excecao <i>retorna valor de classe</i>
     */
    public void setExcecao(Excecao excecao) {
        this.excecao = excecao;
    }

    public MensagemFatura getMensagemFatura() {
        return mensagemFatura;
    }

    public void setMensagemFatura(MensagemFatura mensagemFatura) {
        this.mensagemFatura = mensagemFatura;
    }

    public QuantidadeMassasCadastro getQtdMassas() {
        return qtdMassas;
    }

    public void setQtdMassas(QuantidadeMassasCadastro qtdMassas) {
        this.qtdMassas = qtdMassas;
    }
}
