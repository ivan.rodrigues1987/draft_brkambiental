package runner.Dados.CallCenter;

import stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento;
import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class DadosCallCenter {
    private GestaoUnidadeNegocio gestaoUnidadeNegocio;
    private GestaoCallCenterTipoAtendimento gestaoCallCenterTipoAtendimento;

    /**
     * <b>Metodo retorna get GestaoCallCenterTipoAtendimento </b>
     *
     * @return getGestaoCallCenterTipoAtendimento <i>retorna variable get</i>
     */
    public GestaoCallCenterTipoAtendimento getGestaoCallCenterTipoAtendimento() {
        return gestaoCallCenterTipoAtendimento;
    }

    /**
     * <b>Metodo retorna set gestaoCallCenterTipoAtendimento </b>
     *
     * @return gestaoCallCenterTipoAtendimento <i>retorna variable set</i>
     */
    public void setGestaoCallCenterTipoAtendimento(GestaoCallCenterTipoAtendimento gestaoCallCenterTipoAtendimento) {
        this.gestaoCallCenterTipoAtendimento = gestaoCallCenterTipoAtendimento;
    }

    /**
     * <b>Metodo retorna get gestaoUnidadeNegocio </b>
     *
     * @return gestaoUnidadeNegocio <i>retorna variable get</i>
     */
    public GestaoUnidadeNegocio getGestaoUnidadeNegocio() {
        return gestaoUnidadeNegocio;
    }

    /**
     * <b>Metodo retorna set gestaoUnidadeNegocio </b>
     *
     * @return gestaoUnidadeNegocio <i>retorna variable set</i>
     */
    public void setGestaoUnidadeNegocio(GestaoUnidadeNegocio gestaoUnidadeNegocio) {
        this.gestaoUnidadeNegocio = gestaoUnidadeNegocio;
    }
}
