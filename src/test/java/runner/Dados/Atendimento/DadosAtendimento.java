package runner.Dados.Atendimento;

import stepdefinition.Atendimento.GestaoTipoAtendimento;
import stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento;
import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class DadosAtendimento {
    private GestaoUnidadeNegocio gestaoUnidadeNegocio;
    private GestaoTipoAtendimento gestaoTipoAtendimento;

    /**
     * <b>Metodo retorna get GestaoCallCenterTipoAtendimento </b>
     *
     * @return getGestaoCallCenterTipoAtendimento <i>retorna variable get</i>
     */
    public GestaoTipoAtendimento getGestaoTipoAtendimento() {
        return gestaoTipoAtendimento;
    }

    /**
     * <b>Metodo retorna set gestaoCallCenterTipoAtendimento </b>
     *
     * @return gestaoCallCenterTipoAtendimento <i>retorna variable set</i>
     */
    public void setGestaoTipoAtendimento(GestaoTipoAtendimento gestaoTipoAtendimento) {
        this.gestaoTipoAtendimento = gestaoTipoAtendimento;
    }

    /**
     * <b>Metodo retorna get gestaoUnidadeNegocio </b>
     *
     * @return gestaoUnidadeNegocio <i>retorna variable get</i>
     */
    public GestaoUnidadeNegocio getGestaoUnidadeNegocio() {
        return gestaoUnidadeNegocio;
    }

    /**
     * <b>Metodo retorna set gestaoUnidadeNegocio </b>
     *
     * @return gestaoUnidadeNegocio <i>retorna variable set</i>
     */
    public void setGestaoUnidadeNegocio(GestaoUnidadeNegocio gestaoUnidadeNegocio) {
        this.gestaoUnidadeNegocio = gestaoUnidadeNegocio;
    }

}
