package runner;

import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import runner.CustomCucumber.CustomCucumber;

import static runner.Setup.GradleParams.validarUnidadeInformada;
import static runner.Setup.Json.ParamsInfo.PARAMETROS_EXECUCAO;
import static framework.BrowserManager.BrowsersManager.closeDriver;
import static framework.ConfigFramework.getDriver;
import static framework.ConfigFramework.initializeDriver;
import static main.Reports.setupReports;
import static main.Reports.endReports;

@RunWith(CustomCucumber.class)
@CucumberOptions(
        junit = "--filename-compatible-names",
        features = "features",
        glue = {"stepdefinition", "main", "hooks"},
        tags = {"@Cadastro_LigacaoComercial_CT001", "@21"},
        plugin = {
                "pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "com.github.kirlionik.cucumberallure.AllureReporter",
                "html:target/cucumber-reports/cucumber-pretty",
                "com.cucumber.listener.ExtentCucumberFormatter:"
        }
)
public class RunTest {
    @BeforeClass
    public static void setup() {
        //Verifica parametros passados ao Gradle
        validarUnidadeInformada();
        //Inicializa reports
        setupReports();
        //Inicializa driver
        initializeDriver(PARAMETROS_EXECUCAO.isHeadless());
    }

    @AfterClass
    public static void afterClass() {
        //Close driver
        closeDriver(getDriver());
        //End reports
        endReports();
    }
}
