package runner.Setup.Execucao;




public class ParametrosExecucao {
    private boolean headless;
    private int pageLoadTimeOut;

    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public int getPageLoadTimeOut() {
        return pageLoadTimeOut;
    }

    public void setPageLoadTimeOut(int pageLoadTimeOut) {
        this.pageLoadTimeOut = pageLoadTimeOut;
    }
}
