package runner.Setup.Execucao;

import runner.Setup.Json.Unidades.Unidade;
import framework.DataBase.DataBase;

import java.util.ArrayList;

public class ConfiguracaoAcesso {
    private DataBase dataBaseMaster;
    private ArrayList<Unidade> unidades;
    private Unidade callCenter;

    public DataBase getDataBases() {
        return dataBaseMaster;
    }

    public void setDataBases(DataBase dataBases) {
        dataBaseMaster = dataBases;
    }

    public DataBase getDataBaseMaster() {
        return dataBaseMaster;
    }

    public void setDataBaseMaster(DataBase dataBaseMaster) {
        this.dataBaseMaster = dataBaseMaster;
    }

    public ArrayList<Unidade> getUnidades() {
        return unidades;
    }

    public void setUnidades(ArrayList<Unidade> unidades) {
        this.unidades = unidades;
    }

    public Unidade getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(Unidade callCenter) {
        this.callCenter = callCenter;
    }
}
