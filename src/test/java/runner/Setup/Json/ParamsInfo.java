package runner.Setup.Json;

import runner.Setup.Execucao.ConfiguracaoAcesso;
import runner.Setup.Execucao.ParametrosExecucao;
import runner.Setup.Json.Unidades.Unidade;

import java.io.File;

import static framework.FileManager.JsonManager.getJsonInfo;


/**
 * <b>Classe para Tratativa dos parametros por unidade</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */

public class ParamsInfo {
    private static final String PATH = "parametros" + File.separator;
    public static final ParametrosExecucao PARAMETROS_EXECUCAO =
            getJsonInfo(PATH + "ParametrosExecucao.json", ParametrosExecucao.class);
    public static final ConfiguracaoAcesso CONFIGURACAO_ACESSO =
            getJsonInfo(PATH + "ConfiguracaoAcesso.json", ConfiguracaoAcesso.class);
    private static Unidade unidadeExecucao;


    /**
     * <b>Retorna uma Unidade a ser executada</b>
     *
     * @return Unidade - <i>unidade atual inicializada para execucao no projeto</i>
     * @see Unidade
     */
    public static Unidade getUnidadeExecucao() {
        return unidadeExecucao;
    }

    /**
     * <b>Inicializa o atributo unidadeExecucao com a unidade a ser executada</b>
     *
     * @param runUnidade <i>nome da unidade</i>
     */
    public static void setUnidadeExecucao(String runUnidade) {
        String callCenter = CONFIGURACAO_ACESSO.getCallCenter().getUnidade();
        if (!runUnidade.equals(callCenter)) {
            unidadeExecucao = null;
            for (Unidade unidade : CONFIGURACAO_ACESSO.getUnidades()) {
                if (unidade.getUnidade().equalsIgnoreCase(runUnidade)) {
                    unidadeExecucao = unidade;
                }
            }
        } else if (runUnidade.equals(callCenter)) {
            unidadeExecucao = CONFIGURACAO_ACESSO.getCallCenter();
        }
    }


}
