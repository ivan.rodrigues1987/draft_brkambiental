package runner.Setup.Json;

import main.DBManager.DBCadastro;
import main.DBManager.DBCallCenter;
import runner.Dados.Cadastro.DadosCadastro;
import runner.Dados.CallCenter.DadosCallCenter;
import runner.Dados.Atendimento.DadosAtendimento;

import static framework.Errors.setUpError;
import static runner.Setup.Json.ParamsInfo.getUnidadeExecucao;

/**
 * <b>Classe abstrata para gerenciar os dados dinamicos</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 12/02/2019
 */
public abstract class DadosDinamicos {
    private static DadosCadastro dadosCadastro;
    private static DadosCallCenter dadosCallCenter;
    private static  DadosAtendimento dadosAtendimento;

    /**
     * <b>Armazena os dados dinamicos da unidade nos atributos estaticos</b>
     *
     * @param unidade <i>seleciona unidade desejada</i>
     */
    public static void inicializarDadosUnidade(String unidade) {
        try {
            if (getUnidadeExecucao().getUnidade().equals("CALLCENTER")) {
                dadosCallCenter = new DBCallCenter().getDadosCallCenter(unidade);

            } else {
                dadosCadastro = new DBCadastro().getDadosCadastro();
                dadosAtendimento = dadosCadastro;

                //teste dados atendimento
                if (((DadosCadastro) dadosAtendimento).getCliente() == null)
                    System.out.println(dadosAtendimento.toString());
                else
                    System.out.println(dadosAtendimento.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            setUpError("Nao foi possivel inicializar os dados da unidade");
        }
    }

    /**
     * <b>Retorna os dados de cadastro para a unidade</b>
     *
     * @return DadosCadastro - <i>retorna os dados dinamicos de Cadastro</i>
     * @see DadosCadastro
     */
    public static DadosCadastro getDadosCadastro() {
        return dadosCadastro;
    }

    /**
     * <b>Retorna os dados de Call Center para a unidade</b>
     *
     * @return DadosCallCenter - <i>retorna os dados dinamicos de Call Center</i>
     * @see DadosCallCenter
     */
    public static DadosCallCenter getDadosCallCenter() {
        return dadosCallCenter;
    }


    /**
     * <b>Retorna os dados de Atendimento para a unidade</b>
     *
     * @return DadosAtendimento - <i>retorna os dados dinamicos de Atendimento</i>
     * @see DadosAtendimento
     */
    public static DadosAtendimento getDadosAtendimento() {
        return dadosAtendimento;
    }



}
