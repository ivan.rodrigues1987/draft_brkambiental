package runner.Setup.Json.Unidades;

import framework.DataBase.DataBase;

public class Unidade {
    private int id;
    private String unidade;
    private String url;
    private String user;
    private String password;
    private DataBase dataBaseAccess;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DataBase getDataBaseAccess() {
        return dataBaseAccess;
    }

    public void setDataBaseAccess(DataBase dataBaseAccess) {
        this.dataBaseAccess = dataBaseAccess;
    }
}
