package runner.Setup;

import runner.Setup.Json.Unidades.Unidade;

import static framework.Errors.setUpError;
import static runner.Setup.Json.ParamsInfo.CONFIGURACAO_ACESSO;

/**
 * @author Renan Ferreira dos Santos
 * @since 28/02/2019
 */
public class GradleParams {
    /**
     * <b>Verifica se a unidade foi informada nos parametros passados ao Gradle</b>
     */
    public static void validarUnidadeInformada() {
        String tags = System.getProperty("cucumber.options");
        if (tags != null) {
            if (!tags.isEmpty()) {
                boolean unidadeInformada = false;
                //Verifica se a execucao é do modulo de Call Center
                if (tags.toUpperCase().contains(CONFIGURACAO_ACESSO.getCallCenter().getUnidade())) {
                    unidadeInformada = true;
                } else {
                    //Verifica se foi passado como parametro de execucao a tag da unidade a ser executada
                    for (Unidade unidade : CONFIGURACAO_ACESSO.getUnidades()) {
                        if ((tags.contains("@" + unidade.getId())) ||
                                tags.toUpperCase().contains(unidade.getUnidade())) {
                            unidadeInformada = true;
                            break;
                        }
                    }
                }
                //Nao executa mais se a unidade nao tiver sido inicializada
                if (!unidadeInformada) {
                    setUpError("A TAG da unidade a ser executada nao" +
                            "foi localizada nos parametros passados ao cucumber!");
                }
            }
        }
    }
}
