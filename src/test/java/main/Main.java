package main;

import org.junit.runner.JUnitCore;
import runner.CustomCucumber.CustomRuntimeOptionsFactory;

import java.util.Arrays;
import java.util.List;
import static main.Utils.Utils.split;

public class Main {
    /**
     * <b>Construtor para inicializar</b>
     *
     * @param args <i>variable a inserir</i>
     */
    public static void main(String[] args) {
        setUpTags(args);
        JUnitCore.main(
                "runner.RunTest");
    }

    /**
     * <b>Separa as tags de execucao do --id da unidade</b>
     *
     * @param args <i>variable a inserir</i>
     */
    private static void setUpTags(String[] args) {
        String runTags = Arrays.toString(args).replace("[", "").replace("]", "");
        List<String> paramsInfo;

        //Verifica se o id da unidade foi informado
        if (runTags.contains("--id=")) {
            //ID UNIDADE
            paramsInfo = split(runTags, "--id=");
            CustomRuntimeOptionsFactory.setIdUnidade("@" + paramsInfo.get(1));
            //TAGS
            if (!paramsInfo.get(0).isEmpty()) {
                CustomRuntimeOptionsFactory.setRunTagsArgs(
                        paramsInfo.get(0).substring(0, paramsInfo.get(0).length() - 2));
            }
        } else {
            System.err.println("Informe o id (--id=) da unidade para iniciar a aplicação!");
            System.exit(0);
        }
    }

}
