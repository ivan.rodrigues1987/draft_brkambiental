package main.Utils;

import cucumber.api.DataTable;
import org.openqa.selenium.Keys;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static com.google.common.math.IntMath.mod;
import static framework.ConfigFramework.getActions;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.getInstance;

/**
 * <b>Classe abstrata para metodos uteis do sistema</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public abstract class Utils {

    /**
     * <b>Retorna os campos do datatable dentro do objeto da classe passada como parametro</b>
     *
     * @param table      <i>valores presentes no BDD</i>
     * @param auxClass   <i>classe a ser preenchida</i>
     * @param <T>        <i>classe a ser preenchida</i>
     * @return auxClass - <i>classe com os do datatable valores preenchidos</i>
     */
    public static <T> T getDataTable(DataTable table, Class<T> auxClass) {
        return table.asList(auxClass).get(0);
    }

    /**
     * <b>Desce cursor para baixo nos menus</b>
     */
    public static void scrollDownMenu() {
        getActions().sendKeys(Keys.ARROW_UP).build().perform();
    }

    /**
     * <b>Retorna um CPF gerado aleatoriamente</b>
     *
     * @param mask <i>CPF com mascara(000.000.000-00)</i>
     * @return String - <i>CPF aleatorio</i>
     */
    public static String getCpf(boolean mask) {
        //int n = 9; varialvel nunca é usada
        int n1 = createOneDigitRandomNumber();
        int n2 = createOneDigitRandomNumber();
        int n3 = createOneDigitRandomNumber();
        int n4 = createOneDigitRandomNumber();
        int n5 = createOneDigitRandomNumber();
        int n6 = createOneDigitRandomNumber();
        int n7 = createOneDigitRandomNumber();
        int n8 = createOneDigitRandomNumber();
        int n9 = createOneDigitRandomNumber();
        int d1 = n9 * 2 + n8 * Cpf.NUMERO_TRES.getNumero() + n7 * Cpf.NUMERO_QUATRO.getNumero()
                + n6 * Cpf.NUMERO_CINCO.getNumero() + n5 * Cpf.NUMERO_SEIS.getNumero()
                + n4 * Cpf.NUMERO_SETE.getNumero() + n3 * Cpf.NUMERO_OITO.getNumero()
                + n2 * Cpf.NUMERO_NOVE.getNumero() + n1 * Cpf.NUMERO_DEZ.getNumero();

        d1 = Cpf.NUMERO_ONZE.getNumero() - (mod(d1, Cpf.NUMERO_ONZE.getNumero()));

        if (d1 >= Cpf.NUMERO_DEZ.getNumero()) {
            d1 = 0;
        }
        int d2 = d1 * 2 + n9 * Cpf.NUMERO_TRES.getNumero() + n8 * Cpf.NUMERO_QUATRO.getNumero()
                + n7 * Cpf.NUMERO_CINCO.getNumero() + n6 * Cpf.NUMERO_SEIS.getNumero()
                + n5 * Cpf.NUMERO_SETE.getNumero() + n4 * Cpf.NUMERO_OITO.getNumero()
                + n3 * Cpf.NUMERO_NOVE.getNumero() + n2 * Cpf.NUMERO_DEZ.getNumero() + n1 * Cpf.NUMERO_ONZE.getNumero();

        d2 = Cpf.NUMERO_ONZE.getNumero() - (mod(d2, Cpf.NUMERO_ONZE.getNumero()));

        String retorno = null;

        if (d2 >= Cpf.NUMERO_DEZ.getNumero()) {
            d2 = 0;
        }
        retorno = "";

        if (mask) {
            retorno = "" + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
        } else {
            retorno = "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;
        }
        return retorno;
    }

    /**
     * <b>Retorna um CNPJ gerado aleatoriamente</b>
     *
     * @param mask <i>CNPJ com mascara(00.000.000/0000-00)</i>
     * @return String - <i>CNPJ aleatorio</i>
     */
    public static String getCnpj(boolean mask) {
        int n1 = createOneDigitRandomNumber();
        int n2 = createOneDigitRandomNumber();
        int n3 = createOneDigitRandomNumber();
        int n4 = createOneDigitRandomNumber();
        int n5 = createOneDigitRandomNumber();
        int n6 = createOneDigitRandomNumber();
        int n7 = createOneDigitRandomNumber();
        int n8 = createOneDigitRandomNumber();
        int n9 = 0;
        int n10 = 0;
        int n11 = 0;
        int n12 = 1;
        int d1 = n12 * 2 + n11 * Cnpj.NUMERO_TRES.getNumero() + n10 * Cnpj.NUMERO_QUATRO.getNumero()
                + n9 * Cnpj.NUMERO_CINCO.getNumero() + n8 * Cnpj.NUMERO_SEIS.getNumero()
                + n7 * Cnpj.NUMERO_SETE.getNumero() + n6 * Cnpj.NUMERO_OITO.getNumero()
                + n5 * Cnpj.NUMERO_NOVE.getNumero() + n4 * 2 + n3 * Cnpj.NUMERO_TRES.getNumero()
                + n2 * Cnpj.NUMERO_QUATRO.getNumero() + n1 * Cnpj.NUMERO_CINCO.getNumero();

        d1 = Cnpj.NUMERO_ONZE.getNumero() - (mod(d1, Cnpj.NUMERO_ONZE.getNumero()));

        if (d1 >= Cnpj.NUMERO_DEZ.getNumero()) {
            d1 = 0;
        }
        int d2 = d1 * 2 + n12 * Cnpj.NUMERO_TRES.getNumero() + n11 * Cnpj.NUMERO_QUATRO.getNumero()
                + n10 * Cnpj.NUMERO_CINCO.getNumero() + n9 * Cnpj.NUMERO_SEIS.getNumero()
                + n8 * Cnpj.NUMERO_SETE.getNumero() + n7 * Cnpj.NUMERO_OITO.getNumero()
                + n6 * Cnpj.NUMERO_NOVE.getNumero() + n5 * 2 + n4 * Cnpj.NUMERO_TRES.getNumero()
                + n3 * Cnpj.NUMERO_QUATRO.getNumero() + n2 * Cnpj.NUMERO_CINCO.getNumero()
                + n1 * Cnpj.NUMERO_SEIS.getNumero();

        d2 = Cnpj.NUMERO_ONZE.getNumero() - (mod(d2, Cnpj.NUMERO_ONZE.getNumero()));

        if (d2 >= Cnpj.NUMERO_DEZ.getNumero()) {
            d2 = 0;
        }
        String retorno = null;

        if (mask) {
            retorno = "" + n1 + n2 + "." + n3 + n4 + n5 + "." + n6 +
                    n7 + n8 + "/" + n9 + n10 + n11 + n12 + "-" + d1 + d2;
        } else {
            retorno = "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + d1 + d2;
        }
        return retorno;
    }

    /**
     * <b>Gera um texto com uma quantidade aleatoria de caracteres</b>
     *
     * @param passLength <i>quantidade de caracteres</i>
     * @return String - <i>texto aleatorio</i>
     */
    public static String geradorCharRandom(int passLength) {
        java.util.Random r = new java.util.Random();
        char[] goodChar = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1',
                '2', '3', '4', '5', '6', '7', '8', '9', };
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < passLength; i++) {
            sb.append(goodChar[r.nextInt(goodChar.length)]);
        }
        return sb.toString();
    }

    /**
     * <b>Gera um numero aleatorio</b>
     *
     * @return int - <i>numero aleatorio</i>
     */
    public static int createOneDigitRandomNumber() {
        final int dez = 10;
        int numero = (int) (Math.random() * dez);
        return numero;
    }

    /**
     * <b>Gera um numero com uma quantidade aleatoria de digitos</b>
     *
     * @param passLength <i>quantidade de digitos</i>
     * @return String - <i>numero aleatorio</i>
     */
    public static String geradorNumberRandom(int passLength) {
        java.util.Random r = new java.util.Random();
        char[] goodChar = {'1', '2', '3', '4', '5', '6', '7', '8', '9', };
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < passLength; i++) {
            sb.append(goodChar[r.nextInt(goodChar.length)]);
        }
        return sb.toString();
    }

    /**
     * <b>Retornar data atual</b>
     *
     * @param separador <i>separador</i>
     * @return String - <i>data com separador</i>
     */
    public static String retornarDataAtual(String separador) {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("dd" + separador + "LL" + separador + "yyyy"));
    }

    /**
     * <b>Retira caracteres do texto</b>
     *
     * @param texto <i>texto</i>
     * @return String - <i>texto com apenas os numeros</i>
     */
    public static String getSomenteNumeros(String texto) {
        return texto.replaceAll("[^0-9]", "");
    }

    /**
     * <b>Retira a marcacao [] do BDD</b>
     *
     * @param texto <i>texto</i>
     * @return String - <i>texto sem a marcacao</i>
     */
    public static String retirarMarcacao(String texto) {
        return texto.replaceAll("[\\[\\]]", "");
    }

    /**
     * <b>Verifica se o texto tem as marcacoes '[]'</b>
     *
     * @param texto <i>texto</i>
     * @return boolean - <i>retorna se o texto tem a marcacao de informacao dinamica '[]'</i>
     */
    public static boolean contemMarcacao(String texto) {
        return (texto.contains("[") && texto.contains("]"));
    }

    /**
     * <b>Soma/subtrai meses de uma data</b>
     *
     * @param data    <i>data</i>
     * @param meses   <i>meses a serem adicionados ou subtraidos</i>
     * @param pattern <i>formato da data</i>
     * @return String - <i>data</i>
     * @throws ParseException - <i>parseException</i>
     */
    public static String somarMesesData(String data, int meses, String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Calendar calendar = getInstance();
        calendar.setTime(simpleDateFormat.parse(data));
        calendar.add(MONTH, meses);
        return simpleDateFormat.format(calendar.getTime());
    }

    /**
     * <b>Soma/subtrai meses de uma data</b>
     *
     * @param data    <i>data</i>
     * @param dias   <i>dias a serem adicionados ou subtraidos</i>
     * @param pattern <i>formato da data</i>
     * @return String - <i>data</i>
     * @throws ParseException - <i>parseException</i>
     */
    public static String alterarDataEmDias(String data, int dias, String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Calendar calendar = getInstance();
        calendar.setTime(simpleDateFormat.parse(data));
        calendar.add(Calendar.DAY_OF_MONTH, dias);
        return simpleDateFormat.format(calendar.getTime());
    }

    /**
     * <b>Quebra o texto em varias Strings de acordo com separador</b>
     *
     * @param texto     <i>texto</i>
     * @param separador <i>separador</i>
     * @return List - <i>texto separado</i>
     */
    public static List<String> split(String texto, String separador) {
        return Arrays.asList(texto.split("s*" + separador + "s*"));
    }

    /**
     * <b>Verifica se a String esta vazia ou nula</b>
     *
     * @param texto <i>valor a ser verificado</i>
     * @return boolean <i>true para vazio, false para preenchido</i>
     */
    public static boolean isBlank(String texto) {
        if (texto == null) {
            return true;
        } else if (texto.isEmpty()) {
            return true;
        } else if (texto.equals("")) {
            return true;
        }
        return false;
    }

    /**
     * <b>Verifica se o valor de uma String existe dentro da outra ignorando o tamanho dos caracteres</b>
     *
     * @param texto  <i>String base</i>
     * @param trecho <i>valor a ser buscado na String base</i>
     * @return <i>confirmacao da busca</i>
     */
    public static boolean containsIgnoreCase(String texto, String trecho) {
        return texto.toUpperCase().contains(trecho.toUpperCase());
    }

    /**
     * <b>Retorna caminho dos recursos</b>
     *
     * @return <i>caminho</i>
     */
    public static String getResourcesPath() {
        return System.getProperty("user.dir") + File.separator + "src" + File.separator +
                "test" + File.separator + "resources" + File.separator + "files" + File.separator;
    }

    /**
     * <b> Retorna objeto que contem a uniao dos valores de dois objetos dando
     * prioridade para os dados do primeiro objeto</b>
     *
     * @param <T>       <i>object personalizado</i>
     * @param first     <i>primeiro objeto</i>
     * @param second    <i>segundo objeto</i>
     * @return Object   <i>objeto pos merge</i>
     * @throws IllegalAccessException -     <i>IllegalAccessException</i>
     * @throws InstantiationException -     <i>InstantiationException</i>
     * @throws NoSuchMethodException -      <i>NoSuchMethodException</i>
     * @throws InvocationTargetException -  <i>InvocationTargetException</i>
     */
    public static <T> Object mergeObjects(T first, T second) throws
            IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        if (first.equals(second)) {
            return first;
        }
        Class<?> clazz = first.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Object returnValue = clazz.getConstructor().newInstance();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value1 = field.get(first);
            Object value2 = field.get(second);
            Object value = (value1 != null) ? value1 : value2;
            field.set(returnValue, value);
        }
        return (T) returnValue;
    }

    /**
     * <b>Retorna codigo cnpj com mascara</b>
     *
     * @param cnpj          <i>codigo cnpj</i>
     * @return              <i>cnpj com mascara</i>
     */
    public static String gerarMascaraCNPJ(String cnpj) {
        return cnpj.substring(0, Cnpj.NUMERO_TRES.getNumero()) + "." + cnpj.substring(Cnpj.NUMERO_TRES.getNumero(),
                Cnpj.NUMERO_SEIS.getNumero()) + "." + cnpj.substring(Cnpj.NUMERO_SEIS.getNumero(),
                Cnpj.NUMERO_NOVE.getNumero()) + "/" +
                cnpj.substring(Cnpj.NUMERO_NOVE.getNumero(), Cnpj.NUMERO_TREZE.getNumero()) + "-" +
                cnpj.substring(Cnpj.NUMERO_TREZE.getNumero(), Cnpj.NUMERO_QUINZE.getNumero());
    }

    /**
     * <b>Retorna codigo cpf com mascara</b>
     *
     * @param cpf           <i>codigo cpf</i>
     * @return              <i>cpf com mascara</i>
     */
    public static String gerarMascaraCPF(String cpf) {
        return cpf.substring(0, Cpf.NUMERO_TRES.getNumero()) + "." + cpf.substring(Cpf.NUMERO_TRES.getNumero(),
                Cpf.NUMERO_SEIS.getNumero()) + "." + cpf.substring(Cpf.NUMERO_SEIS.getNumero(),
                Cpf.NUMERO_NOVE.getNumero()) + "-" + cpf.substring(Cpf.NUMERO_NOVE.getNumero(),
                Cpf.NUMERO_ONZE.getNumero());
    }

}
