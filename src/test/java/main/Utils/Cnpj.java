package main.Utils;

public enum Cnpj {
    NUMERO_TRES(3),
    NUMERO_QUATRO(4),
    NUMERO_CINCO(5),
    NUMERO_SEIS(6),
    NUMERO_SETE(7),
    NUMERO_OITO(8),
    NUMERO_NOVE(9),
    NUMERO_DEZ(10),
    NUMERO_ONZE(11),
    NUMERO_TREZE(13),
    NUMERO_QUINZE(15);

    private int numero;

    Cnpj(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }
}
