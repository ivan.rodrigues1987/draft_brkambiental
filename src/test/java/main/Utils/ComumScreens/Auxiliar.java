package main.Utils.ComumScreens;

import framework.BrowserManager.BrowserVerifications;
import framework.ConfigFramework;
import javafx.scene.control.Alert;
import main.WebElementManager.Botoes;
import org.junit.Assume;
import org.openqa.selenium.By;
import static Reports.ExtentReports.appendToReport;
import static framework.AssertManager.assertElementExists;
import static framework.AssertManager.assertElementText;
import static framework.AssertManager.assertElementTextContains;
import static framework.AssertManager.assertElementNotExists;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.setText;
import static framework.BrowserManager.BrowserVerifications.*;
import static framework.BrowserManager.BrowsersManager.loadApplication;
import static main.Utils.Utils.isBlank;
import static main.Utils.Utils.scrollDownMenu;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Combos.ComboInputText.getXPathTextoCombo;
import static main.WebElementManager.Combos.Combos.clicarNoItemCombo;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.A;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.byBotaoFecharJanela;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.LI;
import static main.WebElementManager.Xpath.SPAN;
import static runner.Setup.Json.DadosDinamicos.inicializarDadosUnidade;
import static runner.Setup.Json.ParamsInfo.getUnidadeExecucao;
import static runner.Setup.Json.ParamsInfo.setUnidadeExecucao;

/**
 * <b>Classe abstrata para acoes uteis do sistema</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public abstract class Auxiliar extends ConfigFramework {
    private static boolean loginEfetuado;

    /**
     * <b>Faz o login no sistema</b>
     *
     * @param unidade <i>unidade</i>
     */
    public static void efetuarLogin(String unidade) {
        By xPathUsuario = By.xpath("//input[@id='x-widget-2-input']");
        By xPathSenha = By.xpath("//input[@id='x-widget-3-input']");

        Assume.assumeFalse(unidade.contains("IGNORAR"));
        if (!loginEfetuado) {
            String[] aux = new String[2];
            aux[1] = "";
            if (unidade.contains("CALLCENTER")) {
                aux = unidade.split("-");
                setUnidadeExecucao(aux[0]);
            } else {
                setUnidadeExecucao(unidade);
            }


            loadApplication(getDriver(), getUnidadeExecucao().getUrl());

            //PREENCHE USUARIO E SENHA
            BrowserVerifications.waitElementToBeClickable(xPathUsuario, DEFAULT_TIME_OUT);
            setText(xPathUsuario, getUnidadeExecucao().getUser());
            setText(xPathSenha, getUnidadeExecucao().getPassword());

            //SELECIONA UNIDADE
            clickOnElement(By.xpath("//input[@id='x-widget-4-input']"));
            clicarNoItemCombo(getUnidadeExecucao().getUnidade());

            //BOTÃO ENTRAR
            clicarBotao("Entrar");

            //Inicializa os dados dinamicos da unidade
            if (aux.length < 2) {
                inicializarDadosUnidade("");

            } else {
                inicializarDadosUnidade(aux[1]);
            }
            loginEfetuado = true;
        }
    }

    /**
     * <b>Acessa um submenu</b>
     *
     * @param submenu <i>nome do submenu</i>
     */
    public static void acessarSubMenu(String submenu) {
        By xPathSubmenu = getXPathElementTextEquals(A, submenu);

        if (isElementDisplayed(xPathSubmenu)) {
            clickOnElement(xPathSubmenu);
        } else {
            scrollDownMenu();
            clickOnElement(xPathSubmenu);
        }
    }

    /**
     * <b>Acessa um menu e submenu</b>
     *
     * @param menu    <i>nome do menu</i>
     * @param submenu <i>nome do submenu</i>
     */
    public static void acessarMenu(String menu, String submenu) {
        clickOnElement(getXPathElementTextEquals(DIV, menu));
        acessarSubMenu(submenu);
        aguardarTelaAbrir(menu);
    }

    /**
     * <b>Clica em fechar na tela atual do sistema</b>
     */
    public static void fecharTela() {
        clickOnElement(byBotaoFecharJanela(1));
    }

    /**
     * <b>Clica em fechar em uma tela do sistema</b>
     *
     * @param tela <i>tela a ser fechada</i>
     */
    public static void fecharTela(int tela) {

        clickOnElement(byBotaoFecharJanela(tela));
    }

    /**
     * <b>Clica em fechar em uma tela do sistema</b>
     *
     * @param tela <i>nome da tela a ser fechada</i>
     */
    public static void fecharTela(String tela) {
        clickOnElement(By.xpath("//div[contains(text(),'" + tela + "')]/preceding-sibling::div/table/tbody/tr/td/div[" +
                                        "@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCKEC']"));
    }

    /**
     * <b>Clica em maximizar na tela atual do sistema</b>
     */
    public static void maximizarTela() {
        clickOnElement(By.xpath("//div[@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCOFC']"));
    }

    /**
     * <b>Verifica se a mensagem foi exibida na janela de mensagem</b>
     *
     * @param janela        <i>nome da janela de mensagem</i>
     * @param mensagem      <i>mensagem a ser exibida</i>
     * @param textoIdentico <i>texto a validar</i>
     */
    public static void verificarMensagemJanela(String janela, String mensagem, boolean textoIdentico) {
        //IF específico para o CT011 de instalação de medidor
        if (getDriver().getPageSource().contains("Confirma o cadastro de")){
            clicarBotao("Sim");
        }

        switch (janela) {
            case "Atenção":
                assertElementExists(getXPathElementTextEquals(DIV, mensagem));
                appendToReport(getDriver());
                if (mensagem.contains("Registro salvo com sucesso")
                        || mensagem.contains("Medidor trocado com sucesso.")) {
                    clicarBotao("Ok");
                }
                break;
            case "Mensagem do Sistema":
                By xPath = By.xpath("//b[text()='Mensagem']/../../../../div/div/div/label");
                if (textoIdentico) {
                    assertElementText(xPath, mensagem);
                } else {
                    assertElementTextContains(xPath, mensagem);
                }

                appendToReport(getDriver());
                break;
            case "Ordem Serviço - Duplicadas":
                assertElementExists(getXPathElementTextEquals(DIV, mensagem));
                appendToReport(getDriver());
                break;
            default:
                break;
        }
    }

    /**
     * <b>Verifica se a mensagem apareceu na tela</b>
     *
     * @param mensagem <i>mensagem a ser exibida</i>
     */
    public static void verificarMensagemTela(String mensagem) {
        assertElementExists(getXPathElementWithText(LI, mensagem));
    }

    /**
     * <b>Verifica se a mensagem apareceu na tela</b>
     */
    public static void verificarMensagemTela() {
        assertElementExists(By.xpath("//li[@class='infoMessage']"));
        appendToReport(getDriver());
    }

    /**
     * <b>Verifica se a tela esta fechada</b>
     *
     * @param tela <i>nome da tela</i>
     */
    public static void verificarTelaFechada(String tela) {
        assertElementNotExists(getXPathElementTextEquals(DIV, tela));
    }

    /**
     * <b>Clica na aba</b>
     *
     * @param aba <i>nome da aba</i>
     */
    public static void clicarAba(String aba) {
        clickOnElement(getXPathElementTextEquals(SPAN, aba));
    }

    /**
     * <b>Retorna o campo com um valor</b>
     *
     * @param campoBDD <i>parametro passado no BDD</i>
     * @param campo    <i>parametro dinamico</i>
     * @return String - <i>valor do campo a ser preenchido</i>
     */
    public static String getCampo(String campoBDD, String campo) {
        if (campoBDD.contains("[") && campoBDD.contains("]")) {
            appendToReport(campoBDD + " " + campo);
        } else {
            return campoBDD; //Se nao possuir  [] retorna o valor normalmente
        }
        return campo;
    }

    /**
     * <b>Retorna o campo com um valor, validando se esta vazio</b>
     *
     * @param campoBDD <i>parametro passado no BDD</i>
     * @param campo    <i>parametro dinamico</i>
     * @return String - <i>valor do campo a ser preenchido</i>
     */
    public static String getCampoValidado(String campoBDD, String campo) {
        if (isBlank(campoBDD)) {
            if (isBlank(campo)) {
                campo = " ";
            }
        } else {
            if (isBlank(campo)) {
                campo = getCampo(campoBDD, "");
            } else {
                campo = getCampo(campoBDD, campo);
            }
        } // else
        return campo;
    } //getCampoValidado

    /**
     * <b>Aguarda a tela abrir</b>
     *
     * @param menu <i>menu acessado</i>
     */
    private static void aguardarTelaAbrir(String menu) {
        if (menu.equals("Cadastro")) {
            waitElementToBeVisible(By.xpath("//div[@class='GKPSPJYCMP']//div[1]//div[1]//table[1]//tbody[1]//tr[2]" +
                               "//td[2]//div[1]//div[1]//table[1]//tbody[1]//tr[1]//td[1]//img[1]"), DEFAULT_TIME_OUT);
        }
    }

    /**
     * <b>Confere se uma combo foi alterada com o valor esperado</b>
     *
     * @param combo               <i>nome da combo</i>
     * @param valorEsperado       <i>valor a ser esperado</i>
     * @param validarDesabilitada <i>aguardar a combo ser habilitada para validar</i>
     */
    public static void validarComboAlterada(String combo, String valorEsperado, boolean validarDesabilitada) {
        if (validarDesabilitada) {
            waitElementToBeEnable(combo, DEFAULT_TIME_OUT);
        }
        waitElementAttributeValue(getXPathTextoCombo(combo), "value", valorEsperado, DEFAULT_TIME_OUT);
    }

}





