package main.Utils.ComumScreens;

import stepdefinition.Cadastro.Arrecadador.Arrecadador;
import stepdefinition.Cadastro.Cliente.Cliente;
import stepdefinition.Cadastro.Ligacao.Ligacao;
import stepdefinition.Cadastro.Rubrica.Rubrica;
import stepdefinition.CallCenter.CadastroGestao.CadastroGestao;
import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio;
import main.WebElementManager.InputText.InputText;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Reports.ExtentReports.appendToReport;
import static framework.AssertManager.assertElementExists;
import static framework.AssertManager.assertNumberOfElementsMoreThan;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserVerifications.elementExists;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.retirarMarcacao;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Combos.ComboInputText.setComboEPressionarEnter;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.SPAN;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * <b>Classe para acoes na tela de pesquisa rapida</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public class PesquisaRapida {

    private static String valorPesquisado = "";

    /**
     * <b>Pesquisa escrevendo o texto e clicando enter</b>
     *
     * @param valor <i>texto a ser escrito</i>
     */
    public static void pesquisar(String valor) {
        InputText.setInputTextEPressionarEnter(valor);
    }

    /**
     * <b>Retorna pesquisa simple para Telas que tem Cidade, campo, valor. Valor por defeito é 3</b>
     *
     * @param cidade <i>cidade desejada</i>
     * @param campo <i>campo a ser selecionado para pesquisa</i>
     * @param valor <i>texto a ser escrito</i>
     */
    public static void pesquisar(String cidade, String campo, String valor) {
        final int indice = 3;
        preencherComboCidadePesquisaRapida("Cidade", indice, getCampo(cidade,
                getDadosCadastro().getEndereco().getCidade()));
        pesquisarSimpleBotao(campo, valor);
    }


    /**
     * <b>Pesquisa selecionando o tipo de informacao, escrevendo o texto e clicando enter</b>
     *
     * @param campo <i>campo a ser selecionado para pesquisa</i>
     * @param valor <i>texto a ser escrito</i>
     */
    public static void pesquisar(String campo, String valor) {
        setComboClicando("Campo", campo);
        if (!valor.isEmpty()) {
            sendKeys(Keys.TAB);
            InputText.setInputTextEPressionarEnter(valor);
        }
    }

    /**
     * <b>Pesquisa selecionando o tipo de informacao, escrevendo o texto e clicando enter</b>
     *
     * @param campo <i>campo a ser selecionado para pesquisa</i>
     * @param valor <i>texto a ser escrito</i>
     * @param qtdDIVs <i>quantidade de DIVs</i>
     */
    public static void pesquisar(String campo, String valor, int qtdDIVs) {
        setComboClicando("Campo", campo);
        if (!valor.isEmpty()) {
            InputText.setInputText("Valor", valor, qtdDIVs);
            sendKeys(Keys.ENTER);
        }
    }

    /**
     * <b>Preenche os campos de pesquisa na aba de consulta completa</b>
     *
     * @param campo    <i>campo a ser selecionado para pesquisa</i>
     * @param operador <i>operador</i>
     * @param valor    <i>texto a ser escrito</i>
     */
    public static void preencherConsultaCompleta(String campo, String operador, String valor) {
        setComboClicando("Campo", 2, campo);
        setComboClicando("Operador", operador);

        InputText.setInputText("Valor", getCampo(valor, getCampoPesquisado(valor)), true);
    }

    /**
     * <b>Seleciona o resultado no grid de pesquisa</b>
     *
     * @param valor  <i>valor a ser selecionado</i>
     * @param clicar <i>botao a ser clicado para carregar o resultado da pesquisa</i>
     */
    public static void clicarResultadoPesquisa(String valor, String clicar) {
        clickOnElement(getXPathElementWithText(DIV, valor));
        clicarBotao(clicar);
    }

    /**
     * @param campo <i>campo buscado no resultado</i>
     * @param linha <i>linha a ser alterada</i>
     */
    public static void alterarCampoResultado(String campo, String linha) {

    }

    /**
     * <b>Verifica se o resultado no grid de pesquisa apareceu</b>
     *
     * @param pesquisa <i>valor que deve estar presente no resultado da pesquisa</i>
     */
    public static void verificarResultadoGridPesquisa(String pesquisa) {
        assertElementExists(getXPathElementWithText(DIV, pesquisa));
        appendToReport(getDriver());
    }

    /**
     * <b>Verifica se há algum resultado no grid de pesquisa</b>
     */
    public static void verificarSeExisteResultadoGridPesquisa() {
        assertNumberOfElementsMoreThan(By.xpath("//tbody[2]/tr/td"), 1);
        appendToReport(getDriver());
    }

    /**
     * <b>Retorna os valores dinamicos para pesquisa</b>
     *
     * @param valor <i>valor a ser localizado</i>
     * @return String -  <i>valor que deve estar presente no resultado da pesquisa</i>
     */
    public static String getCampoPesquisado(String valor) {
        final String cadastrado = " CADASTRADO";
        final String cadastradoAcima = cadastrado + " ACIMA";
        String campoPesquisa = retirarMarcacao(valor);

        switch (campoPesquisa.toUpperCase()) {
            //ARRECADADOR
            case "NUMERO BANCO" + cadastrado:
                return getDadosCadastro().getArrecadador().getArrecadadorAtivo().getNrBanco();
            case "ARRECADADOR" + cadastrado:
                return getDadosCadastro().getArrecadador().getArrecadadorAtivo().getArrecadador();
            case "ARRECADADOR" + cadastradoAcima:
                return Arrecadador.getArrecadadorCadastrado();
            case "ARRECADADOR INATIVO":
                return getDadosCadastro().getArrecadador().getArrecadadorInativo().getArrecadador();

            //CLIENTE
            //Sem ligacoes
            case "CLIENTE" + cadastradoAcima:
                return Cliente.getNomeCadastrado();
            case "NOME CPF SEM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCpfSemLigacoes().getNome();
            case "CODIGO CPF SEM LIGACOES":
                return getDadosCadastro().getCliente().getCodFisicoSL();
            case "CPF SEM LIGACOES":
                return getDadosCadastro().getCliente().getCpfFisicoSL();
            case "CNPJ SEM LIGACOES":
                return getDadosCadastro().getCliente().getCnpjJuridicoSL();
            case "CODIGO CNPJ SEM LIGACOES":
                return getDadosCadastro().getCliente().getCodJuridicoSL();
            case "CODIGO CPF COM DEBITO":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCpfComDebito().getCodigo();
            case "CODIGO CNPJ COM DEBITO":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCnpjComDebito().getCodigo();
            //Com ligacoes
            case "CPF COM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCpfComLigacoes().obterCpf();
            case "CNPJ COM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCnpjComLigacoes().obterCnpj();
            case "CODIGO CPF COM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCpfComLigacoes().getCodigo();
            case "CODIGO CNPJ COM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCnpjComLigacoes().getCodigo();
            case "CODIGO CLIENTE INATIVO":
                return getDadosCadastro().getCliente().getCodClienteLigacoesInactivo();
            case "CODIGO CLIENTE ATIVO":
                return getDadosCadastro().getCliente().getCodClienteLigacoesActivo();

            //INSTALACAO MEDIDOR
            case "LIGACAO COM MEDIDOR ATIVO":
                return getDadosCadastro().getInstalacaoMedidor().getLigacaoMedidorAtivo();

            //LIGACAO
            case "LIGACAO CADASTRADA ANTERIORMENTE":
                return Ligacao.getIdLigacaoCadastrada();
            case "LIGACAO CADASTRADA DA MESMA CIDADE":
                return getDadosCadastro().getLigacao().getIdLigacao();
            case "LIGACAO CLIENTE C/ DEBITO":
                return getDadosCadastro().getLigacao().getIdLigacaoClienteDebito();
            case "LIGACAO CODIGO CNPJ SEM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCnpjSemLigacoes().getNome();
            case "LIGACAO CODIGO CPF SEM LIGACOES":
                return getDadosCadastro().getCliente().getTiposDeCliente().getCpfSemLigacoes().getNome();

            //RETENCAO FORCADA FATURA
            case "LIGACAO SEM MENSAGEM CADASTRADA":
                return getDadosCadastro().getRetencaoForcadaFatura().getLigacoes();
            case "LIGACAO MENSAGEM CADASTRADA":
                return getDadosCadastro().getRetencaoForcadaFatura().getLigacaoJaCadastrada();
            case "ID MENSAGEM CADASTRADA":
                return getDadosCadastro().getRetencaoForcadaFatura().getIdMensagemCadastrada();
            case "MENSAGEM CADASTRADA":
                return getDadosCadastro().getRetencaoForcadaFatura().getMensagemCadastrada();

            //MEDIDOR
            case "MEDIDOR DISPONIVEL":
                return getDadosCadastro().getInstalacaoMedidor().getMedidorDisponivel().getNumeroMedidor();

            //RUBRICA
            case "RUBRICA" + cadastradoAcima:
                return Rubrica.getCodigoRubrica();
            case "RUBRICA" + cadastrado:
                return getDadosCadastro().getRubrica().getCodigoCadastrado();
            case "RUBRICA NOME":
                return getDadosCadastro().getRubrica().getDescricaoCadastrado();
            case "RUBRICA EXIBE DESCRICAO":
                return Rubrica.getDescricaoExibe();
            case "RUBRICA DESCRICAO":
                return Rubrica.getDescricaoRubrica();

            //EXCECAO
            case "EXCECAO CADASTRADO":
                return getDadosCadastro().getExcecao().getCodigoCadastrado();

            //BAIRRO
            case "BAIRRO CADASTRADO":
                return Ligacao.getNomeBairro();

            //LOGRADOURO
            case "LOGRADOURO CADASTRADO":
                return Ligacao.getNomeLogradouro();

            //GRUPO
            case "GRUPO CADASTRADO":
                return Ligacao.getNomeGrupo();

            //SUBCATEGORIA
            case "SUBCATEGORIA CADASTRADO":
                return Ligacao.getNomeSubCategoria();

            //CALL CENTER
            case "GESTAO CADASTRADA ANTERIORMENTE":
                return CadastroGestao.getGestaoCadastrada();
            case "GESTAO UNIDADE NEGOCIO CADASTRADA ANTERIORMENTE":
                return GestaoUnidadeNegocio.getGestaoUnidadeNegocioCadastrada();
            default:
                break;
        }
        return valor;
    }

    /**
     * <b>Preenche combo cidade para tela de pesquisa simple com campos cidade, campo e valor</b>
     *
     * @param combo  <i>Local a ser pesquisado</i>
     * @param indice <i>Valor a ser pesquisado</i>
     * @param valor  <i>Botao de confirmacao</i>
     */
    public static void preencherComboCidadePesquisaRapida(String combo, int indice, String valor) {
        if (!getDadosCadastro().isCidadeUnica()) {
            setComboClicando(combo, indice, valor);
        }
    }

    /**
     * <b>Pesquisa quando tem o botao perto do input text</b>
     *
     * @param campo <i>campo a ser selecionado para pesquisa</i>
     * @param valor <i>texto a ser escrito</i>
     */
    public static void pesquisarSimpleBotao(String campo, String valor) {
        setComboClicando("Campo", campo);
        if (!valor.isEmpty()) {
            if (!campo.contains("Codigo")) {
                sendKeys(Keys.TAB);
                InputText.setInputTextEPressionarEnter(valor);
            } else {
                sendKeys(Keys.TAB);
                final int qtdDivs = 5;
                setComboEPressionarEnter("Valor", valor, qtdDivs);
            }
        }
    }

    /**
     * <b>Clica no botao Pesquisa Rapida e repete a acao ate aparecer a tela consulta simples</b>
     *
     * @param by <i>caminho botao</i>
     */
    public static void clicarPesquisaRapida(By by) {
        WebDriverWait wait = new WebDriverWait(getDriver(), DEFAULT_TIME_OUT);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                if (!elementExists(getXPathElementWithText(SPAN, "Consulta Simples"))) {
                    clickOnElement(by);
                    return false;
                }
                return true;
            }
        });
    }


}
