package main.Utils.ComumScreens;


import framework.BrowserManager.BrowserVerifications;
import main.WebElementManager.InputText.InputText;
import main.WebElementManager.Xpath;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserVerifications.waitElementToBeEnable;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.WebElementManager.Botoes.clicarBotaoPesquisaAzul;
import static main.WebElementManager.Botoes.clicarBotaoPesquisar;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;

/**
 * <b>Classe para acoes na tela de consulta personalizada</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class ConsultaPersonalizada extends PesquisaRapida {
    private By xPathFiltrarPor = Xpath.getXPathElementWithText(Xpath.LABEL, "Filtrar por");

    /**
     * <b>Enum de botoes de consulta personalizada</b>
     */
    public enum LupasConsulta {
        LUPA_AZUL,
        LUPA_COMUM;

        LupasConsulta() {

        }
    }

    /**
     * <b>Construtor</b>
     */
    public ConsultaPersonalizada() {


    }

    /**
     * <b>Construtor</b>
     *
     * @param campo <i>campo a ser pesquisado</i>
     * @param lupa  <i>Botao de confirmacao</i>
     */
    public ConsultaPersonalizada(String campo, LupasConsulta lupa) {

        if (!campo.equals("Ligação")) {
            waitElementToBeEnable(campo, DEFAULT_TIME_OUT);
        }
        //CLICA BOTAO PESQUISA
        switch (lupa) {
            case LUPA_AZUL:
                clicarBotaoPesquisaAzul(campo);
                break;
            case LUPA_COMUM:
                clicarBotaoPesquisar(campo);
                break;
            default:
                //break;
        }
    }

    /**
     * <b>Efetua uma pesquisa personalizada e clica no resultado</b>
     *
     * @param valor <i>valor a ser pesquisado</i>
     * @param botao <i>botao de confirmacao</i>
     */
    public void efetuarPesquisaPersonalizada(String valor, String botao) {
        By by = getInputTextXPath("Pesquisar");
        String valorPesquisado = getCampo(valor, (getCampoPesquisado(valor)));
        BrowserVerifications.waitElementToBeClickable(by, DEFAULT_TIME_OUT);
        //LIMPA CAMPO PREVIAMENTE E PESQUISA
        if (!valor.equals(getTextAttribute(getInputTextXPath("Pesquisar"), "value"))) {
            clearField(getInputTextXPath("Pesquisar"));
            clickOnElement(getInputTextXPath("Pesquisar"));
        }

        //VALOR
        InputText.setInputTextEPressionarEnter(valorPesquisado);
        //SELECIONA RESULTADO PESQUISA
        clicarResultadoPesquisa(valorPesquisado, botao);
    } //efetuarPesquisaPersonalizada

    /**
     * <b>Efetua uma pesquisa personalizada e clica no resultado</b>
     *
     * @param filtrar <i>filtro a ser utilizado</i>
     * @param valor   <i>valor a ser pesquisado</i>
     * @param botao   <i>botao de confirmacao</i>
     */
    public void efetuarPesquisaPersonalizada(String filtrar, String valor, String botao) {
        //FILTRAR POR
        BrowserVerifications.waitElementToBeVisible(xPathFiltrarPor, DEFAULT_TIME_OUT);
        setComboClicando("Filtrar por", filtrar);
        sendKeys(Keys.TAB);

        //PESQUISA
        efetuarPesquisaPersonalizada(valor, botao);
    }


    /**
     * <b>Efetua uma pesquisa personalizada e clica no resultado</b>
     *
     * @param campo <i>campo onde clicar em pesquisar</i>
     * @param lupa  <i>tipo de lupa do campo</i>
     * @param texto <i>valor a ser pesquisado</i>
     * @param botao <i>botao de confirmacao</i>
     */
    public static void evocarPesquisaPersonalizada(String campo, LupasConsulta lupa, String texto, String botao) {
        ConsultaPersonalizada consultar = new ConsultaPersonalizada(campo, lupa);
        consultar.efetuarPesquisaPersonalizada(texto, botao);
    }

    /**
     * <b>Efetua uma pesquisa personalizada e clica no resultado</b>
     *
     * @param campo   <i>campo onde clicar em pesquisar</i>
     * @param lupa    <i>tipo de lupa do campo</i>
     * @param filtrar <i>filtro a ser utilizado</i>
     * @param valor   <i>valor a ser pesquisado</i>
     * @param botao   <i>botao de confirmacao</i>
     */
    public static void evocarPesquisaPersonalizada(String campo, LupasConsulta lupa,
                                                   String filtrar, String valor, String botao) {
        ConsultaPersonalizada consultar = new ConsultaPersonalizada(campo, lupa);
        consultar.efetuarPesquisaPersonalizada(filtrar, valor, botao);
    }

    /**
     * <b>Retorna o caminho do campo de input text da lupa azul</b>
     *
     * @param campo <i>nome do campo</i>
     * @return By - <i>xPath do campo</i>
     */
    public static By getLupaAzulInputTextXPath(String campo) {
        final int qtdDivs = 4;
        return getInputTextXPath(campo, qtdDivs);
    }
}
