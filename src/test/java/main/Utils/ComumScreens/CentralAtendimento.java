package main.Utils.ComumScreens;

import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.getTextAttribute;

import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.WebElementManager.CallCenter.ByCallCenter.getByIframePorId;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.LABEL;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.SPAN;
import static main.WebElementManager.Xpath.DIV;

/**
 * <b>Classe para acoes na tela de Central de Atendimento</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 20/03/2019
 */
public class CentralAtendimento {

    private static WebDriver browser = getDriver();

    /**
     * <b>metodo para pesquisar ligacao</b>
     *
     * @param ligacao <i>parametro de consulta</i>
     *
     */
    public static void pesquisarLigacao(String ligacao) {
        By inputLigacaoXPath = By.xpath("//div[@class='GKPSPJYCM-']//input[@type='text']");
        String valorPreenchido;

        //CLICA NO CAMPO DE PESQUISA
        BrowserVerifications.waitElementExists(getXPathElementWithText(LABEL, "Consumidor:"), DEFAULT_TIME_OUT);
        clickOnElement(inputLigacaoXPath);

        //EFETUA PESQUISA
        sendKeys(getCampo(ligacao, getCampoPesquisado(ligacao)));
        sendKeys(Keys.ENTER);

        //CONFERE SE O VALOR FOI PREENCHIDO
        valorPreenchido = getTextAttribute(inputLigacaoXPath, "value");
        if (valorPreenchido.isEmpty() || !valorPreenchido.equals(ligacao)) {
            sendKeys(getCampo(ligacao, getCampoPesquisado(ligacao)));
            sendKeys(Keys.ENTER);
        }
    }

    /**
     * <b>metodo para aguardar processo</b>
     *
     * @param campo         <i>nome de campo</i>
     * @param valorEsperado <i>valor esperado</i>
     *
     */
    public static void validarInfoLigacao(String campo, String valorEsperado) {
        BrowserVerifications.waitElementToBeVisible(By.xpath("//label[text()='" + campo +
                ":']/following-sibling::div/div[text()='" + valorEsperado + "']"), DEFAULT_TIME_OUT);
    }

    /**
     * <b>metodo para selecionar grid Nova Venda</b>
     *
     * @param linha             <i>numero de linha</i>
     * @param coluna            <i>numero de coluna</i>
     *
     */
    public static void clicarItemGridNovaVenda(int linha, int coluna) {
        clickOnElement(By.xpath("//tbody[2]/tr[" + linha + "]/td[" + coluna + "]/div[1]"));
    }

    /**
     * <b>metodo para accesar e sair do iframe</b>
     *
     * @param valor             <i>indica se está saindo o não</i>
     *
     */
    public static void trocarIframe(boolean valor) {

        if (valor) {
            browser.switchTo().frame(BrowserVerifications.getElement(getByIframePorId("centralAtendimento")));
        } else {
            browser.switchTo().defaultContent();
        }
    }

    /**
     * <b>metodo para fechar tela de iframe</b>
     *
     */
    public static void fecharJanelaEmissao() {
        if (BrowserVerifications.elementExists(By.xpath("(//div[@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCKEC'])[1]"))) {
            clickOnElement(By.xpath("(//div[@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCKEC'])[1]"));
        } else {
            clickOnElement(By.xpath("(//div[@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCKEC'])[2]"));
        }
    }

    /**
     * <b>metodo para selecionar aba atendimento</b>
     *
     * @param botao             <i>nome do botao</i>
     * @param valor             <i>nome de aba</i>
     *
     */
    public static void selecionarAbaAtendimento(String botao, String valor) {
        switch (botao) {
            case "Protocolos":
                clickOnElement(getXPathElementTextEquals(SPAN, valor, 1));
                break;
            default:
                //break;
        }
    }

    /**
     * <b>metodo para selecionar os botoes do iframe de Central Atendimento</b>
     *
     * @param valor             <i>nome de botao</i>
     *
     */
    public static void botaoCentralAtendimento(String valor) {
        clickOnElement(getXPathElementTextEquals(DIV, valor, 1));
    }
}
