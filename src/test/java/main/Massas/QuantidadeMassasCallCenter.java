package main.Massas;

/**
 * @author Thiago Tolentino P. dos Santos
 * @since 30/04/2019
 */
public class QuantidadeMassasCallCenter {
    private final int gestaoCallCenterTipoAtendimentoSemOS;
    private final int gestaoCallCenterTipoAtendimentoComDebito;
    private  static final int VALOR_GESTAO_SEM_OS = 15;

    /**
     * <b>Construtor</b>
     */
    public QuantidadeMassasCallCenter() {
        this.gestaoCallCenterTipoAtendimentoSemOS = VALOR_GESTAO_SEM_OS;
        this.gestaoCallCenterTipoAtendimentoComDebito = 1;
    }

    public int getGestaoCallCenterTipoAtendimentoSemOS() {
        return gestaoCallCenterTipoAtendimentoSemOS;
    }

    public int getGestaoCallCenterTipoAtendimentoComDebito() {
        return gestaoCallCenterTipoAtendimentoComDebito;
    }
}
