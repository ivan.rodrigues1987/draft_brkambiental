package main.Massas;

/**
 * @author Thiago Tolentino P. dos Santos
 * @since 30/04/2019
 */
public class QuantidadeMassasAtendimento {
    private final int gestaoTipoAtendimentoSemOS;
    private final int gestaoTipoAtendimentoComDebito;
    private  static final int VALOR_GESTAO_SEM_OS = 15;

    /**
     * <b>Construtor</b>
     */
    public QuantidadeMassasAtendimento() {
        this.gestaoTipoAtendimentoSemOS = VALOR_GESTAO_SEM_OS;
        this.gestaoTipoAtendimentoComDebito = 1;
    }

    public int getGestaoTipoAtendimentoSemOS() {
        return gestaoTipoAtendimentoSemOS;
    }

    public int getGestaoTipoAtendimentoComDebito() {
        return gestaoTipoAtendimentoComDebito;
    }
}
