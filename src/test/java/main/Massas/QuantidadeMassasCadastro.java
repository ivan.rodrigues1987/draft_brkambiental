package main.Massas;

/**
 * @author Thiago Tolentino P. dos Santos
 * @since 11/04/2019
 */
public class QuantidadeMassasCadastro {
    private final int analiseAgua;
    private final int arrecadador;
    private final int cadastroMedidor;
    private final int cliente;
    private final int codigoLeitura;
    private final int consolidacaoFatura;
    private final int criticaMedicao;
    private final int cronograma;
    private final int debitoAutomatico;
    private final int documento;
    private final int excecao;
    private final int grupo;
    private final int instalacaoMedidor;
    private final int ligacao;
    private final int retencaoForcadaFatura;
    private final int rubrica;
    private static final int CONSOLIDACAO_FATURA = 27;
    private static final int DEBITO_AUTOMATICO = 4;
    private static final int INSTALACAO_MEDIDOR = 20;
    private static final int LIGACAO_VALOR = 5;
    /**
     * <b>Construtor</b>
     */
    public QuantidadeMassasCadastro() {
        this.analiseAgua = 1;
        this.arrecadador = 1;
        this.cadastroMedidor = 1;
        this.cliente = 1;
        this.codigoLeitura = 1;
        this.consolidacaoFatura = CONSOLIDACAO_FATURA;
        this.criticaMedicao = 1;
        this.cronograma = 1;
        this.debitoAutomatico = DEBITO_AUTOMATICO;
        this.documento = 1;
        this.excecao = 1;
        this.grupo = 1;
        this.instalacaoMedidor = INSTALACAO_MEDIDOR;
        this.ligacao = LIGACAO_VALOR;
        this.retencaoForcadaFatura = 1;
        this.rubrica = 1;
    }

    public int getAnaliseAgua() {
        return analiseAgua;
    }

    public int getArrecadador() {
        return arrecadador;
    }

    public int getCadastroMedidor() {
        return cadastroMedidor;
    }

    public int getCliente() {
        return cliente;
    }

    public int getCodigoLeitura() {
        return codigoLeitura;
    }

    public int getConsolidacaoFatura() {
        return consolidacaoFatura;
    }

    public int getCriticaMedicao() {
        return criticaMedicao;
    }

    public int getCronograma() {
        return cronograma;
    }

    public int getDebitoAutomatico() {
        return debitoAutomatico;
    }

    public int getDocumento() {
        return documento;
    }

    public int getExcecao() {
        return excecao;
    }

    public int getGrupo() {
        return grupo;
    }

    public int getInstalacaoMedidor() {
        return instalacaoMedidor;
    }

    public int getLigacao() {
        return ligacao;
    }

    public int getRetencaoForcadaFatura() {
        return retencaoForcadaFatura;
    }

    public int getRubrica() {
        return rubrica;
    }
}
