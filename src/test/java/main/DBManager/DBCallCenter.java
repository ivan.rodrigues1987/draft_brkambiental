package main.DBManager;

import stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.DBGestaoCallCenterTipoAtendimento;
import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.DBGestaoUnidadeNegocio;
import framework.DataBase.DBPlataform;
import framework.DataBase.DataBase;
import runner.Dados.CallCenter.DadosCallCenter;

import static main.Utils.Utils.isBlank;
import static runner.Setup.Json.ParamsInfo.setUnidadeExecucao;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class DBCallCenter extends DBActions {
    /**
     * <b>Construtor</b>
     */
    public DBCallCenter() {
        super(DBActions.CALLCENTER, false);
    }

    /**
     * <b>Retorna os dados dinamicos para call center</b>
     *
     * @param  unidade -        <i>unidade a selecionar</i>
     * @return DadosCadastro -  <i>dados para cadastros</i>
     * @throws Exception        <i>exception</i>
     * @see DadosCallCenter
     */
    public DadosCallCenter getDadosCallCenter(String unidade) throws Exception {
        //INICIAR DADOS CALLCENTER
        DataBase dataBase = getDbOracle();
        DadosCallCenter dadosCallCenter = new DadosCallCenter();

        //OPEN CONNECTION CALLCENTER
        dataBase.openConnection(DBPlataform.ORACLE);

        //Cadastro Gestao Unidade Negocio CALLCENTER
        dadosCallCenter.setGestaoUnidadeNegocio(new DBGestaoUnidadeNegocio(dataBase).getDadosGestaoUnidadeNegocio());

        //Close connection CALLCENTER
        dataBase.closeConnection();

        if (!isBlank(unidade)) {
            //INICIAR DADOS UNIDADE
            setUnidadeExecucao(unidade);
            DataBase dBUnidade = getDataBase("SAN");

            //OPEN CONNECTION UNIDADE
            dBUnidade.openConnection(DBPlataform.ORACLE);

            //Gestao CallCenter Tipo Atendimento
            dadosCallCenter.setGestaoCallCenterTipoAtendimento(new DBGestaoCallCenterTipoAtendimento(dBUnidade).
                    getDadosGestaoCallCenter());

            dBUnidade.closeConnection();
        }
        return dadosCallCenter;
    }
}
