package main.DBManager;

import framework.DataBase.DBPlataform;
import framework.DataBase.DataBase;
import main.Massas.QuantidadeMassasCadastro;
import runner.Dados.Cadastro.DadosCadastro;
import runner.Dados.Cadastro.Endereco;
import stepdefinition.Cadastro.AnaliseAgua.DBAnaliseAgua;
import stepdefinition.Cadastro.Arrecadador.DBArrecadador;
import stepdefinition.Cadastro.Cliente.DBCliente;
import stepdefinition.Cadastro.ConsolidacaoFatura.DBConsolidacaoFatura;
import stepdefinition.Cadastro.Cronograma.DBCronograma;
import stepdefinition.Cadastro.DebitoAutomatico.DBDebitoAutomatico;
import stepdefinition.Cadastro.Excecao.DBExcecao;
import stepdefinition.Cadastro.InstalacaoMedidor.DBInstalacaoMedidor;
import stepdefinition.Cadastro.Ligacao.DBLigacao;
import stepdefinition.Cadastro.RetencaoForcadaFatura.DBRetencaoForcadaFatura;
import stepdefinition.Cadastro.Rubrica.DBRubrica;

import java.util.ArrayList;

/**
 * <b>Classe para retornar informacoes de Cliente do banco de dados</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class DBAtendimento extends DBCadastro {
    /**
     * <b>Construtor</b>
     */
    public DBAtendimento() {
        super();
    }

}
