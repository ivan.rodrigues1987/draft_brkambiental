package main.DBManager;


import framework.DataBase.DataBase;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static framework.DataBase.DBPlataform.ORACLE;
import static main.Utils.Utils.containsIgnoreCase;
import static runner.Setup.Json.ParamsInfo.CONFIGURACAO_ACESSO;
import static runner.Setup.Json.ParamsInfo.getUnidadeExecucao;

/**
 * <b>Classe para acoes em banco de dados</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class DBActions {
    //Constraints
    public static final String SAN = "SAN";
    public static final String CALLCENTER = "CallCenter";
    public static final String MASTER = "Master";

    //DATABASE Class
    private static DataBase dataBase;
    private static boolean autoconnect = true;

    /**
     * <b>Construtor para inicializar o ambiente do banco</b>
     *
     * @param environment <i>ambiente do banco</i>
     */
    public DBActions(String environment) {
        initializeDataBase(environment);
    }

    /**
     * <b>Construtor para inicializar o ambiente do banco</b>
     *
     * @param dataBase <i>dados do ambiente do banco</i>
     * @see DataBase
     */
    public DBActions(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    /**
     * <b>Construtor para inicializar o ambiente do banco e desabilitar a conexao automatica se necessario</b>
     *
     * @param environment <i>ambiente do banco</i>
     * @param autoconnect <i>se receber false desabilita a conexao automatica</i>
     */
    public DBActions(String environment, boolean autoconnect) {
        initializeDataBase(environment);
        this.autoconnect = autoconnect;
    }

    /**
     * <b>Inicializa o ambiente do banco</b>
     *
     * @param environment <i>ambiente do banco</i>
     */
    private void initializeDataBase(String environment) {
        switch (environment) {
            case SAN:
                dataBase = getUnidadeExecucao().getDataBaseAccess();
                break;
            case CALLCENTER:
                dataBase = CONFIGURACAO_ACESSO.getCallCenter().getDataBaseAccess();
                break;
            case MASTER:
                dataBase = CONFIGURACAO_ACESSO.getDataBaseMaster();
                break;
            default:
                break;
        }
    }

    /**
     * <b>Inicializa o ambiente do banco</b>
     *
     * @param environment <i>ambiente do banco</i>
     * @return DataBase-  <i>retorna unidade desejada</i>
     */
    public DataBase getDataBase(String environment) {
        switch (environment) {
            case SAN:
                return getUnidadeExecucao().getDataBaseAccess();
            case CALLCENTER:
                return CONFIGURACAO_ACESSO.getCallCenter().getDataBaseAccess();
            case MASTER:
                return CONFIGURACAO_ACESSO.getDataBaseMaster();
            default:
                return null;
        }
    }


    /**
     * <b>Retorna a primeira linha do select com as informacoes das colunas solicitadas</b>
     *
     * @param columnsNames <i>nome das colunas separados por virgula</i>
     * @param select       <i>query de select</i>
     * @return ArrayList String - <i>informacao das colunas</i>
     */
    public static ArrayList<String> getColumnsInfo(String columnsNames, String select) {
        try {
            ArrayList<String> campos = new ArrayList<>();
            List<String> columnsNamesList = Arrays.asList(columnsNames.split("s*,s*"));

            if (autoconnect) {
                dataBase.openConnection(ORACLE);
            }

            ResultSet rs = dataBase.select(select);
            if (rs.next()) {
                for (String columnName : columnsNamesList) {
                    campos.add(rs.getString(columnName));
                }
            }
            if (autoconnect) {
                dataBase.closeConnection();
            }

            return campos;
        } catch (Exception sqlException) {
            dbError("Nao foi possivel retornar as informacoes do SELECT: (" + select + ")");
            return null;
        }
    }

    /**
     * <b>Retorna a primeira linha do select com as informacoes das colunas solicitadas</b>
     *
     * @param columnName <i>nome da coluna</i>
     * @param select     <i>query de select</i>
     * @return String - <i>informacao da coluna</i>
     */
    public static String getColumnInfo(String columnName, String select) {
        ArrayList<String> colunsInfo = getColumnsInfo(columnName, select);
        if (colunsInfo.isEmpty()) {
            return "";
        }
        return colunsInfo.get(0);
    }


    /**
     * <b>Retorna a quantidade de valores da coluna solicitada</b>
     *
     * @param columnName <i>nome da coluna</i>
     * @param select     <i>query de select</i>
     * @param qtdLinhas  <i>numero de informacoes a serem retornadas</i>
     * @return ArrayList String - <i>informacoes na coluna</i>
     */
    public ArrayList<String> getColumnValues(String columnName, String select, int qtdLinhas) {
        try {
            ArrayList<String> valoresColuna = new ArrayList<>();

            if (autoconnect) {
                dataBase.openConnection(ORACLE);
            }

            select += (containsIgnoreCase(select, "WHERE") ? " AND " : " WHERE ") + "ROWNUM<" + (qtdLinhas + 1);
            ResultSet rs = dataBase.select(select);
            while (rs.next()) {
                valoresColuna.add(rs.getString(columnName));
            }
            if (autoconnect) {
                dataBase.closeConnection();
            }

            return valoresColuna;
        } catch (Exception sqlException) {
            dbError("Nao foi possivel retornar os valores da coluna: (" + select + ")");
            return null;
        }
    }

    /**
     * <b>Retorna determinada quantidade de linhas com os valores das colunas solicitadas</b>
     *
     * @param select       <i>query de select</i>
     * @param qtdLinhas    <i>numero de informacoes a serem retornadas</i>
     * @param columnsNames <i>nome das colunas</i>
     * @return ArrayList String - <i>informacoes na coluna</i>
     */
    public ArrayList<HashMap<String, String>> getColumnsValues(String select, int qtdLinhas, String... columnsNames) {
        try {
            ArrayList<HashMap<String, String>> lines = new ArrayList<>();

            if (autoconnect) {
                dataBase.openConnection(ORACLE);
            }

            select += (containsIgnoreCase(select, "WHERE") ? " AND " : " WHERE ") + "ROWNUM<" + (qtdLinhas + 1);
            ResultSet rs = dataBase.select(select);

            while (rs.next()) {
                HashMap<String, String> hashMap = new HashMap<>();
                for (String columnName : columnsNames) {
                    hashMap.put(columnName, rs.getString(columnName));
                }
                lines.add(hashMap);
            }

            if (autoconnect) {
                dataBase.closeConnection();
            }

            return lines;
        } catch (Exception sqlException) {
            dbError("Nao foi possivel retornar os valores da coluna: (" + select + ")");
            return null;
        }
    }

    /**
     * <b>Verifica se a query de select retorna algum resultado</b>
     *
     * @param select <i>query de select</i>
     * @return boolean - <i>true se existirem resultados no select</i>
     */
    public boolean verifyIfSelectHasInfo(String select) {
        try {
            boolean existemRegistros;
            if (autoconnect) {
                dataBase.openConnection(ORACLE);
            }
            ResultSet rs = dataBase.select(select);
            existemRegistros = rs.next();
            if (autoconnect) {
                dataBase.closeConnection();
            }
            return existemRegistros;
        } catch (Exception sqlException) {
            dbError("Nao foi possivel realizar a verificacao: (" + select + ")");
            return false;
        }
    }

    /**
     * <b>Executa uma query no banco (update, delete, insert)</b>
     *
     * @param query <i>comando sql</i>
     */
    public void execute(String query) {
        if (autoconnect) {
            dataBase.openConnection(ORACLE);
        }
        dataBase.executeSqlCommand(query);
        if (autoconnect) {
            dataBase.closeConnection();
        }
    }

    /**
     * <b>Retorna o ambiente do banco de dados</b>
     *
     * @return DataBase - <i>objeto do banco de dados</i>
     * @see DataBase
     */
    public DataBase getDbOracle() {
        return dataBase;
    }

    /**
     * <b>Printa o error de banco</b>
     *
     * @param message - <i>mensagem a ser exibida</i>
     */
    private static void dbError(String message) {
        System.err.println("\n[DB ERROR] " + message);
    }

}
