package main.DBManager;

import stepdefinition.Atendimento.DBGestaoTipoAtendimento;
import stepdefinition.Cadastro.AnaliseAgua.DBAnaliseAgua;
import stepdefinition.Cadastro.Arrecadador.DBArrecadador;
import stepdefinition.Cadastro.Cliente.DBCliente;
import stepdefinition.Cadastro.ConsolidacaoFatura.DBConsolidacaoFatura;
import stepdefinition.Cadastro.Cronograma.DBCronograma;
import stepdefinition.Cadastro.DebitoAutomatico.DBDebitoAutomatico;
import stepdefinition.Cadastro.Excecao.DBExcecao;
import stepdefinition.Cadastro.InstalacaoMedidor.DBInstalacaoMedidor;
import stepdefinition.Cadastro.Ligacao.DBLigacao;
import stepdefinition.Cadastro.MensagemFatura.DBMensagemFatura;
import stepdefinition.Cadastro.RetencaoForcadaFatura.DBRetencaoForcadaFatura;
import stepdefinition.Cadastro.Rubrica.DBRubrica;
import framework.DataBase.DBPlataform;
import framework.DataBase.DataBase;
import main.Massas.QuantidadeMassasCadastro;
import runner.Dados.Cadastro.DadosCadastro;
import runner.Dados.Cadastro.Endereco;
import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.DBGestaoUnidadeNegocio;

import java.util.ArrayList;

/**
 * <b>Classe para retornar informacoes de Cliente do banco de dados</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class DBCadastro extends DBActions {
    /**
     * <b>Construtor</b>
     */
    public DBCadastro() {
        super(DBActions.SAN, false);
    }

    private static QuantidadeMassasCadastro qtdMassasCadastro = new QuantidadeMassasCadastro();

    public static QuantidadeMassasCadastro getQtdMassasCadastro() {
        return qtdMassasCadastro;
    }

    public static void setQtdMassasCadastro(QuantidadeMassasCadastro qtdMassasCadastro) {
        DBCadastro.qtdMassasCadastro = qtdMassasCadastro;
    }

    /**
     * <b>Retorna se a base de dados(unidade) possui apenas uma cidade</b>
     *
     * @return boolean - <i>retorna true se a base possuir apenas uma cidade</i>
     */
    public boolean cidadeUnica() {
        return getColumnValues("id", "SELECT id FROM Cidade", 2).size() < 2;
    }

    /**
     * <b>Retorna os dados dinamicos para cadastro</b>
     *
     * @return DadosCadastro - <i>dados para cadastros</i>
     * @throws Exception <i>exception</i>
     * @see DadosCadastro
     */
    public DadosCadastro getDadosCadastro() throws Exception {
        DataBase dataBase = getDbOracle();
        DadosCadastro dadosCadastro = new DadosCadastro();

        //Open connection
        dataBase.openConnection(DBPlataform.ORACLE);

        //Cidade Unica
        dadosCadastro.setCidadeUnica(cidadeUnica());

        //Endereco
        Endereco endereco = retornarEndereco();
        dadosCadastro.setEndereco(endereco);

        //Analise Agua
        dadosCadastro.setAnaliseAgua(new DBAnaliseAgua(dataBase).getDadosAnaliseAgua(endereco.getCidade()));

        //Arrecadador
        dadosCadastro.setArrecadador(new DBArrecadador(dataBase).getDadosArrecadador());

        //Cliente
        dadosCadastro.setCliente(new DBCliente(dataBase).getDadosCliente());

        //Cronograma
        dadosCadastro.setCronograma(new DBCronograma(dataBase).getDadosCronogramaCidade(endereco.getCidade()));

        //Debito Automatico
         dadosCadastro.setDebitoAutomatico(new DBDebitoAutomatico(dataBase).getDadosAutomatico());

        //Consolidacao Arrecadacao
        dadosCadastro.setConsolidacaoFatura(new DBConsolidacaoFatura(dataBase).getDadosConsolidacaoFatura());

        //Instalacao medidor
        dadosCadastro.setInstalacaoMedidor(new DBInstalacaoMedidor(dataBase).
                getDadosInstalacaoMedidor(endereco.getCidade()));

        //Ligacao
        dadosCadastro.setLigacao(new DBLigacao(dataBase).getDadosLigacao(endereco.getCidade()));

        //Retencao Forcada Fatura
        dadosCadastro.setRetencaoForcadaFatura(new DBRetencaoForcadaFatura(dataBase).
                getDadosRetencaoForcadaFatura(endereco.getCidade()));

        //Rubrica
        dadosCadastro.setRubrica(new DBRubrica(dataBase).getDadosRubrica());

        //Excecao
        dadosCadastro.setExcecao(new DBExcecao(dataBase).getDadosExcecao());

        //Gestao Tipo Atendimento
        dadosCadastro.setGestaoTipoAtendimento(new DBGestaoTipoAtendimento(dataBase).getDadosGestaoAtendimento());

        //Gestao Unidade Negocio
        //dadosCadastro.setGestaoUnidadeNegocio(new DBGestaoUnidadeNegocio(dataBase).getDadosGestaoUnidadeNegocio());

        //Mensagens de Fatura
        dadosCadastro.setMensagemFatura(new DBMensagemFatura(dataBase).getDadosMensagemFatura());

        //Close connection
        dataBase.closeConnection();
        return dadosCadastro;
    }

    /**
     * <b>Retorna os dados dinamicos para endereco</b>
     *
     * @return Endereco - <i>dados para endereco</i>
     * @see Endereco
     */
    private Endereco retornarEndereco() {
        final int cidade = 0;
        final int distrito = 1;
        final int bairro = 2;
        final int logradouro = 3;
        ArrayList<String> dadosEndereco = getColumnsInfo("cidade,distrito,bairro,logradouro",
                "SELECT cid.cidade, dis.distrito, bairro.bairro, logr.logradouro FROM Logradouro logr " +
                        "INNER JOIN Cidade cid ON logr.idCidade = cid.id " +
                        "INNER JOIN LogradouroBairro logBairro ON  logr.id = logBairro.idLogradouro " +
                        "INNER JOIN Bairro bairro ON logBairro.idBairro = bairro.id " +
                        "AND NOT EXISTS(SELECT id from Bairro b WHERE b.bairro = bairro.bairro AND b.id<>bairro.id) " +
                        "INNER JOIN Distrito dis ON bairro.idDistrito = dis.id AND dis.idCidade = cid.id " +
                        "WHERE ROWNUM=1");
        return new Endereco(dadosEndereco.get(cidade), dadosEndereco.get(distrito),
                dadosEndereco.get(bairro), dadosEndereco.get(logradouro));
    }

}
