package main.WebElementManager;

import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserVerifications.getElement;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.PesquisaRapida.clicarPesquisaRapida;
import static main.WebElementManager.Xpath.concatenarIndice;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.DIV;

/**
 * <b>Classe abstrata para manipulacao de botoes</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public abstract class Botoes {
    //Botoes tela
    private static final String BTN_NOVO = "NOVO";
    private static final String BTN_SALVAR = "SALVAR";
    private static final String BTN_EXCLUIR = "EXCLUIR";
    private static final String BTN_PESQUISA_RAPIDA = "PESQUISA RAPIDA";
    private static final String BTN_SAIR = "SAIR";
    private static final String BTN_REPLICAR = "REPLICAR CRONOGRAMA";
    private static final int MAX_TENTATIVAS = 5;
    //Botoes auxiliares
    public static final String BTN_OK = "OK";

    /**
     * <b>Metodo que retorna o endereço do botao</b>
     * @param indice         <i>numero do indice</i>
     * @param tela             <i>nome da tela</i>
     *
     */
    public static By getXPathBotoes(int indice, int tela) {
        return concatenarIndice("//div[@class='GKPSPJYCMP']//div[" + indice + "]//div[1]//table[1]//tbody[1]" +
                "//tr[2]//td[2]//div[1]//div[1]//table[1]//tbody[1]//tr[1]//td[1]//img[1]", tela);
    }

    /**
     * <b>Metodo que retorna o endereço do botao</b>
     * @param indice         <i>numero do indice</i>
     *
     */
    public static By getXPathBotoes(int indice) {
        return getXPathBotoes(indice, 1);
    }

    /**
     * <b>Metodo que retorna o botao ajuda</b>
     * @param indice         <i>numero do indice</i>
     *
     */
    public static By getXPathAjuda(int indice) {
        return By.xpath("//body/div[@class='GKPSPJYCGDC']/div[@class='GKPSPJYCBDC']/div[@class='GKPSPJYCHDC']" +
                "/div[@class='GKPSPJYCKDC']/div[@class='GKPSPJYCFDC']/div[@class='GKPSPJYCODC']" +
                "/div[@class='backgroundwhitepad']/fieldset[@class='GKPSPJYCOSB lessPadding']" +
                "/div[@class='GKPSPJYCMSB']/div/div[3]/div[" + indice + "]/div[1]/table[1]/tbody[1]/tr[2]/td[2]" +
                "/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]");
    }

    /**
     * <b>Metodo que retorna o endereço botao Replicar</b>
     * @param indiceTela         <i>indice da tela</i>
     *
     */
    public static By getXPathReplicar(int indiceTela) {
        return concatenarIndice("//div[@class='clear-button GKPSPJYCMK']//div[@class='GKPSPJYCPM GKPSPJYCLN']" +
                "//img[@onload='this.__gwtLastUnhandledEvent=\"load\";']", indiceTela);
    } //getXPathReplicar

    /**
     * <b>Metodo que seleciona o botao</b>
     * @param botao         <i>nome do botao</i>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotao(String botao, int indiceTela) {
        if (!botao.isEmpty()) {
            switch (botao.toUpperCase()) {
                case BTN_NOVO:
                    clicarBotaoNovo(indiceTela);
                    break;
                case BTN_SALVAR:
                    clicarBotaoSalvar(indiceTela);
                    break;
                case BTN_EXCLUIR:
                    clicarBotaoExcluir(indiceTela);
                    break;
                case BTN_PESQUISA_RAPIDA:
                    clicarBotaoPesquisaRapida(indiceTela);
                    break;
                case BTN_SAIR:
                    clicarBotaoSair(indiceTela);
                    break;
                case BTN_REPLICAR:
                    clickOnElement(getXPathReplicar(indiceTela));
                default:
                    clickOnElement(getXPathElementTextEquals(DIV, botao, indiceTela));
            }
        }
    }

    /**
     * <b>Metodo que seleciona o botao</b>
     * @param botao         <i>nome do botao</i>
     *
     */
    public static void clicarBotao(String botao) {
        clicarBotao(botao, 1);
    }

    /**
     * <b>Metodo que seleciona o botao Novo</b>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoNovo(int indiceTela) {
        clickOnElement(getXPathBotoes(1, indiceTela));
    }

    /**
     * <b>Metodo que seleciona o botao Salvar</b>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoSalvar(int indiceTela) {
        clickOnElement(getXPathBotoes(2, indiceTela));
    }

    /**
     * <b>Metodo que seleciona o botao Excluir</b>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoExcluir(int indiceTela) {
        final int indice = 3;
      //  for (int tentativas = 0; tentativas < MAX_TENTATIVAS &&
        //  !BrowserVerifications.isElementEnable(getXPathBotoes(indice, indiceTela)); tentativas++) {
            BrowserVerifications.wait(2);
            clickOnElement(getXPathBotoes(indice, indiceTela));
       // }
    }

    /**
     * <b>Metodo que seleciona o botao PesquisaRapida</b>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoPesquisaRapida(int indiceTela) {
        final int indice = 4;
        clicarPesquisaRapida(getXPathBotoes(indice, indiceTela));
    }

    /**
     * <b>Metodo que seleciona o botao Sair</b>
     * @param indiceTela     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoSair(int indiceTela) {
        final int indice = 7;
        clickOnElement(getXPathBotoes(indice, indiceTela));
    }

    /**
     * <b>Metodo que seleciona o botao Pesquisar</b>
     *
     */
    public static void clicarBotaoPesquisarConsultaCompleta() {
        clickOnElement(getXPathElementTextEquals(DIV, "Pesquisar", 2));

    }

    /**
     * <b>Clica no botao pesquisar azul no campo</b>
     *
     * @param campo <i>nome do campo</i>
     */
    public static void clicarBotaoPesquisaAzul(String campo) {
        clickOnElement(By.xpath("(//label[text()='" + campo + ":']/following-sibling::div/div/div/div" +
                "/table/tbody/tr/td)[5]"));
    }

    /**
     * <b>Clica no botao pesquisar no campo</b>
     *
     * @param campo <i>nome do campo</i>
     */
    public static void clicarBotaoPesquisar(String campo) {
        BrowserVerifications.waitElementToBeClickable(By.xpath("(//label[text()='" + campo + ":']" +
                "/following-sibling::div/div/div/table/tbody/tr/td/div)[2]"), DEFAULT_TIME_OUT);
        clickOnElement(By.xpath("(//label[text()='" + campo + ":']/following-sibling::div/div/div/" +
                "table/tbody/tr/td/div)[2]"));
    }

    /**
     * <b>Metodo que seleciona o botao Agenda</b>
     *
     */
    public static void clicarNoBotaoAgenda() {
        clickOnElement((By.xpath("(//div[@class='GKPSPJYCJR'])[1]")));
    }

    /**
     * <b>Metodo que seleciona o botao Agenda</b>
     *
     * @param indice <i>nome do campo</i>
     */
    public static void clicarNoBotaoAgenda(int indice) {
        clickOnElement(concatenarIndice("//div[@class='GKPSPJYCJR']", indice));
    }

    /**
     * <b>Metodo que seleciona o botao dia</b>
     *
     * @param dia <i>seleciona dia</i>
     */
    public static void clicarNoDia(String dia) {
        final int diaMaior = 21;
        By by;
        if (Integer.parseInt(dia) > diaMaior) {
            by = By.xpath("(//span[text() = '" + dia + "'])[2]");
            if (!getElement(by).isEnabled()) {
                by = By.xpath("(//span[text() = '" + dia + "'])[1]");
            }
        } else {
            dia = dia.replace("0", "");
            by = By.xpath("(//span[text() = '" + dia + "'])[1]");
        }
        clickOnElement(by);
    }

    /**
     * <b>Metodo que seleciona o botao Span</b>
     * @param texto     <i>indice da tela</i>
     *
     */
    public static void clicarBotaoSpan(String texto) {
        clickOnElement(By.xpath("//span[text()='" + texto + "']"));
    }

    /**
     * <b>Metodo que seleciona o botao Ajuda</b>
     * @param indice     <i>indice da tela</i>
     *
     */
    public static void clicarNoBotaoAjuda(int indice) {
        clickOnElement(getXPathAjuda(indice));
    } //clicarNoBotaoAjuda


    /**
     * <b>Clica no botao pesquisar no campo</b>
     *
     * @param valor      <i>nome do campo</i>
     * @param indiceTela <i>nome do campo</i>
     */
    public static void clicarBotaoCentral(String valor, int indiceTela) {

        clickOnElement(getXPathElementTextEquals(DIV, valor, indiceTela));

    }


}
