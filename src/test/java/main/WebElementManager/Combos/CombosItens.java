package main.WebElementManager.Combos;

import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserVerifications.elementExists;

import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.getXPathElementWithText;

/**
 * <b>Classe para manipulacao das opcoes das combos</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public class CombosItens {
    private By itemXPath;
    private boolean itemExists;

    /**
     * <b>Construtor vazio da Classe CombosItens</b>
     *
     */
    public CombosItens() {

    }

    /**
     * <b>Metodo para fazer chamado a classe CombosItens</b>
     *
     * @param itemXPath       <i>xpath endereco</i>
     * @param itemExists      <i>se existe combo</i>
     *
     */
    public CombosItens(By itemXPath, boolean itemExists) {
        setItem(itemXPath, itemExists);
    }


    /**
     * <b>Identifica elementos do combo</b>
     *
     * @param itemXPathParametro       <i>xpath endereco</i>
     * @param itemExistsParametro      <i>se existe combo</i>
     *
     */
    public void setItem(By itemXPathParametro, boolean itemExistsParametro) {
        this.itemXPath = itemXPathParametro;
        this.itemExists = itemExistsParametro;
    }

    /**
     * <b>Retorna chamado ao elemento CombosItens</b>
     *
     * @param valor         <i>valor</i>
     * @return              <i>chamado ao elemento CombosItens</i>
     */
    protected static CombosItens itemExists(String valor) {
        By valorXPath = getXPathElementTextEquals(DIV, valor);
        By xPathComValor = getXPathElementWithText(DIV, valor);

        //Verifica se existe um item com o mesmo valor
        if (!elementExists(valorXPath)) {
            //Verifica se existe um item que contenha o valor
            if (!elementExists(xPathComValor)) {
                return new CombosItens();
            } else {
                return new CombosItens(xPathComValor, true);
            }
        }
        return new CombosItens(valorXPath, true);
    }

    /**
     * <b>Retorna chamado ao variable</b>
     *
     * @return              <i>valor desejado</i>
     */
    public CombosItens getItem() {
        return this;
    }

    /**
     * <b>Retorna chamado ao variable itemXPath</b>
     *
     * @return              <i>itemXPath</i>
     */
    public By getItemXPath() {
        return itemXPath;
    }

    /**
     * <b>Retorna chamado ao elemento itemXPath</b>
     *
     * @param itemXPath     <i>valor</i>
     * @return              <i>chamado ao elemento itemXPath</i>
     */
    public void setItemXPath(By itemXPath) {
        this.itemXPath = itemXPath;
    }

    /**
     * <b>Retorna chamado ao elemento itemExists</b>
     *
     * @return              <i>chamado ao elemento itemExists</i>
     */
    public boolean isExistingItem() {
        return itemExists;
    }

    /**
     * <b>Retorna se existe item</b>
     *
     * @param itemExists    <i>true or false</i>
     * @return              <i>true or false</i>
     */
    public void setItemExists(boolean itemExists) {
        this.itemExists = itemExists;
    }
}
