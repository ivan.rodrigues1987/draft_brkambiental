package main.WebElementManager.Combos;


import framework.BrowserManager.BrowserVerifications;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserVerifications.elementExists;

import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.Utils.isBlank;


import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.getXPathDisplayed;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;


/**
 * <b>Classe abstrata para manipulacao de combos</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public abstract class Combos extends CombosItens {

    /**
     * <b>Preenche combo inserindo texto e clica no item</b>
     *
     * @param combo      <i>nome da combo</i>
     * @param valor      <i>valor a ser selecionado</i>
     * @param clearField <i>limpa o campo</i>
     */
    public static void setTextoCombo(String combo, String valor, boolean clearField) {
        if (!isBlank(valor)) {
            //Clica no texto da combo
            clickOnElement(ComboInputText.getXPathTextoCombo(combo));
            //Insere texto na combo
            ComboInputText.setCombo(combo, valor, clearField);
            //Clica no item da combo
            clicarNoItemCombo(valor);
        }
    }


    /**
     * <b>Clica na combo e seleciona um item</b>
     *
     * @param comboXPath <i>xPath da combo</i>
     * @param valor      <i>valor a ser selecionado</i>
     */
    public static void clicarCombo(By comboXPath, String valor) {
        By valorXPath = getXPathElementTextEquals(DIV, valor);
        //Click on combo
        CombosItens combosItens;
        clickOnElement(comboXPath);

        //Verifica se o item apareceu
        combosItens = itemExists(valor);
        if (combosItens.isExistingItem()) {
            valorXPath = combosItens.getItemXPath();
        } else {
            clickOnElement(comboXPath);
            combosItens = itemExists(valor);
            if (combosItens.isExistingItem()) {
                valorXPath = combosItens.getItemXPath();
            }
        }

        //Click on item
        clickOnElement(valorXPath);
    }

    /**
     * <b>Clica na combo de cidade e seleciona um item</b>
     *
     * @param combo <i>nome da combo</i>
     * @param valor <i>valor a ser selecionado</i>
     */
    public static void setComboCidade(String combo, String valor) {
        if (!isBlank(valor)) {
            if (!getDadosCadastro().isCidadeUnica()) {
                setComboClicando(combo, valor);
            } else {
                //Verifica se a combo esta em branco
                By xPathTextoCombo = ComboInputText.getXPathTextoCombo(combo);
                if (!elementExists(xPathTextoCombo)) {
                    BrowserVerifications.waitElementToBeEnable("Cidade", DEFAULT_TIME_OUT);
                }
                if (getText(xPathTextoCombo).isEmpty()) {
                    clickOnElement(getXPathCombo(combo));
                    sendKeys(Keys.ENTER);
                }
            }
        }
    }

    /**
     * <b>Clica na combo e seleciona um item</b>
     *
     * @param combo <i>nome da combo</i>
     * @param valor <i>valor a ser selecionado</i>
     */
    public static void setComboClicando(String combo, String valor) {
        //Verifica se o valor esta em branco
        if (!isBlank(valor)) {
            By comboXPath = getXPathCombo(combo);
            clicarCombo(comboXPath, valor);
        }
    }

    /**
     * <b>Clica na combo e seleciona um item e limpa a combo caso o valor esteja em branco</b>
     *
     * @param combo      <i>nome da combo</i>
     * @param valor      <i>valor a ser selecionado</i>
     * @param clearField <i>limpa o campo</i>
     */
    public static void setComboClicando(String combo, String valor, boolean clearField) {
        if (isBlank(valor) && clearField) {
            clearField(ComboInputText.getXPathTextoCombo(combo));
            sendKeys(Keys.ENTER);
        } else {
            setComboClicando(combo, valor);
        }
    }

    /**
     * <b>Clica na combo informando o indice (caso haja mais de uma com o mesmo nome) e seleciona um item</b>
     *
     * @param combo       <i>nome da combo</i>
     * @param indiceCombo <i>indice da combo</i>
     * @param valor       <i>valor a ser selecionado</i>
     */
    public static void setComboClicando(String combo, int indiceCombo, String valor) {
        //Verifica se o valor esta em branco
        if (!isBlank(valor)) {
            By comboXPath = getXPathCombo(combo, indiceCombo);
            clicarCombo(comboXPath, valor);
        }
    }

    /**
     * <b>Clica no elemento que contem o combo</b>
     *
     * @param combo      <i>nome da combo</i>
     * @param valor      <i>valor a ser selecionado</i>
     * @param waitBefore <i>tempo</i>
     */
    public static void setComboClicando(String combo, String valor, int waitBefore) {
        BrowserVerifications.wait(waitBefore);
        setComboClicando(combo, valor);
    }

    /**
     * <b>Clica no elemento do combo</b>
     *
     * @param item <i>nome da combo</i>
     */
    public static void clicarNoItemCombo(String item) {
        clickOnElement(getXPathElementTextEquals(DIV, item));
    }

    /**
     * <b>Clica no elemento do combo</b>
     *
     * @param item       <i>nome da combo</i>
     * @param waitBefore <i>tempo</i>
     */
    public static void clicarNoItemCombo(String item, int waitBefore) {
        BrowserVerifications.wait(waitBefore);
        clicarNoItemCombo(item);
    }

    /**
     * <b>Retorna o xpath do combo</b>
     *
     * @param combo <i>nome da combo</i>
     * @return By - <i>xpath do combo</i>
     */
    public static By getXPathCombo(String combo) {
        return getXPathDisplayed("//label[text()='" + combo + ":']" +
                "/following-sibling::div/div/div/table/tbody/tr/td/div");
    }

    /**
     * <b>Retorna o xpath do combo</b>
     *
     * @param combo  <i>nome da combo</i>
     * @param indice <i>numero do indice</i>
     * @return By - <i>xpath do combo</i>
     */
    public static By getXPathCombo(String combo, int indice) {
        return getXPathDisplayed("(//label[text()='" + combo + ":']/following-sibling::div/div/div/table" +
                "/tbody/tr/td/div)[" + indice + "]");
    }
}
