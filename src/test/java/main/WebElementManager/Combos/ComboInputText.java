package main.WebElementManager.Combos;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static framework.BrowserManager.BrowserActions.setText;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserVerifications.getElement;
import static main.WebElementManager.Xpath.getDivs;
import static org.jsoup.helper.StringUtil.isBlank;


/**
 * <b>Classe para manipular campos de texto em campos com botao(data, pesquisa, combo)</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 28/02/2019
 */
public class ComboInputText {

    /**
     * <b>Insere texto no ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param combo <i>campo a ser selecionado para pesquisa</i>
     * @param texto <i>texto a ser escrito</i>
     */
    public static void setCombo(String combo, String texto) {
        setText(getXPathTextoCombo(combo), texto);
    }


    /**
     * <b>Insere texto no ByCallCenter da combo ou campos com botoes ao lado e limpa o campo se necessario</b>
     *
     * @param combo      <i>campo a ser selecionado para pesquisa</i>
     * @param texto      <i>texto a ser escrito</i>
     * @param clearField <i>limpar campo</i>
     */
    public static void setCombo(String combo, String texto, boolean clearField) {
        if (clearField) {
            clearField(getXPathTextoCombo(combo));
        }
        setText(getXPathTextoCombo(combo), texto);
    }

    /**
     * <b>Insere texto no ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param combo  <i>campo a ser selecionado para pesquisa</i>
     * @param texto  <i>texto a ser escrito</i>
     * @param indice <i>indice do campo</i>
     */
    public static void setCombo(String combo, String texto, int indice) {
        setText(getXPathTextoCombo(indice, combo), texto);
    }

    /**
     * <b>Insere texto no ByCallCenter da combo ou campos com botoes ao lado e pressiona enter</b>
     *
     * @param combo <i>campo a ser selecionado para pesquisa</i>
     * @param texto <i>texto a ser escrito</i>
     */
    public static void setComboEPressionarEnter(String combo, String texto) {
        if (!isBlank(texto)) {
            getElement(getXPathTextoCombo(combo)).clear();
            clickOnElement(getXPathTextoCombo(combo));
            sendKeys(texto);
            sendKeys(Keys.ENTER);
        }
    }

    /**
     * <b>Insere texto no ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param combo   <i>campo a ser selecionado para pesquisa</i>
     * @param texto   <i>texto a ser escrito</i>
     * @param qtdDivs <i>texto a ser escrito</i>
     */
    public static void setComboEPressionarEnter(String combo, String texto, int qtdDivs) {
        if (!isBlank(texto)) {
            getElement(getXPathTextoCombo(combo, qtdDivs)).clear();
            clickOnElement(getXPathTextoCombo(combo, qtdDivs));
            sendKeys(texto);
            sendKeys(Keys.ENTER);
        }
    }

    /**
     * <b>Retorna xPath do campo de ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param combo <i>nome do campo</i>
     * @return xpath-  <i>retorna xPath</i>
     */
    public static By getXPathTextoCombo(String combo) {
        return By.xpath("//label[text()='" + combo + ":']/following-sibling::div/div/div/table/tbody/tr/td/input");
    }

    /**
     * <b>Retorna xPath do campo de ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param indice <i>indice do campo</i>
     * @param combo  <i>nome do campo</i>
     * @return xpath-  <i>retorna xPath</i>
     */
    public static By getXPathTextoCombo(int indice, String combo) {
        return By.xpath("(//label[text()='" + combo + ":']/following-sibling::div/div/div/table/tbody/tr/td/input)" +
                "[" + indice + "]");

    }

    /**
     * <b>Retorna xPath do campo de ByCallCenter da combo ou campos com botoes ao lado</b>
     *
     * @param combo   <i>campo a ser selecionado para pesquisa</i>
     * @param qtdDivs <i>quantidade de DIVs ate a tag INPUT</i>
     * @return xpath-  <i>retorna xPath</i>
     */
    public static By getXPathTextoCombo(String combo, int qtdDivs) {
        return By.xpath("//label[text()='" + combo + ":']/following-sibling::" + getDivs(qtdDivs) + "/input");
    }

}
