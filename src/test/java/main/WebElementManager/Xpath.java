package main.WebElementManager;

import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserVerifications.elementExists;
import static framework.BrowserManager.BrowserVerifications.isElementDisplayed;

/**
 * <b>Classe abstrata para manipulacao de xPaths</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public abstract class Xpath {
    public static final String A = "//a";
    public static final String B = "//b";
    public static final String DIV = "//div";
    public static final String LABEL = "//label";
    public static final String LI = "//li";
    public static final String SPAN = "//span";
    /**
     * <b>Retorna elemento com o texto igual, indice passado como parametro</b>
     *
     * @param element                   <i>nome do elemento</i>
     * @param text                     <i>texto</i>
     * @param indice                <i>numero de indice</i>
     *
     */
    public static By getXPathElementTextEquals(String element, String text, int indice) {
        return concatenarIndice(element + "[text()='" + text + "']", indice);
    }

    /**
     * <b>Retorna endereco do elemento</b>
     *
     * @param element                   <i>nome do elemento</i>
     * @param text                     <i>texto</i>
     *
     */
    public static By getXPathElementTextEquals(String element, String text) {
        return concatenarIndice(element + "[text()='" + text + "']", 1);
    }

    /**
     * <b>Retorna elemento com o texto igual, indice passado como parametro</b>
     *
     * @param element                   <i>nome do elemento</i>
     * @param text                     <i>texto</i>
     *
     */
    public static By getXPathElementWithText(String element, String text) {
        return concatenarIndice(element + "[contains(text(),'" + text + "')]", 1);
    }

    /**
     * <b>Retorna endereco do elemento</b>
     *
     * @param xPath                   <i>xpath</i>
     *
     */
    public static By getXPathDisplayed(String xPath) {
        By byXPath;
        final int j = 4;
        for (int i = 1; i < j; i++) {
            byXPath = concatenarIndice(xPath, i);
            if (isElementDisplayed(byXPath)) {
                return byXPath;
            }
        }
        return By.xpath(xPath);
    }

    /**
     * <b>Metdo que concatena o xpath e indice</b>
     *
     * @param xPath                      <i>xpath</i>
     * @param indice                     <i>texto</i>
     *
     */
    public static By concatenarIndice(String xPath, int indice) {
        return By.xpath("(" + xPath + ")[" + indice + "]");
    }
    /**
     * <b>Retorna quantidade de divs desejadas</b>
     *
     * @param qtdDivs                      <i>quantidade de divs</i>
     *
     */
    public static String getDivs(int qtdDivs) {
        String divs = "";
        for (int i = 0; i < qtdDivs; i++) {
            if ((i + 1) == qtdDivs) {
                divs += "div";
            } else {
                divs += "div/";
            }
        }
        return divs;

    }

    /**
     * <b>Retorna endereco do botao fechar janela</b>
     *
     * @param indice                      <i>numero do indice</i>
     *
     */
    public static By byBotaoFecharJanela(int indice) {
        return concatenarIndice("//div[@class='GKPSPJYCCP GKPSPJYCHK GKPSPJYCKEC']", indice);
    }

    /**
     * <b>Retorna endereco do xpath seguido por label</b>
     *
     * @param label                      <i>nome do label</i>
     * @param campo                      <i>nome do campo</i>
     * @param qtdDIVs                      <i>quantidade de indices</i>
     *
     */
    public static By getCampoXPathSeguidoPorLabel(String label, String campo, int qtdDIVs) {
        if (!campo.equals("")) {
            campo = "/" + campo;
        }
        return By.xpath("//label[text()='" + label + ":']/following-sibling::" + getDivs(qtdDIVs) + campo);
    }

    /**
     * <b>Retorna endereco do xpath seguido por label</b>
     *
     * @param label                      <i>nome do label</i>
     * @param campo                      <i>nome do campo</i>
     * @param qtdDIVs                      <i>quantidade de indices</i>
     *
     */
    public static By getCampoXPathSeguidoPorLabelContains(String label, String campo, int qtdDIVs) {
        if (!campo.equals("")) {
            campo = "/" + campo;
        }
        return By.xpath("//label[contains(text(),'" + label + ":')]/following-sibling::" + getDivs(qtdDIVs) + campo);
    }

    /**
     * <b>Retorna endereco do elemento</b>
     *
     * @param elemento                      <i>nome do elemento</i>
     * @param texto                      <i>texto inserido</i>
     *
     */
    public static By getElementPorTexto(String elemento, String texto) {
        return By.xpath("//" + elemento + "[contains(text(),'" + texto + "')]");
    }
    /**
     * <b>Aguarda o Elemento Aparecer sem Validacao</b>
     *
     * @param by                            <i>endereço xpath</i>
     * @param segundos                      <i>tempo de espera</i>
     *
     */
    public static boolean waitElementAppear(By by, int segundos) {
        for (int i = 0; i < segundos; i++) {
            if (elementExists(by)) {
                return true;
            }
        }
        return false;
    }

}
