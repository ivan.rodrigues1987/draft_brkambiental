package main.WebElementManager;

import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.setText;

/**
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */
public abstract class TextArea {

    /**
     * <b>Metodo que retorna o endereço do TextArea</b>
     *
     * @param textArea          <i>nome do campo</i>
     */
    private static By getTextAreaXPath(String textArea) {
        return By.xpath("//label[text()='" + textArea + ":']/following-sibling::div/div/div/textarea");
    }

    /**
     * <b>Metodo que preenche valor do TextArea</b>
     *
     * @param textArea          <i>nome do campo</i>
     * @param texto          <i>nome do campo</i>
     *
     */
    public static void setTextArea(String textArea, String texto) {
        if (!texto.isEmpty()) {
            setText(getTextAreaXPath(textArea), texto);
        }
    }

    /**
     * <b>Metodo que preenche valor do TextArea</b>
     *
     * @param textArea              <i>nome do campo</i>
     * @param texto                 <i>nome do campo</i>
     * @param clearField            <i>valida se limpa o não o campo</i>
     *
     */
    public static void setTextArea(String textArea, String texto, boolean clearField) {
        if (clearField) {
            clearField(getTextAreaXPath(textArea));
        }
        setTextArea(textArea, texto);
    }
}
