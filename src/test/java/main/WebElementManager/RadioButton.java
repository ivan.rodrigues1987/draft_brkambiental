package main.WebElementManager;

import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clickOnElement;

/**
 * <b>Classe abstrata para manipulacao de radioButtons</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public abstract class RadioButton {

    /**
     * <b>Metodo que clica RadioButton</b>
     *
     * @param radioButton          <i>nome do campo</i>
     * @param opcao            <i>nome do campo</i>
     */
    public static void clicarRadioButton(String radioButton, String opcao) {
        clickOnElement(getXPathRadioButton(radioButton, opcao));
    }

    /**
     * <b>Metodo que retorno o endereço do RadioButton</b>
     *
     * @param radioButton          <i>nome do campo</i>
     * @param opcao            <i>nome do campo</i>
     */
    private static By getXPathRadioButton(String radioButton, String opcao) {
        return By.xpath("//label[contains(text(),'" + radioButton + ":')]" +
                "/following-sibling::div/div/div/div/label[text()='" + opcao + "']/../input");
    }
}
