package main.WebElementManager.Atendimento;

import org.openqa.selenium.By;

/**
 * @author Thiago Tolentino P. dos Santos
 * @since 16/04/2019
 */
public class ByAtendimento {
    private static final By BY_LIGACAO = By.xpath("//*[@id=\"x-widget-3-input\"]");
    private static final By BY_LUPA =
            By.xpath("/html[1]/body[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/" +
                    "div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[1]/div[1]/div[3]/div[1]/div[1]/" +
                    "table[1]/tbody[1]/tr[1]/td[1]/img[1]");
    private static final By BY_PROTOCOLO =
            By.xpath("/html[1]/body[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/" +
                    "div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/input[1]");
    private static final By BY_LOGRADOURO_TEXT =
            By.xpath("//label[text()='Digitar Logradouro']/../../following-sibling::div/div/input");
    private static final By BY_SELECIONA_RESULTADO =
            By.xpath("/html/body/div[3]/div[2]/div[1]/div/div/div[1]" +
                    "/div/div[2]/div[1]/div[2]/div[2]/div[1]/table/tbody[2]/tr[1]");
    private static final By BY_ICONE_LOCALIZACAO =
            By.xpath("//td[@class='meio']/div/div/div/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]");
    private static final By BY_ICONE_FERIADO =
            By.xpath("//td[@class='meio']/div/div/div/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]");


    /**
     * <b>Retorna xpath da aba iframe</b>
     *
     * @param aba <i>nome da aba</i>
     * @return By - <i>xpath com nome aba CallCenter</i>
     *
     */
    public static By getByJanela(String aba) {
        return By.xpath("//div[text()='" + aba + "']");
    }


    /**
     * <b>xpath menssagem CPF</b>
     *
     * @param valor <i>nome do mensagem</i>
     * @return By - <i>xpath com nome aba CallCenter</i>
     *
     */
    public static By getMessagemCPF(String valor) {
        return By.xpath("//li[@class='infoMessage'][contains(text(),'" + valor + "')]");
    }

    /**
     * <b>xpath botao pesquisa iframe</b>
     *
     * @param opcao <i>nome do mensagem</i>
     * @return By - <i>xpath com nome do botao</i>
     *
     */
    public static By getByPesquisarPor(String opcao) {
        return By.xpath("//label[text()='" + opcao + "']/../input");
    }


    /**
     * <b>xpath icone localizacao tempo iframe</b>
     *
     * @param valor <i>nome do botao</i>
     * @return By - <i>xpath com nome do botao</i>
     *
     */
    public static By getByIconeLocalizacao(String valor) {
        return By.xpath("//h2[contains(text(),'" + valor + "')]");
    }

    /**
     * <b>xpath botao pesquisa iframe</b>
     *
     * @param id <i>nome do iframe</i>
     * @return By - <i>xpath com id desejada</i>
     *
     */
    public static By getByIframePorIdAtendimento(String id) {
        return By.xpath("//iframe[@id='" + id + "']");
    }

    public static By getByLigacaoATendimento() {
        return BY_LIGACAO;
    }

    public static By getByLupaAtendimento() {
        return BY_LUPA;
    }

    public static By getByProtocoloAtendimento() {
        return BY_PROTOCOLO;
    }

    public static By getByLogradouroTextAtendimento() {
        return BY_LOGRADOURO_TEXT;
    }

    public static By getBySelecionaResultadoAtendimento() {
        return BY_SELECIONA_RESULTADO;
    }

    public static By getByIconeLocalizacaoAtendimento() {
        return BY_ICONE_LOCALIZACAO;
    }

    public static By getByIconeFeriadoAtendimento() {
        return BY_ICONE_FERIADO;
    }
}
