package main.WebElementManager;

import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.WebElementManager.Xpath.LABEL;

/**
 * <b>Classe abstrata para manipulacao de checkboxes</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */

public abstract class Checkbox {

    /**
     * <b>Metodo que clica checkbox</b>
     *
     * @param checkBox          <i>nome do campo</i>
     * @param clicar            <i>nome do campo</i>
     */
    public static void clicarCheckBox(String checkBox, String clicar) {
        if (clicar.toUpperCase().contains("S")) {
            clickOnElement(getXPathCheckbox(checkBox));
        }
    }

    /**
     * <b>Metodo que retorna o endereço do checkbox</b>
     *
     * @param checkBox          <i>nome do campo</i>
     */
    private static By getXPathCheckbox(String checkBox) {
        return By.xpath(LABEL + "[text()='" + checkBox + "']/preceding-sibling::input");
    }

    /**
     * <b>Metodo que clica checkbox</b>
     *
     * @param opcao          <i>nome do campo</i>
     */
    public static void clicarCheckBox(String opcao) {
        clickOnElement(By.xpath("//div[contains(text(),'" + opcao + "')]"));
    }
}
