package main.WebElementManager.InputText;

import org.openqa.selenium.By;

import static main.WebElementManager.Xpath.concatenarIndice;
import static main.WebElementManager.Xpath.getDivs;

/**
 * @author Renan Ferreira dos Santos
 * @since 07/03/2019
 */
public abstract class InputTextXPath {
    /**
     * <b>Metodo que retorno o endereço do campo procurado</b>
     * @param inputText         <i>nome do campo</i>
     *
     */
    public static By getInputTextXPath(String inputText) {
        return By.xpath("//label[text()='" + inputText + ":']/following-sibling::div/div/div/input");
    }

    /**
     * <b>Metodo que retorno o endereço do campo procurado</b>
     * @param inputText         <i>nome do campo</i>
     * @param indice             <i>numero de indice</i>
     *
     */
    public static By getInputTextXPath(int indice, String inputText) {
        return concatenarIndice("//label[text()='" + inputText + ":']/following-sibling::div/div/div/input", indice);
    }

    /**
     * <b>Metodo que retorno o endereço do campo procurado</b>
     * @param inputText         <i>nome do campo</i>
     * @param qtdDIVs             <i>quantidade de Divs</i>
     *
     */
    public static By getInputTextXPath(String inputText, int qtdDIVs) {
        return By.xpath("//label[text()='" + inputText + ":']/following-sibling::" + getDivs(qtdDIVs) + "/input");
    }

}
