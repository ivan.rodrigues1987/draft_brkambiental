package main.WebElementManager.InputText;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserActions.setText;
import static framework.ConfigFramework.getDriver;
import static main.Utils.Utils.isBlank;

/**
 * <b>Classe abstrata para manipulacao de inputTexts</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public abstract class InputText extends InputTextXPath {

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param inputText         <i>nome do campo</i>
     * @param texto             <i>texto</i>
     * @param qtdDIVs           <i>quantidade de divs</i>
     * @param clearField        <i>se tem que limpar o campo</i>
     *
     */
    public static void setInputTextEPressionarEnter(String inputText, String texto, int qtdDIVs, boolean clearField) {
        if (clearField) {
            clearField(getInputTextXPath(inputText, qtdDIVs));
        }
        setInputTextEPressionarEnter(inputText, texto, qtdDIVs);
    }

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param inputText         <i>nome do campo</i>
     * @param texto             <i>texto</i>
     * @param qtdDIVs           <i>quantidade de divs</i>
     *
     */
    public static void setInputTextEPressionarEnter(String inputText, String texto, int qtdDIVs) {
        if (!isBlank(texto)) {
            clickOnElement(getInputTextXPath(inputText, qtdDIVs));
            sendKeys(texto);
            sendKeys(Keys.ENTER);
        }
    }

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param inputText         <i>nome do campo</i>
     * @param texto             <i>texto</i>
     *
     */
    public static void setInputTextEPressionarEnter(String inputText, String texto) {
        setInputTextEPressionarEnter(1, inputText, texto);
    }

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param inputText             <i>nome do campo</i>
     * @param indice                <i>numero do indice</i>
     * @param texto                 <i>texto</i>
     *
     */
    public static void setInputTextEPressionarEnter(int indice, String inputText, String texto) {
        clickOnElement(getInputTextXPath(indice, inputText));
        sendKeys(texto);
        sendKeys(Keys.ENTER);
    }
    /**
     * <b>Verificar forma melhor de implementar
     *     a ideia é o metodo validar se o campo foi preenchido, se nao for ele tenta preencher novamente</b>
     * @param inputText             <i>nome do campo</i>
     * @param indice                <i>numero do indice</i>
     * @param texto                 <i>texto</i>
     * @param seconds               <i>tempo</i>
     *
     */
    public static void setInputTextEPressionarEnter(int indice, String inputText, String texto, int seconds) {
        if (!texto.isEmpty()) {
            WebDriverWait wait = new WebDriverWait(getDriver(), seconds);
            wait.until(new ExpectedCondition<Boolean>() {
                @NullableDecl
                @Override
                public Boolean apply(@NullableDecl WebDriver driver) {
                    setInputTextEPressionarEnter(indice, inputText, texto);
                    return !getTextAttribute(getInputTextXPath(indice, inputText), "value").isEmpty();
                }
            });
        }
    }

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param inputText         <i>nome do campo</i>
     * @param texto             <i>texto</i>
     * @param clearField        <i>se tem que limpar o campo</i>
     *
     */
    public static void setInputTextEPressionarEnter(String inputText, String texto, boolean clearField) {
        if (clearField) {
            clearField(getInputTextXPath(inputText));
        }
        clickOnElement(getInputTextXPath(inputText));
        sendKeys(texto);
        sendKeys(Keys.ENTER);
    }

    /**
     * <b>Metodo que preenche e seleciona enter</b>
     * @param texto             <i>texto</i>
     *
     */
    public static void setInputTextEPressionarEnter(String texto) {
        sendKeys(texto);
        sendKeys(Keys.ENTER);
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                 <i>texto</i>
     * @param inputText             <i>nome do campo</i>
     *
     */
    public static void setInputText(String inputText, String texto) {
        if (!isBlank(texto)) {
            setText(getInputTextXPath(inputText), texto);
        }
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                 <i>texto</i>
     * @param inputText             <i>nome do campo</i>
     * @param indice                 <i>numero de indices</i>
     */
    public static void setInputText(int indice, String inputText, String texto) {
        setText(getInputTextXPath(indice, inputText), texto);
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                 <i>texto</i>
     * @param inputText             <i>nome do campo</i>
     * @param indice                 <i>numero de indices</i>
     * @param clearField        <i>se tem que limpar o campo</i>
     */
    public static void setInputText(int indice, String inputText, String texto, boolean clearField) {
        if (clearField) {
            clearField(getInputTextXPath(indice, inputText));
        }
        setText(getInputTextXPath(indice, inputText), texto);
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                     <i>texto</i>
     * @param inputText                 <i>nome do campo</i>
     * @param clearField                <i>se tem que limpar o campo</i>
     */
    public static void setInputText(String inputText, String texto, boolean clearField) {
        if (clearField) {
            clearField(getInputTextXPath(inputText));
        }
        setText(getInputTextXPath(inputText), texto);
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                     <i>texto</i>
     * @param inputText                 <i>nome do campo</i>
     * @param qtdDIVs                   <i>wquantidade de divs</i>
     */
    public static void setInputText(String inputText, String texto, int qtdDIVs) {
        setText(getInputTextXPath(inputText, qtdDIVs), texto);
    }

    /**
     * <b>Metodo que preenche o texto</b>
     * @param texto                         <i>texto</i>
     * @param by                            <i>xpath</i>
     */
    public static void setInputText(By by, String texto) {
        setText(by, texto);
    }

}
