package hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


import static Reports.ExtentReports.setCucumberLanguage;

import static framework.ConfigFramework.getDriver;
import static main.Reports.logReports;

/**
 * <b>Classe Hook para gerenciar a execucao dos cenarios</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/01/2019
 */
public class Hook {
    /**
     * <b>Metodo de setup para o cenario, executa antes do cenario</b>
     *
     * @param scenario <i>cenario</i>
     */
    @Before
    public void setUp(Scenario scenario) {
        //Set cucumber language extent report
        setCucumberLanguage();
    }

    /**
     * <b>Metodo que executa ao final do cenario, depois de sua execucao</b>
     *
     * @param scenario <i>cenario</i>
     */
    @After
    public void cleanUp(Scenario scenario) {
        //End reports
        logReports(getDriver(), scenario);
        //Refresh browser
        getDriver().navigate().refresh();
    }
}
