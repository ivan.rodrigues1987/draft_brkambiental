package stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.lang.reflect.InvocationTargetException;

import static Reports.ExtentReports.appendToReport;
import static main.WebElementManager.CallCenter.ByCallCenter.*;
import static stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento.validaMensagemCPF;
import static stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento.selecionaResultado;
import static stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento.clicarEmPesquisarPor;
import static stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento.inserirLogradouro;
import static stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento.clicarLogradouro;
import static framework.AssertManager.assertElementExists;
import static framework.AssertManager.assertElementText;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserVerifications.elementExists;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.fecharTela;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.CentralAtendimento.trocarIframe;
import static main.Utils.ComumScreens.CentralAtendimento.botaoCentralAtendimento;
import static main.Utils.ComumScreens.CentralAtendimento.selecionarAbaAtendimento;
import static main.Utils.Utils.getDataTable;
import static main.Utils.Utils.gerarMascaraCPF;
import static main.Utils.Utils.mergeObjects;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Botoes.clicarBotaoSpan;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static main.WebElementManager.Xpath.getCampoXPathSeguidoPorLabel;
import static main.WebElementManager.Xpath.getElementPorTexto;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.waitElementAppear;
import static main.WebElementManager.Xpath.byBotaoFecharJanela;
import static main.WebElementManager.Xpath.getCampoXPathSeguidoPorLabelContains;
import static runner.Setup.Json.DadosDinamicos.getDadosCallCenter;

/**
 * @author Juan Castillo
 * @since 15/04/2019
 */
public class StepsGestaoCallCenterTipoAtendimento {

    private GestaoCallCenterTipoAtendimento gestaoCallCenterTipoAtendimento =
            getDadosCallCenter().getGestaoCallCenterTipoAtendimento();
    private GestaoCallCenterTipoAtendimento dbDados = new GestaoCallCenterTipoAtendimento();

    private static final int SEGUNDOS = 4;
    private static final int QTD_DIVS = 3;


    @E("^clicar em \"([^\"]*)\" em Gestao CallCenterTipoAtendimento$")
    public void clicarEmEmGestaoCallCenterTipoAtendimentoCC(String valor) {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        clicarBotao(valor);
        trocarIframe(false);

    }

    @E("^em Dados Solicitante selecionar a Forma de Atendimento \"([^\"]*)\"$")
    public void emDadosSolicitanteSelecionarAFormaDeAtendimentoCC(String valor) {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        BrowserVerifications.waitElementToBeEnable("Forma Atend", DEFAULT_TIME_OUT);
        setTextoCombo("Forma Atend", valor, true);

        trocarIframe(false);

    }

    @E("^preencher os seguintes dados do solicitante$")
    public void preencherOsSeguintesDadosDoSolicitanteCC(DataTable table)
            throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String aux;

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        GestaoCallCenterTipoAtendimento gestaoCallCenterTipoAtendimentoBDD = getDataTable(table,
                GestaoCallCenterTipoAtendimento.class);
        gestaoCallCenterTipoAtendimento = gestaoCallCenterTipoAtendimento.
                getCampoGestaoCallCenterTipoAtendimento(gestaoCallCenterTipoAtendimento,
                        gestaoCallCenterTipoAtendimentoBDD);
        gestaoCallCenterTipoAtendimento = (GestaoCallCenterTipoAtendimento)
                mergeObjects(new GestaoCallCenterTipoAtendimento(
                        true, gestaoCallCenterTipoAtendimentoBDD.getOrigemAtend(),
                        gestaoCallCenterTipoAtendimentoBDD.obterCpf()), gestaoCallCenterTipoAtendimento);

        //Origem Atend
        setTextoCombo("Origem Atend", getCampo(gestaoCallCenterTipoAtendimentoBDD.getOrigemAtend(),
                gestaoCallCenterTipoAtendimento.getOrigemAtend()), true);


        //Solicitante
        setInputText("Solicitante", getCampo(gestaoCallCenterTipoAtendimentoBDD.getSolicitante(),
                gestaoCallCenterTipoAtendimento.getSolicitante()), true);

        //Identidade
        setInputText("Identidade", getCampo(gestaoCallCenterTipoAtendimentoBDD.getIdentidade(),
                gestaoCallCenterTipoAtendimento.getIdentidade()));

        //CPF
        aux = getCampo(gestaoCallCenterTipoAtendimentoBDD.obterCpf(), gestaoCallCenterTipoAtendimento.obterCpf());
        aux = gerarMascaraCPF(aux);
        BrowserVerifications.waitPersistentTextSet(getInputTextXPath("C.P.F."), aux, DEFAULT_TIME_OUT);
        getCampo("CPF", aux);

        //Telefone
        setInputText("Tel", getCampo(gestaoCallCenterTipoAtendimentoBDD.getTel(),
                gestaoCallCenterTipoAtendimento.getTel()));

        //Referencia
        setInputText("Referência", getCampo(gestaoCallCenterTipoAtendimentoBDD.getReferencia(),
                gestaoCallCenterTipoAtendimento.getReferencia()));

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);
    }


    @E("^em Servicos, selecionar o Servico Standard \"([^\"]*)\" e a Prioridade \"([^\"]*)\"$")
    public void emServicosSelecionarOServicoStandardEAPrioridadeCC(String servico, String prioridade) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Serviço Standard
        BrowserVerifications.waitElementToBeEnable("Serviço Standard", DEFAULT_TIME_OUT);
        setComboClicando("Serviço Standard", servico, true);

        //Prioridade
        setComboClicando("Prioridade", prioridade, true);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^em \"([^\"]*)\" inserir a observacao \"([^\"]*)\"$")
    public void emInserirAObservacaoCC(String selecionaAba, String observacao) throws Throwable {
        if (!observacao.isEmpty()) {
            dbDados.preencherAbasOrdemServico(selecionaAba, observacao);
        }
    }

    @E("^em Central de Atendimento clicar no botao \"([^\"]*)\" e salvar$")
    public void emCentralDeAtendimentoClicarNoBotaoESalvarCC(String botao) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Serviço Standard
        botaoCentralAtendimento(botao);
        botaoCentralAtendimento("Salvar");

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^em Central de Atendimento clicar no botao \"([^\"]*)\"$")
    public void emCentralDeAtendimentoClicarNoBotaoCC(String botao) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Serviço Standard
        botaoCentralAtendimento(botao);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^deve ser apresentada a tela de Emissao de Protocolo com a Solicitacao$")
    public void deveSerApresentadaATelaDeEmissaoDeProtocoloComASolicitacaoCC() {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        gestaoCallCenterTipoAtendimento.setProtocolo(
                getText(getCampoXPathSeguidoPorLabelContains("Solicitação", "", 2)));
        assertElementText(getCampoXPathSeguidoPorLabelContains("Solicitação", "", 2),
                gestaoCallCenterTipoAtendimento.getProtocolo());
        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

        appendToReport(getDriver());
    }

    @E("^fechar a janela de Emissao de Protocolo$")
    public void fecharAJanelaDeEmissaoDeProtocoloCC() {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        if (elementExists(byBotaoFecharJanela(1))) {
            fecharTela();
        } else {
            fecharTela(2);
        }
        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^selecionar a Aba \"([^\"]*)\"$")
    public void selecionarAAbaCC(String valor) throws Throwable {


        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Selecionar ABA
        clicarBotaoSpan(valor);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @Entao("^validar o protocolo gerado$")
    public void validarOProtocoloGeradoCC() {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Selecionar REGISTRO
        clicarBotao(gestaoCallCenterTipoAtendimento.getProtocolo(), 1);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

        //TIRA PRINT DA VALIDACAO
        appendToReport(getDriver());
    }

    @Entao("^validar que a solicitacao esta sendo apresentada na situacao \"([^\"]*)\"$")
    public void validarQueASolicitacaoEstaSendoApresentadaNaSituacaoCC(String situacao) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //VERIFICA A SITUACAO PARA O PROTOCOLO SALVO ANTERIORMENTE

        //Selecionar REGISTRO
        situacao = gestaoCallCenterTipoAtendimento.getDadoCampoGestaoCallCenterTipoAtendimento(situacao);
        clicarBotao(situacao, 1);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

        appendToReport(getDriver());

    }

    @E("^clicar no botao \"([^\"]*)\" em Central de Atendimento$")
    public void clicarNoBotaoEmCentralDeAtendimentoCC(String valor) throws Throwable {
        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        if (valor.equals("Sim")) {
            if (waitElementAppear(getXPathElementTextEquals(DIV, "Sim"), SEGUNDOS)) {
                clicarBotao(valor);
            }
        } else {
            clicarBotao(valor);
        }
        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }


    @E("^salvar e deve ser apresentado a tela de Encerramento Automatico$")
    public void deveSerApresentadoATelaDeEncerramentoAutomaticoCC() {

        //VERIFICA A SITUACAO PARA O PROTOCOLO SALVO ANTERIORMENTE

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //ADICIONA E SALVA
        //clicarBotao("Adicionar");

        botaoCentralAtendimento("Salvar");

        //VERIFICA A SITUACAO PARA O PROTOCOLO SALVO ANTERIORMENTE

        assertElementExists(getElementPorTexto("div", "Encerramento Automático"));

        BrowserVerifications.wait(2);
        botaoCentralAtendimento("Confirmar");

        fecharTela(2);
        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);


    }


    @E("^clicar em \"([^\"]*)\" em Gestao CallCenterTipoAtendimento e selecionar a aba \"([^\"]*)\"$")
    public void clicarEmEmGestaoCallCenterTipoAtendimentoESelecionarAAbaCC(String botao, String aba) throws Throwable {
        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        botaoCentralAtendimento(botao);
        selecionarAbaAtendimento(botao, aba);
        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @Quando("^preencher o campo Unidade com \"([^\"]*)\" e selecionar ENTER$")
    public void preencherOCampoUnidadeComESelecionarENTERCC(String unidade) throws Throwable {

        By janela = getByJanela("Central de atendimento");

        setTextoCombo("Unidade", unidade, true);

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        BrowserVerifications.waitElementToBeVisible(janela, DEFAULT_TIME_OUT);
        //VOLTA PARA FRAME PRINCIPAL
        trocarIframe(false);

        clickOnElement(getByLupa());

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        BrowserVerifications.waitElementToBeVisible(getElementPorTexto("div", "Pesquisa Ligação"), DEFAULT_TIME_OUT);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^preencher os seguintes campos de filtro da tela de Pesquisa de Ligacao$")
    public void preencherOsSeguintesCamposDeFiltroDaTelaDePesquisaDeLigacaoCC(DataTable table) {

        GestaoCallCenterTipoAtendimento gestaoCallCenterTipoAtendimentoBDD =
                getDataTable(table, GestaoCallCenterTipoAtendimento.class);
        gestaoCallCenterTipoAtendimento = new GestaoCallCenterTipoAtendimento(true,
                gestaoCallCenterTipoAtendimentoBDD.getOrigemAtend(), gestaoCallCenterTipoAtendimentoBDD.obterCpf());

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        //Cidade
        setComboClicando("Cidade", getCampo(gestaoCallCenterTipoAtendimentoBDD.getCidade(),
                gestaoCallCenterTipoAtendimento.getCidade()));

        //Logradouro
        clicarLogradouro();
        inserirLogradouro(getCampo(gestaoCallCenterTipoAtendimentoBDD.getLogradouro(),
                gestaoCallCenterTipoAtendimento.getLogradouro()));

        //nroInmovel
        setInputText("Nr. Imóvel", getCampo(gestaoCallCenterTipoAtendimentoBDD.getNrImovel(),
                gestaoCallCenterTipoAtendimento.getNrImovel()));

        //Complemento
        setInputText("Complemento", getCampo(gestaoCallCenterTipoAtendimentoBDD.getComplemento(),
                gestaoCallCenterTipoAtendimento.getComplemento()));

        //CEP
        setInputText(
                "CEP", getCampo(gestaoCallCenterTipoAtendimentoBDD.getCep(), gestaoCallCenterTipoAtendimento.getCep()));

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);


    }

    @E("^em pesquisar por marcar a opcao \"([^\"]*)\"$")
    public void emPesquisarPorMarcarAOpcaoCC(String valor) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        clicarEmPesquisarPor(valor);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }

    @E("^selecionar um dos logradouros retornado na pesquisa e confirmar$")
    public void selecionarUmDosLogradourosRetornadoNaPesquisaCC() {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        selecionaResultado();
        botaoCentralAtendimento("Confirmar");

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }


    @E("^em Dados Solicitante inserir o CPF \"([^\"]*)\"$")
    public void emDadosSolicitanteInserirOCPFCC(String valor) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);
        setInputText("C.P.F.", valor, true);
        setInputText("Solicitante", "value", true);
        trocarIframe(false);

        gestaoCallCenterTipoAtendimento.setProtocolo(getText(getByProtocolo()));

    }

    @Entao("^a janela de mensagem callcenter sera: \"([^\"]*)\"$")
    public void aJanelaDeMensagemCallcenterSeraCC(String valor) throws Throwable {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        validaMensagemCPF(valor);

        trocarIframe(false);

    }

    @E("^validar que a cidade da ligacao e igual a cidade exibida em Central de Atendimento$")
    public void validarQueACidadeDaLigacaoEIgualACidadeExibidaEmCentralDeAtendimentoCC() {
        trocarIframe(true);
        gestaoCallCenterTipoAtendimento.setCidadeLigacao(getText(getCampoXPathSeguidoPorLabel("Cidade", "", QTD_DIVS)));
        trocarIframe(false);

    }

    @E("^em Central de Atendimento clicar no icone \"([^\"]*)\"$")
    public void emCentralDeAtendimentoClicarNoIconeCC(String icone) throws Throwable {
        switch (icone) {
            case "Localização":
                clickOnElement(getByIconeLocalizacao());
                break;
            case "Feriado":
                clickOnElement(getByIconeFeriado());
                break;
            default:
                //break;
        }
    }

    @Entao("^no tooltip \"([^\"]*)\" deve ser exibido a informacao desejada$")
    public void noTooltipDeveSerExibidoAInformacaoDesejadaCC(String icone) throws Throwable {
        switch (icone) {
            case "Localização":
                assertElementExists(getByIconeLocalizacao(gestaoCallCenterTipoAtendimento.getCidadeLigacao()));
                break;
            case "Feriado":
                assertElementExists(getByIconeLocalizacao("Feriados"));
                break;
            default:
                //break;
        }
        appendToReport(getDriver());
    }


    @Quando("^preencher o campo Unidade com \"([^\"]*)\" e informar" +
            " a ligacao \"([^\"]*)\" em ordem de servico e teclar ENTER$")
    public void preencherOCampoUnidadeComEInformarALigacaoOrdemDeServicoETeclarENTERCC(String unidade, String valor)
            throws Throwable {
        String aux;

        //By janela = getByJanela("Central de atendimento");

        //setTextoCombo("Unidade", unidade, true);

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        //trocarIframe(true);
        //BrowserVerifications.waitElementToBeVisible(janela, DEFAULT_TIME_OUT);
        //VOLTA PARA FRAME PRINCIPAL
        //trocarIframe(false);

        gestaoCallCenterTipoAtendimento.getLigacaoPorEstadoOS(valor);
        aux = gestaoCallCenterTipoAtendimento.getDadoCampoGestaoCallCenterTipoAtendimento(valor);

        clickOnElement(getByLigacao());
        if (valor.toUpperCase().contains("PROTOCOLO") && !aux.toUpperCase().contains("P")) {
            sendKeys("P" + aux);
        } else {
            sendKeys(aux);
        }
        sendKeys(Keys.ENTER);
        BrowserVerifications.wait(2);

        //Exibir no relatorio campo e seu dado
        getCampo(valor, aux);
    }
}
