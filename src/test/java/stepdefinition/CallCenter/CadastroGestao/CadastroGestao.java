package stepdefinition.CallCenter.CadastroGestao;

import main.Utils.Utils;

/**
 * @author Renan Ferreira dos Santos
 * @since 27/03/2019
 */
public class CadastroGestao {
    private static String gestaoCadastrada;
    private String gestao;
    private String nomeReduzido;
    private String ativo;
    private String informacao;

    private static final int TAMANHO_GESTAO = 8;
    private static final int TAMANHO_NOME_REDUZIDO = 3;

    public static String getGestaoCadastrada() {
        return gestaoCadastrada;
    }

    public static void setGestaoCadastrada(String gestaoCadastrada) {
        CadastroGestao.gestaoCadastrada = gestaoCadastrada;
    }

    public String getGestao() {
        return gestao;
    }

    public void setGestao(String gestao) {
        this.gestao = gestao;
    }

    public String getNomeReduzido() {
        return nomeReduzido;
    }

    public void setNomeReduzido(String nomeReduzido) {
        this.nomeReduzido = nomeReduzido;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getInformacao() {
        return informacao;
    }

    public void setInformacao(String informacao) {
        this.informacao = informacao;
    }

    public void inicializarDadosGestao() {
        this.gestao = "Gestao " + Utils.geradorNumberRandom(TAMANHO_GESTAO);
        this.nomeReduzido = "Rlg " + Utils.geradorNumberRandom(TAMANHO_NOME_REDUZIDO);
    }


}
