package stepdefinition.CallCenter.CadastroGestao;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Checkbox;
import main.WebElementManager.InputText.InputText;
import main.WebElementManager.TextArea;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;

/**
 * @author Renan Ferreira dos Santos
 * @since 27/03/2019
 */
public class StepsCadastroGestao {


    @Quando("^preencher os seguintes campos em Cadastro Gestao Call Center:$")
    public void preencherOsSeguintesCamposEmCadastroGestaoCallCenter(DataTable dataTable) {
        CadastroGestao cadastroGestaoBDD = getDataTable(dataTable, CadastroGestao.class);
        CadastroGestao cadastroGestao = new CadastroGestao();

        //Inicializa dados dinamicos
        cadastroGestao.inicializarDadosGestao();

        //GESTAO
        CadastroGestao.setGestaoCadastrada(getCampo(cadastroGestaoBDD.getGestao(), cadastroGestao.getGestao()));
        InputText.setInputText("Gestão", CadastroGestao.getGestaoCadastrada(), true);

        //NOME REDUZIDO
        InputText.setInputText("Nome Reduzido", getCampo(cadastroGestaoBDD.getNomeReduzido(),
                cadastroGestao.getNomeReduzido()), true);

        //ATIVO
        Checkbox.clicarCheckBox("Ativo", cadastroGestaoBDD.getAtivo());

        //INFORMACAO
        TextArea.setTextArea("Informação", cadastroGestaoBDD.getInformacao(), true);
    }
}
