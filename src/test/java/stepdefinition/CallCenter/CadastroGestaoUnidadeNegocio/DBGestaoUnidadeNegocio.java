package stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;
import java.util.Stack;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class DBGestaoUnidadeNegocio extends DBActions {
    private static final int QTD_LINHAS = 20;

    public DBGestaoUnidadeNegocio(String environment) {
        super(environment);
    }

    public DBGestaoUnidadeNegocio(DataBase dataBase) {
        super(dataBase);
    }

    public GestaoUnidadeNegocio getDadosGestaoUnidadeNegocio() {
        GestaoUnidadeNegocio gestaoUnidadeNegocio = new GestaoUnidadeNegocio();

        gestaoUnidadeNegocio.setGestoesDisponiveis(getGestoes(true));

        return gestaoUnidadeNegocio;
    }

    //Retorna as gestoes
    public Stack<String> getGestoes(boolean disponiveis) {
        Stack<String> gestoes = new Stack<>();
        ArrayList<String> result = getColumnValues("gestao",
                "SELECT ges.gestao FROM GestaoCallCenter ges " +
                        "WHERE ativo='S' AND " + ((disponiveis) ? "NOT" : "") +
                        " EXISTS (SELECT id FROM GestaoCallCenterUnidadeNegocio WHERE idGestaoCallCenter = ges.id)",
                QTD_LINHAS);

        for (String rs : result) {
            gestoes.push(rs);
        }
        return gestoes;
    }
}
