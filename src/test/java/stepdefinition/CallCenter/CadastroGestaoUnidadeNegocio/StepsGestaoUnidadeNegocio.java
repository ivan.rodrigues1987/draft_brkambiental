package stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio;

import stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.Abas.ConfiguracaoEmergenciais;
import cucumber.api.DataTable;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.Combos;

import static stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio.removerServicoInformacao;
import static stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio.pesquisarServico;
import static stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio.validarServicoInformacao;
import static stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio.preencherObservacoes;
import static stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.GestaoUnidadeNegocio.getGestao;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Checkbox.clicarCheckBox;

import static main.WebElementManager.Combos.Combos.setComboClicando;

import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.TextArea.setTextArea;
import static runner.Setup.Json.DadosDinamicos.getDadosCallCenter;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class StepsGestaoUnidadeNegocio {

    @Quando("^preencher os seguintes campos em {2}Cadastro Gestao Call Center Unidade Negocio:$")
    public void preencherOsSeguintesCamposEmCadastroGestaoCallCenterUnidadeNegocio(DataTable dataTable) {
        GestaoUnidadeNegocio infoBDD = getDataTable(dataTable, GestaoUnidadeNegocio.class);
        String gestaoDinamica = getDadosCallCenter().getGestaoUnidadeNegocio().getGestaoDisponivel();

        //GESTAO
        GestaoUnidadeNegocio.setGestaoUnidadeNegocioCadastrada(getGestao(infoBDD.getGestao(), gestaoDinamica));
        Combos.setTextoCombo("Gestão", GestaoUnidadeNegocio.getGestaoUnidadeNegocioCadastrada(), true);
        //GestaoUnidadeNegocio.setCombo("Gestão", GestaoUnidadeNegocio.gestaoUnidadeNegocioCadastrada);

        //UNIDADE
        GestaoUnidadeNegocio.setCombo("Unidade", infoBDD.getUnidade());

        //CIDADE
        //GestaoUnidadeNegocio.setCombo("Cidade", infoBDD.cidade);
        //Combos.setComboCidade("Cidade", infoBDD.cidade);
        Combos.setTextoCombo("Cidade", infoBDD.getCidade(), true);

        //SERVICO INICIAL
        pesquisarServico("Serviço Inicial", infoBDD.getServicoInicial());

        //TIPO ATENDIMENTO
        setComboClicando("Tipo Atendimento", infoBDD.getTipoAtendimento());

        //(CHECKBOX) NAO ATENDIDO
        clicarCheckBox("Não Atendido pelo CallCenter", infoBDD.getNaoAtendido());

        //SERVICO INFORMACAO
        pesquisarServico("Serviço Informação", infoBDD.getServicoInformacao());

        //REQ UNIDADE
        setTextArea("Req. Unidade", infoBDD.getReqUnidade());

        //DOCUMENTACAO
        setTextArea("Documentação", infoBDD.getDocumentacao());

        //OBSERVACOES
        preencherObservacoes(infoBDD.getObservacoes());
    }

    @E("^na aba Configuracao Emergenciais preencher os seguintes campos:$")
    public void naAbaConfiguracaoEmergenciaisPreencherOsSeguintesCampos(DataTable dataTable) {
        ConfiguracaoEmergenciais infoBDD = getDataTable(dataTable, ConfiguracaoEmergenciais.class);

        //CLICA NA ABA
        clicarAba("Configuração Emergenciais");
        //Seleciona novamente a combo da gestao para habilitar os campos
        setComboClicando("Gestão", GestaoUnidadeNegocio.getGestaoUnidadeNegocioCadastrada());

        //(CHECKBOX) EMERGENCIA
        clicarCheckBox("Emergência", infoBDD.getEmergencia());

        //TIPO ATENDIMENTO
        setComboClicando("Tipo Atendimento", 2, infoBDD.getTipoAtendimento());

        //DIA SEMANA - HORARIO INICIO
        setInputTextEPressionarEnter(1, "Horário Inicio", infoBDD.getDiaSemanaHorarioInicio(), DEFAULT_TIME_OUT);

        //DIA SEMANA - HORARIO FIM
        setInputTextEPressionarEnter(1, "Horário Fim", infoBDD.getDiaSemanaHorarioFim(), DEFAULT_TIME_OUT);

        //FIM DE SEMANA - HORARIO INICIO
        setInputTextEPressionarEnter(2, "Horário Inicio", infoBDD.getFinaldeSemanaHorarioInicio(), DEFAULT_TIME_OUT);

        //FIM DE SEMANA - HORARIO FIM
        setInputTextEPressionarEnter(2, "Horário Fim", infoBDD.getFinaldeSemanaHorarioFim(), DEFAULT_TIME_OUT);
    }

    @E("^adicionar o servico de informacao \"([^\"]*)\"$")
    public void adicionarOServicoDeInformacao(String servicoInformacao) {
        pesquisarServico("Serviço Informação", servicoInformacao);
        clicarBotao("Adicionar");
    }

    @E("^remover o servico de informacao \"([^\"]*)\"$")
    public void removerOServicoDeInformacao(String servicoInformacao) {
        String servico = getCampo(servicoInformacao, getCampoPesquisado(servicoInformacao));
        removerServicoInformacao(servico);
    }

    @E("^validar que o servico de informacao \"([^\"]*)\" foi \"([^\"]*)\"$")
    public void validarQueOServicoDeInformacaoFoi(String servicoInformacao, String acao) {
        String servico = getCampo(servicoInformacao, getCampoPesquisado(servicoInformacao));
        validarServicoInformacao(servico, acao);
    }


}
