package stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio.Abas;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class ConfiguracaoEmergenciais {
    private String emergencia;
    private String tipoAtendimento;
    private String diaSemanaHorarioInicio;
    private String diaSemanaHorarioFim;
    private String finaldeSemanaHorarioInicio;
    private String finaldeSemanaHorarioFim;

    public String getEmergencia() {
        return emergencia;
    }

    /*public void setEmergencia(String emergencia) {
        this.emergencia = emergencia;
    }*/

    public String getTipoAtendimento() {
        return tipoAtendimento;
    }

    /*public void setTipoAtendimento(String tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }*/

    public String getDiaSemanaHorarioInicio() {
        return diaSemanaHorarioInicio;
    }

    /*public void setDiaSemanaHorarioInicio(String diaSemanaHorarioInicio) {
        this.diaSemanaHorarioInicio = diaSemanaHorarioInicio;
    }*/

    public String getDiaSemanaHorarioFim() {
        return diaSemanaHorarioFim;
    }

    /*public void setDiaSemanaHorarioFim(String diaSemanaHorarioFim) {
        this.diaSemanaHorarioFim = diaSemanaHorarioFim;
    }*/

    public String getFinaldeSemanaHorarioInicio() {
        return finaldeSemanaHorarioInicio;
    }

    /*public void setFinaldeSemanaHorarioInicio(String finaldeSemanaHorarioInicio) {
        this.finaldeSemanaHorarioInicio = finaldeSemanaHorarioInicio;
    }*/

    public String getFinaldeSemanaHorarioFim() {
        return finaldeSemanaHorarioFim;
    }

    /*public void setFinaldeSemanaHorarioFim(String finaldeSemanaHorarioFim) {
        this.finaldeSemanaHorarioFim = finaldeSemanaHorarioFim;
    }*/
}
