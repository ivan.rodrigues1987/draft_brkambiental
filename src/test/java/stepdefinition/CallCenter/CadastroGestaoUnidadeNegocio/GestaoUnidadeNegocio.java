package stepdefinition.CallCenter.CadastroGestaoUnidadeNegocio;

import stepdefinition.CallCenter.CadastroGestao.CadastroGestao;
import framework.BrowserManager.BrowserVerifications;

import main.WebElementManager.Combos.Combos;

import main.WebElementManager.Xpath;
import org.openqa.selenium.By;

import java.util.Stack;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.setText;
import static framework.BrowserManager.BrowserActions.doubleClickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserVerifications.waitElementExists;
import static framework.BrowserManager.BrowserVerifications.waitElementNotExists;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Botoes.clicarBotao;

import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.getXPathElementTextEquals;
import static org.jsoup.helper.StringUtil.isBlank;

/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class GestaoUnidadeNegocio {
    private static String gestaoUnidadeNegocioCadastrada;
    private Stack<String> gestoesDisponiveis;
    private GestaoUnidadeNegocio gestaoUnidadeNegocioComServico;
    private String idGestao;
    private String gestao;
    private String unidade;
    private String cidade;
    private String servicoInicial;
    private String tipoAtendimento;
    private String naoAtendido;
    private String servicoInformacao;
    private String reqUnidade;
    private String documentacao;
    private String observacoes;

    public static String getGestaoUnidadeNegocioCadastrada() {
        return gestaoUnidadeNegocioCadastrada;
    }

    public static void setGestaoUnidadeNegocioCadastrada(String gestaoUnidadeNegocioCadastrada) {
        GestaoUnidadeNegocio.gestaoUnidadeNegocioCadastrada = gestaoUnidadeNegocioCadastrada;
    }

    public Stack<String> getGestoesDisponiveis() {
        return gestoesDisponiveis;
    }

    public String getIdGestao() {
        return idGestao;
    }

    public void setIdGestao(String idGestao) {
        this.idGestao = idGestao;
    }

    public String getGestao() {
        return gestao;
    }

    public void setGestao(String gestao) {
        this.gestao = gestao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getServicoInicial() {
        return servicoInicial;
    }

    public void setServicoInicial(String servicoInicial) {
        this.servicoInicial = servicoInicial;
    }

    public String getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(String tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public String getNaoAtendido() {
        return naoAtendido;
    }

    public void setNaoAtendido(String naoAtendido) {
        this.naoAtendido = naoAtendido;
    }

    public String getServicoInformacao() {
        return servicoInformacao;
    }

    public void setServicoInformacao(String servicoInformacao) {
        this.servicoInformacao = servicoInformacao;
    }

    public String getReqUnidade() {
        return reqUnidade;
    }

    public void setReqUnidade(String reqUnidade) {
        this.reqUnidade = reqUnidade;
    }

    public String getDocumentacao() {
        return documentacao;
    }

    public void setDocumentacao(String documentacao) {
        this.documentacao = documentacao;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public static void setCombo(String combo, String valor) {
        BrowserVerifications.waitElementToBeEnable(combo, DEFAULT_TIME_OUT);

        //Clica na combo
        clickOnElement(Combos.getXPathCombo(combo));

        //Envia valor
        sendKeys(valor);

        //Clica no Texto que apareceu
        clickOnElement(getXPathElementTextEquals(DIV, valor));
    }

    public static void pesquisarServico(String campoServico, String valor) {

        if (!isBlank(valor)) {
            //AGUARDA AS LUPAS SEREM HABILITADAS
            BrowserVerifications.
                    waitElementNotExists(By.xpath("//div[@class='clear-button GKPSPJYCMK GKPSPJYCOJ']"),
                    DEFAULT_TIME_OUT);

            //CLICA NA LUPA
            clickOnElement(By.xpath("(//label[text()='" + campoServico +
                    ":']/../following-sibling::div/div/table/tbody/tr/td/div/div/table/tbody/tr/td)[1]"));

            //SELECIONA O ITEM E CLICA EM OK
            doubleClickOnElement(Xpath.getXPathElementWithText(DIV, valor));
            clicarBotao(BTN_OK);
        }
    }

    public static void preencherObservacoes(String observacoes) {
        setText(By.xpath("//div[2]/div[1]/div[1]/div[1]/textarea[1]"), observacoes);
    }

    //Remove um servico de informacao do grid
    public static void removerServicoInformacao(String servicoInformacao) {
        //Seleciona a linha no grid
        clickOnElement(getXPathElementWithText(DIV, servicoInformacao));
        //Clica em remover
        clicarBotao("Remover");
    }

    //Valida que o servico de informacao foi adicionado ou removido
    public static void validarServicoInformacao(String servicoInformacao, String acao) {
        By byCaminhoServicoGrid = getXPathElementWithText(DIV, servicoInformacao);

        switch (acao.toUpperCase()) {
            case "ADICIONADO":
                waitElementExists(byCaminhoServicoGrid, DEFAULT_TIME_OUT);
                break;
            case "REMOVIDO":
                waitElementNotExists(byCaminhoServicoGrid, DEFAULT_TIME_OUT);
                break;
            default:
                //break;
        }
    }

    public String getGestaoDisponivel() {
        return gestoesDisponiveis.pop();
    }

    public void setGestoesDisponiveis(Stack<String> gestoesDisponiveis) {
        this.gestoesDisponiveis = gestoesDisponiveis;
    }

    public GestaoUnidadeNegocio getGestaoUnidadeNegocioComServico() {
        return gestaoUnidadeNegocioComServico;
    }

    public void setGestaoUnidadeNegocioComServico(GestaoUnidadeNegocio gestaoUnidadeNegocioComServico) {
        this.gestaoUnidadeNegocioComServico = gestaoUnidadeNegocioComServico;
    }

    public static String getGestao(String gestaoBDD, String gestao) {
        if (gestaoBDD.toUpperCase().equals("[GESTAO CADASTRADA ANTERIORMENTE]")) {
            gestao = CadastroGestao.getGestaoCadastrada();
        }
        return getCampo(gestaoBDD, gestao);
    }


}
