package stepdefinition.CallCenter.CadastroTipoAtendimento;


import cucumber.api.java.pt.Quando;
import main.Utils.Utils;
import main.WebElementManager.InputText.InputText;

import static main.Utils.ComumScreens.Auxiliar.getCampo;


/**
 * @author Renan Ferreira dos Santos
 * @since 29/03/2019
 */
public class StepsCadastroTipoAtendimento {
    private static final int TAM_FIELD = 8; // Tamanho do Campo para geração de numero aleatório
    @Quando("^preencher o campo \"([^\"]*)\" com \"([^\"]*)\" em Cadastro Tipo Atendimento$")
    public void preencherOCampoComEmCadastroTipoAtendimento(String campo, String valor) {
        //TIPO ATENDIMENTO
        InputText.setInputText(campo, getCampo(valor, "Tipo Atendimento " +
                Utils.geradorNumberRandom(TAM_FIELD)), true);
    }
}
