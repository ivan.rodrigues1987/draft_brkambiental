package stepdefinition.Atendimento;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;
import main.Massas.QuantidadeMassasAtendimento;
import main.Massas.QuantidadeMassasCallCenter;
import stepdefinition.CallCenter.CadastroGestaoCallCenterTipoAtendimento.GestaoCallCenterTipoAtendimento;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Juan Castillo
 * @since 12/04/2019
 */
public class DBGestaoTipoAtendimento extends DBActions {

    private QuantidadeMassasAtendimento qtdMassasAtendimento = new QuantidadeMassasAtendimento();

    public DBGestaoTipoAtendimento(String environment) {
        super(environment);
    }

    public DBGestaoTipoAtendimento(DataBase dataBase) {
        super(dataBase);
    }

    public GestaoTipoAtendimento getDadosGestaoAtendimento() throws SQLException {
        GestaoTipoAtendimento gestaoCall = new GestaoTipoAtendimento();

        gestaoCall.getListMapLigacao().put("Ligações sem OS", retornarLigacaoSemOrdemServicoAberto());
        gestaoCall.getListMapLigacao().put("Ligações com OS", retornarProtocoloComOSAberta());
        gestaoCall.setProtocolo(retornarProtocoloSemOrdemServicoAberto());
        gestaoCall.getListMapLigacao().put("Ligações com Débito", retornarLigacaoComDebito());
        return gestaoCall;
    }


//    //ID CIDADE
//    public String retornarIdcidade(String cidade) throws SQLException {
//        return getColumnInfo("id", "SELECT id FROM CIDADE LIKE '" + cidade + "%'");
//    }

    //LOCALIZA LIGACAO E PROTOCOLOS SEM ORDEM DE SERVICO ABERTO
    public ArrayList retornarLigacaoSemOrdemServicoAberto() throws SQLException {

        return getColumnValues("idligacao",
                               "SELECT l.id AS idLigacao, o.id AS os FROM ligacao l LEFT OUTER JOIN ordemservico o " +
                                       "ON l.id = o.idligacao WHERE o.idligacao is null",
                qtdMassasAtendimento.getGestaoTipoAtendimentoSemOS());


    }

    //LOCALIZA PROTOCOLOS SEM ORDEM DE SERVICO ABERTO
    public String retornarProtocoloSemOrdemServicoAberto() throws SQLException {
        GestaoTipoAtendimento gestaoTipoAtendimento = new GestaoTipoAtendimento();

        return getColumnValues("Protocolo",
                               "SELECT concat('P',o.id) AS Protocolo FROM ligacao l" +
                                       " LEFT OUTER JOIN ordemservico o ON l.id = o.idligacao" +
                                       " WHERE o.idligacao is not null", 1).get(0);

    }

    //Retornar protocolo com Ordem de Servico Aberta
    public ArrayList retornarProtocoloComOSAberta() throws SQLException {

        return getColumnValues("idligacao",
                               "select idLigacao from ordemservico inner join ordemservicodetalhe osd on" +
                                       " osd.servicostandardcallcenter = 'Falta de Água Informada pelo Cliente' and" +
                                       " osd.idordemservico = ordemservico.id",
                qtdMassasAtendimento.getGestaoTipoAtendimentoComDebito());

    }


    //Localiza ligações CORTADAS (com débito) que NÃO possuem ordem de serviço em aberto
    public ArrayList retornarLigacaoComDebito() throws SQLException {
        GestaoTipoAtendimento gestaoTipoAtendimento = new GestaoTipoAtendimento();

        ArrayList<String> info = getColumnsInfo("idligacao", "SELECT f.idligacao " +
                "FROM fatura f " +
                "INNER JOIN parametrosfaturamento pf ON pf.idcidade = f.idcidade " +
                "WHERE f.idfaturasituacao = 1 " +
                "AND f.datapagtoprovisorio IS NULL " +
                "AND nvl(f.pagamentoparcialrajada, 'N') = 'N' " +
                "AND to_date(f.datavencimento, 'yyyymmdd') + (pf.nrdiasvencimento) < current_date " +
                "AND ROWNUM = 1 " +
                "AND NOT EXISTS( " +
                "select os.IDSTATUSORDEMSERVICO from ORDEMSERVICO os " +
                "inner join ordemservicodetalhe detalhe on os.id = detalhe.idordemservico " +
                "where os.IDLIGACAO = f.idligacao and detalhe.IDSTATUSORDEMSERVICODETALHE = 1 " +
                ")");

        return info;

    }

}
