package stepdefinition.Atendimento;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.lang.reflect.InvocationTargetException;

import static Reports.ExtentReports.appendToReport;
import static framework.AssertManager.assertElementExists;
import static framework.AssertManager.assertElementText;
import static framework.BrowserManager.BrowserActions.*;
import static framework.BrowserManager.BrowserVerifications.elementExists;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.fecharTela;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.CentralAtendimento.*;
import static main.Utils.Utils.*;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Botoes.clicarBotaoSpan;
import static main.WebElementManager.Atendimento.ByAtendimento.*;
import static main.WebElementManager.CallCenter.ByCallCenter.getByLigacao;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static main.WebElementManager.Xpath.*;
import static runner.Setup.Json.DadosDinamicos.getDadosAtendimento;
import static stepdefinition.Atendimento.GestaoTipoAtendimento.*;

/**
 * @author Roger Rosolia
 * @since 21/08/2019
 */
public class StepsGestaoTipoAtendimento {

    private GestaoTipoAtendimento gestaoTipoAtendimento = getDadosAtendimento().getGestaoTipoAtendimento();
    private GestaoTipoAtendimento dbDados = new GestaoTipoAtendimento();

    private static final int SEGUNDOS = 4;
    private static final int QTD_DIVS = 3;


    @E("^na aba \"([^\"]*)\" acessar o item \"([^\"]*)\"$")
    public void naAbaAcessarOItem(String itemMenuLateral, String subItem) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Quando("^Informar a ligacao \"([^\"]*)\" em ordem de servico e apertar ENTER$")
    public void informarALigacaoEmOrdemDeServicoEApertarENTER(String valor) throws Throwable {
        String aux;

        gestaoTipoAtendimento.getLigacaoPorEstadoOS(valor);
        aux = gestaoTipoAtendimento.getDadoCampoGestaoTipoAtendimento(valor);
        clickOnElement(getByLigacaoATendimento());
        if (valor.toUpperCase().contains("PROTOCOLO") && !aux.toUpperCase().contains("P")) {
            sendKeys("P" + aux);
        } else {
            setText(By.xpath("//*[@id=\"x-widget-3-input\"]"),aux);
        }
        //TECLA ENTER
        sendKeys(Keys.ENTER);
        BrowserVerifications.wait(2);

        //Exibir no relatorio campo e seu dado
        getCampo(valor, aux);
    }
}

