package stepdefinition.Atendimento;

import framework.BrowserManager.BrowserActions;
import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import runner.Dados.Atendimento.DadosAtendimento;
import runner.Setup.Json.DadosDinamicos;

import java.util.ArrayList;
import java.util.HashMap;

import static framework.AssertManager.assertElementExists;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.getCampoValidado;
import static main.Utils.Utils.*;
import static main.WebElementManager.Botoes.clicarBotaoSpan;
import static main.WebElementManager.Atendimento.ByAtendimento.*;
import static main.WebElementManager.InputText.InputText.setInputText;
import static runner.Setup.Json.DadosDinamicos.getDadosAtendimento;

/**
 * @author Roger Rosolia
 * @since 04/04/2019
 */

public class GestaoTipoAtendimento {

    private WebDriver browser = getDriver();
    private String iframes = "//iframe[@id='centralAtendimento']";


    private String origemAtend;
    private String solicitante;
    private String identidade;
    private String cpf;
    private String tel;
    private String referencia;
    private String cidade;
    private String logradouro;
    private String nrImovel;
    private String complemento;
    private String cep;

    //NRO E PROTOCOLO LIGACOES SEM ORDEM DE SERVICO ABERTO
    private String codLigSA;
    private HashMap<String, ArrayList> listMapLigacao = new HashMap<>();

    //ATRIBUTOS INTERNOS PARA FINS LOCAIS
    private boolean primeiraExecucaoSO;
    private boolean primeiraExecucaoCO;
    private int indiceSO = 0;
    private int indiceCO = 0;

    //NRO LIGACOES COM DEBITO NAO ABERTO
    private String codLigCD;

    //NRO LIGACOES COM ORDEM DE SERVICO ABERTO
    private String codLigCA;

    //STATICS - REUTILIZA DADOS PREENCHIDOS
    private String idLigacaoCall;
    private String protocolo;
    private String cidadeLigacao;

    private static final int TAMANHO_SOLICITANTE = 8;
    private static final int TAMANHO_IDENTIDADE = 5;
    private static final int TAMANHO_TEL = 4;
    private static final int TAMANHO_REFERENCIA = 8;

    public GestaoTipoAtendimento() {
        listMapLigacao.put("Ligações sem OS", null);
        listMapLigacao.put("Ligações com Debito", null);
        listMapLigacao.put("Ligações com OS", null);
        this.primeiraExecucaoSO = true;
        this.primeiraExecucaoCO = true;
    }

    public GestaoTipoAtendimento(boolean gerarDadosTipoAtendimento, String origemAtend, String cpf) {
        if (gerarDadosTipoAtendimento) {
            gerarDadosTipoAtendimento(origemAtend, cpf);
        }
    }

    public GestaoTipoAtendimento getCampoGestaoTipoAtendimento(GestaoTipoAtendimento
                                            gestaoTipoAtendimento,
                                                                         GestaoTipoAtendimento gestaoTipoAtendimentoBDD) {
        this.cpf = getCampoValidado(gestaoTipoAtendimentoBDD.cpf, this.cpf);
        this.origemAtend = getCampoValidado(gestaoTipoAtendimentoBDD.origemAtend, this.origemAtend);
        this.codLigCA = getCampoValidado(gestaoTipoAtendimentoBDD.codLigCA,
                (String) getDadosAtendimento().getGestaoTipoAtendimento().
                        listMapLigacao.get("Ligações com OS").get(this.indiceCO));
        this.codLigCD = getCampoValidado(gestaoTipoAtendimentoBDD.codLigCD, this.codLigCD);
        this.codLigSA = getCampoValidado(gestaoTipoAtendimentoBDD.codLigSA,
                (String) DadosDinamicos.getDadosAtendimento().getGestaoTipoAtendimento().
                        listMapLigacao.get("Ligações sem OS").get(this.indiceSO));
        this.identidade = getCampoValidado(gestaoTipoAtendimentoBDD.identidade, this.identidade);
        this.referencia = getCampoValidado(gestaoTipoAtendimentoBDD.referencia, this.referencia);
        this.tel = getCampoValidado(gestaoTipoAtendimentoBDD.tel, this.tel);
        this.iframes = getCampoValidado(gestaoTipoAtendimentoBDD.iframes, this.iframes);
        this.cep = getCampoValidado(gestaoTipoAtendimentoBDD.cep, this.cep);
        this.cidade = getCampoValidado(gestaoTipoAtendimentoBDD.cidade, this.cidade);
        this.complemento = getCampoValidado(gestaoTipoAtendimentoBDD.complemento, this.complemento);
        this.logradouro = getCampoValidado(gestaoTipoAtendimentoBDD.logradouro, this.logradouro);
        this.nrImovel = getCampoValidado(gestaoTipoAtendimentoBDD.nrImovel, this.nrImovel);
        this.solicitante = getCampoValidado(gestaoTipoAtendimentoBDD.solicitante, this.solicitante);
        return gestaoTipoAtendimento;
    }

    public void getLigacaoPorEstadoOS(String estadoOS) {
        if (this.primeiraExecucaoSO && estadoOS.toUpperCase().contains("LIGAÇÃO SEM OS ABERTA")) {
            this.primeiraExecucaoSO = false;
        } else if (this.primeiraExecucaoCO && estadoOS.toUpperCase().contains("LIGAÇÃO COM OS ABERTA")) {
            this.primeiraExecucaoCO = false;
        } else if (!this.primeiraExecucaoCO && estadoOS.toUpperCase().contains("LIGAÇÃO COM OS ABERTA")) {
            this.indiceCO++;
        } else if (!this.primeiraExecucaoSO && (estadoOS.toUpperCase().contains("LIGAÇÃO SEM OS ABERTA"))) {
            this.indiceSO++;
        }
        String campoPesquisa = retirarMarcacao(estadoOS);

        switch (campoPesquisa.toUpperCase()) {
            case "LIGAÇÃO SEM OS ABERTA":
                this.idLigacaoCall = (String) getDadosAtendimento().getGestaoTipoAtendimento().
                        listMapLigacao.get("Ligações sem OS").get(this.indiceSO);
                break;
            case "LIGAÇÃO COM OS ABERTA":
                this.idLigacaoCall = (String) getDadosAtendimento().getGestaoTipoAtendimento().
                        listMapLigacao.get("Ligações com OS").get(this.indiceCO);
                break;
            case "LIGAÇÃO COM OS DEBITO":
            case "LIGAÇÃO":
                this.idLigacaoCall = (String) getDadosAtendimento().getGestaoTipoAtendimento().
                        listMapLigacao.get("Ligações com Débito").get(this.indiceCO);
                break;
            default:
                //break;
        }
    }

    public String getOrigemAtend() {
        return origemAtend;
    }

    public void setOrigemAtend(String origemAtend) {
        this.origemAtend = origemAtend;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public String obterCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNrImovel() {
        return nrImovel;
    }

    public void setNrImovel(String nrImovel) {
        this.nrImovel = nrImovel;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCodLigSA() {
        return codLigSA;
    }

    public void setCodLigSA(String codLigSA) {
        this.codLigSA = codLigSA;
    }

    public HashMap<String, ArrayList> getListMapLigacao() {
        return listMapLigacao;
    }

    public void setListMapLigacao(HashMap<String, ArrayList> listMapLigacao) {
        this.listMapLigacao = listMapLigacao;
    }

    public boolean isPrimeiraExecucaoSO() {
        return primeiraExecucaoSO;
    }

    public void setPrimeiraExecucaoSO(boolean primeiraExecucaoSO) {
        this.primeiraExecucaoSO = primeiraExecucaoSO;
    }

    public boolean isPrimeiraExecucaoCO() {
        return primeiraExecucaoCO;
    }

    public void setPrimeiraExecucaoCO(boolean primeiraExecucaoCO) {
        this.primeiraExecucaoCO = primeiraExecucaoCO;
    }

    public int getIndiceSO() {
        return indiceSO;
    }

    public void setIndiceSO(int indiceSO) {
        this.indiceSO = indiceSO;
    }

    public int getIndiceCO() {
        return indiceCO;
    }

    public void setIndiceCO(int indiceCO) {
        this.indiceCO = indiceCO;
    }

    public String getCodLigCD() {
        return codLigCD;
    }

    public void setCodLigCD(String codLigCD) {
        this.codLigCD = codLigCD;
    }

    public String getCodLigCA() {
        return codLigCA;
    }

    public void setCodLigCA(String codLigCA) {
        this.codLigCA = codLigCA;
    }

    public String getIdLigacaoCall() {
        return idLigacaoCall;
    }

    public void setIdLigacaoCall(String idLigacaoCall) {
        this.idLigacaoCall = idLigacaoCall;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getCidadeLigacao() {
        return cidadeLigacao;
    }

    public void setCidadeLigacao(String cidadeLigacao) {
        this.cidadeLigacao = cidadeLigacao;
    }

    private void gerarDadosTipoAtendimento(String origemAtendParametro, String cpfParametro) {
        this.solicitante = geradorCharRandom(TAMANHO_SOLICITANTE);
        this.origemAtend = origemAtendParametro;
        this.identidade = geradorCharRandom(TAMANHO_IDENTIDADE);
        this.cpf = getCpf(false);
        this.tel = "114444" + geradorNumberRandom(TAMANHO_TEL);
        this.referencia = geradorCharRandom(TAMANHO_REFERENCIA);

    }


    public void preencherAbasOrdemServico(String selecionaAba, String texto) {
        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        browser.switchTo().frame(BrowserVerifications.getElement(By.xpath(iframes)));

        clicarBotaoSpan(selecionaAba);
        BrowserActions.setText(By.xpath("//div[@class='noborders']/div[@class='GKPSPJYCEX']/textarea"), texto);

        //MUDA PARA O FRAME DEFAULT
        browser.switchTo().defaultContent();
    }

    public static void clicarLogradouro() {
        //Clicar checkbox
        clickOnElement(getByPesquisarPor("Digitar Logradouro"));
    }

    public static void inserirLogradouro(String valor) {
        setInputText(getByLogradouroTextAtendimento(), valor);
    }

    public static void clicarEmPesquisarPor(String valor) {
        //clicar checkbox
        clickOnElement(getByPesquisarPor(valor));
    }

    public static void selecionaResultado() {
        clickOnElement(getBySelecionaResultadoAtendimento());
    }

    public static void validaMensagemCPF(String valor) {

        assertElementExists(getMessagemCPF(valor));

    }

    public String getDadoCampoGestaoTipoAtendimento(String campo) {
        String aux = campo.toUpperCase();
        if (aux.contains("LIGAÇÃO")) {
            return this.idLigacaoCall;
        } else if (aux.contains("PROTOCOLO")) {
            return this.protocolo;
        }

        return campo;
    }

}
