package stepdefinition.Cadastro.MensagemFatura;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;
import java.util.Random;

public class DBMensagemFatura extends DBActions {

    public DBMensagemFatura(String environment){
        super(environment);
    }

    public DBMensagemFatura(DataBase dataBase) {
        super(dataBase);
    }

    public MensagemFatura getDadosMensagemFatura(){
        MensagemFatura mensagemFatura = new MensagemFatura();

        //Setando os atributos do objeto
        mensagemFatura.setCidade(getCidadeMensagemFatura());
        mensagemFatura.setMensagem(getMensagem());
        mensagemFatura.setPrioridade(getPrioridade());

        //retornando a mensagem fatura
        return mensagemFatura;
    }

    //Retorna a cidade
    public String getCidadeMensagemFatura(){
        return getColumnInfo("CIDADE", "SELECT CIDADE FROM CIDADE WHERE ROWNUM = 1");
    }

    //Retorna a mensagem da fatura
    public String getMensagem(){
        return getColumnInfo("MENSAGENSFATURA", "SELECT MENSAGENSFATURA FROM " +
                "MENSAGENSFATURA ORDER BY DATAFINAL DESC");
    }

    public String getPrioridade(){
        Random aux = new Random();
        int prioridadeAux = aux.nextInt(10)+1;
        String prioridade = String.valueOf(prioridadeAux);
        return prioridade;
    }
}
