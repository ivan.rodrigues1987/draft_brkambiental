package stepdefinition.Cadastro.MensagemFatura;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static main.Utils.Utils.retornarDataAtual;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

public class MensagemFatura {
    //CAMPOS DO FOMRULARIO
    private String cidade;
    private static String dataInicio = retornarDataAtual("/");
    private static String dataFinal = retornarDataAtual("/");
    private String mensagem;
    private String tipoImpressao;
    private String prioridade;




    //GETTERS E SETTERS
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public String getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getTipoImpressao() {
        return tipoImpressao;
    }

    public void setTipoImpressao(String tipoImpressao) {
        this.tipoImpressao = tipoImpressao;
    }

    public String getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(String prioridade) {
        this.prioridade = prioridade;
    }
    }
