package stepdefinition.Cadastro.MensagemFatura;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.ComboInputText;
import main.WebElementManager.InputText.InputText;
import org.apache.tika.metadata.Database;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

public class StepsMensagemFatura {
    private MensagemFatura mensagemFatura;

    public StepsMensagemFatura(){
        mensagemFatura = getDadosCadastro().getMensagemFatura();
    }


    @Quando("^incluir dados$")
    public void incluirDados(DataTable table) throws Throwable {
        MensagemFatura mensagemFaturaBDD = getDataTable(table, MensagemFatura.class);
        mensagemFaturaBDD.setCidade(getCampo(mensagemFaturaBDD.getCidade(), mensagemFatura.getCidade()));
        mensagemFaturaBDD.setMensagem(getCampo(mensagemFaturaBDD.getMensagem(),mensagemFatura.getMensagem()));
        mensagemFaturaBDD.setTipoImpressao(getCampo(mensagemFaturaBDD.getTipoImpressao(), mensagemFatura.getTipoImpressao()));
        mensagemFatura = mensagemFaturaBDD;

        //CIDADE
        setComboCidade("Cidade", mensagemFatura.getCidade());
        //DATA INICIO
        ComboInputText.setComboEPressionarEnter("Data Início", mensagemFatura.getDataInicio());
        //DATA FINAL
        ComboInputText.setComboEPressionarEnter("Data Início", mensagemFatura.getDataFinal());
        //TIPO IMPRESSAO
        clicarCheckBox("Convencional", getCampo(mensagemFaturaBDD.getTipoImpressao(), mensagemFatura.getTipoImpressao()));
        //PRIORIDADE
        InputText.setInputText("Prioridade", getCampo(mensagemFaturaBDD.getPrioridade(),mensagemFatura.getPrioridade()));
        //MENSAGEM
        InputText.setInputText("Mensagem", getCampo(mensagemFaturaBDD.getMensagem(),mensagemFatura.getMensagem()));
    }

    @Entao("^a mensagem criada e incluida no combo conforme esperado e listada em data decrescente$")
    public void aMensagemCriadaEIncluidaNoComboConformeEsperadoEListadaEmDataDecrescente() throws Throwable {
        //A COLUNA DATA FINAL DEVE SER ANALISADA

        //VERIFICAR SE A MENSAGEM FOI INCLUÍDA
    }
}
