package stepdefinition.Cadastro.Cliente;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import main.Utils.ComumScreens.PesquisaRapida;
import runner.Dados.Cadastro.Endereco;


import java.sql.SQLException;

import static stepdefinition.Cadastro.Excecao.Excecao.setText;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;

import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.Combos.Combos.getXPathCombo;

import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;
import static stepdefinition.Cadastro.Cliente.Cliente.PESSOA_FISICA;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;

public class StepsCliente {
    private Endereco endereco;
    private Cliente cliente;

    public StepsCliente() {
        endereco = getDadosCadastro().getEndereco();
    }

    @Quando("^preencher os seguintes campos em Cadastro de Cliente:$")
    public void preencherOsSeguintesCamposEmCadastroDeCliente(DataTable dataTable) throws SQLException {
        Cliente clienteBDD = getDataTable(dataTable, Cliente.class);
        cliente = new Cliente(true, clienteBDD.getTipoCliente(), clienteBDD.obterCpf(), clienteBDD.obterCnpj());

        //NOME
        BrowserVerifications.waitElementToBeVisible(getInputTextXPath("Nome"), DEFAULT_TIME_OUT);
        String nomeCliente = getCampo(clienteBDD.getNome().trim(), cliente.getNome());
        setText("Nome", nomeCliente, true);
        Cliente.setNomeCadastrado(nomeCliente);

        //TIPO CLIENTE
        setComboClicando("Tipo Cliente", clienteBDD.getTipoCliente());

        //CIDADE
        setComboCidade("Cidade", getCampo(clienteBDD.getCidade(), endereco.getCidade()));

        //PREENCHE CAMPOS ESPECIFICOS - BAIRRO E LOGRADOURO
        clienteBDD.preencherCamposEspecificos(endereco, cliente, clienteBDD.getTipoCliente());

        //NUMERO IMOVEL
        setInputText("Nr. Imóvel", getCampo(clienteBDD.getNumLogradouro(), endereco.getNumero()));

        //COMPLEMENTO
        setInputText("Complemento", getCampo(clienteBDD.getComplemento(), endereco.getComplemento()), true);

        //CEP
        setInputText("CEP", getCampo(clienteBDD.getCep(), endereco.getCep()), true);

        //ESTADO - entra automaticamente mas pode-se verificar**

        //UF
        setTextoCombo("UF", clienteBDD.getUf(), true);

        //TELEFONE FIXO
        setInputText("Telefone Fixo", getCampo(clienteBDD.getTelefoneFixo(), cliente.getTelefoneFixo()), true);

        //TELEFONE MOVEL
        setInputText("Telefone Movel", getCampo(clienteBDD.getTelefoneMovel(), cliente.getTelefoneMovel()), true);

        //EMAIL
        setInputText("E-mail", getCampo(clienteBDD.getEmail(), cliente.getEmail()), true);

        //OBSERVACAO
        setInputText("Observação", clienteBDD.getObservacao(), true);

        //PESSOA FISICA - Pendente**
        if (clienteBDD.getTipoCliente().equals(PESSOA_FISICA)) {
            //RG
            setInputText("R.G.", getCampo(clienteBDD.getRg(), clienteBDD.getRg()), true);

            //CPF
            setInputTextEPressionarEnter("C.P.F.", getCampo(clienteBDD.obterCpf(), cliente.obterCpf()));

            //NOME DO PAI
            setInputText("Nome do Pai", clienteBDD.getNomePai(), true);

            //NOME DA MAE
            setInputText("Nome da Mãe", clienteBDD.getNomeMae(), true);
        } else {
            setInputText("C.N.P.J.", "   " + getCampo(clienteBDD.obterCnpj(), cliente.obterCnpj()), true);
        }

    }

    @Quando("^na tela de Pesquisa eu preencher a cidade \"([^\"]*)\", o campo \"([^\"]*)\" e o valor \"([^\"]*)\"$")
    public void naTelaDePesquisaEuPreencherACidadeOCampoEOValor(String cidade, String campo, String valor)
            throws Throwable {
        BrowserVerifications.waitElementToBeVisible(getXPathCombo("Cidade"), 1);
        PesquisaRapida.pesquisar(cidade, campo, getCampo(valor, getCampoPesquisado(valor)));
    }

    @Entao("^os campos em cadastro de cliente estarao em branco$")
    public void osCamposEmCadastroDeClienteEstaraoEmBranco() {
        Cliente.validarCamposClienteBranco();
    }

    @E("^selecionar o combo \"([^\"]*)\" para \"([^\"]*)\"$")
    public void selecionarOComboPara(String campo, String valor) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

         //BrowserVerifications.waitElementToBeVisible(getXPathCombo(campo),2);
        if (!valor.isEmpty()) {
            setTextoCombo(campo, valor, true);
        }

    }
}