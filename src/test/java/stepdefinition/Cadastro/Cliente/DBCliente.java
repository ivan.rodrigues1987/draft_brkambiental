package stepdefinition.Cadastro.Cliente;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;
import stepdefinition.Cadastro.Ligacao.Ligacao;

import java.util.ArrayList;

public class DBCliente extends DBActions {

    private static final int DEFINE_CNPJ = 3;
    /**
     * <b>Conexão ao banco de dados</b>
     *
     * @param  environment            <i>environment</i>
     *
     */
    public DBCliente(String environment) {
        super(environment);
    }

    /**
     * <b>Conexão ao banco de dados</b>
     *
     * @param  dataBase            <i>dataBase</i>
     *
     */
    public DBCliente(DataBase dataBase) {
        super(dataBase);
    }

    /**
     * <b>Construtor DBCliente</b>
     *
     */
    public Cliente getDadosCliente() {
        String cidadeCliente = getCidadeCliente();
        Cliente cliente = new Cliente();
        TiposDeCliente tiposDeCliente = new TiposDeCliente();
        Cliente clienteFisico;
        clienteFisico = getClienteFisicoSemLigacoes(cidadeCliente);
        Cliente clienteJuridico;
        clienteJuridico = getClienteJuridicoSemLigacoes(cidadeCliente);
        Cliente clienteInativo;
        clienteInativo = getClienteLigacoesInativas(cidadeCliente);
        Cliente clienteAtivo;
        clienteAtivo = getClienteLigacoesAtivas(cidadeCliente);


        //CPFs
        tiposDeCliente.setCpfComLigacoes(
                getClienteCadastrado(
                        Ligacao.getListaTiposDeClientes(), cidadeCliente, 'F', true));
        tiposDeCliente.setCpfSemLigacoes(
                getClienteCadastrado(
                        Ligacao.getListaTiposDeClientes(), cidadeCliente, 'F', false));
        tiposDeCliente.setCpfComDebito(getClienteComDebito('F'));
        //CPNJs
        tiposDeCliente.setCnpjComLigacoes(
                getClienteCadastrado(
                        Ligacao.getListaTiposDeClientes(), cidadeCliente, 'J', true));
        tiposDeCliente.setCnpjSemLigacoes(
                getClienteCadastrado(
                        Ligacao.getListaTiposDeClientes(), cidadeCliente, 'J', false));
        tiposDeCliente.setCnpjComDebito(getClienteComDebito('J'));

        cliente.setTiposDeCliente(tiposDeCliente);

        //CONSULTA CPF - CNPJ
        cliente.setCodFisicoSL(clienteFisico.getCodFisicoSL());
        cliente.setCpfFisicoSL(clienteFisico.getCpfFisicoSL());

        cliente.setCodJuridicoSL(clienteJuridico.getCodJuridicoSL());
        cliente.setCnpjJuridicoSL(clienteJuridico.getCnpjJuridicoSL());

        cliente.setCodClienteLigacoesInactivo(clienteInativo.getCodClienteLigacoesInactivo());
        cliente.setCodClienteLigacoesActivo(clienteAtivo.getCodClienteLigacoesActivo());

        return cliente;
    }

    /**
     * <b>RETORNA UMA CIDADE A SER UTILIZADA PARA LOCALIZAR OS CLIENTES</b>
     *
     * @return getColumnInfo<i>getColumnInfo</i>
     *
     */
    public String getCidadeCliente() {
        return getColumnInfo("CIDADE", "SELECT CIDADE " +
                "FROM CIDADE " +
                "WHERE ROWNUM = 1 ");
    }

    /**
     * <b>RETORNA CLIENTE CADASTRADO</b>
     *
     * @return clienteAux<i>clienteAux</i>
     */
    public Cliente getClienteCadastrado(ArrayList list, String cidade, char tipoCliente, boolean comLigacoes) {
        Cliente clienteAux = new Cliente();

        //MONTA WHERE DE ACORDO COM OS PARAMETROS
        String sWhere = "B.CIDADE ='" + cidade + "' AND ";
        sWhere += "C.TPCLIENTE = '" + tipoCliente + "' AND ";
        sWhere += "C." + ((tipoCliente == 'F') ? "CPF" : "CNPJ") +
                " IS NOT NULL AND "; //VERIFICA SE É PESSOA FISICA OU JURIDICA
        sWhere += " RG IS NOT NULL AND "; //VERIFICA SE TEM RG E CPF CADASTRADO
        sWhere += ((comLigacoes) ? "" : "NOT ") +
                " EXISTS (SELECT * FROM LIGACAO L WHERE C.ID =  L.IDCLIENTE AND L.ID  in" +
                "(SELECT D.idLIGACAO FROM DEBITOAUTOMATICO D WHERE D.ATIVO = 'S')) "; //VERIFICA SE POSSUI LIGACOES
         sWhere += " AND NOT EXISTS (SELECT * FROM FATURA F WHERE C.ID =  F.IDCLIENTEINQUILINO)";
        //VERIFICA SE NAO TEM DEBITO ATIVO

        ArrayList<String> infoCliente = getColumnsInfo("ID,CLIENTE,CPF,CNPJ",
                "SELECT C.ID, C.CLIENTE,C.CPF, C.CNPJ " +
                        "FROM CLIENTE C " +
                        "INNER JOIN CIDADE B " +
                        "ON B.ID = C.IDCIDADE " +
                        "WHERE " + sWhere);

        clienteAux.setCodigo(infoCliente.get(0));
        clienteAux.setNome(infoCliente.get(1));
        clienteAux.setCpf(infoCliente.get(2));
        clienteAux.setCnpj(infoCliente.get(DEFINE_CNPJ));

        if (comLigacoes) {
            clienteAux.setCodigoLigacao(getLigacaoCliente(clienteAux.getCodigo()));
        }

        return clienteAux;
    }

    /**
     * <b>RETORNA LIGACAO DO CLIENTE</b>
     *
     * @return getColumnInfo<i>getColumnInfo</i>
     */
    public String getLigacaoCliente(String idCliente) {
        return getColumnInfo("id", "SELECT id FROM Ligacao WHERE ROWNUM=1 AND idCliente = " + idCliente);
    }

    /**
     * <b>RETORNA CLIENTE COM DEBITO AIIVO</b>
     *
     * @return getColumnInfo<i>getColumnInfo</i>
     */
    public Cliente getClienteComDebito(char tipoCliente) {
        Cliente cliente = new Cliente();
        String idClienteInquilino = getColumnInfo("idClienteInquilino", " SELECT idClienteInquilino  " +
                "FROM Fatura fat INNER JOIN Cliente cli ON fat.idClienteInquilino = cli.id WHERE cli.tpCliente = '" +
                tipoCliente + "' and rownum = 1");
        if (tipoCliente == 'F') {
            cliente.setCodFisicoSL(idClienteInquilino);
        }
        if (tipoCliente == 'J') {
                cliente.setCodJuridicoSL(idClienteInquilino);
        }
        return cliente;
    }

    /**
     * <b>RETORNA CODIGO CLIENTE SEM LIGACOES - CPF</b>
     *
     * @return cliente<i>cliente</i>
     */
    public Cliente getClienteFisicoSemLigacoes(String cidade) {
        Cliente cliente = new Cliente();
        ArrayList<String> info = getColumnsInfo("ID,CPF", "SELECT C.ID,C.CPF FROM CLIENTE C " +
                "INNER JOIN CIDADE B ON B.ID = C.IDCIDADE " +
                "WHERE NOT EXISTS " +
                "( SELECT * FROM LIGACAO L " +
                "WHERE C.ID =  L.IDCLIENTE AND L.ID in" +
                "(SELECT D.idLIGACAO FROM DEBITOAUTOMATICO D WHERE D.ATIVO = 'N') ) " +
                "AND C.CPF IS NOT NULL " +
                "AND B.CIDADE = '" + cidade + "' " +
                "AND ROWNUM = 1"
        );
        cliente.setCodFisicoSL(info.get(0));
        cliente.setCpfFisicoSL(info.get(1));
        return cliente;
    }

    /**
     * <b>RETORNA CODIGO JURIDICO SEM LIGACOES - CPF</b>
     *
     * @return cliente<i>cliente</i>
     */
    public Cliente getClienteJuridicoSemLigacoes(String cidade) {
        Cliente cliente = new Cliente();
        ArrayList<String> info = getColumnsInfo("ID,CNPJ", "SELECT C.ID, C.CNPJ FROM CLIENTE C " +
                "INNER JOIN CIDADE B ON B.ID = C.IDCIDADE " +
                "WHERE NOT EXISTS " +
                "(SELECT * FROM LIGACAO L " +
                "WHERE C.ID =  L.IDCLIENTE and L.ID in " +
                "(select D.idLigacao from DebitoAutomatico D where D.ativo = 'S')) " +
                "AND C.CNPJ IS NOT NULL " +
                "AND B.CIDADE = '" + cidade + "' " +
                "AND ROWNUM = 1 "
        );
        cliente.setCodJuridicoSL(info.get(0));
        cliente.setCnpjJuridicoSL(info.get(1));
        return cliente;
    }

    /**
     * <b>RETORNA CLIENTE LIGACOES INATIVAS - SEM DEBITO</b>
     *
     * @return cliente<i>cliente</i>
     */
    public Cliente getClienteLigacoesInativas(String cidade) {
        Cliente cliente = new Cliente();
        ArrayList<String> info = getColumnsInfo("IDCLIENTE", "SELECT * FROM " +
                "(SELECT LIG.IDCLIENTE FROM LIGACAO LIG " +
                "INNER JOIN CIDADE CID ON LIG.IDCIDADE = CID.ID " +
                "INNER JOIN CLIENTE CLI ON LIG.IDCLIENTE = CLI.ID " +
                "WHERE LIG.IDSITUACAOLIGACAO = 3 " +
                "AND CID.CIDADE = '" + cidade + "' " +
                "AND CPF IS NOT NULL " +
                "AND NOT EXISTS (SELECT * FROM FATURA F " +
                "WHERE CLI.ID =  F.IDCLIENTEINQUILINO) " +
                "HAVING COUNT(LIG.IDCLIENTE)<=1 " +
                "GROUP BY LIG.IDCLIENTE) " +
                "WHERE ROWNUM = 1 "
        );
        cliente.setCodClienteLigacoesInactivo(info.get(0));
        return cliente;
    }

    /**
     * <b>RETORNA CLIENTE LIGACOES ATIVAS - SEM DEBITO</b>
     *
     * @return cliente<i>cliente</i>
     */
    public Cliente getClienteLigacoesAtivas(String cidade) {
        Cliente cliente = new Cliente();
        ArrayList<String> info = getColumnsInfo("IDCLIENTE", "SELECT * FROM " +
                "(SELECT LIG.IDCLIENTE FROM LIGACAO LIG " +
                "INNER JOIN CIDADE CID ON LIG.IDCIDADE = CID.ID " +
                "INNER JOIN CLIENTE CLI ON LIG.IDCLIENTE = CLI.ID " +
                "WHERE LIG.IDSITUACAOLIGACAO = 1 " +
                "AND CID.CIDADE = '" + cidade + "' " +
                "AND CPF IS NOT NULL " +
                "AND NOT EXISTS (SELECT * FROM FATURA F " +
                "WHERE CLI.ID =  F.IDCLIENTEINQUILINO) " +
                "HAVING COUNT(LIG.IDCLIENTE)<=1 " +
                "GROUP BY LIG.IDCLIENTE) " +
                "WHERE ROWNUM = 1 "
        );
        cliente.setCodClienteLigacoesActivo(info.get(0));
        return cliente;
    }


}
