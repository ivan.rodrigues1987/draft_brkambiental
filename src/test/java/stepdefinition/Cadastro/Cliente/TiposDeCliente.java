package stepdefinition.Cadastro.Cliente;

/**
 * @author Renan Ferreira dos Santos
 * @since 06/03/2019
 */
public class TiposDeCliente {
    private Cliente cpfComLigacoes;
    private Cliente cpfSemLigacoes;
    private Cliente cpfComDebito;
    private Cliente cnpjComLigacoes;
    private Cliente cnpjSemLigacoes;
    private Cliente cnpjComDebito;

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpfComLigacoes <i>cpfComLigacoes</i>
     */
    public Cliente getCpfComLigacoes() {
        return cpfComLigacoes;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpfComLigacoes <i>cpfComLigacoes</i>
     */
    public void setCpfComLigacoes(Cliente cpfComLigacoes) {
        this.cpfComLigacoes = cpfComLigacoes;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpfSemLigacoes <i>cpfSemLigacoes</i>
     */
    public Cliente getCpfSemLigacoes() {
        return cpfSemLigacoes;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpfSemLigacoes <i>cpfSemLigacoes</i>
     */
    public void setCpfSemLigacoes(Cliente cpfSemLigacoes) {
        this.cpfSemLigacoes = cpfSemLigacoes;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpfComDebito <i>cpfComDebito</i>
     */
    public Cliente getCpfComDebito() {
        return cpfComDebito;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpfComDebito <i>cpfComDebito</i>
     */
    public void setCpfComDebito(Cliente cpfComDebito) {
        this.cpfComDebito = cpfComDebito;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpjComLigacoes <i>cnpjComLigacoes</i>
     */
    public Cliente getCnpjComLigacoes() {
        return cnpjComLigacoes;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpjComLigacoes <i>cnpjComLigacoes</i>
     */
    public void setCnpjComLigacoes(Cliente cnpjComLigacoes) {
        this.cnpjComLigacoes = cnpjComLigacoes;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpjSemLigacoes <i>cnpjSemLigacoes</i>
     */
    public Cliente getCnpjSemLigacoes() {
        return cnpjSemLigacoes;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpjSemLigacoes <i>cnpjSemLigacoes</i>
     */
    public void setCnpjSemLigacoes(Cliente cnpjSemLigacoes) {
        this.cnpjSemLigacoes = cnpjSemLigacoes;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpjComDebito <i>cnpjComDebito</i>
     */
    public Cliente getCnpjComDebito() {
        return cnpjComDebito;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpjComDebito <i>cnpjComDebito</i>
     */
    public void setCnpjComDebito(Cliente cnpjComDebito) {
        this.cnpjComDebito = cnpjComDebito;
    }
}
