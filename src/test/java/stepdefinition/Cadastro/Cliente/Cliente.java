package stepdefinition.Cadastro.Cliente;


import framework.BrowserManager.BrowserVerifications;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;
import runner.Dados.Cadastro.Endereco;

import java.sql.SQLException;
import java.util.Random;

import static Reports.ExtentReports.appendToReport;
import static framework.AssertManager.assertElementText;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static framework.ConfigFramework.getDriver;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;
import static main.Utils.Utils.createOneDigitRandomNumber;
import static main.Utils.Utils.getCnpj;
import static main.Utils.Utils.getCpf;
import static main.WebElementManager.Combos.Combos.getXPathCombo;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.InputText.InputText.getInputTextXPath;
import static main.WebElementManager.InputText.InputText.setInputText;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * <b>Classe com as variables do Cadastro de Cliente</b>
 *
 * @author Juan Francisco Villarruel Castillo
 * @since 03/05/2019
 */
public class Cliente {
    private String codigo;
    private String nome;
    private String nomeClienteCadastrado;
    private String tipoCliente;
    private String cidade;
    private String sexo;
    private String bairro;
    private String dataNascimento;
    private String logradouro;
    private String numLogradouro;
    private String complemento;
    private String cep;
    private String uf;
    private String rg;
    private String dataExpRg;
    private String cpf;
    private String cnpj;
    private String telefoneFixo;
    private String telefoneMovel;
    private String email;
    private String nomePai;
    private String nomeMae;
    private String observacao;
    private String codigoLigacao;
    private static String nomeCadastrado;

    //Sem ligacoes
    private String codFisicoSL;
    private String cpfFisicoSL;

    private String codJuridicoSL;
    private String cnpjJuridicoSL;

    //Com ligacoes
    private String codClienteLigacoesInactivo;
    private String codClienteLigacoesActivo;

    //Tipos de Cliente
    private TiposDeCliente tiposDeCliente;

    //Static
    public static final String PESSOA_FISICA = "FISICA";

    private static final int TAMANHO_NOME = 5;
    private static final int TAMANHO_RG = 9;
    private static final int TAMANHO_TELEFONE_FIXO = 4;
    private static final int TAMANHO_TELEFONE_MOVEL = 8;
    private static final int TAMANHO_EMAIL = 5;

    /**
     * <b>Construtor vazio da Classe Cliente</b>
     *
     */
    public Cliente() {

    }

    /**
     * <b>Construtor para utilizar os dados para a tela Cliente</b>
     *
     * @param gerarDadosCliente <i>verifica se vai retornar valores o não</i>
     * @param tipoCliente       <i>variavel Tipo Cliente</i>
     * @param cpf               <i>variavel CPF</i>
     * @param cpnj              <i>variavel CPNJ</i>
     * @throws SQLException     <i>uso de Banco de Dados</i>
     */
    public Cliente(boolean gerarDadosCliente, String tipoCliente, String cpf, String cpnj) throws SQLException {
        if (gerarDadosCliente) {
            gerarDadosCliente(tipoCliente, cpf, cpnj);
        }
    }

    /**
     * <b>Construtor para utilizar os dados para a tela Cliente</b>
     *
     * @param gerarDadosPesquisa    <i>verifica se vai retornar valores o não</i>
     * @param valor                 <i>variavel Valor</i>
     * @param codigo                <i>variavel codigo</i>
     * @throws SQLException         <i>uso de Banco de Dados</i>
     */
    public Cliente(boolean gerarDadosPesquisa, String valor, String codigo) throws SQLException {
        gerarDadosPesquisa(valor, codigo);
    }
    /*Getters and Setters*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codigo <i>codigo</i>
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codigo <i>codigo</i>
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nome <i>nome</i>
     */
    public String getNome() {
        return nome;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nome <i>nome</i>
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /*public String getNomeClienteCadastrado() {
        return nomeClienteCadastrado;
    }

    public void setNomeClienteCadastrado(String nomeClienteCadastrado) {
        this.nomeClienteCadastrado = nomeClienteCadastrado;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return tipoCliente <i>tipoCliente</i>
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /*public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cidade <i>cidade</i>
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cidade <i>cidade</i>
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /*public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return dataNascimento <i>dataNascimento</i>
     */
    public String getDataNascimento() {
        return dataNascimento;
    }

    /*public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return logradouro <i>logradouro</i>
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  logradouro <i>logradouro</i>
     */
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return numLogradouro <i>numLogradouro</i>
     */
    public String getNumLogradouro() {
        return numLogradouro;
    }

    /*public void setNumLogradouro(String numLogradouro) {
        this.numLogradouro = numLogradouro;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return complemento <i>complemento</i>
     */
    public String getComplemento() {
        return complemento;
    }

    /*public void setComplemento(String complemento) {
        this.complemento = complemento;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cep <i>cep</i>
     */
    public String getCep() {
        return cep;
    }

    /*public void setCep(String cep) {
        this.cep = cep;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return uf <i>uf</i>
     */
    public String getUf() {
        return uf;
    }

    /*public void setUf(String uf) {
        this.uf = uf;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return rg <i>rg</i>
     */
    public String getRg() {
        return rg;
    }

    /*public void setRg(String rg) {
        this.rg = rg;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return dataExpRg <i>dataExpRg</i>
     */
    public String getDataExpRg() {
        return dataExpRg;
    }

    /*public void setDataExpRg(String dataExpRg) {
        this.dataExpRg = dataExpRg;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpf <i>cpf</i>
     */
    public String obterCpf() {
        return cpf;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpf <i>cpf</i>
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpj <i>cnpj</i>
     */
    public String obterCnpj() {
        return cnpj;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpj <i>cnpj</i>
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return telefoneFixo <i>telefoneFixo</i>
     */
    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    /*public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return telefoneMovel <i>telefoneMovel</i>
     */
    public String getTelefoneMovel() {
        return telefoneMovel;
    }

    /*public void setTelefoneMovel(String telefoneMovel) {
        this.telefoneMovel = telefoneMovel;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return email <i>email</i>
     */
    public String getEmail() {
        return email;
    }

    /*public void setEmail(String email) {
        this.email = email;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nomePai <i>nomePai</i>
     */
    public String getNomePai() {
        return nomePai;
    }

    /*public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nomeMae <i>nomeMae</i>
     */
    public String getNomeMae() {
        return nomeMae;
    }

    /*public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return observacao <i>observacao</i>
     */
    public String getObservacao() {
        return observacao;
    }

    /*public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getCodigoLigacao() {
        return codigoLigacao;
    }*/

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codigoLigacao <i>codigoLigacao</i>
     */
    public void setCodigoLigacao(String codigoLigacao) {
        this.codigoLigacao = codigoLigacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codFisicoSL <i>codFisicoSL</i>
     */
    public String getCodFisicoSL() {
        return codFisicoSL;
    }

    public void setCodFisicoSL(String codFisicoSL) {
        this.codFisicoSL = codFisicoSL;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpfFisicoSL <i>cpfFisicoSL</i>
     */
    public String getCpfFisicoSL() {
        return cpfFisicoSL;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpfFisicoSL <i>cpfFisicoSL</i>
     */
    public void setCpfFisicoSL(String cpfFisicoSL) {
        this.cpfFisicoSL = cpfFisicoSL;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codJuridicoSL <i>codJuridicoSL</i>
     */
    public String getCodJuridicoSL() {
        return codJuridicoSL;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codJuridicoSL <i>codJuridicoSL</i>
     */
    public void setCodJuridicoSL(String codJuridicoSL) {
        this.codJuridicoSL = codJuridicoSL;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpjJuridicoSL <i>cnpjJuridicoSL</i>
     */
    public String getCnpjJuridicoSL() {
        return cnpjJuridicoSL;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpjJuridicoSL <i>cnpjJuridicoSL</i>
     */
    public void setCnpjJuridicoSL(String cnpjJuridicoSL) {
        this.cnpjJuridicoSL = cnpjJuridicoSL;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codClienteLigacoesInactivo <i>codClienteLigacoesInactivo</i>
     */
    public String getCodClienteLigacoesInactivo() {
        return codClienteLigacoesInactivo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codClienteLigacoesInactivo <i>codClienteLigacoesInactivo</i>
     */
    public void setCodClienteLigacoesInactivo(String codClienteLigacoesInactivo) {
        this.codClienteLigacoesInactivo = codClienteLigacoesInactivo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codClienteLigacoesActivo <i>codClienteLigacoesActivo</i>
     */
    public String getCodClienteLigacoesActivo() {
        return codClienteLigacoesActivo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codClienteLigacoesActivo <i>codClienteLigacoesActivo</i>
     */
    public void setCodClienteLigacoesActivo(String codClienteLigacoesActivo) {
        this.codClienteLigacoesActivo = codClienteLigacoesActivo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nomeCadastrado <i>nomeCadastrado</i>
     */
    public static String getNomeCadastrado() {
        return nomeCadastrado;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nomeCadastrado <i>nomeCadastrado</i>
     */
    public static void setNomeCadastrado(String nomeCadastrado) {
        Cliente.nomeCadastrado = nomeCadastrado;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return tiposDeCliente <i>tiposDeCliente</i>
     */
    public TiposDeCliente getTiposDeCliente() {
        return tiposDeCliente;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  tiposDeCliente <i>tiposDeCliente</i>
     */
    public void setTiposDeCliente(TiposDeCliente tiposDeCliente) {
        this.tiposDeCliente = tiposDeCliente;
    }

    /**
     * <b>construtor que gera dados do cliente</b>
     *
     * @param  tipoClienteParametro      <i>tipoCliente</i>
     * @param  cpfParametro              <i>cpf</i>
     * @param  cpnj             <i>cpnj</i>
     * @throws SQLException     <i>uso de Banco de Dados</i>
     *
     */
    private void gerarDadosCliente(String tipoClienteParametro, String cpfParametro, String cpnj) throws SQLException {

        this.nome = "CLIENTE" + geradorCharRandom(TAMANHO_NOME);
        this.tipoCliente = tipoClienteParametro;
        //Pessoa fisica
        if (tipoClienteParametro.equals(PESSOA_FISICA)) {
            this.sexo = retornarSexoRandomico();
            this.dataNascimento = retornarDataRandomica();
            this.rg = geradorNumberRandom(TAMANHO_RG);
            this.dataExpRg = retornarDataRandomica();
            this.cpf = "  " + retornarCPF(cpfParametro);
            this.nomePai = "PAI " + nome;
            this.nomeMae = "MAE " + nome;
        } else {
        //Pessoa juridica
            this.cnpj = " " + retornarCNPJ(cpnj);
        }

        this.telefoneFixo = "114444" + geradorNumberRandom(TAMANHO_TELEFONE_FIXO);
        this.telefoneMovel = "119" + geradorNumberRandom(TAMANHO_TELEFONE_MOVEL);
        this.email = geradorCharRandom(TAMANHO_EMAIL) + "@email.com";
    }

    /**
     * <b>construtor que gera dados do cliente</b>
     *
     * @param  valor            <i>valor</i>
     * @param  codigoParametro           <i>codigo</i>
     * @throws SQLException     <i>uso de Banco de Dados</i>
     *
     */
    private void gerarDadosPesquisa(String valor, String codigoParametro) throws SQLException {
        this.nomeClienteCadastrado = retornarNomeClienteCadastrado(valor);
        this.codigo = retornarCodigoCadastrado(codigoParametro);
    }

    /**
     * <b>retorna dado sexo</b>
     *
     * @return sexo                 <i>sexo</i>
     *
     */
    private String retornarSexoRandomico() {
        String[] sexoAleatorio = {"MASCULINO", "FEMININO"};
        return sexoAleatorio[new Random().nextInt(sexoAleatorio.length)];
    }

    /**
     * <b>retorna data random</b>
     *
     * @return createOneDigitRandomNumber <i>createOneDigitRandomNumber</i>
     *
     */
    private String retornarDataRandomica() {
        return "2" + createOneDigitRandomNumber() + "/01" + "/199" + createOneDigitRandomNumber();
    }

    /**
     * <b>Metodo retorna CNPJ</b>
     *
     * @param documento                 <i>documento</i>
     * @return getCnpj                  <i>getCnpj</i>
     */
    private String retornarCNPJ(String documento) {
        if (documento.contains("Cadastrado")) {
            //return gerarMascaraCNPJ(getDadosCadastro().getCliente().cnpjCadastrado);
            return getDadosCadastro().getCliente().tiposDeCliente.getCnpjSemLigacoes().cnpj;
        } else {
            return getCnpj(false);
        }
    }

    /**
     * <b>Metodo retorna CPF</b>
     *
     * @param documento   <i>documento</i>
     * @return getCpf     <i>getCnpj</i>
     */
    private String retornarCPF(String documento) {
        if (documento.contains("Cadastrado")) {
            return getDadosCadastro().getCliente().tiposDeCliente.getCpfSemLigacoes().cpf;
        } else {
            return getCpf(false);
        }
    }

    /**
     * <b>Metodo preenche campos endereço, bairro, logradouro</b>
     *
     * @param endereco   <i>endereco</i>
     * @param cliente   <i>cliente</i>
     * @param tipoClienteParametro   <i>documento</i>
     */
    public void preencherCamposEspecificos(Endereco endereco, Cliente cliente, String tipoClienteParametro) {
        //Verifica se o campo bairro for inputtext

        if (!endereco.getCidade().equals("CACHOEIRO DE ITAPEMIRIM")) {
            final int segundos = 4;
            BrowserVerifications.waitElementToBeVisible(getXPathCombo("Bairro"), segundos);
        }
        if (!BrowserVerifications.isElementDisplayed(getInputTextXPath("Bairro"))) {
            //BAIRRO
            BrowserVerifications.waitElementToBeVisible(getXPathCombo("Bairro"), DEFAULT_TIME_OUT);
            setTextoCombo("Bairro", getCampo(this.bairro.trim(), endereco.getBairro().trim()), true);
            //LOGRADOURO
            Combos.setComboClicando("Logradouro", getCampo(this.logradouro, endereco.getLogradouro()), true);
            //SEXO
            if (tipoClienteParametro.equals(PESSOA_FISICA)) {
                Combos.setComboClicando("Sexo", getCampo(this.sexo, cliente.sexo));
            }
        } else {
            //BAIRRO
            setInputText("Bairro", getCampo(this.bairro, endereco.getBairro()));
            //LOGRADOURO
            setInputText("Logradouro", getCampo(this.logradouro, endereco.getLogradouro()).trim());
        }
    }

    /**
     * <b>Retorna nome cliente cadastrado</b>
     *
     * @param valor   <i>valor</i>
     * @return nomeClienteCadastrado   <i>nomeClienteCadastrado</i>
     */
    private String retornarNomeClienteCadastrado(String valor) throws SQLException {
        return getDadosCadastro().getCliente().nomeClienteCadastrado;
    }

    /**
     * <b>Retorna codigo cliente cadastrado</b>
     *
     * @return codigo   <i>codigo</i>
     */
    private String retornarCodigoCadastrado(String codigoParametro) throws SQLException {
        return getDadosCadastro().getCliente().codigo;
    }

    /**
     * <b>Valida campos cliente branco</b>
     *
     */
    public static void validarCamposClienteBranco() {
        assertElementText(InputText.getInputTextXPath("Código"), "");
        assertElementText(InputText.getInputTextXPath("Nome"), "");

        appendToReport(getDriver());
    }


}