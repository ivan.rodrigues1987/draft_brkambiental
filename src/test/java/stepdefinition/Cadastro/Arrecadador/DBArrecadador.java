package stepdefinition.Cadastro.Arrecadador;


import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;

/**
 * <b>Classe para retornar informacoes do Arrecadador do banco de dados</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 19/02/2019
 */
public class DBArrecadador extends DBActions {

    /**
     * <b>Inicializa conexão com banco de dados</b>
     *
     * @param  environment <i>environment</i>
     */
    public DBArrecadador(String environment) {
        super(environment);
    }

    /**
     * <b>Inicializa conexão com banco de dados</b>
     *
     * @param  dataBase <i>nome do database</i>
     */
    public DBArrecadador(DataBase dataBase) {
        super(dataBase);
    }

    /**
     * <b>Constructor de classe</b>
     *
     * @return  arrecadador <i>objeto arrecadador</i>
     */
    public Arrecadador getDadosArrecadador() {
        Arrecadador arrecadador = new Arrecadador();
        arrecadador.setArrecadadorAtivo(retornarArrecadadorCadastrado(true));
        arrecadador.setArrecadadorInativo(retornarArrecadadorCadastrado(false));
        arrecadador.setBandeira(retornarBandeiraBanco());
        return arrecadador;
    }

    /**
     * <b>Retorna arrecadador cadastrado</b>
     *
     * @return  arrecadador <i>objeto arrecadador</i>
     */
    public Arrecadador retornarArrecadadorCadastrado(boolean ativo) {
        String sWhereAtivo = " AND ativo = '" + ((ativo) ? "S" : "N") + "'";
        ArrayList<String> arrecadadorInfo = getColumnsInfo("arrecadador,numeroBanco",
                "SELECT arrecadador,numeroBanco FROM Arrecadador " +
                        "WHERE " +
                        //"    numeroBanco>1111111 AND ROWNUM=1 " +
                        "    numeroBanco>1 AND ROWNUM=1 " +
                        sWhereAtivo);
        Arrecadador arrecadador = new Arrecadador();
        arrecadador.setArrecadador(arrecadadorInfo.get(0));
        arrecadador.setNrBanco(arrecadadorInfo.get(1));
        return arrecadador;
    }

    /**
     * <b>Retorna uma bandeira de banco cadastrado</b>
     *
     * @return  arrecadador <i>objeto arrecadador</i>
     */
    public String retornarBandeiraBanco() {
        return getColumnInfo("bandeiraBankLine", "SELECT bandeiraBankLine FROM BankLineBandeira WHERE ROWNUM=1");
    }

    /**
     * <b>Verifica se existe um arrecadador vinculado ao nr Banco</b>
     *
     * @return  arrecadador <i>objeto arrecadador</i>
     */
    public boolean existeArrecadadorVinculadoNrBanco(String nrBanco) {
        return verifyIfSelectHasInfo("SELECT numeroBanco FROM Arrecadador WHERE numeroBanco = '" + nrBanco + "'");
    }
}
