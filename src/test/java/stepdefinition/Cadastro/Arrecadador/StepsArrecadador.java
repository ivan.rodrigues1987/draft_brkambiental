package stepdefinition.Cadastro.Arrecadador;

import cucumber.api.DataTable;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.Combos;

import main.WebElementManager.InputText.InputText;



import static stepdefinition.Cadastro.Arrecadador.Arrecadador.alterarComboSituacao;
import static main.Utils.ComumScreens.Auxiliar.maximizarTela;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Checkbox.clicarCheckBox;

import static main.WebElementManager.RadioButton.clicarRadioButton;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * <b>Classe para implementar os Steps de Arrecadador</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 19/02/2019
 */
public class StepsArrecadador {
    private Arrecadador arrecadador;

    public StepsArrecadador() {
        this.arrecadador = getDadosCadastro().getArrecadador();
    }

    @Quando("^preencher os seguintes campos em Arrecadador:$")
    public void preencherOsSeguintesCamposEmArrecadador(DataTable dataTable) {
        Arrecadador arrecadadorBDD = getDataTable(dataTable, Arrecadador.class);
        arrecadador.inicializarDados(arrecadadorBDD.getNrBanco());

        //ARRECADADOR
        String nomeArrecadador = getCampo(arrecadadorBDD.getArrecadador(), arrecadador.getArrecadador());
        InputText.setInputTextEPressionarEnter("Arrecadador", nomeArrecadador);
        Arrecadador.setArrecadadorCadastrado(nomeArrecadador);

        //DIAS CARENCIA
        InputText.setInputTextEPressionarEnter("Dias de carência envio D.A.", arrecadadorBDD.getDiasCarencia());

        //NUMERO BANCO
        InputText.setInputTextEPressionarEnter("Número do Banco", getCampo(arrecadadorBDD.getNrBanco(),
                arrecadador.getNrBanco()));

        //SITUACAO
        Combos.setComboClicando("Situação", arrecadadorBDD.getSituacao());

        //NR CONVENIO ARRECADACAO
        InputText.setInputTextEPressionarEnter("Nr. convênio arrecadação", arrecadadorBDD.getNrConvArrecadacao());

        //NSA ARRECADACAO
        InputText.setInputTextEPressionarEnter("NSA arrecadação", arrecadadorBDD.getNsaArrecadacao());

        //TARIFA ARRECADACAO
        InputText.setInputTextEPressionarEnter("Tarifa arrecadação", arrecadadorBDD.getTarifaArrecadacao());

        //CAMARA COMPENSACAO
        InputText.setInputTextEPressionarEnter("Câmara compensação", arrecadadorBDD.getCamaraCompensacao());

        //NR CONVENIO DA
        InputText.setInputTextEPressionarEnter("Nr. convênio D.A.", arrecadadorBDD.getNrConvDA());

        //NSA ENVIO DA
        InputText.setInputTextEPressionarEnter("NSA envio D.A.", arrecadadorBDD.getNsaEnvioDA());

        //NSA RETORNO DA
        InputText.setInputTextEPressionarEnter("NSA retorno D.A.", arrecadadorBDD.getNsaRetornoDA());

        //TARIFA SOBRE DA
        InputText.setInputTextEPressionarEnter("Tarifa sobre D.A.", arrecadadorBDD.getTarifaSobreDA());

        //NSA BAIXA MANUAL
        InputText.setInputTextEPressionarEnter("NSA Baixa Manual", arrecadadorBDD.getNsaBaixaMan());

        //VERSAO ARQUIVO DA
        Combos.setComboClicando("Versao Arquivo DA.", arrecadadorBDD.getVersaoArquivoDA());

        //PREENCHER CPF/CNPJ
        Combos.setComboClicando("Preencher CPF/CNPJ", arrecadadorBDD.getPreencherCPFCNPJ());

        //QUANTIDADE DIGITOS AG
        InputText.setInputTextEPressionarEnter("Quantidade Dígitos Ag", arrecadadorBDD.getQtdDigitosAg());

        //AGENCIA COM DV
        clicarRadioButton("Agência com Dv", arrecadadorBDD.getAgComDv());

        //PREENCHER AGENCIA COM ZEROS A ESQUERDA
        clicarRadioButton("Preencher Agência com zeros a esquerda", arrecadadorBDD.getPreencherAgZeroEsq());

        //QUANTIDADE DIGITOS CC
        InputText.setInputTextEPressionarEnter("Quantidade Dígitos CC", arrecadadorBDD.getQtdDigitosCC());

        //CONTA COM DV
        clicarRadioButton("Conta com Dv", arrecadadorBDD.getCcComDv());

        //PREENCHER CONTA COM ZEROS A ESQUERDA
        clicarRadioButton("Preencher Conta com zeros a esquerda", arrecadadorBDD.getPreencherCCZeroEsq());

        //VALIDA NSA IMPORTACAO
        Combos.setComboClicando("Valida NSA importação", arrecadadorBDD.getValidaNSAImport());

        //UTILIZAR DV LIGACAO
        Combos.setComboClicando("Utilizar DV ligação", arrecadadorBDD.getUtilizarDvLig());

        //PREFIXO CODIGO LIGACAO
        InputText.setInputTextEPressionarEnter("Prefixo código ligação", arrecadadorBDD.getPrefixCodLig());

        //QTD DIGITOS CODIGO LIGACAO
        InputText.setInputTextEPressionarEnter("Qtd. dígitos código ligação", arrecadadorBDD.getQtdDigCodLig());

        //QTD DIGITOS DV LIGACAO
        InputText.setInputTextEPressionarEnter("Qtd. dígitos DV ligação", arrecadadorBDD.getQtdDigDVLig());

        //CREDITO REGISTRO Z
        clicarRadioButton("Crédito registro Z", arrecadadorBDD.getCreditoRegZ());

        //Maximiza a tela para a os valores abaixo ficarem visiveis-----------------------------------------------------
        maximizarTela();

        //BANDEIRA
        Combos.setComboClicando("Bandeira", getCampo(arrecadadorBDD.getBandeira(), arrecadador.getBandeira()));

        //VALOR TARIFA
        InputText.setInputTextEPressionarEnter("Valor tarifa", arrecadadorBDD.getValorTarifa());

        //UTILIZAR INTEGRACAO FINANCEIRA
        clicarCheckBox("Utilizar integração financeira", arrecadadorBDD.getUtilizarIntegracaoFin());
        if (arrecadadorBDD.getUtilizarIntegracaoFin().equalsIgnoreCase("S")) {
            //CODIGO CONTA RECEITA
            InputText.setInputTextEPressionarEnter("Código Conta MyWebDay Receita", arrecadadorBDD.getCodReceita());

            //CODIGO CONTA DESPESA
            InputText.setInputTextEPressionarEnter("Código Conta MyWebDay Despesa", arrecadadorBDD.getCodDespesa());

            //CODIGO CONTA DA
            InputText.setInputTextEPressionarEnter("Código Conta Banco MyWebDay D.A.", arrecadadorBDD.getCodDA());

            //CODIGO CONTA BANCO ARRECADACAO
            InputText.setInputTextEPressionarEnter("Código Conta Banco MyWebDay Arrecadação",
                    arrecadadorBDD.getCodArrecadacao());
        }
    }

    @E("^selecionar alterar a situacao para \"([^\"]*)\" e clicar em \"([^\"]*)\"$")
    public void selecionarAlterarASituacaoParaEClicarEm(String situacao, String clicar) {
        alterarComboSituacao(situacao);
        clicarBotao(clicar);
    }


}
