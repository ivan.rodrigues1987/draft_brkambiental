package stepdefinition.Cadastro.Arrecadador;

import framework.BrowserManager.BrowserVerifications;
import framework.ConfigFramework;
import main.DBManager.DBActions;
import main.WebElementManager.Combos.Combos;
import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * <b>Classe Arrecadador</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 19/02/2019
 */
public class Arrecadador {
    private static final int TAMANHO_NRBANCO = 9;
    private static final int MAXIMO_TENTATIVAS = 5;
    private String arrecadador;
    private String diasCarencia;
    private String nrBanco;
    private String situacao;
    private String nrConvArrecadacao;
    private String nsaArrecadacao;
    private String tarifaArrecadacao;
    private String camaraCompensacao;
    private String nrConvDA;
    private String nsaEnvioDA;
    private String nsaRetornoDA;
    private String tarifaSobreDA;
    private String nsaBaixaMan;
    private String versaoArquivoDA;
    private String preencherCPFCNPJ;
    private String qtdDigitosAg;
    private String agComDv;
    private String preencherAgZeroEsq;
    private String qtdDigitosCC;
    private String ccComDv;
    private String preencherCCZeroEsq;
    private String validaNSAImport;
    private String utilizarDvLig;
    private String prefixCodLig;
    private String qtdDigCodLig;
    private String qtdDigDVLig;
    private String creditoRegZ;
    private String bandeira;
    private String valorTarifa;
    private String utilizarIntegracaoFin;
    private String codReceita;
    private String codDespesa;
    private String codDA;
    private String codArrecadacao;
    private String aba;
    private String campo;
    private String operador;
    private String valor;
    private String verificador;
    private Arrecadador arrecadadorAtivo;
    private Arrecadador arrecadadorInativo;
    private static String arrecadadorCadastrado;

//GETTERS E SETTERS

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return arrecadador <i>arrecadador</i>
     */
    public String getArrecadador() {
        return arrecadador;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  arrecadador <i>arrecadador</i>
     */
    public void setArrecadador(String arrecadador) {
        this.arrecadador = arrecadador;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return diasCarencia <i>diasCarencia</i>
     */
    public String getDiasCarencia() {
        return diasCarencia;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  diasCarencia <i>diasCarencia</i>
     */
    public void setDiasCarencia(String diasCarencia) {
        this.diasCarencia = diasCarencia;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nrBanco <i>nrBanco</i>
     */
    public String getNrBanco() {
        return nrBanco;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nrBanco <i>nrBanco</i>
     */
    public void setNrBanco(String nrBanco) {
        this.nrBanco = nrBanco;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return situacao <i>situacao</i>
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  situacao <i>situacao</i>
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nrConvArrecadacao <i>nrConvArrecadacao</i>
     */
    public String getNrConvArrecadacao() {
        return nrConvArrecadacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nrConvArrecadacao <i>nrConvArrecadacao</i>
     */
    public void setNrConvArrecadacao(String nrConvArrecadacao) {
        this.nrConvArrecadacao = nrConvArrecadacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nsaArrecadacao <i>nsaArrecadacao</i>
     */
    public String getNsaArrecadacao() {
        return nsaArrecadacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nsaArrecadacao <i>nsaArrecadacao</i>
     */
    public void setNsaArrecadacao(String nsaArrecadacao) {
        this.nsaArrecadacao = nsaArrecadacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return tarifaArrecadacao <i>tarifaArrecadacao</i>
     */
    public String getTarifaArrecadacao() {
        return tarifaArrecadacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  tarifaArrecadacao <i>tarifaArrecadacao</i>
     */
    public void setTarifaArrecadacao(String tarifaArrecadacao) {
        this.tarifaArrecadacao = tarifaArrecadacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return camaraCompensacao <i>camaraCompensacao</i>
     */
    public String getCamaraCompensacao() {
        return camaraCompensacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  camaraCompensacao <i>camaraCompensacao</i>
     */
    public void setCamaraCompensacao(String camaraCompensacao) {
        this.camaraCompensacao = camaraCompensacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nrConvDA <i>nrConvDA</i>
     */
    public String getNrConvDA() {
        return nrConvDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nrConvDA <i>nrConvDA</i>
     */
    public void setNrConvDA(String nrConvDA) {
        this.nrConvDA = nrConvDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nsaEnvioDA <i>nsaEnvioDA</i>
     */
    public String getNsaEnvioDA() {
        return nsaEnvioDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nsaEnvioDA <i>nsaEnvioDA</i>
     */
    public void setNsaEnvioDA(String nsaEnvioDA) {
        this.nsaEnvioDA = nsaEnvioDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nsaRetornoDA <i>nsaRetornoDA</i>
     */
    public String getNsaRetornoDA() {
        return nsaRetornoDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nsaRetornoDA <i>nsaRetornoDA</i>
     */
    public void setNsaRetornoDA(String nsaRetornoDA) {
        this.nsaRetornoDA = nsaRetornoDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return tarifaSobreDA <i>tarifaSobreDA</i>
     */
    public String getTarifaSobreDA() {
        return tarifaSobreDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  tarifaSobreDA <i>tarifaSobreDA</i>
     */
    public void setTarifaSobreDA(String tarifaSobreDA) {
        this.tarifaSobreDA = tarifaSobreDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nsaBaixaMan <i>nsaBaixaMan</i>
     */
    public String getNsaBaixaMan() {
        return nsaBaixaMan;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nsaBaixaMan <i>nsaBaixaMan</i>
     */
    public void setNsaBaixaMan(String nsaBaixaMan) {
        this.nsaBaixaMan = nsaBaixaMan;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return versaoArquivoDA <i>versaoArquivoDA</i>
     */
    public String getVersaoArquivoDA() {
        return versaoArquivoDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  versaoArquivoDA <i>versaoArquivoDA</i>
     */
    public void setVersaoArquivoDA(String versaoArquivoDA) {
        this.versaoArquivoDA = versaoArquivoDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return preencherCPFCNPJ <i>preencherCPFCNPJ</i>
     */
    public String getPreencherCPFCNPJ() {
        return preencherCPFCNPJ;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  preencherCPFCNPJ <i>preencherCPFCNPJ</i>
     */
    public void setPreencherCPFCNPJ(String preencherCPFCNPJ) {
        this.preencherCPFCNPJ = preencherCPFCNPJ;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return qtdDigitosAg <i>qtdDigitosAg</i>
     */
    public String getQtdDigitosAg() {
        return qtdDigitosAg;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  qtdDigitosAg <i>qtdDigitosAg</i>
     */
    public void setQtdDigitosAg(String qtdDigitosAg) {
        this.qtdDigitosAg = qtdDigitosAg;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return agComDv <i>agComDv</i>
     */
    public String getAgComDv() {
        return agComDv;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  agComDv <i>agComDv</i>
     */
    public void setAgComDv(String agComDv) {
        this.agComDv = agComDv;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return preencherAgZeroEsq <i>preencherAgZeroEsq</i>
     */
    public String getPreencherAgZeroEsq() {
        return preencherAgZeroEsq;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  preencherAgZeroEsq <i>preencherAgZeroEsq</i>
     */
    public void setPreencherAgZeroEsq(String preencherAgZeroEsq) {
        this.preencherAgZeroEsq = preencherAgZeroEsq;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return qtdDigitosCC <i>qtdDigitosCC</i>
     */
    public String getQtdDigitosCC() {
        return qtdDigitosCC;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  qtdDigitosCC <i>qtdDigitosCC</i>
     */
    public void setQtdDigitosCC(String qtdDigitosCC) {
        this.qtdDigitosCC = qtdDigitosCC;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return ccComDv <i>ccComDv</i>
     */
    public String getCcComDv() {
        return ccComDv;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  ccComDv <i>ccComDv</i>
     */
    public void setCcComDv(String ccComDv) {
        this.ccComDv = ccComDv;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return preencherCCZeroEsq <i>preencherCCZeroEsq</i>
     */
    public String getPreencherCCZeroEsq() {
        return preencherCCZeroEsq;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  preencherCCZeroEsq <i>preencherCCZeroEsq</i>
     */
    public void setPreencherCCZeroEsq(String preencherCCZeroEsq) {
        this.preencherCCZeroEsq = preencherCCZeroEsq;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return validaNSAImport <i>validaNSAImport</i>
     */
    public String getValidaNSAImport() {
        return validaNSAImport;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  validaNSAImport <i>validaNSAImport</i>
     */
    public void setValidaNSAImport(String validaNSAImport) {
        this.validaNSAImport = validaNSAImport;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return utilizarDvLig <i>utilizarDvLig</i>
     */
    public String getUtilizarDvLig() {
        return utilizarDvLig;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  utilizarDvLig <i>utilizarDvLig</i>
     */
    public void setUtilizarDvLig(String utilizarDvLig) {
        this.utilizarDvLig = utilizarDvLig;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return prefixCodLig <i>prefixCodLig</i>
     */
    public String getPrefixCodLig() {
        return prefixCodLig;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  prefixCodLig <i>prefixCodLig</i>
     */
    public void setPrefixCodLig(String prefixCodLig) {
        this.prefixCodLig = prefixCodLig;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return qtdDigCodLig <i>qtdDigCodLig</i>
     */
    public String getQtdDigCodLig() {
        return qtdDigCodLig;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  qtdDigCodLig <i>qtdDigCodLig</i>
     */
    public void setQtdDigCodLig(String qtdDigCodLig) {
        this.qtdDigCodLig = qtdDigCodLig;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return qtdDigDVLig <i>qtdDigDVLig</i>
     */
    public String getQtdDigDVLig() {
        return qtdDigDVLig;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  qtdDigDVLig <i>qtdDigDVLig</i>
     */
    public void setQtdDigDVLig(String qtdDigDVLig) {
        this.qtdDigDVLig = qtdDigDVLig;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return creditoRegZ <i>creditoRegZ</i>
     */
    public String getCreditoRegZ() {
        return creditoRegZ;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  creditoRegZ <i>creditoRegZ</i>
     */
    public void setCreditoRegZ(String creditoRegZ) {
        this.creditoRegZ = creditoRegZ;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return bandeira <i>bandeira</i>
     */
    public String getBandeira() {
        return bandeira;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  bandeira <i>bandeira</i>
     */
    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return valorTarifa <i>valorTarifa</i>
     */
    public String getValorTarifa() {
        return valorTarifa;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  valorTarifa <i>valorTarifa</i>
     */
    public void setValorTarifa(String valorTarifa) {
        this.valorTarifa = valorTarifa;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return utilizarIntegracaoFin <i>utilizarIntegracaoFin</i>
     */
    public String getUtilizarIntegracaoFin() {
        return utilizarIntegracaoFin;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  utilizarIntegracaoFin <i>utilizarIntegracaoFin</i>
     */
    public void setUtilizarIntegracaoFin(String utilizarIntegracaoFin) {
        this.utilizarIntegracaoFin = utilizarIntegracaoFin;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codReceita <i>codReceita</i>
     */
    public String getCodReceita() {
        return codReceita;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codReceita <i>codReceita</i>
     */
    public void setCodReceita(String codReceita) {
        this.codReceita = codReceita;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codDespesa <i>codDespesa</i>
     */
    public String getCodDespesa() {
        return codDespesa;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codDespesa <i>codDespesa</i>
     */
    public void setCodDespesa(String codDespesa) {
        this.codDespesa = codDespesa;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codDA <i>codDA</i>
     */
    public String getCodDA() {
        return codDA;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codDA <i>codDA</i>
     */
    public void setCodDA(String codDA) {
        this.codDA = codDA;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codArrecadacao <i>codArrecadacao</i>
     */
    public String getCodArrecadacao() {
        return codArrecadacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  codArrecadacao <i>codArrecadacao</i>
     */
    public void setCodArrecadacao(String codArrecadacao) {
        this.codArrecadacao = codArrecadacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return aba <i>aba</i>
     */
    public String getAba() {
        return aba;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  aba <i>aba</i>
     */
    public void setAba(String aba) {
        this.aba = aba;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return campo <i>campo</i>
     */
    public String getCampo() {
        return campo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  campo <i>campo</i>
     */
    public void setCampo(String campo) {
        this.campo = campo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return operador <i>operador</i>
     */
    public String getOperador() {
        return operador;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  operador <i>operador</i>
     */
    public void setOperador(String operador) {
        this.operador = operador;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return valor <i>valor</i>
     */
    public String getValor() {
        return valor;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  valor <i>valor</i>
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return verificador <i>verificador</i>
     */
    public String getVerificador() {
        return verificador;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  verificador <i>verificador</i>
     */
    public void setVerificador(String verificador) {
        this.verificador = verificador;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return arrecadadorAtivo <i>arrecadadorAtivo</i>
     */
    public Arrecadador getArrecadadorAtivo() {
        return arrecadadorAtivo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  arrecadadorAtivo <i>arrecadadorAtivo</i>
     */
    public void setArrecadadorAtivo(Arrecadador arrecadadorAtivo) {
        this.arrecadadorAtivo = arrecadadorAtivo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return arrecadadorInativo <i>arrecadadorInativo</i>
     */
    public Arrecadador getArrecadadorInativo() {
        return arrecadadorInativo;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  arrecadadorInativo <i>arrecadadorInativo</i>
     */
    public void setArrecadadorInativo(Arrecadador arrecadadorInativo) {
        this.arrecadadorInativo = arrecadadorInativo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return arrecadadorCadastrado <i>arrecadadorCadastrado</i>
     */
    public static String getArrecadadorCadastrado() {
        return arrecadadorCadastrado;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  arrecadadorCadastrado <i>arrecadadorCadastrado</i>
     */
    public static void setArrecadadorCadastrado(String arrecadadorCadastrado) {
        Arrecadador.arrecadadorCadastrado = arrecadadorCadastrado;
    }

    /**
     * <b>metodo para inicializar banco de dados</b>
     *
     * @param  numeroBanco <i>numeroBanco</i>
     */
    public void inicializarDados(String numeroBanco) {
        //ARRECADADOR
        final int tamanhoNumero = 3;
        final int tamanhoChar = 5;
        this.arrecadador = geradorNumberRandom(tamanhoNumero) + "Arrecadador " + geradorCharRandom(tamanhoChar);

        //NUMERO BANCO
        if (numeroBanco.contains("cadastrado")) {
            this.nrBanco = arrecadadorAtivo.nrBanco;
        } else {
            this.nrBanco = retornarNumeroBancoValido();
        }
    }

    /**
     * <b>metodo que retorna numero de banco valido</b>
     *
     * @return  nrBanco <i>nrBanco</i>
     */
    public String retornarNumeroBancoValido() {
        DBArrecadador dbArrecadador = new DBArrecadador(DBActions.SAN);
        nrBanco = geradorNumberRandom(TAMANHO_NRBANCO);
        //Efetua um maximo de 5 tentativas
        for (int tentativas = 0; tentativas < MAXIMO_TENTATIVAS && dbArrecadador.existeArrecadadorVinculadoNrBanco(
                nrBanco); tentativas++) {
            nrBanco = geradorNumberRandom(TAMANHO_NRBANCO);
        }
        return nrBanco;
    }

    /**
     * <b>altera o combo situacao</b>
     *
     * @param  situacao <i>situacao</i>
     */
    public static void alterarComboSituacao(String situacao) {
        BrowserVerifications.waitElementToBeEnable("Situação", ConfigFramework.DEFAULT_TIME_OUT);
        Combos.setComboClicando("Situação", situacao);
    }

}
