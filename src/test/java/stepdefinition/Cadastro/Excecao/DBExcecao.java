package stepdefinition.Cadastro.Excecao;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

/**
 * @author Juan Castillo
 * @since 25/03/2019
 */
public class DBExcecao extends DBActions {

    public DBExcecao(String environment) {
        super(environment);
    }

    public DBExcecao(DataBase dataBase) {
        super(dataBase);
    }


    public Excecao getDadosExcecao() {

        Excecao excecao = new Excecao();

        String codigoCadastrado = getExcecaoCadastrado();
        excecao.setCodigoCadastrado(codigoCadastrado);

        return excecao;
    }



    public String getExcecaoCadastrado() {
        return getColumnInfo("IDEXCECAO", "SELECT * FROM (SELECT excp.idexcecao " +
                "FROM EXCECAOPRODUTO excp " +
                "INNER JOIN EXCECAO exc " +
                "ON excp.idexcecao = exc.id " +
                "WHERE exc.ATIVA = 'S' " +
                "AND exc.EXCECAO IS NOT NULL " +
                "having count(excp.idexcecao)>1 " +
                "group by excp.idexcecao) " +
                "WHERE ROWNUM = 1 "
        );
    }


}
