package stepdefinition.Cadastro.Excecao;

import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.sql.SQLException;

import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static main.Utils.Utils.geradorCharRandom;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.DIV;

/**
 * @author Juan Castillo
 * @since 22/03/2019
 */
public class Excecao {

    private String cidade;
    private String excecao;
    private String ativa;
    private String tipo;
    private String aplicarsobre;
    private String momentocalcula;
    private String tipofaturamentoadmitido;
    private String motivopadrao;

    private String perdacomercial;
    private String considerarnorecalculo;
    private String permiteligacaofilho;
    private String retencaoforzada;
    private String aplicaligacaocortada;
    private String consrecalculo;
    private String cadastravel;
    private String visualizadapelocallcenter;

    private String codigoCadastrado;

    private static int linha = 0;
    private static int indica = 0;
    static final int I_PARAMETRO = 3;

    //Construtores
    public Excecao() {

    }

    public Excecao(boolean gerarDadosExcecao, String excecao, String ativa) throws SQLException {
        if (gerarDadosExcecao) {
            gerarDadosExcecao(excecao, ativa);
        }
    }

    //Getters and Setters
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getExcecao() {
        return excecao;
    }

    public void setExcecao(String excecao) {
        this.excecao = excecao;
    }

    public String getAtiva() {
        return ativa;
    }

    public void setAtiva(String ativa) {
        this.ativa = ativa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAplicarsobre() {
        return aplicarsobre;
    }

    public void setAplicarsobre(String aplicarsobre) {
        this.aplicarsobre = aplicarsobre;
    }

    public String getMomentocalcula() {
        return momentocalcula;
    }

    public void setMomentocalcula(String momentocalcula) {
        this.momentocalcula = momentocalcula;
    }

    public String getTipofaturamentoadmitido() {
        return tipofaturamentoadmitido;
    }

    public void setTipofaturamentoadmitido(String tipofaturamentoadmitido) {
        this.tipofaturamentoadmitido = tipofaturamentoadmitido;
    }

    public String getMotivopadrao() {
        return motivopadrao;
    }

    public void setMotivopadrao(String motivopadrao) {
        this.motivopadrao = motivopadrao;
    }

    public String getPerdacomercial() {
        return perdacomercial;
    }

    public void setPerdacomercial(String perdacomercial) {
        this.perdacomercial = perdacomercial;
    }

    public String getConsiderarnorecalculo() {
        return considerarnorecalculo;
    }

    public void setConsiderarnorecalculo(String considerarnorecalculo) {
        this.considerarnorecalculo = considerarnorecalculo;
    }

    public String getPermiteligacaofilho() {
        return permiteligacaofilho;
    }

    public void setPermiteligacaofilho(String permiteligacaofilho) {
        this.permiteligacaofilho = permiteligacaofilho;
    }

    public String getRetencaoforzada() {
        return retencaoforzada;
    }

    public void setRetencaoforzada(String retencaoforzada) {
        this.retencaoforzada = retencaoforzada;
    }

    public String getAplicaligacaocortada() {
        return aplicaligacaocortada;
    }

    public void setAplicaligacaocortada(String aplicaligacaocortada) {
        this.aplicaligacaocortada = aplicaligacaocortada;
    }

    public String getConsrecalculo() {
        return consrecalculo;
    }

    public void setConsrecalculo(String consrecalculo) {
        this.consrecalculo = consrecalculo;
    }

    public String getCadastravel() {
        return cadastravel;
    }

    public void setCadastravel(String cadastravel) {
        this.cadastravel = cadastravel;
    }

    public String getVisualizadapelocallcenter() {
        return visualizadapelocallcenter;
    }

    public void setVisualizadapelocallcenter(String visualizadapelocallcenter) {
        this.visualizadapelocallcenter = visualizadapelocallcenter;
    }

    public String getCodigoCadastrado() {
        return codigoCadastrado;
    }

    public void setCodigoCadastrado(String codigoCadastrado) {
        this.codigoCadastrado = codigoCadastrado;
    }

    private void gerarDadosExcecao(String excecaoParametro, String ativaParametro)throws SQLException {
        this.excecao = "Excecao" + geradorCharRandom(I_PARAMETRO);
    }

    public static void clicarComboOpcoes(int identificador, String texto) throws InterruptedException {

        linha++;

        String xPath = "//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[2]" +
                "/tr[" + linha + "]/td[" + identificador + "]/div[1]";

        if (!BrowserVerifications.elementExists(By.xpath(xPath))) {
            linha--;
            xPath = "//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[2]" +
                    "/tr[" + linha + "]/td[" + identificador + "]/div[1]";
        }

        clickOnElement(By.xpath(xPath));
        sendKeys(texto);
        sendKeys(Keys.ENTER);
        clickOnElement(getXPathElementWithText(DIV, texto));
        //clickOnElement(By.xpath("//div[@class='GKPSPJYCIUB']"));

        if (linha == I_PARAMETRO) {
            indica++;
            if (indica == 2) {
                linha = 0;
            }
        }
    }

    //Insere o texto e verifica se o campo nao esta zerado
    public static void setText(String campo, String texto, boolean clearField) {
        String valorPreenchido;
        // Insere texto
        setInputTextEPressionarEnter(campo, texto);


        //Recebe valor preenchido
        valorPreenchido = getTextAttribute(getInputTextXPath(campo), "value").trim();

        //Verifica se o campo esta zerado ou com valor diferente do que deveria preencher
        if (valorPreenchido.equals("0") || !valorPreenchido.equals(texto)) {
            setInputText(campo, texto, clearField);
        }
    }


}
