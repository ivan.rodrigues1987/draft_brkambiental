package stepdefinition.Cadastro.Excecao;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserActions;
import main.WebElementManager.Combos.ComboInputText;
import main.WebElementManager.InputText.InputText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import runner.Dados.Cadastro.Endereco;

import java.sql.SQLException;

import static stepdefinition.Cadastro.Excecao.Excecao.clicarComboOpcoes;
import static stepdefinition.Cadastro.Excecao.Excecao.setText;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.preencherComboCidadePesquisaRapida;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Juan Castillo
 * @since 22/03/2019
 */
public class StepsExcecao {

    private Excecao excecao;
    private Endereco endereco;

    public StepsExcecao() {
        endereco = getDadosCadastro().getEndereco();
    }



    @Entao("^validar os criterios de pesquisa de Cadastro de Excecao$")
    public void validarOsCriteriosDePesquisaDeCadastroDeExcecao() {

        setComboClicando("Campo", "Codigo");
        setComboClicando("Campo", "aceitaemligcons");
        setComboClicando("Campo", "idcidade");
        setComboClicando("Campo", "idexcecaotipo");
        setComboClicando("Campo", "momentocalculo");
    }

    @E("^na tela de Pesquisa tem que preencher na " +
            "cidade \"([^\"]*)\", o campo \"([^\"]*)\" e o valor \"([^\"]*)\" para Cadastro de Excecao$")
    public void naTelaDePesquisaTemQuePreencherNaCidadeOCampoEOValorParaCadastroDeExcecao(String cidade,
                                                                                          String campo, String valor)
            throws Throwable {

        preencherComboCidadePesquisaRapida("Cidade", 2, getCampo(cidade, getDadosCadastro().getEndereco().getCidade()));

        setComboClicando("Campo", campo);

        sendKeys(Keys.TAB);
        InputText.setInputTextEPressionarEnter(valor);

    }

    @Quando("^Preencher os seguintes campos em Cadastro de Excecao$")
    public void preencherOsSeguintesCamposEmCadastroDeExcecao(DataTable table) throws SQLException {

        Excecao excecaoBDD = getDataTable(table, Excecao.class);
        excecao = new Excecao(true, excecaoBDD.getExcecao(), excecaoBDD.getAtiva());

        //Cidade
        setTextoCombo("Cidade", excecaoBDD.getCidade(), true);

//        //Excecao
//        setText("Exceção", getCampo(excecaoBDD.excecao, excecao.excecao),true);

        //Ativa
        clicarCheckBox("Ativa", getCampo(excecaoBDD.getAtiva(), excecao.getAtiva()));

        //Tipo
        setTextoCombo("Tipo", getCampo(excecaoBDD.getTipo(), excecao.getTipo()), true);

        //Aplicar Sobre
        setComboClicando("Aplicar Sobre", getCampo(excecaoBDD.getAplicarsobre(), excecao.getAplicarsobre()));

        //Excecao
        setText("Exceção", getCampo(excecaoBDD.getExcecao(), excecao.getExcecao()), true);

        //Momento Cálculo
        ComboInputText.setComboEPressionarEnter("Momento Cálculo", getCampo(excecaoBDD.getMomentocalcula(),
                excecao.getMomentocalcula()));

        //Tipo de Faturamento Admitido
        setComboClicando("Tipo de Faturamento Admitido", getCampo(excecaoBDD.getTipofaturamentoadmitido(),
                excecao.getTipofaturamentoadmitido()));

        //Motivo Padrão
        setComboClicando("Motivo Padrão", getCampo(excecaoBDD.getMotivopadrao(), excecao.getMotivopadrao()));

        //Perda Comercial
        clicarCheckBox("Perda Comercial", getCampo(excecaoBDD.getPerdacomercial(), excecao.getPerdacomercial()));

        //Considerar no Recálculo
        clicarCheckBox("Considerar no Recálculo", getCampo(excecaoBDD.getConsiderarnorecalculo(),
                excecao.getConsiderarnorecalculo()));

        //Permite Ligação Filho
        clicarCheckBox("Permite Ligação Filho", getCampo(excecaoBDD.getPermiteligacaofilho(),
                excecao.getPermiteligacaofilho()));

        //Retenção Forçada
        clicarCheckBox("Retenção Forçada", getCampo(excecaoBDD.getRetencaoforzada(), excecao.getRetencaoforzada()));

        //Aplica Ligação Cortada
        clicarCheckBox("Aplica Ligação Cortada", getCampo(excecaoBDD.getAplicaligacaocortada(),
                excecao.getAplicaligacaocortada()));

        //Cons. Recálculo Simultânea
        clicarCheckBox("Cons. Recálculo Simultânea", getCampo(excecaoBDD.getConsrecalculo(),
                excecao.getConsrecalculo()));

        //Cadastrável
        clicarCheckBox("Cadastrável", getCampo(excecaoBDD.getCadastravel(), excecao.getCadastravel()));

        //Visualizada pelo Call Center
        clicarCheckBox("Visualizada pelo Call Center", getCampo(excecaoBDD.getVisualizadapelocallcenter(),
                excecao.getVisualizadapelocallcenter()));


    }

    @E("^na tela de Cadastro de Excecao clicar o botao direito do mouse clicar em \"([^\"]*)\"$")
    public void naTelaDeCadastroDeExcecaoClicarOBotaoDireitoDoMouseClicarEm(String botao) throws Throwable {

        switch (botao) {
            case "Adicionar":
                BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("//div[@class='GKPSPJYCIUB']"));
                break;
            case "Remover":
                BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("//fieldset[1]/div[1]/div[1]/div[2]" +
                        "/div[1]/table[1]/tbody[2]/tr[1]/td[2]/div[1]"));
                break;
            default:
                break;
        }
    }


    @E("^adicionar os valores \"([^\"]*)\" e \"([^\"]*)\" em Cadastrar Excecao$")
    public void adicionarOsValoresEEmCadastrarExcecao(String valor1, String valor2) throws Throwable {

        clicarComboOpcoes(1, valor1);
        clicarComboOpcoes(2, valor2);

    }
}
