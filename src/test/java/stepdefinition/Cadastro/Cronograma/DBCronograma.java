package stepdefinition.Cadastro.Cronograma;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;

import static stepdefinition.Cadastro.Cronograma.Cronograma.somarMesesPeriodo;

public class DBCronograma extends DBActions {
    public static final String PERIODO_ATUAL = getPeriodoAtivo();
    //LocalDate.now().format(DateTimeFormatter.ofPattern("LL/yyyy"));

    public DBCronograma(String environment) {
        super(environment);
    }

    public DBCronograma(DataBase dataBase) {
        super(dataBase);
    }

    public Cronograma getDadosCronogramaCidade(String cidade) throws Exception {
        return retornarDadosCronograma(" cidade = '" + cidade + "'");
    }
    private Cronograma retornarDadosCronograma(String where) throws Exception {
        Cronograma cronograma = new Cronograma();

        if (!where.isEmpty()) {
            where = " AND " + where;
        }
        ArrayList<String> infoGrupo = getColumnsInfo("grupo,cidade",
                "SELECT gru.grupo, cid.cidade FROM Grupo gru" +
                        "    INNER JOIN Cidade cid ON gru.idcidade = cid.id" +
                        "    WHERE ROWNUM = 1 " + where);

        cronograma.setGrupo(infoGrupo.get(0));
        cronograma.setCidade(infoGrupo.get(1));
        cronograma.setPeriodo(PERIODO_ATUAL);

        return cronograma;
    }

    private static String getPeriodoAtivo() {
        return getColumnInfo(
                "periodo", "select periodo from periodo where inativo = 'N' and" +
                            " ano = Extract(year from sysdate) and mes = Extract(month from sysdate)-1 and rownum = 1");
                            }

    public Cronograma retornarGrupoSemCronogramaCadastrado() throws Exception {
        return retornarDadosCronograma(
                "NOT EXISTS(" +
                        "    SELECT cro.id FROM Cronograma cro " +
                        "    INNER JOIN Grupo gruAux ON cro.idGrupo = gruAux.id" +
                        "    INNER JOIN Cidade cidAux ON gruAux.idCidade = cidAux.id" +
                        "    WHERE gruAux.Grupo = gru.Grupo AND cidAux.Cidade = cid.Cidade)");
    }

    public Cronograma retornarGrupoComCronogramaPeriodoAnterior() throws Exception {
        return retornarDadosCronograma(
                " EXISTS(" +
                        "    SELECT cro.id FROM Cronograma cro" +
                        "    INNER JOIN Grupo gruAux ON cro.idGrupo = gruAux.id" +
                        "    INNER JOIN Cidade cidAux ON gruAux.idCidade = cidAux.id" +
                        "    WHERE gruAux.Grupo = gru.Grupo AND cidAux.Cidade = cid.Cidade)" +
                        " AND " +
                        " NOT EXISTS(" +
                        "    SELECT cro.id FROM Cronograma cro " +
                        "    INNER JOIN Grupo gruAux ON cro.idGrupo = gruAux.id" +
                        "    INNER JOIN Cidade cidAux ON gruAux.idCidade = cidAux.id" +
                        "    INNER JOIN Periodo periodo ON cro.idPeriodo = periodo.id" +
                        "    WHERE gruAux.Grupo = gru.Grupo AND cidAux.Cidade = cid.Cidade " +
                        "    AND periodo.periodo = '" + somarMesesPeriodo(PERIODO_ATUAL, 1) + "')"

        );
    }

    public Cronograma retornarGrupoComCronograma() throws Exception {
        Cronograma cronograma;
        cronograma = retornarDadosCronograma(
                " EXISTS(" +
                        "    SELECT cro.id FROM Cronograma cro " +
                        "    INNER JOIN Grupo gruAux ON cro.idGrupo = gruAux.id" +
                        "    INNER JOIN Cidade cidAux ON gruAux.idCidade = cidAux.id" +
                        "    WHERE gruAux.Grupo = gru.Grupo AND cidAux.Cidade = cid.Cidade)");

        return cronograma;
    }

    public Cronograma retornarCronogramaSituacao(String situacao) throws Exception {
        Cronograma cronograma = new Cronograma();
        ArrayList<String> infoCronograma = getColumnsInfo("cidade,periodo,grupo",
                "SELECT cid.Cidade, periodo.Periodo, gru.Grupo FROM Cronograma cro" +
                        "    INNER JOIN Grupo gru ON cro.idGrupo = gru.id" +
                        "    INNER JOIN Cidade cid ON gru.idCidade = cid.id" +
                        "    INNER JOIN Periodo periodo ON cro.idPeriodo = periodo.id" +
                        "    WHERE ROWNUM = 1 AND cro.status = '" + situacao + "'");

        cronograma.setCidade(infoCronograma.get(0));
        cronograma.setPeriodo(infoCronograma.get(1));
        cronograma.setGrupo(infoCronograma.get(2));

        return cronograma;
    }

}
