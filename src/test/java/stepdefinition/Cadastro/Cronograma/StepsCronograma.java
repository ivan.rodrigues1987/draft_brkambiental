package stepdefinition.Cadastro.Cronograma;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;


import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.Utils.Utils.getDataTable;
import static main.Utils.Utils.mergeObjects;
import static main.Utils.Utils.alterarDataEmDias;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static org.testng.Assert.assertTrue;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;
import static main.Utils.ComumScreens.Auxiliar.getCampo;

import static main.WebElementManager.Combos.Combos.setComboCidade;

import static main.WebElementManager.Combos.Combos.setComboClicando;
import static stepdefinition.Cadastro.Cronograma.Cronograma.somarMesesPeriodo;
import static stepdefinition.Cadastro.Cronograma.Cronograma.setDataPeriodo;
import static stepdefinition.Cadastro.Cronograma.Cronograma.getPeriodoBDD;
import static stepdefinition.Cadastro.Cronograma.Cronograma.validarDatas;
import static stepdefinition.Cadastro.Cronograma.Cronograma.verificarFaseSituacao;


public class StepsCronograma {
    private Cronograma cronograma;
    private Cronograma cronoBuffer;

    public StepsCronograma() {
        cronograma = getDadosCadastro().getCronograma();
    }


    @Quando("^preencher Cidade \"([^\"]*)\", periodo \"([^\"]*)\", Grupo \"([^\"]*)\" e validar" +
            " que a Fase \"([^\"]*)\" esta \"([^\"]*)\"$")
    public void preencherCidadePeriodoGrupoEValidarQueAFaseEsta(
            String cidade, String periodo, String grupo, String fase, String situacao) throws ParseException {
        //CIDADE
        setComboCidade("Cidade", getCampo(cidade, cronograma.getCidade()));

        //PERIODO
        Cronograma.setStaticPeriodo(getCampo(periodo, getPeriodoBDD(periodo)));
        setComboClicando("Período", Cronograma.getPeriodo(), 1);

        //GRUPO
        setComboClicando("Grupo", getCampo(grupo, cronograma.getGrupo()));

        //FASE
        verificarFaseSituacao(fase, situacao);

    } //preencherCidadePeriodoGrupoEValidarQueAFaseEsta

    @Quando("^preencher Cidade \"([^\"]*)\", periodo \"([^\"]*)\", Grupo \"([^\"]*)\"$")
    public void preencherCidadePeriodoGrupo(String cidade, String periodo, String grupo) throws ParseException {
        //CIDADE
        setComboCidade("Cidade", getCampo(cidade, cronograma.getCidade()));

        //PERIODO
        Cronograma.setStaticPeriodo(getCampo(periodo, getPeriodoBDD(periodo)));
        setComboClicando("Período", Cronograma.getPeriodo(), 1);

        //GRUPO
        setComboClicando("Grupo", getCampo(grupo, cronograma.getGrupo()));

    } //preencherCidadePeriodoGrupo

    @E("^em Fases do Cronograma prencher os seguintes campos$")
    public void preencherEmTodasAsLinhasDoFasesDoCronograma(DataTable table)
            throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //ALIMENTANDO ATRIBUTOS DE CRONOGRAMA RELATIVOS A FASE CRONOGRAMA
        Cronograma aux;
        aux = getDataTable(table, Cronograma.class);
        aux = aux.getCamposFaseCronograma(cronograma);
        aux = (Cronograma) mergeObjects(aux, cronograma);
        aux = (Cronograma) validarDatas(aux);

        //PREENCHENDO CAMPOS DE FASE CRONOGRAMA
        aux.setDadoFaseCronograma("separacao coletor", aux.getDataPrevistaSeparacaoColetor());
        aux.setDadoFaseCronograma("envio para coletor", aux.getDataPrevistaEnvioParaColetor());
        aux.setDadoFaseCronograma("retorno coletor", aux.getDataPrevistaRetornoColetor());
        aux.setDadoFaseCronograma("repasse", aux.getDataPrevistaRepasse());
        aux.setDadoFaseCronograma("retorno repasse", aux.getDataPrevistaRetornoRepasse());
        aux.setDadoFaseCronograma("calculo faturas", aux.getDataPrevistaCalculoFatura());
        aux.setDadoFaseCronograma("impressao", aux.getDataPrevistaImpressao());


    } // preencherEmTodasAsLinhasDoGridDeParametrosCronograma


    @E("^em Fases do Cronograma alterar o campo \"([^\"]*)\" com \"([^\"]*)\" em Dados Localizacao$")
    public void preencherCampoEmCronograma(String campo, String dado) throws ParseException {
        boolean mudaMesAcima;
        mudaMesAcima = dado.contains("Acima");


        //VALIDA SE O CAMPO EH DINAMICO
        if (dado.contains("[") && dado.contains("]") && !mudaMesAcima) {
            dado = cronograma.getCampoTelaCronograma(dado, campo);
        }

        //VERIFICA SE FOI PASSADO APENAS O DIA
        if (!dado.contains("/")) {
            dado = setDataPeriodo(dado, Cronograma.getPeriodo());
        }

        //VALIDA SE O CAMPO VENCIMENTO PRECISA SER ALTERADO EM UM MES
        if (mudaMesAcima) {
            dado = setDataPeriodo(dado, somarMesesPeriodo(Cronograma.getPeriodo(), 1));
        } else {
            dado = alterarDataEmDias(dado, 1, "dd/MM/yyyy");
        }

        cronograma.setDadoDadosLocalizacao(campo, dado);
    }

    @E("^em Dados Localizacao preencher os seguintes campos$")
    public void preencherEmTodasAsLinhasDoDadosLocalizacao(DataTable table)
            throws InvocationTargetException, NoSuchMethodException, InstantiationException,
            IllegalAccessException, ParseException {

        //ALIMENTANDO ATRIBUTOS DE CRONOGRAMA RELATIVOS A DADOS LOCALIZACAO
        Cronograma aux;
        boolean mudaMesAcima;
        aux = getDataTable(table, Cronograma.class);
        mudaMesAcima = aux.getVencimento().contains("Acima");
        aux = aux.getCamposDadosLocalizacao(cronograma);
        aux = (Cronograma) mergeObjects(aux, cronograma);
        aux = (Cronograma) validarDatas(aux);

        //VALIDA SE O CAMPO VENCIMENTO PRECISA SER ALTERADO
        if (mudaMesAcima) {
            aux.setVencimento(setDataPeriodo(aux.getVencimento(), somarMesesPeriodo(Cronograma.getPeriodo(), 1)));
        }

        //PREENCHENDO CAMPOS DE DADOS LOCALIZACAO
        aux.setDadoDadosLocalizacao("vencimento", aux.getVencimento());
        aux.setDadoDadosLocalizacao("entrega", aux.getEntrega());
        aux.setDadoDadosLocalizacao("leitAtual", aux.getLeitAtual());
        aux.setDadoDadosLocalizacao("proximaleit", aux.getProximaLeit());
        aux.setDadoDadosLocalizacao("infdevendainicio", aux.getInfDeVendaInicio());
    }

    @E("^validar os campos da coluna Data Prevista$")
    public void validarColunaDataPrevista() {
        cronoBuffer = cronograma.validarColunaDataPrevista();
    } //validarColunaDataPrevista

    @E("^em Cadastro de Cronograma clicar no botao \"([^\"]*)\" e selecionar a opcao \"([^\"]*)\" e Salvar$")
    public void replicarCronograma(String botao, String opcao) {
        clicarBotao(botao);
        clicarCheckBox(opcao);
        clickOnElement(cronograma.getXPathBotaoReplicarCronograma());
    }

    @E("^confirmar que os dados do cronograma foram atualizados com os dados do cronograma replicado$")
    public void validarAtualizacao() {
        Cronograma aux;
        aux = cronograma.validarColunaDataPrevista();
        assertTrue(cronograma.isEqualCronograma(aux, cronoBuffer));
    }
}
