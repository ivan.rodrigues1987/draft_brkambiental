package stepdefinition.Cadastro.Cronograma;


import main.Utils.Utils;
import org.openqa.selenium.By;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.SeparacaoColetor;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.EnvioColetor;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.RetornoColetor;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.Repasse;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.RetornoRepasse;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.CalculoFaturas;
import static stepdefinition.Cadastro.Cronograma.Cronograma.TabelaFaseCronograma.Impressao;
import static framework.AssertManager.assertElementText;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserVerifications.waitPersistentTextSet;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.isBlank;
import static main.Utils.Utils.retirarMarcacao;
import static org.testng.Assert.assertTrue;


public class Cronograma {

    private String cidade;
    private static String periodo;
    private String grupo;
    //Inicializa as datas com o dia para depois concatenar com o mes e o ano quando o processo for dinamico
    private String dataPrevistaSeparacaoColetor = "03";
    private String dataPrevistaEnvioParaColetor = "03";
    private String dataPrevistaRetornoColetor = "04";
    private String dataPrevistaRepasse = "04";
    private String dataPrevistaRetornoRepasse = "04";
    private String dataPrevistaCalculoFatura = "04";
    private String dataPrevistaImpressao = "04";
    private String vencimento = "15";
    private String entrega = "04";
    private String leitAtual = "04";
    private String proximaLeit = "07";
    private String infDeVendaInicio = "04";
    private String coletarDadosLocalizacao = "";

    /**
     * <b>construtor vazio</b>
     *
     */
    public Cronograma() {
    }



    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public static String getPeriodo() {
        return periodo;
    }

    public static void setStaticPeriodo(String periodoParametro) {
        //Método setter do membro estático periodo
        Cronograma.periodo = periodoParametro;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getDataPrevistaSeparacaoColetor() {
        return dataPrevistaSeparacaoColetor;
    }

    public void setDataPrevistaSeparacaoColetor(String dataPrevistaSeparacaoColetor) {
        this.dataPrevistaSeparacaoColetor = dataPrevistaSeparacaoColetor;
    }

    public String getDataPrevistaEnvioParaColetor() {
        return dataPrevistaEnvioParaColetor;
    }

    public void setDataPrevistaEnvioParaColetor(String dataPrevistaEnvioParaColetor) {
        this.dataPrevistaEnvioParaColetor = dataPrevistaEnvioParaColetor;
    }

    public String getDataPrevistaRetornoColetor() {
        return dataPrevistaRetornoColetor;
    }

    public void setDataPrevistaRetornoColetor(String dataPrevistaRetornoColetor) {
        this.dataPrevistaRetornoColetor = dataPrevistaRetornoColetor;
    }

    public String getDataPrevistaRepasse() {
        return dataPrevistaRepasse;
    }

    public void setDataPrevistaRepasse(String dataPrevistaRepasse) {
        this.dataPrevistaRepasse = dataPrevistaRepasse;
    }

    public String getDataPrevistaRetornoRepasse() {
        return dataPrevistaRetornoRepasse;
    }

    public void setDataPrevistaRetornoRepasse(String dataPrevistaRetornoRepasse) {
        this.dataPrevistaRetornoRepasse = dataPrevistaRetornoRepasse;
    }

    public String getDataPrevistaCalculoFatura() {
        return dataPrevistaCalculoFatura;
    }

    public void setDataPrevistaCalculoFatura(String dataPrevistaCalculoFatura) {
        this.dataPrevistaCalculoFatura = dataPrevistaCalculoFatura;
    }

    public String getDataPrevistaImpressao() {
        return dataPrevistaImpressao;
    }

    public void setDataPrevistaImpressao(String dataPrevistaImpressao) {
        this.dataPrevistaImpressao = dataPrevistaImpressao;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public String getEntrega() {
        return entrega;
    }

    public void setEntrega(String entrega) {
        this.entrega = entrega;
    }

    public String getLeitAtual() {
        return leitAtual;
    }

    public void setLeitAtual(String leitAtual) {
        this.leitAtual = leitAtual;
    }

    public String getProximaLeit() {
        return proximaLeit;
    }

    public void setProximaLeit(String proximaLeit) {
        this.proximaLeit = proximaLeit;
    }

    public String getInfDeVendaInicio() {
        return infDeVendaInicio;
    }

    public void setInfDeVendaInicio(String infDeVendaInicio) {
        this.infDeVendaInicio = infDeVendaInicio;
    }

    public String getColetarDadosLocalizacao() {
        return coletarDadosLocalizacao;
    }

    public void setColetarDadosLocalizacao(String coletarDadosLocalizacao) {
        this.coletarDadosLocalizacao = coletarDadosLocalizacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  periodo          <i>inscricaoEstadual</i>
     * @throws  ParseException  <i>ParseException</i>
     */



    public void setPeriodo(String periodo) throws ParseException {
        Cronograma.periodo = periodo;
        setDatasPeriodo(periodo);
    }

    /**
     * <b>construtor</b>
     *
     */
    public enum TabelaFaseCronograma {
        SeparacaoColetor(1), EnvioColetor(2), RetornoColetor(3),
        Repasse(4), RetornoRepasse(5), CalculoFaturas(6), Impressao(7);
        private int valor;

        TabelaFaseCronograma(int i) {
            valor = i;
        }
        public int getValor() {
            return valor;
        }
    }

    /**
     * <b>Acerta as datas quando o periodo for localizado dinamicamente</b>
     *
     * @param periodoParametro<i>periodo</i>
     * @throws  ParseException  <i>ParseException</i>
     *
     */
    public void setDatasPeriodo(String periodoParametro) throws ParseException {

        this.dataPrevistaSeparacaoColetor = setDataPeriodo(dataPrevistaSeparacaoColetor, periodoParametro);
        this.dataPrevistaEnvioParaColetor = setDataPeriodo(dataPrevistaEnvioParaColetor, periodoParametro);
        this.dataPrevistaRetornoColetor = setDataPeriodo(dataPrevistaRetornoColetor, periodoParametro);
        this.dataPrevistaRepasse = setDataPeriodo(dataPrevistaRepasse, periodoParametro);
        this.dataPrevistaRetornoRepasse = setDataPeriodo(dataPrevistaRetornoRepasse, periodoParametro);
        this.dataPrevistaCalculoFatura = setDataPeriodo(dataPrevistaCalculoFatura, periodoParametro);
        this.dataPrevistaImpressao = setDataPeriodo(dataPrevistaImpressao, periodoParametro);
        this.vencimento = setDataPeriodo(vencimento, periodoParametro);
        this.entrega = setDataPeriodo(entrega, periodoParametro);
        this.leitAtual = setDataPeriodo(leitAtual, periodoParametro);
        this.proximaLeit = setDataPeriodo(proximaLeit, somarMesesPeriodo(periodoParametro, 1));
        this.infDeVendaInicio = setDataPeriodo(infDeVendaInicio, somarMesesPeriodo(periodoParametro, -1));
    } //setDatasPeriodo

    /**
     * <b>Seleciona data de periodo</b>
     *
     * @param data      <i>data</i>
     * @param periodoParametro   <i>periodo</i>
     * @return  data    <i>data</i>
     *
     */
    public static String setDataPeriodo(String data, String periodoParametro) {
        if (data.length() > 2) {
            data = data.split("/")[0];
        } //if
        data += "/" + periodoParametro;
        return data;
    } //setDataPeriodo

    /**
     * <b>Verifica fase situacao</b>
     *
     * @param fase      <i>fase</i>
     * @param situacao   <i>situacao</i>
     *
     */
    public static void verificarFaseSituacao(String fase, String situacao) {
        assertElementText(By.xpath("//tr[" + fase + "]/td/div[text()='" + situacao + "']"), situacao);
    }

    /**
     * <b>soma meses do periodo</b>
     *
     * @param periodoParametro           <i>periodo</i>
     * @param meses             <i>meses</i>
     * @throws ParseException   <i>ParseException</i>
     *
     */
    public static String somarMesesPeriodo(String periodoParametro, int meses) throws ParseException {
        return Utils.somarMesesData(periodoParametro, meses, "MM/yyyy");
    }

    /**
     * <b>soma meses data</b>
     *
     * @param data              <i>data</i>
     * @param meses             <i>meses</i>
     * @throws ParseException   <i>ParseException</i>
     *
     */
    public static String somarMesesData(String data, int meses) throws ParseException {
        return Utils.somarMesesData(data, meses, "dd/MM/yyyy");
    }

    /**
     * <b>obter periodo BDD</b>
     *
     * @param periodoParametro           <i>periodo</i>
     * @throws ParseException   <i>ParseException</i>
     *
     */
    public static String getPeriodoBDD(String periodoParametro) throws ParseException {
        periodoParametro = retirarMarcacao(periodoParametro);
        final int mesesProximoPeriodo = 2;
        final int mesesPeriodoSeparado = -2;
        switch (periodoParametro) {
            case "Periodo Atual":
                return DBCronograma.PERIODO_ATUAL;
            case "Proximo Periodo":
                return somarMesesPeriodo(DBCronograma.PERIODO_ATUAL, mesesProximoPeriodo);
            case "Periodo Separado":
                return somarMesesPeriodo(DBCronograma.PERIODO_ATUAL, mesesPeriodoSeparado);
            default:
                return periodoParametro;
        }
    }

    /**
     * <b>obter campos fase cronograma</b>
     *
     * @param cronograma <i>cronograma</i>
     *
     */
    public Cronograma getCamposFaseCronograma(Cronograma cronograma) {
        this.dataPrevistaCalculoFatura = getCampo(this.dataPrevistaCalculoFatura, cronograma.dataPrevistaCalculoFatura);
        this.dataPrevistaEnvioParaColetor = getCampo(
                this.dataPrevistaEnvioParaColetor, cronograma.dataPrevistaEnvioParaColetor);
        this.dataPrevistaImpressao = getCampo(this.dataPrevistaImpressao, cronograma.dataPrevistaImpressao);
        this.dataPrevistaRepasse = getCampo(
                this.dataPrevistaRepasse, cronograma.dataPrevistaRepasse);
        this.dataPrevistaRetornoColetor = getCampo(
                this.dataPrevistaRetornoColetor, cronograma.dataPrevistaRetornoColetor);
        this.dataPrevistaRetornoRepasse = getCampo(
                this.dataPrevistaRetornoRepasse, cronograma.dataPrevistaRetornoRepasse);
        this.dataPrevistaSeparacaoColetor = getCampo(
                this.dataPrevistaSeparacaoColetor, cronograma.dataPrevistaSeparacaoColetor);
        return this;
    }

    public void setDadoFaseCronograma(String campo, String dado) {
        final int coluna = 3;
        if (!isBlank(dado)) {
            By by = null;
            By cellInputText = getInputTextXPathCronograma();
            switch (campo.toLowerCase()) {
                case ("separacao coletor"):
                    by = getCampoXPathFaseCronograma(SeparacaoColetor.getValor(), coluna);
                    break;
                case ("envio para coletor"):
                    by = getCampoXPathFaseCronograma(EnvioColetor.getValor(), coluna);
                    break;
                case ("retorno coletor"):
                    by = getCampoXPathFaseCronograma(RetornoColetor.getValor(), coluna);
                    break;
                case ("repasse"):
                    by = getCampoXPathFaseCronograma(Repasse.getValor(), coluna);
                    break;
                case ("retorno repasse"):
                    by = getCampoXPathFaseCronograma(RetornoRepasse.getValor(), coluna);
                    break;
                case ("calculo faturas"):
                    by = getCampoXPathFaseCronograma(CalculoFaturas.getValor(), coluna);
                    break;
                case ("impressao"):
                    by = getCampoXPathFaseCronograma(Impressao.getValor(), coluna);
                    break;
                case ("coletar dados localizacao"):
                    if (dado.toUpperCase().contains("S")) {
                        clickOnElement(By.xpath("//label[contains(text(),'Coletar Dados Localização:')]"));
                    }
                    break;
                default:
                    break;
            } // switch
            if (!dado.contains("x")) {
                if (!campo.equals("coletar dados localizacao")) {
                    try {
                        clickOnElement(by);
                        waitPersistentTextSet(cellInputText, dado, DEFAULT_TIME_OUT);
                    } catch (Exception e) {
                        clickOnElement(by);
                        waitPersistentTextSet(cellInputText, dado, DEFAULT_TIME_OUT);
                    }

                } //if
            } else {
                clickOnElement(by);
                clearField(cellInputText);
            }
        } //if

    } // setFaseCronograma

    public By getCampoXPathFaseCronograma(int linha, int coluna) {
        return By.xpath("//div[2]/div[1]/table[1]/tbody[2]/tr[" + linha + "]/td[" + coluna + "]");
    }

    public By getCampoXPathDadosLocalizacao(int indiceX, int indiceY) {
        return By.xpath("(//div[" + indiceX + "]/div[1]/div[1]/div[1]/table[1]/" +
                "tbody[1]/tr[1]/td[1]/input[1])[" + indiceY + "]");
    }

    public By getInputTextXPathCronograma() {
        return By.xpath("//div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]");
    }

    public By getXPathBotaoReplicarCronograma() {
        return By.xpath(
                "//div[@class='GKPSPJYCODC']//div[@class='GKPSPJYCPM GKPSPJYCLN GKPSPJYCAN x-has-width']//img[1]");
    }

    public Cronograma getCamposDadosLocalizacao(Cronograma cronograma) {
        this.vencimento = getCampo(this.vencimento, cronograma.vencimento);
        this.entrega = getCampo(this.entrega, cronograma.entrega);
        this.leitAtual = getCampo(this.leitAtual, cronograma.leitAtual);
        this.proximaLeit = getCampo(this.proximaLeit, cronograma.proximaLeit);
        this.infDeVendaInicio = getCampo(this.infDeVendaInicio, cronograma.infDeVendaInicio);

        return this;
    }

    public void setDadoDadosLocalizacao(String campo, String valor) {
        final int indiceXVencimento = 1, indiceYVencimento = 2;
        final int indiceXEntrega = 1, indiceYEntrega = 3;
        final int indiceXLeiAtual = 1, indiceYLeiAtual = 4;
        final int indiceXProximaLeiT = 2, indiceYProximaLeiT = 2;
        final int indiceXInfDeVendaInicio = 1, indiceYInfDeVendaInicio = 5;
        if (!isBlank(valor)) {
            By by = null;
            switch (campo.toLowerCase()) {
                case ("vencimento"):
                    by = getCampoXPathDadosLocalizacao(indiceXVencimento, indiceYVencimento);
                    break;
                case ("entrega"):
                    by = getCampoXPathDadosLocalizacao(indiceXEntrega, indiceYEntrega);
                    break;
                case ("leitatual"):
                    by = getCampoXPathDadosLocalizacao(indiceXLeiAtual, indiceYLeiAtual);
                    break;
                case ("proximaleit"):
                    by = getCampoXPathDadosLocalizacao(indiceXProximaLeiT, indiceYProximaLeiT);
                    break;
                case ("infdevendainicio"):
                    by = getCampoXPathDadosLocalizacao(indiceXInfDeVendaInicio, indiceYInfDeVendaInicio);
                    break;
                default:
                    break;
            } // switch
            if (valor.contains("x")) {
                clearField(by);
            } else {
                waitPersistentTextSet(by, valor, DEFAULT_TIME_OUT);
            }
        } // if
    } //setDadoDadosLocalizacao


    public Cronograma validarColunaDataPrevista() {
        By by;
        final int tamVetor = 7;
        final int coluna = 3;
        final int posDataPrevistaRepasse = 3;
        final int posDataPrevistaRetornoRepasse = 4;
        final int posDataPrevistaCalculoFatura = 5;
        final int posDataPrevistaImpressao = 6;
        Cronograma aux = new Cronograma();
        String[] dado = new String[tamVetor];
        for (int i = 0; i < tamVetor; i++) {
            by = getCampoXPathFaseCronograma(i + 1, coluna);
            clickOnElement(by);
            dado[i] = getTextAttribute(getInputTextXPathCronograma(), "value");
            assertTrue(!isBlank(dado[i]));
        } //for
        aux.dataPrevistaSeparacaoColetor = dado[0];
        aux.dataPrevistaEnvioParaColetor = dado[1];
        aux.dataPrevistaRetornoColetor = dado[2];
        aux.dataPrevistaRepasse = dado[posDataPrevistaRepasse];
        aux.dataPrevistaRetornoRepasse = dado[posDataPrevistaRetornoRepasse];
        aux.dataPrevistaCalculoFatura = dado[posDataPrevistaCalculoFatura];
        aux.dataPrevistaImpressao = dado[posDataPrevistaImpressao];

        return aux;
    }

    public boolean isEqualCronograma(Cronograma first, Cronograma second) {
        if (!first.dataPrevistaSeparacaoColetor.equals(second.dataPrevistaSeparacaoColetor)) {
            return false;
        }
        if (!first.dataPrevistaEnvioParaColetor.equals(second.dataPrevistaEnvioParaColetor)) {
            return false;
        }
        if (!first.dataPrevistaRetornoColetor.equals(second.dataPrevistaRetornoColetor)) {
            return false;
        }
        if (!first.dataPrevistaRepasse.equals(second.dataPrevistaRepasse)) {
            return false;
        }
        if (!first.dataPrevistaRetornoRepasse.equals(second.dataPrevistaRetornoRepasse)) {
            return false;
        }
        if (!first.dataPrevistaCalculoFatura.equals(second.dataPrevistaCalculoFatura)) {
            return false;
        }
        return first.dataPrevistaImpressao.equals(second.dataPrevistaImpressao);
    }

    public static <T> Object validarDatas(T first)
            throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        Class<?> clazz = first.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Object returnValue = clazz.getConstructor().newInstance();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value1 = field.get(first);
            Object value = (value1.toString().contains("/")) ? value1 : setDataPeriodo(value1.toString(), periodo);
            field.set(returnValue, value);
        }
        return returnValue;
    }

    public String getCampoTelaCronograma(String dado, String campo) {
        By by = null;
        final int coluna = 3;
        final int vencimentoY = 2, proximaLeiT = 2;
        final int entregaY = 3;
        final int leiAtualY = 4;
        final int infY = 5;
        int linha = 1;
        switch (campo.toLowerCase()) {
            case ("separacao coletor"):
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("envio para coletor"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("retorno coletor"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("repasse"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("retorno repasse"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("calculo faturas"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("impressao"):
                linha += 1;
                by = getCampoXPathFaseCronograma(linha, coluna);
                break;
            case ("vencimento"):
                linha = 1;
                by = getCampoXPathDadosLocalizacao(linha, vencimentoY);
                break;
            case ("entrega"):
                by = getCampoXPathDadosLocalizacao(linha, entregaY);
                break;
            case ("leitatual"):
                by = getCampoXPathDadosLocalizacao(linha, leiAtualY);
                break;
            case ("proximaleit"):
                by = getCampoXPathDadosLocalizacao(linha++, proximaLeiT);
                break;
            case ("infdevendainicio"):
                by = getCampoXPathDadosLocalizacao(linha--, infY);
                break;
            default:
                break;
        } // switch
        clickOnElement(by);
        return getCampo(dado, getTextAttribute(by, "value").trim());
    }
} // classe Cronograma
