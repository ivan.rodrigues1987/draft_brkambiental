package stepdefinition.Cadastro.Grupo;

import main.Utils.Utils;
import main.WebElementManager.Xpath;
import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.WebElementManager.Botoes.getXPathBotoes;
import static main.WebElementManager.Xpath.DIV;

public class Grupo {
    //DataTable Principal
    private String cidade;
    private String grupo;
    private String ordem;
    private String vencPadrao;
    private String tipoLeitura;
    private String exigeCritica;
    private String reavisodeDebito;
    private String grupodeLigacaoEstimada;
    private String corteLight;
    private String fiscalizacaodeCorte;
    private String estimadas;
    private String retdoColetorAutomatico;
    private String caldeConsumoAutomatico;
    private String caldeFaturamentoAutomatico;
    private String mensagemCanhotoEsgoto;
    //DataTable da Pesquisa
    private String aba;
    private String cidadePesquisa;
    private String campo;
    private String operador;
    private String valor;
    private String verificador;

    private static final int TAMANHO_GRUPO = 5;
    private static final int TAMANHO_ORDEM = 8;
    private static final int INDICE_SAIR = 4;
    private static final int INDICE_PESQUISAR = 3;

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getOrdem() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem = ordem;
    }

    public String getVencPadrao() {
        return vencPadrao;
    }

    public void setVencPadrao(String vencPadrao) {
        this.vencPadrao = vencPadrao;
    }

    public String getTipoLeitura() {
        return tipoLeitura;
    }

    public void setTipoLeitura(String tipoLeitura) {
        this.tipoLeitura = tipoLeitura;
    }

    public String getExigeCritica() {
        return exigeCritica;
    }

    public void setExigeCritica(String exigeCritica) {
        this.exigeCritica = exigeCritica;
    }

    public String getReavisodeDebito() {
        return reavisodeDebito;
    }

    public void setReavisodeDebito(String reavisodeDebito) {
        this.reavisodeDebito = reavisodeDebito;
    }

    public String getGrupodeLigacaoEstimada() {
        return grupodeLigacaoEstimada;
    }

    public void setGrupodeLigacaoEstimada(String grupodeLigacaoEstimada) {
        this.grupodeLigacaoEstimada = grupodeLigacaoEstimada;
    }

    public String getCorteLight() {
        return corteLight;
    }

    public void setCorteLight(String corteLight) {
        this.corteLight = corteLight;
    }

    public String getFiscalizacaodeCorte() {
        return fiscalizacaodeCorte;
    }

    public void setFiscalizacaodeCorte(String fiscalizacaodeCorte) {
        this.fiscalizacaodeCorte = fiscalizacaodeCorte;
    }

    public String getEstimadas() {
        return estimadas;
    }

    public void setEstimadas(String estimadas) {
        this.estimadas = estimadas;
    }

    public String getRetdoColetorAutomatico() {
        return retdoColetorAutomatico;
    }

    public void setRetdoColetorAutomatico(String retdoColetorAutomatico) {
        this.retdoColetorAutomatico = retdoColetorAutomatico;
    }

    public String getCaldeConsumoAutomatico() {
        return caldeConsumoAutomatico;
    }

    public void setCaldeConsumoAutomatico(String caldeConsumoAutomatico) {
        this.caldeConsumoAutomatico = caldeConsumoAutomatico;
    }

    public String getCaldeFaturamentoAutomatico() {
        return caldeFaturamentoAutomatico;
    }

    public void setCaldeFaturamentoAutomatico(String caldeFaturamentoAutomatico) {
        this.caldeFaturamentoAutomatico = caldeFaturamentoAutomatico;
    }

    public String getMensagemCanhotoEsgoto() {
        return mensagemCanhotoEsgoto;
    }

    public void setMensagemCanhotoEsgoto(String mensagemCanhotoEsgoto) {
        this.mensagemCanhotoEsgoto = mensagemCanhotoEsgoto;
    }

    public String getAba() {
        return aba;
    }

    public void setAba(String aba) {
        this.aba = aba;
    }

    public String getCidadePesquisa() {
        return cidadePesquisa;
    }

    public void setCidadePesquisa(String cidadePesquisa) {
        this.cidadePesquisa = cidadePesquisa;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getVerificador() {
        return verificador;
    }

    public void setVerificador(String verificador) {
        this.verificador = verificador;
    }

    public void inicializarDadosDinamicos() {
        grupo = "GRUPO " + Utils.geradorNumberRandom(TAMANHO_GRUPO);
        ordem = Utils.geradorNumberRandom(TAMANHO_ORDEM);
    }

    public static void clicarBotaoPesquisar() {
        clickOnElement(getXPathBotoes(INDICE_PESQUISAR));
    }

    public static void clicarBotaoSair() {

        clickOnElement(getXPathBotoes(INDICE_SAIR));
    }

    public static void clicarBotaoReplicarVencimento() {
        clickOnElement(Xpath.getXPathElementTextEquals(DIV, "Replicar", 2));
    }

    public static void selecionarReplicacaoVencimento(String grupo, String cidade) {
        clickOnElement(By.xpath("//div[text()='" + cidade + "']/../../td/div[text()='" + grupo + "']/../..//td[1]"));
    }

}
