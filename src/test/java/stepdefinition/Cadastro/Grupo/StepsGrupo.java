package stepdefinition.Cadastro.Grupo;

import cucumber.api.DataTable;


import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;



import static stepdefinition.Cadastro.Grupo.Grupo.selecionarReplicacaoVencimento;
import static stepdefinition.Cadastro.Grupo.Grupo.clicarBotaoPesquisar;
import static stepdefinition.Cadastro.Grupo.Grupo.clicarBotaoReplicarVencimento;
import static stepdefinition.Cadastro.Grupo.Grupo.clicarBotaoSair;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;

public class StepsGrupo {

    @Quando("^preencher os seguintes campos do Cadastro de Grupo:$")
    public void preencherOsSeguintesCamposDoCadastroDeGrupo(DataTable dataTable) {
        Grupo grupoBDD = getDataTable(dataTable, Grupo.class);
        Grupo grupo = new Grupo();

        //GERA INFORMACOES DINAMICAS
        grupo.inicializarDadosDinamicos();

        //CIDADE
        Combos.setComboCidade("Cidade", grupoBDD.getCidade());

        //GRUPO
        String infoGrupo = getCampo(grupoBDD.getGrupo(), grupo.getGrupo());
        InputText.setInputText("Grupo", infoGrupo);

        //ORDEM
        String infoOrdem = getCampo(grupoBDD.getOrdem(), grupo.getOrdem());
        InputText.setInputText("Ordem", infoOrdem);

        //VENCIMENTO PADRAO
        Combos.setComboClicando("Venc. Padrão", grupoBDD.getVencPadrao());

        //TIPO LEITURA
        Combos.setComboClicando("Tipo Leitura", grupoBDD.getTipoLeitura());

        //EXIGE CRITICA
        clicarCheckBox("Exige Crítica", grupoBDD.getExigeCritica());

        //REVISAO DE BEBITO
        clicarCheckBox("Reaviso de Débito", grupoBDD.getReavisodeDebito());

        //GRUPO DE LIGAÇÃO ESTIMADA
        clicarCheckBox("Grupo de Ligação Estimada", grupoBDD.getGrupodeLigacaoEstimada());

        //CORTE LIGHT
        clicarCheckBox("Corte Light", grupoBDD.getCorteLight());

        //FISCALIZACAO DE CORTE
        clicarCheckBox("Fiscalização de Corte", grupoBDD.getFiscalizacaodeCorte());

        //ESTIMADAS
        clicarCheckBox("Estimadas", grupoBDD.getEstimadas());

        //RETDOCOLETORAUTOMATICO
        clicarCheckBox("Ret. do Coletor Automático", grupoBDD.getRetdoColetorAutomatico());

        //CALDECONSUMOAUTOMATICO
        clicarCheckBox("Cal. de Consumo Automático", grupoBDD.getCaldeConsumoAutomatico());

        //CALDEFATURAMENTOAUTOMATICO
        clicarCheckBox("Cal. de Faturamento Automático", grupoBDD.getCaldeFaturamentoAutomatico());

        //MENSAGEMCANHOTOESGOTO
        String infoCanhoto = getCampo(grupoBDD.getMensagemCanhotoEsgoto(), grupo.getMensagemCanhotoEsgoto());
        InputText.setInputText("Mensagem Canhoto Esgoto", infoCanhoto);

    }

    @E("^em Vencimentos Alternativos selecionar o dia \"([^\"]*)\"$")
    public void emVencimentosAlternativosSelecionarODia(String diaVencto) {
        clicarCheckBox(diaVencto, "S");
    }

    @Quando("^clicar no botao \"([^\"]*)\" em Grupo$")
    public void clicarNoBotaoEmGrupo(String botao) {
        if (botao.equals("Sair")) {
            clicarBotaoSair();
        } else if (botao.equals("Replicar")) {
            clicarBotaoReplicarVencimento();
        } else {
            clicarBotaoPesquisar();
        }
    }

    @E("^selecionar o grupo \"([^\"]*)\" referente a cidade \"([^\"]*)\"$")
    public void selecionarOGrupoReferenteACidade(String grupo, String cidade) {
        selecionarReplicacaoVencimento(grupo, cidade);
    }
}
