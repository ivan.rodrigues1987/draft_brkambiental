package stepdefinition.Cadastro.Rubrica;


import cucumber.api.DataTable;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserActions;
import framework.BrowserManager.BrowserVerifications;

import main.WebElementManager.Combos.Combos;
import main.WebElementManager.Xpath;
import org.openqa.selenium.By;

import runner.Dados.Cadastro.Endereco;


import java.sql.SQLException;

import static stepdefinition.Cadastro.Rubrica.Rubrica.selecionarRubricaCentralAtendimento;
import static stepdefinition.Cadastro.Rubrica.Rubrica.clicarComboAbaAtraso;
import static stepdefinition.Cadastro.Rubrica.Rubrica.setText;
import static stepdefinition.Cadastro.Rubrica.Rubrica.getCodigoRubrica;
import static stepdefinition.Cadastro.Rubrica.Rubrica.setCodigoRubrica;
import static stepdefinition.Cadastro.Rubrica.Rubrica.setDescricaoRubrica;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.CentralAtendimento.clicarItemGridNovaVenda;
import static main.Utils.ComumScreens.PesquisaRapida.preencherComboCidadePesquisaRapida;
import static main.Utils.ComumScreens.PesquisaRapida.pesquisarSimpleBotao;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.Utils.geradorNumberRandom;
import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.ComboInputText.setComboEPressionarEnter;
import static main.WebElementManager.Combos.ComboInputText.setCombo;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.Combos.Combos.getXPathCombo;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.getInputTextXPath;
import static main.WebElementManager.Xpath.DIV;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Juan Castillo
 * @since 21/02/2019
 */
public class StepsRubrica {

    private Rubrica rubrica;
    private Endereco endereco;

    private static final int TAMANHO = 5;
    private static final int COLUNA = 7;

    public StepsRubrica() {
        endereco = getDadosCadastro().getEndereco();
    }


    @Quando("^preencher os seguintes campos em Cadastro de Rubrica:$")
    public void preencherOsSeguintesCamposEmCadastroDeRubrica(DataTable table) throws SQLException {
        Rubrica rubricaBDD = getDataTable(table, Rubrica.class);
        rubrica = new Rubrica(true, rubricaBDD.getCidade(), rubricaBDD.getGrupoDeRubricas(),
                rubricaBDD.getClassificacaoContabil(), rubricaBDD.getGrupoReceita(),
                rubricaBDD.getTipoIntegracaoFinanceira());

        //CIDADE
        BrowserVerifications.waitElementToBeVisible(getXPathCombo("Cidade"), DEFAULT_TIME_OUT);
        setComboCidade("Cidade", getCampo(rubricaBDD.getCidade(), endereco.getCidade()));
        Rubrica.setCidadenome(rubricaBDD.getCidade());

        //Grupo de Rubricas
        BrowserVerifications.waitElementToBeVisible(getXPathCombo("Grupo de Rubricas"), DEFAULT_TIME_OUT);
        BrowserVerifications.waitElementToBeClickable(getXPathCombo("Grupo de Rubricas"), DEFAULT_TIME_OUT);
        BrowserVerifications.wait(2);

        setComboClicando("Grupo de Rubricas", getCampo(rubricaBDD.getGrupoDeRubricas(), rubrica.getGrupoDeRubricas()), true);

        //Classificacao Contabil
        setTextoCombo("Classificação Contábil", getCampo(rubricaBDD.getClassificacaoContabil(),
                rubrica.getClassificacaoContabil()), true);


        //Tipo rubrica
        setTextoCombo("Tipo Rubrica", getCampo(rubricaBDD.getTipoRubrica(), rubrica.getTipoRubrica()), true);
        if (rubricaBDD.getTipoRubrica().isEmpty()) {
            clickOnElement(getXPathCombo("Tipo Rubrica"));
        }

        //Grupo receita
        setTextoCombo("Grupo Receita", getCampo(rubricaBDD.getGrupoReceita(), rubrica.getGrupoReceita()), true);

        //Tipo Integracao Financiera
        setTextoCombo("Tipo Integração Financeira", getCampo(rubricaBDD.getTipoIntegracaoFinanceira(),
                rubrica.getTipoIntegracaoFinanceira()), true);

        //Codigo Auxiliar
        setInputText("Código Auxiliar", getCampo(rubricaBDD.getCodigoAuxiliar(), rubrica.getCodigoAuxiliar()));

        //Situação Faturar?
        clicarCheckBox("Situação Faturar", getCampo(rubricaBDD.getSituacaoFaturar(), rubrica.getSituacaoFaturar()));

        //Rubrica de parcelamento associado
        setComboClicando("Rubrica de Parcelamento Associado", getCampo(rubricaBDD.getRubricaDeParcelamentoAssociado(),
                rubrica.getRubricaDeParcelamentoAssociado()));

        //inserirTextoVigencia(rubricaBDD.rubricaDeParcelamentoAssociado);

        //Rubrica de Parcelamento?
        clicarCheckBox("Rubrica de Parcelamento", getCampo(rubricaBDD.getRubricaDeParcelamento(),
                rubrica.getRubricaDeParcelamento()));

        //Rubrica de desconto Parcelamento?
        clicarCheckBox("Rubrica de  desconto parcelamento.", getCampo(rubricaBDD.getRubricaDeDescontoParcelamento(),
                rubrica.getRubricaDeDescontoParcelamento()));

        //Aplicar como:

        if (!rubricaBDD.getAplicarComo().isEmpty()) {
            BrowserVerifications.waitElementToBeVisible(getXPathCombo("Aplicar como"), DEFAULT_TIME_OUT);
            Combos.setComboClicando("Aplicar como", getCampo(rubricaBDD.getAplicarComo(),
                    rubrica.getAplicarComo()), true);
        }





        //--- ESSES CAMPOS ESTAVAM ZERANDO NO PREENCHIMENTO AS VEZES
        //Descricao simple
        setText("Descrição Simples", getCampo(rubricaBDD.getDescricaoSimples(), rubrica.getDescricaoSimples()), true);

        //Descricao completa
        setText("Descrição Completa", getCampo(rubricaBDD.getDescricaoCompleta(),
                rubrica.getDescricaoCompleta()), true);
        setDescricaoRubrica(rubrica.getDescricaoCompleta());

        //Codigo
        setText("Código", getCampo(rubricaBDD.getCodigo(), rubrica.getCodigo()), true);
        setCodigoRubrica(rubrica.getCodigo().trim());
        System.out.println(getCodigoRubrica());
        //BrowserVerifications.waitElementValue(getXPathTextoCombo("Cidade"),
        // "value",Rubrica.cidadenome,DEFAULT_TIME_OUT);

        //Revisao
        setText("Revisão", getCampo(rubricaBDD.getRevisao(), rubrica.getRevisao()), true);

        //Vigencia
        setComboEPressionarEnter("Vigência", rubricaBDD.getVigencia().trim());

        //Valor
        setText("Valor", getCampo(rubricaBDD.getValor(), rubrica.getValor()), true);

        //Fator Reducao
        setText("Fator Redução", getCampo(rubricaBDD.getFatorReducao(), rubrica.getFatorReducao()), true);
        //-------------

        //Ativa
        BrowserVerifications.waitElementToBeVisible(getInputTextXPath("Código"), DEFAULT_TIME_OUT);
        clicarCheckBox("Ativa", getCampo(rubricaBDD.getAtiva(), rubrica.getAtiva()));

        //Contratual
        clicarCheckBox("Contratual", getCampo(rubricaBDD.getContratual(), rubrica.getContratual()));

    }


    @E("^clicar com o botao direito do mouse e clicar em \"([^\"]*)\"$")
    public void clicarComOBotaoDireitoDoMouseEClicarEm(String botao) {
        // Selecionar sobre a opcao desejada
        BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("//div[@class='GKPSPJYCIUB']"));
    }

    @E("^selecionar a Taxa de Atraso \"([^\"]*)\"$")
    public void selecionarATaxaDeAtraso(String valor) throws Throwable {
        // Selecionar taxa desejada
        clicarComboAbaAtraso("Taxa de Atraso", valor);
    }


    @Dado("^a tela Cadastro de Rubrica esta conforme os seguintes parametros da entidade \"([^\"]*)\":$")
    public void aTelaCadastroDeRubricaEstaConformeOsSeguintesParametrosDaEntidade(String entidade, DataTable dataTable)
            throws Throwable {
        //PARAMETROS QUE SAO EXIBIDOS NO RELATORIO FINAL
    }


    @E("^na aba Aba Fatura preencher os seguintes campos:$")
    public void naAbaAbaFaturaPreencherOsSeguintesCampos(DataTable table) throws Throwable {

        Rubrica rubricaBDD = getDataTable(table, Rubrica.class);
        rubrica = new Rubrica(true, rubricaBDD.getGrupoDeExibicaoDaRubrica());

        clicarAba("Fatura");
        //Grupo de Exibicao da Rubrica
        BrowserVerifications.waitElementToBeEnable("Grupo de Exibição da Rubrica", DEFAULT_TIME_OUT);
        setCombo("Grupo de Exibição da Rubrica", getCampoPesquisado(getCampo(rubricaBDD.getGrupoDeExibicaoDaRubrica(),
                rubrica.getGrupoDeExibicaoDaRubrica())), true);
        //Exibir na fatura
        clicarCheckBox("Exibir na Fatura", getCampo(rubricaBDD.getExibirNaFatura(), rubrica.getExibirNaFatura()));
        //Considerar no total da fatura
        clicarCheckBox("Considerar no Total da Fatura", getCampo(rubricaBDD.getConsiderarNoTotalDaFatura(),
                rubrica.getConsiderarNoTotalDaFatura()));


    }

    @E("^na aba Aba Arrecadacao preencher os seguintes campos:$")
    public void naAbaAbaArrecadacaoPreencherOsSeguintesCampos(DataTable table) throws Throwable {

        Rubrica rubricaBDD = getDataTable(table, Rubrica.class);
        rubrica = new Rubrica(true, rubricaBDD.getGrupoarrecadacao(), rubrica.getGrupoarrecadacao());
        //Selecionar Abas
        clicarAba("Arrecadação");
        //Clicar em Grupo Arrecadacao
        setComboClicando("Grupo Arrecadação", getCampo(rubricaBDD.getGrupoarrecadacao(),
                rubrica.getGrupoarrecadacao()), true);

    }

    @E("^na aba Aba Fiscal preencher os seguintes campos:$")
    public void naAbaAbaFiscalPreencherOsSeguintesCampos(DataTable table) throws Throwable {

        Rubrica rubricaBDD = getDataTable(table, Rubrica.class);
        rubrica = new Rubrica(true, rubricaBDD.getClassifFiscal(),
                rubricaBDD.getClassifFiscalIndireta(), rubrica.getClassifFiscal());

        clicarAba("Fiscal");

        //CLASS FISCAL
        setComboClicando("Classif.Fiscal", getCampo(rubricaBDD.getClassifFiscal(), rubrica.getClassifFiscal()), true);

        //CLASS FISCAL INDIRETA
        setComboClicando("Classif.Fiscal Indireta", getCampo(rubricaBDD.getClassifFiscalIndireta(),
                rubrica.getClassifFiscalIndireta()), true);

        //ALIQUOTA PIS
        setInputTextEPressionarEnter("Aliquota PIS", getCampo(rubricaBDD.getAliquotaPIS(),
                rubrica.getAliquotaPIS()), true);

        //ALIQUOTA CONFINS
        setInputTextEPressionarEnter("Aliquota COFINS", getCampo(rubricaBDD.getAliquotaCOFINS(),
                rubrica.getAliquotaCOFINS()), true);

        //ALIQUOTA ISS
        setInputTextEPressionarEnter("Aliquota ISS", getCampo(rubricaBDD.getAliquotaISS(),
                rubrica.getAliquotaISS()), true);

        //ALIQUOTA ICMS
        setInputTextEPressionarEnter("Aliquota ICMS", getCampo(rubricaBDD.getAliquotaICMS(),
                rubrica.getAliquotaICMS()), true);

        //ALIQUOTA INSS
        setInputTextEPressionarEnter("Aliquota INSS", getCampo(rubricaBDD.getAliquotaINSS(),
                rubrica.getAliquotaINSS()), true);

    }

    @E("^na aba \"([^\"]*)\" selecionar no campo \"([^\"]*)\", em operador \"([^\"]*)\" e no valor \"([^\"]*)\"$")
    public void naAbaSelecionarNoCampoEmOperadorENoValor(String aba, String campo, String operador, String valor)
            throws Throwable {

        clicarAba(aba);

        setComboClicando("Campo", 2, campo);
        setComboClicando("Operador", operador);
        setInputText(2, "Valor", getCampo(valor, getCampoPesquisado(valor)), true);

    }

    @E("^selecionar no campo \"([^\"]*)\", em operador \"([^\"]*)\" e em valor \"([^\"]*)\"$")
    public void selecionarNoCampoEmOperadorEEmValor(String campo, String operador, String valor) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        setComboClicando("Campo", 2, campo);
        setComboClicando("Operador", operador);
        setInputText(2, "Valor", getCampo(valor, getCampoPesquisado(valor)), true);
    }

    @E("^na tela de Pesquisa tem que preencher a cidade \"([^\"]*)\", o campo \"([^\"]*)\" e o valor \"([^\"]*)\"$")
    public void naTelaDePesquisaTemQuePreencherACidadeOCampoEOValor(String campo, String operador, String valor)
            throws Throwable {
        setComboClicando("Campo", 2, campo);
        setComboClicando("Operador", operador);
        setInputText(2, "Valor", getCampo(valor, getCampoPesquisado(valor)), true);
    }


    @E("^na tela de Pesquisa tem que preencher na cidade \"([^\"]*)\", o campo \"([^\"]*)\" e o valor \"([^\"]*)\"$")
    public void naTelaDePesquisaTemQuePreencherNaCidadeOCampoEOValor(String cidade, String campo, String valor)
            throws Throwable {
        preencherComboCidadePesquisaRapida("Cidade", 2, getCampo(cidade, getDadosCadastro().getEndereco().getCidade()));
        pesquisarSimpleBotao(campo, getCampo(valor, getCampoPesquisado(valor)));

    }

    @E("^clicar com o botao direito do mouse e clicar em \"([^\"]*)\" e selecionar em Taxa de Atraso \"([^\"]*)\"$")
    public void clicarComOBotaoDireitoDoMouseEClicarEmESelecionarEmTaxaDeAtraso(String botao, String valor)
            throws Throwable {

        BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("//div[contains(text(),'" + valor + "')]"));
    }

    @E("^preencher os seguintes campos em Cadastro de Grupo Exibe Rubrica:$")
    public void preencherOsSeguintesCamposEmCadastroDeGrupoExibeRubrica(DataTable table) {
        Rubrica rubricaBDD = getDataTable(table, Rubrica.class);
        rubrica = new Rubrica();

        //CIDADE
        setComboCidade("Cidade", getCampo(rubricaBDD.getCidade(), endereco.getCidade()));

        //Descricao
        String descricaoSimples = geradorCharRandom(TAMANHO);
        setInputText("Descrição", getCampo(rubricaBDD.getDescricaoSimples(), descricaoSimples), true);
        Rubrica.setDescricaoExibe(descricaoSimples);

        //Ordem Exibicao
        String ordemExibicao = geradorNumberRandom(2);
        setInputText("Ordem Exibição", getCampo(rubricaBDD.getOrdemExibicao(), ordemExibicao), true);

        clicarCheckBox("Exibe Período de Referência",
                getCampo(rubricaBDD.getExibePeriodoDeReferencia(), rubrica.getExibePeriodoDeReferencia()));

        clicarCheckBox("Embutir Imposto", getCampo(rubricaBDD.getEmbutirImposto(), rubrica.getEmbutirImposto()));

    }

    @E("^na aba \"([^\"]*)\" de Pesquisa Rapida$")

    public void naAbaDePesquisaRapida(String valor) {
        clicarAba(valor);
    }

    @Entao("^validar todas as opcoes do campo Campo$")
    public void validarTodasAsOpcoesDoCampoCampo() {
        setComboClicando("Campo", "codigo");
        setComboClicando("Campo", "considerartotalfatura");
        setComboClicando("Campo", "contratual");
        setComboClicando("Campo", "debitocredito");
        setComboClicando("Campo", "exibirfatura");
    }

    @E("^selecionar a Rubrica \"([^\"]*)\" em Composicao e clicar em \"([^\"]*)\"$")
    public void selecionarARubricaEmComposicaoEClicarEm(String valor, String clicar) {
        selecionarRubricaCentralAtendimento(valor, clicar);
    }

    @E("^preencher em Entrada o valor \"([^\"]*)\"$")
    public void preencherEmEntradaOValor(String valor) {
        clicarItemGridNovaVenda(1, COLUNA);
        setInputTextEPressionarEnter(valor);
    }

    @E("^selecionar em Periodo o valor \"([^\"]*)\"$")
    public void selecionarEmPeriodoOValor(String valor) {
        clickOnElement(By.xpath("//td[@class='GKPSPJYCJIC x-grid-cell-last']//div[@class='GKPSPJYCLIC']"));
        clickOnElement(By.xpath("//div[@class='GKPSPJYCJY']"));
        clickOnElement(Xpath.getXPathElementWithText(DIV, valor));
    }

    @E("^clicar na tela de Cadastro Rubrica o botao direito do mouse clicar em \"([^\"]*)\"$")
    public void clicarNaTelaDeCadastroRubricaOBotaoDireitoDoMouseClicarEm(String botao) {
        // Selecionar sobre a opcao desejada
        BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("//div[@class='GKPSPJYCIUB']"));
    }

    @E("^preencher em Parcela o valor \"([^\"]*)\"$")
    public void preencherEmParcelaOValor(String valor) {
        clickOnElement(By.xpath("//div[@class='GKPSPJYCIUB']//td[9]//div[1]"));
        setInputTextEPressionarEnter(valor);

    }

    @E("^alterar o cadastro de Rubrica para o status \"([^\"]*)\"$")
    public void alterarOCadastroDeRubricaParaOStatus(String status) {
        clicarCheckBox("Ativa", "S");

    }


}
