package stepdefinition.Cadastro.Rubrica;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;

/**
 * @author Juan Castillo
 * @since 21/02/2019
 */
public class DBRubrica extends DBActions {

    public DBRubrica(String environment) {
        super(environment);
    }

    public DBRubrica(DataBase dataBase) {
        super(dataBase);
    }

    public Rubrica getDadosRubrica() {
        Rubrica rubrica = new Rubrica();
        Rubrica rubricaAux;

        rubricaAux = retornarRubrica(retornaCidadeRubrica());

        //Consulta BD
        rubrica.setCodigoCadastrado(rubricaAux.getCodigoCadastrado());
        rubrica.setDescricaoCadastrado(rubricaAux.getDescricaoCadastrado());

        return rubrica;
    }

    //Retorna cidade
    public String retornaCidadeRubrica() {
        return getColumnInfo("CIDADE", "SELECT CIDADE " +
                "FROM CIDADE " +
                "WHERE ROWNUM = 1 "); //SEMPRE SELECIONA SAO DOM. DO ARAGUAI
                //"ORDER BY SIGLA DESC");
    }

    //RETORNAR DADOS RUBRICA GERAL
    public Rubrica retornarRubrica(String cidade) {
        Rubrica rubrica = new Rubrica();

        ArrayList<String> info = getColumnsInfo("CODIGO,RUBRICA",
                "SELECT CODIGO, RUBRICA FROM (SELECT rub.CODIGO, rub.RUBRICA " +
                "FROM Rubrica rub " +
                "INNER JOIN Cidade cid ON rub.IDCIDADE = cid.ID " +
                "INNER JOIN RubricaTipoTaxaAtraso rubTxAtraso ON rub.id = rubTxAtraso.idRubrica " +
                "WHERE rub.SITUACAO = 'A' " +
                "AND cid.CIDADE LIKE '" + cidade + "%' " +
                "AND rubTxAtraso.idTipoTaxaAtraso = 1 AND rub.Codigo > 0 AND length(rub.rubricaresumida)<22 " +
                "ORDER BY 1 DESC)" +
                "WHERE ROWNUM = 1"
        );

        rubrica.setCodigoCadastrado(info.get(0));
        rubrica.setDescricaoCadastrado(info.get(1));

        return rubrica;
    }

}
