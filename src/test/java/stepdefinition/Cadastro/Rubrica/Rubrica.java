package stepdefinition.Cadastro.Rubrica;


import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;

import java.sql.SQLException;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.PesquisaRapida.clicarResultadoPesquisa;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.Utils.geradorNumberRandom;
import static main.Utils.Utils.geradorCharRandom;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static main.WebElementManager.Xpath.concatenarIndice;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.DIV;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Juan Castillo
 * @since 21/02/2019
 */


public class Rubrica {

    //ABA PRINCIPAL
    private String cidade;
    private String codigo;
    private String codigoCadastrado;
    private String revisao;   //--Aleatorio
    private String vigencia;
    private String ativa;
    private String contratual;
    private String grupoDeRubricas;
    private String classificacaoContabil;
    private String idclassificacaoContabil;
    private String valor;   //--Aleatorio
    private String fatorReducao;   //--Aleatorio
    private String descricaoCompleta;   //--Aleatorio
    private String descricaoCadastrado;
    private String descricaoSimples;   //--Aleatorio
    private String tipoRubrica;
    private String grupoReceita;
    private String tipoIntegracaoFinanceira;
    private String codigoAuxiliar;   //--Aleatorio
    private String situacaoFaturar;
    private String rubricaDeParcelamentoAssociado;
    private String rubricaDeParcelamento;
    private String rubricaDeDescontoParcelamento;
    private String aplicarComo;

    private String taxaAtrasso;

    //ABA FATURA
    private String grupoDeExibicaoDaRubrica;
    //radiobutton
    private String exibirNaFatura;
    private String considerarNoTotalDaFatura;

    //ABA ARRECADACAO
    private String grupoarrecadacao;
    //Campo nao disponivel
    //public String incidencia;

    //ABA FISCAL
    private String classifFiscal;
    private String classifFiscalIndireta;
    private String aliquotaPIS;
    private String aliquotaCOFINS;
    private String aliquotaISS;
    private String aliquotaICMS;
    private String aliquotaINSS;

    //Dados estaticos
    private static String cidadenome;
    private static String codigoRubrica;
    private static String descricaoExibe;
    private static String descricaoRubrica;
    private static int linha = 0;

    //Exibe rubrica
    private String ordemExibicao;
    private String exibePeriodoDeReferencia;
    private String embutirImposto;


    private static final int TAMANHO_CODIGO = 5;
    private static final int TAMANHO_REVISAO = 3;
    private static final int TAMANHO_VALOR = 2;
    private static final int TAMANHO_FATOR_REDUCAO = 2;
    private static final int TAMANHO_DESCRICAO_COMPLETA = 5;
    private static final int TAMANHO_DESCRICAO_SIMPLES = 3;
    private static final int TAMANHO_CODIGO_AUXILIAR = 6;
    private static final int LINHA_TRES = 3;
    private static final int TENTATIVAS = 3;


    public Rubrica() {
    }

    //public Rubrica(boolean gerarDadosRubrica, String documento, String classCon, String grupoRe, String tipoInteg,
    // String grupoExi) throws SQLException{
    //Metodo para fazer chamado geral
    public Rubrica(boolean gerarDadosRubrica, String cidade, String documento, String classCon, String grupoRe,
                   String tipoInteg) throws SQLException {
        if (gerarDadosRubrica) {
            gerarDadosRubrica(cidade, documento, classCon, grupoRe, tipoInteg);
        }
    }

    //Metodo para fazer chamado especifico a Abas Fatura
    public Rubrica(boolean gerarDadosFatura, String grupoExi) throws SQLException {
        gerarDadosAbaFatura(grupoExi);
    }

    //Metodo para fazer chamado especifico a Abas Arrecadador
    public Rubrica(boolean gerarDadosAbaArrecadador, String valor, String a) throws SQLException {
        gerarDadosAbaArrecadador(valor);
    }

    //Metodo para fazer chamado especifico a Abas Fiscal
    public Rubrica(boolean gerarDadosAbaFiscal, String valor1, String valor2, String valorc) throws SQLException {
        gerarDadosAbaFiscal(valor1, valor2);
    }
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoCadastrado() {
        return codigoCadastrado;
    }

    public void setCodigoCadastrado(String codigoCadastrado) {
        this.codigoCadastrado = codigoCadastrado;
    }

    public String getRevisao() {
        return revisao;
    }

    public void setRevisao(String revisao) {
        this.revisao = revisao;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getAtiva() {
        return ativa;
    }

    public void setAtiva(String ativa) {
        this.ativa = ativa;
    }

    public String getContratual() {
        return contratual;
    }

    public void setContratual(String contratual) {
        this.contratual = contratual;
    }

    public String getGrupoDeRubricas() {
        return grupoDeRubricas;
    }

    public void setGrupoDeRubricas(String grupoDeRubricas) {
        this.grupoDeRubricas = grupoDeRubricas;
    }

    public String getClassificacaoContabil() {
        return classificacaoContabil;
    }

    public void setClassificacaoContabil(String classificacaoContabil) {
        this.classificacaoContabil = classificacaoContabil;
    }

    public String getIdclassificacaoContabil() {
        return idclassificacaoContabil;
    }

    public void setIdclassificacaoContabil(String idclassificacaoContabil) {
        this.idclassificacaoContabil = idclassificacaoContabil;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getFatorReducao() {
        return fatorReducao;
    }

    public void setFatorReducao(String fatorReducao) {
        this.fatorReducao = fatorReducao;
    }

    public String getDescricaoCompleta() {
        return descricaoCompleta;
    }

    public void setDescricaoCompleta(String descricaoCompleta) {
        this.descricaoCompleta = descricaoCompleta;
    }

    public String getDescricaoCadastrado() {
        return descricaoCadastrado;
    }

    public void setDescricaoCadastrado(String descricaoCadastrado) {
        this.descricaoCadastrado = descricaoCadastrado;
    }

    public String getDescricaoSimples() {
        return descricaoSimples;
    }

    public void setDescricaoSimples(String descricaoSimples) {
        this.descricaoSimples = descricaoSimples;
    }

    public String getTipoRubrica() {
        return tipoRubrica;
    }

    public void setTipoRubrica(String tipoRubrica) {
        this.tipoRubrica = tipoRubrica;
    }

    public String getGrupoReceita() {
        return grupoReceita;
    }

    public void setGrupoReceita(String grupoReceita) {
        this.grupoReceita = grupoReceita;
    }

    public String getTipoIntegracaoFinanceira() {
        return tipoIntegracaoFinanceira;
    }

    public void setTipoIntegracaoFinanceira(String tipoIntegracaoFinanceira) {
        this.tipoIntegracaoFinanceira = tipoIntegracaoFinanceira;
    }

    public String getCodigoAuxiliar() {
        return codigoAuxiliar;
    }

    public void setCodigoAuxiliar(String codigoAuxiliar) {
        this.codigoAuxiliar = codigoAuxiliar;
    }

    public String getSituacaoFaturar() {
        return situacaoFaturar;
    }

    public void setSituacaoFaturar(String situacaoFaturar) {
        this.situacaoFaturar = situacaoFaturar;
    }

    public String getRubricaDeParcelamentoAssociado() {
        return rubricaDeParcelamentoAssociado;
    }

    public void setRubricaDeParcelamentoAssociado(String rubricaDeParcelamentoAssociado) {
        this.rubricaDeParcelamentoAssociado = rubricaDeParcelamentoAssociado;
    }

    public String getRubricaDeParcelamento() {
        return rubricaDeParcelamento;
    }

    public void setRubricaDeParcelamento(String rubricaDeParcelamento) {
        this.rubricaDeParcelamento = rubricaDeParcelamento;
    }

    public String getRubricaDeDescontoParcelamento() {
        return rubricaDeDescontoParcelamento;
    }

    public void setRubricaDeDescontoParcelamento(String rubricaDeDescontoParcelamento) {
        this.rubricaDeDescontoParcelamento = rubricaDeDescontoParcelamento;
    }

    public String getAplicarComo() {
        return aplicarComo;
    }

    public void setAplicarComo(String aplicarComo) {
        this.aplicarComo = aplicarComo;
    }

    public String getTaxaAtrasso() {
        return taxaAtrasso;
    }

    public void setTaxaAtrasso(String taxaAtrasso) {
        this.taxaAtrasso = taxaAtrasso;
    }

    public String getGrupoDeExibicaoDaRubrica() {
        return grupoDeExibicaoDaRubrica;
    }

    public void setGrupoDeExibicaoDaRubrica(String grupoDeExibicaoDaRubrica) {
        this.grupoDeExibicaoDaRubrica = grupoDeExibicaoDaRubrica;
    }

    public String getExibirNaFatura() {
        return exibirNaFatura;
    }

    public void setExibirNaFatura(String exibirNaFatura) {
        this.exibirNaFatura = exibirNaFatura;
    }

    public String getConsiderarNoTotalDaFatura() {
        return considerarNoTotalDaFatura;
    }

    public void setConsiderarNoTotalDaFatura(String considerarNoTotalDaFatura) {
        this.considerarNoTotalDaFatura = considerarNoTotalDaFatura;
    }

    public String getGrupoarrecadacao() {
        return grupoarrecadacao;
    }

    public void setGrupoarrecadacao(String grupoarrecadacao) {
        this.grupoarrecadacao = grupoarrecadacao;
    }

    public String getClassifFiscal() {
        return classifFiscal;
    }

    public void setClassifFiscal(String classifFiscal) {
        this.classifFiscal = classifFiscal;
    }

    public String getClassifFiscalIndireta() {
        return classifFiscalIndireta;
    }

    public void setClassifFiscalIndireta(String classifFiscalIndireta) {
        this.classifFiscalIndireta = classifFiscalIndireta;
    }

    public String getAliquotaPIS() {
        return aliquotaPIS;
    }

    public void setAliquotaPIS(String aliquotaPIS) {
        this.aliquotaPIS = aliquotaPIS;
    }

    public String getAliquotaCOFINS() {
        return aliquotaCOFINS;
    }

    public void setAliquotaCOFINS(String aliquotaCOFINS) {
        this.aliquotaCOFINS = aliquotaCOFINS;
    }

    public String getAliquotaISS() {
        return aliquotaISS;
    }

    public void setAliquotaISS(String aliquotaISS) {
        this.aliquotaISS = aliquotaISS;
    }

    public String getAliquotaICMS() {
        return aliquotaICMS;
    }

    public void setAliquotaICMS(String aliquotaICMS) {
        this.aliquotaICMS = aliquotaICMS;
    }

    public String getAliquotaINSS() {
        return aliquotaINSS;
    }

    public void setAliquotaINSS(String aliquotaINSS) {
        this.aliquotaINSS = aliquotaINSS;
    }

    public static String getCidadenome() {
        return cidadenome;
    }

    public static void setCidadenome(String cidadenome) {
        Rubrica.cidadenome = cidadenome;
    }

    public static String getCodigoRubrica() {
        return codigoRubrica;
    }

    public static void setCodigoRubrica(String codigoRubrica) {
        Rubrica.codigoRubrica = codigoRubrica;
    }

    public static String getDescricaoExibe() {
        return descricaoExibe;
    }

    public static void setDescricaoExibe(String descricaoExibe) {
        Rubrica.descricaoExibe = descricaoExibe;
    }

    public static String getDescricaoRubrica() {
        return descricaoRubrica;
    }

    public static void setDescricaoRubrica(String descricaoRubrica) {
        Rubrica.descricaoRubrica = descricaoRubrica;
    }

    public static int getLinha() {
        return linha;
    }

    public static void setLinha(int linha) {
        Rubrica.linha = linha;
    }

    public String getOrdemExibicao() {
        return ordemExibicao;
    }

    public void setOrdemExibicao(String ordemExibicao) {
        this.ordemExibicao = ordemExibicao;
    }

    public String getExibePeriodoDeReferencia() {
        return exibePeriodoDeReferencia;
    }

    public void setExibePeriodoDeReferencia(String exibePeriodoDeReferencia) {
        this.exibePeriodoDeReferencia = exibePeriodoDeReferencia;
    }

    public String getEmbutirImposto() {
        return embutirImposto;
    }

    public void setEmbutirImposto(String embutirImposto) {
        this.embutirImposto = embutirImposto;
    }

    //Dados Geral
    private void gerarDadosRubrica(String cidadeParametro, String documento, String classCon,
                                   String grupoRe, String tipoInteg)
            throws SQLException {
        this.cidade = getCidadeRubrica(cidadeParametro);
        this.codigo = geradorNumberRandom(TAMANHO_CODIGO);
        this.revisao = "1" + geradorNumberRandom(TAMANHO_REVISAO);
        this.valor = geradorNumberRandom(TAMANHO_VALOR);
        this.fatorReducao = geradorNumberRandom(TAMANHO_FATOR_REDUCAO);
        this.descricaoCompleta = "Completa" + " " + geradorCharRandom(TAMANHO_DESCRICAO_COMPLETA);
        this.descricaoSimples = geradorCharRandom(TAMANHO_DESCRICAO_SIMPLES);
        this.codigoAuxiliar = geradorNumberRandom(TAMANHO_CODIGO_AUXILIAR);
        this.grupoDeRubricas = getGrupoRubrica(documento);
        this.classificacaoContabil = getClassContabil(classCon);
        this.grupoReceita = getGrupoReceita(grupoRe);
        this.tipoIntegracaoFinanceira = getTipoIntegracao(tipoInteg);

    }

    //Dados Abas
    private void gerarDadosAbaFatura(String grupoExi) throws SQLException {
        this.grupoDeExibicaoDaRubrica = getGrupoExibeRubrica(grupoExi);
        this.exibirNaFatura = exibirNaFatura;
        this.considerarNoTotalDaFatura = considerarNoTotalDaFatura;
    }

    //Dados Arrecadador
    private void gerarDadosAbaArrecadador(String valorParametro) throws SQLException {
        this.grupoarrecadacao = getGrupoArrecadador(valorParametro);
    }

    //Dados Fiscal
    private void gerarDadosAbaFiscal(String valor1, String valor2) throws SQLException {
        this.classifFiscal = getClassificacaoFiscal(valor1);
        this.classifFiscalIndireta = getClassificacaoFiscalIndireta(valor2);
        this.aliquotaPIS = "0";
        this.aliquotaCOFINS = "0";
        this.aliquotaISS = "0";
        this.aliquotaICMS = "0";
        this.aliquotaINSS = "0";
    }

    //Para Abas de Tasa de Atrasso
    public static void clicarComboAbaAtraso(String identificador, String texto) throws InterruptedException {
        String xPath = "(//span[text()='" + identificador + "'])" +
                "[1]/../../../../../../../../div/div/div/div/div/table/tbody/tr/td[@cellindex=1]";
        //Seleciona linha

        linha++;
        if (!BrowserVerifications.elementExists(concatenarIndice(xPath, linha))) {
            linha--;
        }
        clickOnElement(concatenarIndice(xPath, linha));
        sendKeys(texto);
        clickOnElement(getXPathElementWithText(DIV, texto));

        if (linha == LINHA_TRES) {
            linha = 0;
        }

    }

    //Para Grupo de rubricas - Chamar DB
    private String getGrupoRubrica(String documento) throws SQLException {
        if (documento.contains("Grupo de Rubricas")) {
            return getDadosCadastro().getRubrica().grupoDeRubricas;
        } else {
            return documento;
        }
    }

    //Para Class Contabil
    private String getClassContabil(String classCon) throws SQLException {
        if (classCon.contains("Classificação Contábil")) {
            return getDadosCadastro().getRubrica().classificacaoContabil;
        } else {
            return classCon;
        }
    }

    //Para Grupo Receita
    private String getGrupoReceita(String grupoRe) throws SQLException {
        if (grupoRe.contains("Grupo Receita")) {
            return getDadosCadastro().getRubrica().grupoReceita;
        } else {
            return grupoRe;
        }
    }

    //Para Tipo Integracion
    private String getTipoIntegracao(String tipoInteg) throws SQLException {
        if (tipoInteg.contains("Tipo Integração Financeira")) {
            return getDadosCadastro().getRubrica().tipoIntegracaoFinanceira;
        } else {
            return tipoInteg;
        }
    }

    //Para Grupo Exibe Rubrica
    private String getGrupoExibeRubrica(String grupoExi) throws SQLException {
        if (grupoExi.contains("Grupo de Exibição da Rubrica")) {
            return getDadosCadastro().getRubrica().grupoDeExibicaoDaRubrica;
        } else {
            return grupoExi;
        }
    }

    //Para Grupo Arrecadador
    private String getGrupoArrecadador(String valorParametro) throws SQLException {
        if (valorParametro.contains("Grupo Arrecadador")) {
            return getDadosCadastro().getRubrica().grupoarrecadacao;
        } else {
            return valorParametro;
        }
    }

    //Para Classficacao Fiscal
    private String getClassificacaoFiscal(String valor1) throws SQLException {
        if (valor1.contains("Classificacao Fiscal")) {
            return getDadosCadastro().getRubrica().classifFiscal;
        } else {
            return valor1;
        }
    }

    //Para Classficacao Indireta
    private String getClassificacaoFiscalIndireta(String valor2) throws SQLException {
        if (valor2.contains("Classificacao Fiscal Indireta")) {
            return getDadosCadastro().getRubrica().classifFiscalIndireta;
        } else {
            return valor2;
        }
    }

    //Para retornar Cidade
    private String getCidadeRubrica(String valorParametro) throws SQLException {
        if (valorParametro.contains("Cidade")) {
            return getDadosCadastro().getRubrica().cidade;
        } else {
            return valorParametro;
        }
    }

    //Insere o texto e verifica se o campo nao esta zerado
    public static void setText(String campo, String texto, boolean clearField) {

        String valorPreenchido;
        //Insere texto
        setInputText(campo, texto, clearField);

        //Recebe valor preenchido
        valorPreenchido = getTextAttribute(getInputTextXPath(campo), "value").trim();

        //Verifica se o campo esta zerado ou com valor diferente do que deveria preencher
        if (valorPreenchido.equals("0") || !valorPreenchido.equals(texto)) {
            setInputText(campo, texto, clearField);
        }
    }

    public static void selecionarRubricaCentralAtendimento(String valor, String clicar) {
        String valorPesquisado = getCampoPesquisado(valor);

        //Selecionar em "..."
        clickOnElement(By.xpath("//div[contains(text(),'. . .')]"));

        //Clicar inputtext
        clickOnElement(By.xpath("(//input[@class='GKPSPJYCMX GKPSPJYCHX'])[3]"));

        //Insere texto
        setInputTextEPressionarEnter(valorPesquisado);

        //Aguarda o Grid possuir apenas um resultado
        BrowserVerifications.waitElementSize(By.xpath("//div[2]/div[2]/div[1]/table[1]/tbody[2]/tr/td[2]"),
                1, DEFAULT_TIME_OUT);

        //Clica resultado
        clicarResultadoPesquisa(valorPesquisado, clicar);
    }
}
