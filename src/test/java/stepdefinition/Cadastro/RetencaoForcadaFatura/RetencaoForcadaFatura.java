package stepdefinition.Cadastro.RetencaoForcadaFatura;


import framework.AssertManager;
import main.WebElementManager.InputText.InputTextXPath;

import java.util.Stack;

/**
 * @author Renan Ferreira dos Santos
 * @since 22/03/2019
 */
public class RetencaoForcadaFatura {
    //Campos dinamicos
    private Stack<String> ligacoes;
    private Stack<String> ligacoesJaCadastradas;
    private Stack<RetencaoForcadaFatura> mensagensCadastradas;

    //Campos BDD
    private String id;
    private String cidade;
    private String ligacao;
    private String periodoInicial;
    private String periodoFinal;
    private String ativo;
    private String mensagem;

    public static final int LIMITE_CONSULTAS_VALOR = 2;
    private static int consultasValor = 0;

    private static final int QTD_DIVS = 4;

    public Stack<String> getLigacoesJaCadastradas() {
        return ligacoesJaCadastradas;
    }

    public void setLigacoesJaCadastradas(Stack<String> ligacoesJaCadastradas) {
        this.ligacoesJaCadastradas = ligacoesJaCadastradas;
    }

    public Stack<RetencaoForcadaFatura> getMensagensCadastradas() {
        return mensagensCadastradas;
    }

    public void setMensagensCadastradas(Stack<RetencaoForcadaFatura> mensagensCadastradas) {
        this.mensagensCadastradas = mensagensCadastradas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLigacao() {
        return ligacao;
    }

    public void setLigacao(String ligacao) {
        this.ligacao = ligacao;
    }

    public String getPeriodoInicial() {
        return periodoInicial;
    }

    public void setPeriodoInicial(String periodoInicial) {
        this.periodoInicial = periodoInicial;
    }

    public String getPeriodoFinal() {
        return periodoFinal;
    }

    public void setPeriodoFinal(String periodoFinal) {
        this.periodoFinal = periodoFinal;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String obterMensagem() {
        return mensagem;
    }
    //Retorna a informacao da pilha e na segunda vez elimina
    public RetencaoForcadaFatura getMensagem() {
        RetencaoForcadaFatura mensagemObtida;
        consultasValor++;
        if (consultasValor == LIMITE_CONSULTAS_VALOR) {
            consultasValor = 0;
            mensagemObtida = mensagensCadastradas.pop();
        } else {
            mensagemObtida = mensagensCadastradas.peek();
        }

        return mensagemObtida;
    }

    public String getIdMensagemCadastrada() {
        return getMensagem().id;
    }

    public String getMensagemCadastrada() {
        return getMensagem().mensagem;
    }

    public void setMensagemCadastrada(Stack<RetencaoForcadaFatura> mensagensCadastradasParametro) {
        this.mensagensCadastradas = mensagensCadastradasParametro;
    }

    public String getLigacaoJaCadastrada() {
        return ligacoesJaCadastradas.pop();
    }

    public void setLigacaoJaCadastrada(Stack<String> ligacoesJaCadastradasParametro) {
        this.ligacoesJaCadastradas = ligacoesJaCadastradasParametro;
    }

    public String getLigacoes() {
        return ligacoes.pop();
    }

    public void setLigacoes(Stack<String> ligacoes) {
        this.ligacoes = ligacoes;
    }

    public static void verificarDadosCarregados() {
        //Verifica se o inputText de ligacao foi preenchido
        AssertManager.assertElementAttributeTextEmpty(
                InputTextXPath.getInputTextXPath("Ligação", QTD_DIVS), "value", false);
    }


}
