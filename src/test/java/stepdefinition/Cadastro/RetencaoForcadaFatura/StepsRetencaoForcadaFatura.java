package stepdefinition.Cadastro.RetencaoForcadaFatura;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import main.Utils.Utils;

import runner.Dados.Cadastro.Endereco;

import static stepdefinition.Cadastro.RetencaoForcadaFatura.RetencaoForcadaFatura.verificarDadosCarregados;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.Combos.Combos.setComboClicando;

import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static main.WebElementManager.TextArea.setTextArea;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Renan Ferreira dos Santos
 * @since 22/03/2019
 */
public class StepsRetencaoForcadaFatura {
    private Endereco enderecoDinamico;
    private static final int QTD_DIVS = 4;
    private static final int TAMANHO = 7;

    public StepsRetencaoForcadaFatura() {
        this.enderecoDinamico = getDadosCadastro().getEndereco();
    }

    @Quando("^preencher os seguintes campos em Retencao Forcada de Fatura:$")
    public void preencherOsSeguintesCamposEmRetencaoForcadaDeFatura(DataTable dataTable) {
        RetencaoForcadaFatura retencaoForcadaFatura = getDataTable(dataTable, RetencaoForcadaFatura.class);
        String ligacaoBDD = retencaoForcadaFatura.getLigacao();

        //LIGACAO
        BrowserVerifications.waitElementToBeEnable("Ligação", DEFAULT_TIME_OUT);
        setInputTextEPressionarEnter("Ligação", getCampo(ligacaoBDD, getCampoPesquisado(ligacaoBDD)), QTD_DIVS, true);

        //CIDADE
        setComboCidade("Cidade", getCampo(retencaoForcadaFatura.getCidade(), enderecoDinamico.getCidade()));

        //ATIVO
        clicarCheckBox("Ativo", retencaoForcadaFatura.getAtivo());

        //PERIODO INICIAL
        setComboClicando("Período Inicial", retencaoForcadaFatura.getPeriodoInicial());

        //PERIODO FINAL
        setComboClicando("Período Final", retencaoForcadaFatura.getPeriodoFinal());

        //MENSAGEM
        setTextArea("Mensagem", getCampo(retencaoForcadaFatura.obterMensagem(),
                "Mensagem " + Utils.geradorCharRandom(TAMANHO)), true);
    }

    @Entao("^serao carregados os dados da Mensagem cadastrada para a ligacao$")
    public void seraoCarregadosOsDadosDaMensagemCadastradaParaALigacao() {
        verificarDadosCarregados();
    }
}
