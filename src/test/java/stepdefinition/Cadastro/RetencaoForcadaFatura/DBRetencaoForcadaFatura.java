package stepdefinition.Cadastro.RetencaoForcadaFatura;


import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * @author Renan Ferreira dos Santos
 * @since 22/03/2019
 */
public class DBRetencaoForcadaFatura extends DBActions {
    private static final int QTD_LINHAS = 20;

    public DBRetencaoForcadaFatura(String environment) {
        super(environment);
    }

    public DBRetencaoForcadaFatura(DataBase dataBase) {
        super(dataBase);
    }

    public RetencaoForcadaFatura getDadosRetencaoForcadaFatura(String cidade) {
        RetencaoForcadaFatura retencaoForcadaFatura = new RetencaoForcadaFatura();
        //Get Ligacoes
        retencaoForcadaFatura.setLigacoes(getLigacoesDisponiveis(false));
        retencaoForcadaFatura.setLigacaoJaCadastrada(getLigacoesDisponiveis(true));
        //Ja cadastradas
        retencaoForcadaFatura.setMensagemCadastrada(getMensagensCadastradas());

        return retencaoForcadaFatura;
    }

    private Stack<String> getLigacoesDisponiveis(boolean jaCadastradas) {
        Stack<String> ligacoes = new Stack<>();

        ArrayList<String> result = getColumnValues(
                "id",
                "SELECT lig.id FROM Ligacao lig WHERE " + (jaCadastradas ? "" : "NOT") + " EXISTS" +
                        "(SELECT id FROM MensagemRetencaoForcada WHERE lig.id = idligacao)",
                QTD_LINHAS);

        for (String ligacao : result) {
            ligacoes.push(ligacao);
        }

        return ligacoes;

    }

    //Retorna uma lista com mensagens cadastradas
    private Stack<RetencaoForcadaFatura> getMensagensCadastradas() {
        Stack<RetencaoForcadaFatura> mensagens = new Stack<>();
        ArrayList<HashMap<String, String>> result = getColumnsValues(
                "SELECT msg.id,msg.mensagem FROM MensagemRetencaoForcada msg " +
                "WHERE msg.mensagem IS NOT NULL", QTD_LINHAS, "id", "mensagem");

        for (HashMap<String, String> rs : result) {
            RetencaoForcadaFatura retencaoForcadaFatura = new RetencaoForcadaFatura();
            retencaoForcadaFatura.setId(rs.get("id"));
            retencaoForcadaFatura.setMensagem(rs.get("mensagem"));
            mensagens.add(retencaoForcadaFatura);
        }
        return mensagens;
    }

}
