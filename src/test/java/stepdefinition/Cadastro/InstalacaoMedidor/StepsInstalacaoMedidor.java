package stepdefinition.Cadastro.InstalacaoMedidor;

import cucumber.api.DataTable;
import cucumber.api.java.it.E;

import framework.BrowserManager.BrowserVerifications;
import main.WebElementManager.Combos.ComboInputText;


import static stepdefinition.Cadastro.InstalacaoMedidor.InstalacaoMedidor.selecionarMedidor;
import static framework.BrowserManager.BrowserVerifications.waitElementToBeEnable;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.getCampo;


import static main.Utils.Utils.getDataTable;
import static main.Utils.Utils.retornarDataAtual;


import static main.WebElementManager.Combos.ComboInputText.setCombo;

import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.TextArea.setTextArea;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */

public class StepsInstalacaoMedidor {
    @E("^preencher os seguintes campos em Instalacao de Medidor:$")
    public void preencherOsSeguintesCamposEmInstalacaoDeMedidor(DataTable dataTable) {
        InstalacaoMedidor instalacaoMedidor = getDataTable(dataTable, InstalacaoMedidor.class);


        //PERIODO LEITURA ANTERIOR
        BrowserVerifications.waitElementToBeEnable(" Período de Leitura Anterior ", DEFAULT_TIME_OUT);
        setComboClicando(" Período de Leitura Anterior ", instalacaoMedidor.getPeriodoLeituraAnterior());

        //LEITURA NA RETIRADA
        setInputText("Leitura na Retirada", instalacaoMedidor.getLeituraNaRetirada(), true);

        //INSTALACAO
        String instalacaoBDD = instalacaoMedidor.getInstalacao();
        String instalacaoDinamica = " " + retornarDataAtual("");
        BrowserVerifications.waitElementToBeEnable("Instalação", DEFAULT_TIME_OUT);
        ComboInputText.setComboEPressionarEnter("Instalação", getCampo(instalacaoBDD, instalacaoDinamica));

        //MEDIDOR
        String medidorBDD = instalacaoMedidor.getMedidor();
        String medidorDinamico = getDadosCadastro().getInstalacaoMedidor().getMedidorDisponivel().getNumeroMedidor();
        selecionarMedidor("Numero do Medidor", getCampo(medidorBDD, medidorDinamico));

        //LEITURA
        setInputText("Leitura", instalacaoMedidor.getLeitura());

        //EXECUTOR
        setTextoCombo("Executor", instalacaoMedidor.getExecutor(), false);

        //MOTIVO TROCA
        setTextoCombo("Motivo da Troca", instalacaoMedidor.getMotivoDaTroca(), false);

        //PROGRAMA DE TROCA
        setTextoCombo("Programa de Troca", instalacaoMedidor.getProgramaDeTroca(), false);

        //LACRE
        setInputText("Lacre", instalacaoMedidor.getLacre());

        //OBSERVACAO
        setTextArea("Observação", instalacaoMedidor.getObservacao());
    }

    @cucumber.api.java.pt.E("^preencher a combo de Cidade \"([^\"]*)\" em Instalacao de Medidor$")
    public void preencherAComboDeCidadeEmInstalacaoDeMedidor(String cidade) {
        waitElementToBeEnable("Cidade", DEFAULT_TIME_OUT);
        setCombo("Cidade", getCampo(cidade, getDadosCadastro().getEndereco().getCidade()), true);
    }
}
