package stepdefinition.Cadastro.InstalacaoMedidor;


import stepdefinition.Cadastro.Medidor.Medidor;
import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;
import java.util.Stack;

import static main.DBManager.DBCadastro.getQtdMassasCadastro;

/**
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */
public class DBInstalacaoMedidor extends DBActions {
    private static final int QTD_LINHAS = 10;

    public DBInstalacaoMedidor(String environment) {
        super(environment);
    }

    public DBInstalacaoMedidor(DataBase dataBase) {
        super(dataBase);
    }

    public InstalacaoMedidor getDadosInstalacaoMedidor(String cidade) {
        InstalacaoMedidor instalacaoMedidor = new InstalacaoMedidor();
        instalacaoMedidor.setMedidoresDisponiveis(
                getMedidor(cidade, false, getQtdMassasCadastro().getInstalacaoMedidor()));
        instalacaoMedidor.setLigacaoMedidorAtivo(getLigacao(cidade, true));
        return instalacaoMedidor;
    }

    //Retorna pilha com medidores disponiveis
    private Stack<Medidor> getMedidor(String cidade, boolean instalado, int qtdLinhas) {
        Stack<Medidor> medidores = new Stack<>();
        ArrayList<String> nrMedidores = getColumnValues("medidor",
                "SELECT med.medidor FROM Medidor med " +
                        "INNER JOIN Cidade cid ON med.idCidade = cid.id " +
                        "WHERE instalado = '" + (instalado ? "S" : "N") + "' " +
                        "AND cid.Cidade = '" + cidade + "' " +
                        "AND " + (instalado ? "" : "NOT") + " EXISTS" +
                        "(SELECT id FROM InstalacaoMedidor WHERE idMedidor = med.id)", qtdLinhas);

        for (String nrMedidor : nrMedidores) {
            medidores.push(new Medidor(nrMedidor));
        }

        return medidores;
    }

    private Stack<String> getLigacao(String cidade, boolean medidorInstalado) {
        Stack<String> ligacoesMedidorAtivos = new Stack<>();
        ArrayList<String> result = getColumnValues("id", "SELECT lig.id FROM Ligacao lig " +
                "INNER JOIN Cidade cid ON lig.idcidade = cid.id " +
                "WHERE lig.idMedidor IS " + ((medidorInstalado) ? "NOT" : "") + " NULL AND " +
                "cid.Cidade = '" + cidade + "' ", QTD_LINHAS);

        for (String rs : result) {
            ligacoesMedidorAtivos.push(rs);
        }

        return ligacoesMedidorAtivos;

    }
}
