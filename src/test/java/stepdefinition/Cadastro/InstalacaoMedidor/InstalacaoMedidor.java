package stepdefinition.Cadastro.InstalacaoMedidor;

import stepdefinition.Cadastro.Medidor.Medidor;
import main.Utils.ComumScreens.ConsultaPersonalizada;
import org.openqa.selenium.By;

import java.util.Stack;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserVerifications.waitElementNotExists;
import static framework.BrowserManager.BrowserVerifications.waitElementToBeEnable;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.Utils.isBlank;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Botoes.clicarBotaoPesquisar;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.getXPathElementWithText;

/**
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */
public class InstalacaoMedidor {
    private Stack<Medidor> medidoresDisponiveis;
    private Stack<String> ligacoesMedidorAtivo;
    private String periodoLeituraAnterior;
    private String leituraNaRetirada;
    private String instalacao;
    private String medidor;
    private String leitura;
    private String executor;
    private String motivoDaTroca;
    private String programaDeTroca;
    private String lacre;
    private String observacao;

    /*public Stack<Medidor> getMedidoresDisponiveis() {
        return medidoresDisponiveis;
    }

    public Stack<String> getLigacoesMedidorAtivo() {
        return ligacoesMedidorAtivo;
    }

    public void setLigacoesMedidorAtivo(Stack<String> ligacoesMedidorAtivo) {
        this.ligacoesMedidorAtivo = ligacoesMedidorAtivo;
    }*/

    public String getPeriodoLeituraAnterior() {
        return periodoLeituraAnterior;
    }

    /*public void setPeriodoLeituraAnterior(String periodoLeituraAnterior) {
        this.periodoLeituraAnterior = periodoLeituraAnterior;
    }*/

    public String getLeituraNaRetirada() {
        return leituraNaRetirada;
    }

    /*public void setLeituraNaRetirada(String leituraNaRetirada) {
        this.leituraNaRetirada = leituraNaRetirada;
    }*/

    public String getInstalacao() {
        return instalacao;
    }

    /*public void setInstalacao(String instalacao) {
        this.instalacao = instalacao;
    }*/

    public String getMedidor() {
        return medidor;
    }

    /*public void setMedidor(String medidor) {
        this.medidor = medidor;
    }*/

    public String getLeitura() {
        return leitura;
    }

    /*public void setLeitura(String leitura) {
        this.leitura = leitura;
    }*/

    public String getExecutor() {
        return executor;
    }

    /*public void setExecutor(String executor) {
        this.executor = executor;
    }*/

    public String getMotivoDaTroca() {
        return motivoDaTroca;
    }

    /*public void setMotivoDaTroca(String motivoDaTroca) {
        this.motivoDaTroca = motivoDaTroca;
    }*/

    public String getProgramaDeTroca() {
        return programaDeTroca;
    }

    /*public void setProgramaDeTroca(String programaDeTroca) {
        this.programaDeTroca = programaDeTroca;
    }*/

    public String getLacre() {
        return lacre;
    }

    /*public void setLacre(String lacre) {
        this.lacre = lacre;
    }*/

    public String getObservacao() {
        return observacao;
    }

    /*public void setObservacao(String observacao) {
        this.observacao = observacao;
    }*/

    public void setMedidoresDisponiveis(Stack<Medidor> medidoresDisponiveis) {
        this.medidoresDisponiveis = medidoresDisponiveis;
    }

    public Medidor getMedidorDisponivel() {
        return medidoresDisponiveis.pop();
    }

    public String getLigacaoMedidorAtivo() {
        return ligacoesMedidorAtivo.pop();
    }

    public void setLigacaoMedidorAtivo(Stack<String> ligacoesMedidorAtivoParametro) {
        this.ligacoesMedidorAtivo = ligacoesMedidorAtivoParametro;
    }

    //SELECIONA O MEDIDOR
    public static void selecionarMedidor(String filtrar, String medidor) {
        if (!isBlank(medidor)) {
            ConsultaPersonalizada consultaPersonalizada = new ConsultaPersonalizada();
            String campoMedidor = "Medidor";

            //CLICA EM PESQUISAR
            waitElementToBeEnable(campoMedidor, 2, DEFAULT_TIME_OUT);
            clicarBotaoPesquisar(campoMedidor);

            //EFETUA A PESQUISA
            consultaPersonalizada.efetuarPesquisaPersonalizada(filtrar, medidor, BTN_OK);
        }
    }

    public static void removerMedidor(String botao) {
        waitElementNotExists(By.xpath("//div[@class='GKPSPJYCFK GKPSPJYCOJ GKPSPJYCMK']"), DEFAULT_TIME_OUT);
        clickOnElement(getXPathElementWithText(DIV, botao));
    }
}
