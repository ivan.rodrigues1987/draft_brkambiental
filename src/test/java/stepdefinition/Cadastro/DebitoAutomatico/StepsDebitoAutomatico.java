package stepdefinition.Cadastro.DebitoAutomatico;

import cucumber.api.DataTable;
import cucumber.api.java.it.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import main.WebElementManager.Combos.ComboInputText;
import org.openqa.selenium.Keys;

import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.setText;
import static main.Utils.ComumScreens.Auxiliar.*;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.InputText.InputText.setInputText;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

public class StepsDebitoAutomatico {

    private DebitoAutomatico debitoAutomatico;

    public StepsDebitoAutomatico() {
        debitoAutomatico = getDadosCadastro().getDebitoAutomatico();
    } //StepsDebitoAutomatico

    @E("^preencher o campo ligacao \"([^\"]*)\" \"([^\"]*)\" Debito Ativo em Debito Automatico$")
    public void definirTipoDeLigacao(String valor, String texto) {
        //RETORNANDO CAMPO LIGACAO
        debitoAutomatico.setDadosCadastroDA(debitoAutomatico.getStatusLigacao(texto));
        debitoAutomatico.setLigacao(getCampo(valor, debitoAutomatico.getLigacao()));
        debitoAutomatico.preencherCampoDA("Ligação");

        //AGUARDA RETORNO NA TELA
        BrowserVerifications.waitElementAttributeValue(
                debitoAutomatico.retornarXPathDA(
                        "Ligação"), "value", debitoAutomatico.getLigacao(), DEFAULT_TIME_OUT);
    } //definirTipoDeLigacao

    @E("^preencher o campo \"([^\"]*)\" em Débito Automático$")
    public void emConsultaPersonalizadaPesquisar(String campo) {
        debitoAutomatico.preencherCampoDA(campo);
    }

    @E("^em \"([^\"]*)\" Pesquisar com o Filtro \"([^\"]*)\" por \"([^\"]*)\" em Débito Automático$")
    public void emConsultaPersonalizadaPesquisarDA(String campo, String filtro, String texto) {
        String aux;

        //VALIDANDO CAMPO

        if (debitoAutomatico.validarTipoDado(texto)) {
            //PREENCHENDO O CAMPO E PRESSIONANDO ENTER
            aux = debitoAutomatico.getAtributoDA(campo);
            debitoAutomatico.preencherCampoDA(campo);
        } else {
            aux = texto;
        }

        debitoAutomatico.preencherCampoComFiltroDA(campo, filtro, aux);

        //LIGACAO
        getCampo(texto, aux);

    }

    @E("^preencher em todas as linhas do Grid de parametros do debito automatico$")
    public void preencherEmTodasAsLinhasDoGridDeParametrosDebitoAutomatico(DataTable table) {
        DebitoAutomatico auxDA;
        auxDA = getDataTable(table, DebitoAutomatico.class);

        debitoAutomatico = debitoAutomatico.retornarCampoDA(auxDA, debitoAutomatico);
        String[] aux = debitoAutomatico.getCadastroDA().split("/");

        //CADASTRO DA
        ComboInputText.setComboEPressionarEnter("Cadastro DA", " " + debitoAutomatico.getCadastroDA());
//        clicarNoBotaoAgenda();
//        clicarNoDia(aux[0]);

        //ARRECADADOR
        ComboInputText.setComboEPressionarEnter("Arrecadador", debitoAutomatico.getArrecadador());

        //AGENCIA
        setInputText("Agência", debitoAutomatico.getAgencia());

        //CONTA
        setInputText("Conta", debitoAutomatico.getConta());
    }

    @E("^preencher o campo \"([^\"]*)\" com \"([^\"]*)\" e pressionar ENTER em Débito Automatico$")
    public void preencherLigacaoEPressionarEnter(String campo, String texto) {
        //AUX
        String aux;

        //VALIDANDO CAMPO
        if (debitoAutomatico.validarTipoDado(texto)) {
            //PREENCHENDO O CAMPO E PRESSIONANDO ENTER
            aux = debitoAutomatico.getAtributoDA(campo);
            debitoAutomatico.preencherCampoDA(campo);
        } else {
            aux = texto;
            setText(debitoAutomatico.retornarXPathDA(campo), texto);
            sendKeys(Keys.ENTER);
        } //else

        //VERIFICA SE O VALOR FOI INSERIDO NO CAMPO
        BrowserVerifications.waitElementAttributeValue(
                debitoAutomatico.retornarXPathDA(campo), "value", aux, DEFAULT_TIME_OUT);

        //RETORNA VALOR PARA O BDD
        getCampo(texto, aux);
    } // preencherCampoEPressionarEnter

    @E("^validar o campo \"([^\"]*)\" na tela de Debito Automático$")
    public void validarCampoDA(String campo) {
        //valorEsperado
        String valorEsperado;
        valorEsperado = debitoAutomatico.getAtributoDA(campo);

        //VALIDAR CAMPO COM ESPERA
        BrowserVerifications.waitElementAttributeValue(
                debitoAutomatico.retornarXPathDA(campo), "value", valorEsperado, DEFAULT_TIME_OUT);

    } //seraExibidaAMensagemEmTela

    @E("^validar o dado \"([^\"]*)\" no campo \"([^\"]*)\" na tela de Debito Automático$")
    public void validarCampoDA(String valorEsperado, String campo) {

        valorEsperado = getCampo(valorEsperado, debitoAutomatico.getCadastroDA());
        //VALIDAR CAMPO COM ESPERA
        BrowserVerifications.waitElementAttributeValue(
                debitoAutomatico.retornarXPathDA(campo), "value", valorEsperado, DEFAULT_TIME_OUT);


    } //seraExibidaAMensagemEmTela

    @E("^clicar no botao ajuda em \"([^\"]*)\" em Débito Automático$")
    public void clicarEmAjuda(String campo) {
        debitoAutomatico.clicarNoBotaoAjudaDebitoAutomatico(campo);
    }

    @Entao("^a janela de mensagem \"([^\"]*)\" contera: \"([^\"]*)\"$")
    public void aJanelaDeMensagemContera(String janela, String mensagem) {
        verificarMensagemJanela(janela, mensagem, false);
    }

    //4 Divs, há possibilidade de passar por parâmetro?
    @Quando("^preencher o campo \"([^\"]*)\" com \"([^\"]*)\" em Debito Automatico$")
    public void preencherCampoDiv(String campo, String texto) {
        String aux;

        if (debitoAutomatico.validarTipoDado(texto)) {
            aux = debitoAutomatico.getAtributoDA(campo);
            getCampo(texto, aux);
            texto = aux;
        } // if
        debitoAutomatico.preencherCampoDA(campo, texto);

    } //preencherCampoDiv

    @Entao("^a janela de mensagem \"([^\"]*)\" sera: \"([^\"]*)\" em Debito Automatico$")
    public void aJanelaDeMensagemSera(String janela, String mensagem) {
        //SUBSTITUI ASPAS SIMPLES POR COMPOSTA
        if (mensagem.contains("'")) {
            mensagem = mensagem.replace("'", "\"");
        }

        //VERIFICA MENSAGEM DA JANELA
        verificarMensagemJanela(janela, mensagem, true);
    }

    @E("^fechar tela em Debito Automatico$")
    public void fecharTelaEmDebitoAutomatico() {
        debitoAutomatico.fecharTelaDA();
    }

    @Entao("^buscar uma ligacao \"([^\"]*)\" Debito Ativo em Debito Automatico$")
    public void pedirNovaLigacao(String tipoDebito) {
        if (tipoDebito.toUpperCase().equals("SEM")) {
            if (!debitoAutomatico.isPrimeiraExecucaoDI()) {
                debitoAutomatico.setIndiceDI();
            } else {
                debitoAutomatico.setPrimeiraExecucaoDI(false);
            }
        } else if (tipoDebito.toUpperCase().equals("COM")) {
            if (!debitoAutomatico.isPrimeiraExecucaoDA()) {
                debitoAutomatico.setIndiceDA();
            } else {
                debitoAutomatico.setPrimeiraExecucaoDA(false);
            }
        }
    }
} // class StepsDebitoAutomatico
