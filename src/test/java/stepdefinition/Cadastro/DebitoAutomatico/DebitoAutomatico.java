package stepdefinition.Cadastro.DebitoAutomatico;

import main.WebElementManager.Combos.ComboInputText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.ArrayList;

import static framework.BrowserManager.BrowserActions.setText;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.Utils.ComumScreens.Auxiliar.getCampoValidado;
import static main.Utils.ComumScreens.ConsultaPersonalizada.LupasConsulta.LUPA_AZUL;
import static main.Utils.ComumScreens.ConsultaPersonalizada.evocarPesquisaPersonalizada;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Botoes.clicarNoBotaoAjuda;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

public class DebitoAutomatico {
    //Atributos Globais
    private int indiceDA = 0;
    private int indiceDI = 0;
    private boolean primeiraExecucaoDA = true;
    private boolean primeiraExecucaoDI = true;
    //ParametrosExecucao
    private String ligacao;
    private ArrayList<String> ligacaoDebitoAtivo = new ArrayList<>();
    private ArrayList<String> ligacaoDebitoInativo = new ArrayList<>();
    private ArrayList<String> cadastroDADebitoAtivo = new ArrayList<>();
    private ArrayList<String> cadastroDADebitoInativo = new ArrayList<>();
    private String cadastroDA;
    private String arrecadador;
    private String agencia;
    private String conta;
    private static final int INDICE_AGENCIA = 2;
    private static final int INDICE_AGENCIA_DV = 4;
    private static final int INDICE_CONTA = 6;
    private static final int INDICE_CONTA_DV = 8;

    public int getIndiceDA() {
        return indiceDA;
    }

    public void setIndiceDA(int indiceDA) {
        this.indiceDA = indiceDA;
    }

    public int getIndiceDI() {
        return indiceDI;
    }

    public void setIndiceDI(int indiceDI) {
        this.indiceDI = indiceDI;
    }

    public boolean isPrimeiraExecucaoDA() {
        return primeiraExecucaoDA;
    }

    public void setPrimeiraExecucaoDA(boolean primeiraExecucaoDA) {
        this.primeiraExecucaoDA = primeiraExecucaoDA;
    }

    public boolean isPrimeiraExecucaoDI() {
        return primeiraExecucaoDI;
    }

    public void setPrimeiraExecucaoDI(boolean primeiraExecucaoDI) {
        this.primeiraExecucaoDI = primeiraExecucaoDI;
    }

    public String getLigacao() {
        return ligacao;
    }

    public void setLigacao(String ligacao) {
        this.ligacao = ligacao;
    }

    public ArrayList<String> getLigacaoDebitoAtivo() {
        return ligacaoDebitoAtivo;
    }

    public void setLigacaoDebitoAtivo(ArrayList<String> ligacaoDebitoAtivo) {
        this.ligacaoDebitoAtivo = ligacaoDebitoAtivo;
    }

    public ArrayList<String> getLigacaoDebitoInativo() {
        return ligacaoDebitoInativo;
    }

    public void setLigacaoDebitoInativo(ArrayList<String> ligacaoDebitoInativo) {
        this.ligacaoDebitoInativo = ligacaoDebitoInativo;
    }

    public ArrayList<String> getCadastroDADebitoAtivo() {
        return cadastroDADebitoAtivo;
    }

    public void setCadastroDADebitoAtivo(ArrayList<String> cadastroDADebitoAtivo) {
        this.cadastroDADebitoAtivo = cadastroDADebitoAtivo;
    }

    public ArrayList<String> getCadastroDADebitoInativo() {
        return cadastroDADebitoInativo;
    }

    public void setCadastroDADebitoInativo(ArrayList<String> cadastroDADebitoInativo) {
        this.cadastroDADebitoInativo = cadastroDADebitoInativo;
    }

    public String getCadastroDA() {
        return cadastroDA;
    }

    public void setCadastroDA(String cadastroDA) {
        this.cadastroDA = cadastroDA;
    }

    public String getArrecadador() {
        return arrecadador;
    }

    public void setArrecadador(String arrecadador) {
        this.arrecadador = arrecadador;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public void setDadosCadastroDA(boolean estadoDebito) {
        this.ligacao = getDadosCadastro().getDebitoAutomatico().ligacaoDebitoInativo.get(indiceDI);
        this.cadastroDA = getDadosCadastro().getDebitoAutomatico().cadastroDADebitoInativo.get(indiceDI);
        if (estadoDebito) {
            this.ligacao = getDadosCadastro().getDebitoAutomatico().ligacaoDebitoAtivo.get(indiceDA);
            this.cadastroDA = getDadosCadastro().getDebitoAutomatico().cadastroDADebitoAtivo.get(indiceDA);
        }
    } //preencherLigacao



    public String getAtributoDA(String campo) {

        //Retorna valor de um campo requerido
        switch (campo.toUpperCase()) {
            case ("LIGAÇÃO"):
                return this.ligacao;
            case ("CADASTRO DA"):
                return this.cadastroDA;
            default:
                return "Campo Inexistente";
        }
    } //getAtributoDA

    public By retornarXPathDA(String campo) {
        final int qtdDivs = 4;
        switch (campo.toUpperCase()) {
            case ("LIGAÇÃO"):
                return getInputTextXPath(campo, qtdDivs);
            case ("CADASTRO DA"):
                return ComboInputText.getXPathTextoCombo(campo);
            default:
                return getInputTextXPath(campo);
        } //switch
    } //retornarXPathDA

    public void preencherCampoDA(String campo) {
        switch (campo.toUpperCase()) {
            case ("LIGAÇÃO"):
                evocarPesquisaPersonalizada(campo, LUPA_AZUL, this.ligacao, BTN_OK);
                break;
            default:
                setText(this.retornarXPathDA(campo), this.getAtributoDA(campo));
                sendKeys(Keys.ENTER);
        } //switch
    } //preencherCampoDA

    public void preencherCampoDA(String campo, String texto) {
        switch (campo.toUpperCase()) {
            case ("LIGAÇÃO"):
                evocarPesquisaPersonalizada(campo, LUPA_AZUL, texto, BTN_OK);
                break;
            default:
                setText(this.retornarXPathDA(campo), this.getAtributoDA(campo));
                sendKeys(Keys.ENTER);
        } //switch
    } //preencherCampoDA

    public void preencherCampoComFiltroDA(String campo, String filtro, String texto) {
        evocarPesquisaPersonalizada(campo, LUPA_AZUL, filtro, texto, BTN_OK);
    } //preencherCampoComFiltroDA

    public void clicarNoBotaoAjudaDebitoAutomatico(String campo) {
        switch (campo) {
            case "Agência":
                clicarNoBotaoAjuda(INDICE_AGENCIA);
                break;
            case "Agência DV":

                clicarNoBotaoAjuda(INDICE_AGENCIA_DV);
                break;
            case "Conta":
                clicarNoBotaoAjuda(INDICE_CONTA);
                break;
            case "Conta DV":
                clicarNoBotaoAjuda(INDICE_CONTA_DV);
                break;
            default:
                break;
        } //switch
    } //preencherCampoComFiltroDA

    //VALIDA SE O DADO EH DINAMICO
    public boolean validarTipoDado(String texto) {
        return texto.contains("[") && texto.contains("]");
    }


    public void fecharTelaDA() {
        clickOnElement(By.xpath(
                "/html[1]/body[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/div[1]"));
    }

    public boolean getStatusLigacao(String texto) {
        return !texto.toLowerCase().contains("sem");
    }

    public void setIndiceDA() {
        this.indiceDA++;
    }

    public DebitoAutomatico retornarCampoDA(DebitoAutomatico auxDA, DebitoAutomatico debitoAutomatico) {
        debitoAutomatico.ligacao = getCampoValidado(auxDA.ligacao, debitoAutomatico.ligacao);
        debitoAutomatico.cadastroDA = getCampoValidado(auxDA.cadastroDA, debitoAutomatico.cadastroDA);
        debitoAutomatico.arrecadador = getCampoValidado(auxDA.arrecadador, debitoAutomatico.arrecadador);
        debitoAutomatico.agencia = getCampoValidado(auxDA.agencia, debitoAutomatico.agencia);
        debitoAutomatico.conta = getCampoValidado(auxDA.conta, debitoAutomatico.conta);
        return debitoAutomatico;
    }

    public void setIndiceDI() {
        this.indiceDI++;
    }
} // Class  DebitoAutomatico
