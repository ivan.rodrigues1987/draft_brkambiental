package stepdefinition.Cadastro.DebitoAutomatico;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;

import static main.DBManager.DBCadastro.getQtdMassasCadastro;

public class DBDebitoAutomatico extends DBActions {

    public DBDebitoAutomatico(String environment) {
        super(environment);
    }
    public DBDebitoAutomatico(DataBase dataBase) {
        super(dataBase);
    }

    public ArrayList getLigacaoID(boolean estadoDebito) {
        ArrayList<String> aux;
        aux = getColumnValues("id",
                "SELECT  lig.id FROM  Ligacao lig" +
                        " WHERE " +
                        "    " + ((estadoDebito) ? "" : "NOT") + " EXISTS(" +
                        "            SELECT  debAut.idLigacao FROM DebitoAutomatico debAut" +
                        "            WHERE" +
                        "                ativo = 'S' AND debAut.idLigacao = lig.id" +
                        "          ) and lig.id not in ('0')", getQtdMassasCadastro().getDebitoAutomatico());

        return aux;
    }

    public String getDataCadDA(String idLigacao) {
        return getColumnInfo("datacadastro", "select datacadastro from " +
                "(select rownum,datacadastro from debitoautomatico " +
                "where idligacao = '" + idLigacao + "' order by rownum desc) " +
                "where rownum = 1");
    }


    public DebitoAutomatico getDadosAutomatico() {
        DebitoAutomatico debitoAutomatico = new DebitoAutomatico();

        debitoAutomatico.setLigacaoDebitoAtivo(getLigacaoID(true));
        debitoAutomatico.setLigacaoDebitoInativo(getLigacaoID(false));

        for (int i = 0; i < getQtdMassasCadastro().getDebitoAutomatico(); i++) {
            debitoAutomatico.getCadastroDADebitoAtivo().add(i, getDataCadDA(
                    debitoAutomatico.getLigacaoDebitoAtivo().get(i)));
            debitoAutomatico.getCadastroDADebitoInativo().add(
                    i, getDataCadDA(debitoAutomatico.getLigacaoDebitoInativo().get(i)));
        }
        return debitoAutomatico;
    }
}
