package stepdefinition.Cadastro.Documento;

import cucumber.api.DataTable;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import framework.BrowserManager.BrowserActions;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.TextArea;
import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserVerifications.getElement;
import static framework.BrowserManager.BrowserVerifications.waitElementValue;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.Utils.getDataTable;
import static main.Utils.Utils.getResourcesPath;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.getXPathElementWithText;

public class StepsDocumento {

    @E("^preencher os seguintes campos do Cadastro de Documentos:$")
    public void preencherOsSeguintesCamposDoCadastroDeDocumentos(DataTable table) {
        Documento documentoBdd = getDataTable(table, Documento.class);
        // Documento documento = new Documento();
        //TIPO DOC
        Combos.setComboClicando("Tipo Doc.", documentoBdd.getTipoDoc());
        //MOTIVO
        Combos.setComboClicando("Motivo", documentoBdd.getMotivo());
        //OBSERVACAO
        TextArea.setTextArea("Obs", documentoBdd.getObs());


    }

    @E("^clicar em \"([^\"]*)\" e selecionar o arquivo \"([^\"]*)\"$")
    public void clicarEmESelecionarOArquivo(String buscar, String arquivo) {
        //CLICA BUSCAR
        clicarBotao(buscar);

        //SELECIONA O ARQUIVO
        getElement(By.xpath(".//input[@type='file']")).sendKeys(getResourcesPath() + arquivo);
        //verificarMensagemJanela("Envio de Arquivos", "Finalizado", false);
        //BrowserVerifications.wait(2);
        //CLICA OK
        clicarBotao(BTN_OK);
    }

    @E("^clicar em \"([^\"]*)\", selecionar o arquivo \"([^\"]*)\" e clicar em \"([^\"]*)\"$")
    public void clicarEmSelecionarOArquivoEClicarEm(String buscar, String arquivo, String botao) {
        //CLICA BUSCAR
        clicarBotao(buscar);

        //SELECIONA O ARQUIVO
        getElement(By.xpath(".//input[@type='file']")).sendKeys(getResourcesPath() + arquivo);

        //CLICA NO BOTAO
        waitElementValue(DIV, "Finalizado", DEFAULT_TIME_OUT);
        clicarBotao(botao);
    }

    @Entao("^em Documentos, clicar com o botao direito do mouse" +
            " no registro do Tipo \"([^\"]*)\" e clicar em \"([^\"]*)\"$")
    public void emDocumentosClicarComOBotaoDireitoDoMouseNoRegistroDoTipoEClicarEm(String tipoDocumento, String opcao) {
        clickOnElement(getXPathElementWithText(DIV, tipoDocumento));
        BrowserActions.selectOptionRightClickOnElement(opcao, getXPathElementWithText(DIV, tipoDocumento));
        if (opcao.contains("Remover")) {
            clicarBotao("Sim");
        }
    }
}
