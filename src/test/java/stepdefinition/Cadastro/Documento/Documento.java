package stepdefinition.Cadastro.Documento;


public class Documento {
    private String cidade;
    private String filtrarPor;
    private String pesquisar;
    private String tipoDoc;
    private String arquivo;
    private String motivo;
    private String obs;

    public Documento(String cidade, String filtrarPor, String pesquisar, String tipoDoc,
                     String arquivo, String motivo, String obs) {

        this.cidade = cidade;
        this.filtrarPor = filtrarPor;
        this.pesquisar = pesquisar;
        this.tipoDoc = tipoDoc;
        this.arquivo = arquivo;
        this.motivo = motivo;
        this.obs = obs;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getPesquisar() {
        return pesquisar;
    }

    public void setPesquisar(String pesquisar) {
        this.pesquisar = pesquisar;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getObs() {
        return obs;
    }
}




