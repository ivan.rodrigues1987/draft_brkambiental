package stepdefinition.Cadastro.CodigoLeitura;

import main.WebElementManager.Combos.ComboInputText;
import org.openqa.selenium.Keys;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.Xpath.LABEL;
import static main.WebElementManager.Xpath.getXPathElementWithText;

public class CodigoLeitura {
    //Codigo de leitura
    private String id;
    private String cidade;
    private String descricao;
    private String padrao;

    //Dados Medicao
    private String descricaoColetor;
    private String prevencaodeAcidentes = "";
    private String aceitaLeitura;
    private String leituraHMNaoConfere;
    private String emiteAvisoRetencao;
    private String repasseSimultanea;
    private String enviarColetor;
    private String alterarCategoria;
    private String repasse;
    private String estimada;
    private String alterarnumeroEconomias;

    //Dados Faturamento
    private String criteriosDeFaturamento = "";
    private String descricaoDF;
    private String criterioFaturamentoFixo;
    private String padraoRecalculoSimultanea;
    private String considerarParaMedia;
    private String imprimirAvisoImpedimentoleitura;


    private static final int TAMANHO_DESCRICAO = 5;
    private static final int TAMANHO_DESCRICAO_COLETOR = 5;
    private static final int TAMANHO_DESCRICAO_DF = 8;
    /**
     * <b>Construtor para utilizar os dados para codigo leitura</b>
     *
     * @param gerarDadosCodigoLeitura    <i>verifica se vai retornar valores o não</i>
     *
     */
    public CodigoLeitura(boolean gerarDadosCodigoLeitura) {
        if (gerarDadosCodigoLeitura) {
            this.descricao = "CODIGO " + geradorNumberRandom(TAMANHO_DESCRICAO);
            this.descricaoColetor = "COLETOR " + geradorCharRandom(TAMANHO_DESCRICAO_COLETOR);
            this.descricaoDF = "DF " + geradorCharRandom(TAMANHO_DESCRICAO_DF);
        }
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return id <i>id</i>
     */
    public String getId() {
        return id;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  id <i>id</i>
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cidade <i>cidade</i>
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cidade <i>cidade</i>
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return descricao <i>descricao</i>
     */
    public String getDescricao() {
        return descricao;
    }

    /*public void setDescricao(String descricao) {
        this.descricao = descricao;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return padrao <i>padrao</i>
     */
    public String getPadrao() {
        return padrao;
    }

    /*public void setPadrao(String padrao) {
        this.padrao = padrao;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return descricaoColetor <i>descricaoColetor</i>
     */
    public String getDescricaoColetor() {
        return descricaoColetor;
    }

    /*public void setDescricaoColetor(String descricaoColetor) {
        this.descricaoColetor = descricaoColetor;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return prevencaodeAcidentes <i>prevencaodeAcidentes</i>
     */
    public String getPrevencaodeAcidentes() {
        return prevencaodeAcidentes;
    }

    /*public void setPrevencaodeAcidentes(String prevencaodeAcidentes) {
        this.prevencaodeAcidentes = prevencaodeAcidentes;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return aceitaLeitura <i>aceitaLeitura</i>
     */
    public String getAceitaLeitura() {
        return aceitaLeitura;
    }

    /*public void setAceitaLeitura(String aceitaLeitura) {
        this.aceitaLeitura = aceitaLeitura;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return leituraHMNaoConfere <i>leituraHMNaoConfere</i>
     */
    public String getLeituraHMNaoConfere() {
        return leituraHMNaoConfere;
    }

    /*public void setLeituraHMNaoConfere(String leituraHMNaoConfere) {
        this.leituraHMNaoConfere = leituraHMNaoConfere;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return emiteAvisoRetencao <i>emiteAvisoRetencao</i>
     */
    public String getEmiteAvisoRetencao() {
        return emiteAvisoRetencao;
    }

    /*public void setEmiteAvisoRetencao(String emiteAvisoRetencao) {
        this.emiteAvisoRetencao = emiteAvisoRetencao;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return repasseSimultanea <i>repasseSimultanea</i>
     */
    public String getRepasseSimultanea() {
        return repasseSimultanea;
    }

    /*public void setRepasseSimultanea(String repasseSimultanea) {
        this.repasseSimultanea = repasseSimultanea;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return enviarColetor <i>enviarColetor</i>
     */
    public String getEnviarColetor() {
        return enviarColetor;
    }

    /*public void setEnviarColetor(String enviarColetor) {
        this.enviarColetor = enviarColetor;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return alterarCategoria <i>alterarCategoria</i>
     */
    public String getAlterarCategoria() {
        return alterarCategoria;
    }

    /*public void setAlterarCategoria(String alterarCategoria) {
        this.alterarCategoria = alterarCategoria;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return repasse <i>repasse</i>
     */
    public String getRepasse() {
        return repasse;
    }

    /*public void setRepasse(String repasse) {
        this.repasse = repasse;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return estimada <i>estimada</i>
     */
    public String getEstimada() {
        return estimada;
    }

    /*public void setEstimada(String estimada) {
        this.estimada = estimada;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return alterarnumeroEconomias <i>alterarnumeroEconomias</i>
     */
    public String getAlterarnumeroEconomias() {
        return alterarnumeroEconomias;
    }

    /*public void setAlterarnumeroEconomias(String alterarnumeroEconomias) {
        this.alterarnumeroEconomias = alterarnumeroEconomias;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return criteriosDeFaturamento <i>criteriosDeFaturamento</i>
     */
    public String getCriteriosDeFaturamento() {
        return criteriosDeFaturamento;
    }

    /*public void setCriteriosDeFaturamento(String criteriosDeFaturamento) {
        this.criteriosDeFaturamento = criteriosDeFaturamento;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return descricaoDF <i>descricaoDF</i>
     */
    public String getDescricaoDF() {
        return descricaoDF;
    }

    /*public void setDescricaoDF(String descricaoDF) {
        this.descricaoDF = descricaoDF;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return criterioFaturamentoFixo <i>criterioFaturamentoFixo</i>
     */
    public String getCriterioFaturamentoFixo() {
        return criterioFaturamentoFixo;
    }

    /*public void setCriterioFaturamentoFixo(String criterioFaturamentoFixo) {
        this.criterioFaturamentoFixo = criterioFaturamentoFixo;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return padraoRecalculoSimultanea <i>padraoRecalculoSimultanea</i>
     */
    public String getPadraoRecalculoSimultanea() {
        return padraoRecalculoSimultanea;
    }

    /*public void setPadraoRecalculoSimultanea(String padraoRecalculoSimultanea) {
        this.padraoRecalculoSimultanea = padraoRecalculoSimultanea;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return considerarParaMedia <i>considerarParaMedia</i>
     */
    public String getConsiderarParaMedia() {
        return considerarParaMedia;
    }

    /*public void setConsiderarParaMedia(String considerarParaMedia) {
        this.considerarParaMedia = considerarParaMedia;
    }*/

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return imprimirAvisoImpedimentoleitura <i>imprimirAvisoImpedimentoleitura</i>
     */
    public String getImprimirAvisoImpedimentoleitura() {
        return imprimirAvisoImpedimentoleitura;
    }

    /*public void setImprimirAvisoImpedimentoleitura(String imprimirAvisoImpedimentoleitura) {
        this.imprimirAvisoImpedimentoleitura = imprimirAvisoImpedimentoleitura;
    }*/

    /**
     * <b>Preenche a cidade ou limpa o campo de combo</b>
     *
     * @param cidadeDinamica <i>cidadeDinamica</i>
     */
    public void preencherCidade(String cidadeDinamica) {
        if (this.cidade.isEmpty()) {
            //Limpa combo
            clearField(ComboInputText.getXPathTextoCombo("Cidade"));
            sendKeys(Keys.TAB);
            //Clica na combo
            clickOnElement(ComboInputText.getXPathTextoCombo("Cidade"));
            //Clica fora da combo
            clickOnElement(getXPathElementWithText(LABEL, "Código"));
        } else {
            setComboCidade("Cidade", getCampo(this.cidade, cidadeDinamica));
        }
    }
}
