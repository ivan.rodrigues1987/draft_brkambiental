package stepdefinition.Cadastro.CodigoLeitura;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;
import runner.Dados.Cadastro.Endereco;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;

import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

public class StepsCodigoLeitura {

    private Endereco endereco;
    private CodigoLeitura codigoLeitura;
    private Object clickOnElemente;

    public StepsCodigoLeitura() {
        endereco = getDadosCadastro().getEndereco();
    }

    @Quando("^eu preencher os seguintes campos em Codigo de Leitura:$")
    public void euPreencherOsSeguintesCamposEmCodigoDeLeitura(DataTable dataTable) {
        CodigoLeitura codigoLeituraBDD = getDataTable(dataTable, CodigoLeitura.class);
        codigoLeitura = new CodigoLeitura(true);

        //CIDADE
        codigoLeituraBDD.preencherCidade(endereco.getCidade());

        //DESCRICAO
        InputText.setInputText("Descrição", getCampo(codigoLeituraBDD.getDescricao(), codigoLeitura.getDescricao()));

        //PADRAO SIM/NAO
        clicarCheckBox("Padrão", codigoLeituraBDD.getPadrao());
    }

    @E("^em dados medicao preencher os seguintes campos$")
    public void emDadosMedicaoPreencherOsSeguintesCampos(DataTable dataTable) {
        CodigoLeitura codigoLeituraBDD = getDataTable(dataTable, CodigoLeitura.class);

        //DESCRICAO COLETOR
        InputText.setInputText(
                "Descrição Coletor", getCampo(codigoLeituraBDD.getDescricaoColetor(),
                        codigoLeitura.getDescricaoColetor()));

        //PREVENCAO DE ACIDENTENS
        Combos.setComboClicando(
                "Prevenção de Acidentes", getCampo(codigoLeituraBDD.getPrevencaodeAcidentes(),
                        codigoLeitura.getPrevencaodeAcidentes()));

        //PREENCHE AS CHECK BOX
        //Aceita Leitura
        clicarCheckBox("Aceita Leitura", codigoLeituraBDD.getAceitaLeitura());
        clicarCheckBox("Leitura HM Não Confere", codigoLeituraBDD.getLeituraHMNaoConfere());
        clicarCheckBox("Emite Aviso Retenção", codigoLeituraBDD.getEmiteAvisoRetencao());
        clicarCheckBox("Repasse Simultânea", codigoLeituraBDD.getRepasseSimultanea());
        clicarCheckBox("Enviar Coletor", codigoLeituraBDD.getEnviarColetor());
        clicarCheckBox("Alterar Categoria", codigoLeituraBDD.getAlterarCategoria());
        clicarCheckBox("Repasse", codigoLeituraBDD.getRepasse());
        clicarCheckBox("Estimada", codigoLeituraBDD.getEstimada());
        clicarCheckBox("Alterar Número Economias", codigoLeituraBDD.getAlterarnumeroEconomias());
    }

    @E("^em dados faturamento preencher os seguintes campos$")
    public void emDadosFaturamentoPreencherOsSeguintesCampos(DataTable dataTable) {
        CodigoLeitura codigoleituraBDD = getDataTable(dataTable, CodigoLeitura.class);

        //PREENCHE CRITERIOS DE FATURAMENTO
        Combos.setComboClicando(
                "Critérios de Faturamento", getCampo(codigoleituraBDD.getCriteriosDeFaturamento(),
                        codigoLeitura.getCriteriosDeFaturamento()), true);

        //PREENCHE DESCRICAO "DADOS FATURAMENTO"
        InputText.setInputText(2, "Descrição", getCampo(codigoleituraBDD.getDescricaoDF(),
                codigoLeitura.getDescricaoDF()));

        //CHECK BOX "DADOS FATURAMENTO"
        clicarCheckBox("Critério Faturamento Fixo", codigoleituraBDD.getCriterioFaturamentoFixo());
        clicarCheckBox("Padrão Recálculo Simultânea", codigoleituraBDD.getPadraoRecalculoSimultanea());
        clicarCheckBox("Considerar Para Média", codigoleituraBDD.getConsiderarParaMedia());
        clicarCheckBox("Imprimir Aviso Impedimento Leitura", codigoleituraBDD.getImprimirAvisoImpedimentoleitura());
    }
}