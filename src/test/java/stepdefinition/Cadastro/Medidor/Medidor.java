package stepdefinition.Cadastro.Medidor;


/**
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */
public class Medidor {
    private String codigo;
    private String numeroMedidor;

    public Medidor() {

    }

    public Medidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    /*public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }*/
}
