package stepdefinition.Cadastro.ConsolidacaoFatura;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import java.util.ArrayList;

import static main.DBManager.DBCadastro.getQtdMassasCadastro;


/**
 * @author Thiago Tolentino P. dos Santos
 * @since 12/03/2019
 */
public class DBConsolidacaoFatura extends DBActions {

    /**
     * <b>Conexão ao banco de dados</b>
     *
     * @param  environment            <i>environment</i>
     *
     */
    public DBConsolidacaoFatura(String environment) {
        super(environment);
    }

    /**
     * <b>Conexão ao banco de dados</b>
     *
     * @param  dataBase            <i>dataBase</i>
     *
     */
    public DBConsolidacaoFatura(DataBase dataBase) {
        super(dataBase);
    }

    /**
     * <b>Retorna ligacao ID</b>
     *
     * @return  getColumnValues            <i>getColumnValues</i>
     *
     */
    public ArrayList retornarLigacaoID() {
        return getColumnValues("id",
                "select id from (select id from ligacao\n" +
                        "minus \n" +
                        "select idLigacao from consolidacaoArrecadacaoDet) where id not in('0')",
                getQtdMassasCadastro().getConsolidacaoFatura());
    }

    /**
     * <b>Retorna ligacao ID</b>
     *
     * @return  consolidacaoFatura  <i>consolidacaoFatura</i>
     *
     */
    public ConsolidacaoFatura getDadosConsolidacaoFatura() {
        ConsolidacaoFatura consolidacaoFatura = new ConsolidacaoFatura(true);
        consolidacaoFatura.setIdLigacao(retornarLigacaoID());


        return consolidacaoFatura;
    }
}
