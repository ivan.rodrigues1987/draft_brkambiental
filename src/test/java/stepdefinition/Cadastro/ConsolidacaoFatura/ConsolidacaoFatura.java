package stepdefinition.Cadastro.ConsolidacaoFatura;

import main.WebElementManager.Combos.ComboInputText;
import org.openqa.selenium.By;
import runner.Dados.Cadastro.Endereco;

import java.util.ArrayList;

import static framework.BrowserManager.BrowserActions.setText;
import static main.Utils.Utils.getCnpj;
import static main.Utils.Utils.getCpf;
import static main.Utils.Utils.isBlank;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;


/**
 * @author Renan Ferreira dos Santos
 * @since 21/02/2019
 */
public class ConsolidacaoFatura {
    //Atributos Globais
    private static int indiceLigacaoCF = 1;
    private static boolean primeiraExecucao = true;
    //ParametrosExecucao
    private ArrayList<String> idLigacao = new ArrayList<>();
    private String nome;
    private String cnpj;
    private String cpf;
    private String inscricaoEstadual;
    private String diadeVencimento;
    private String enviarSefaz;
    private Endereco endereco;

    /**
     * <b>Retorna se está vazio o não</b>
     *
     * @param inicioVazio <i>inicioVazio</i>
     */
    public ConsolidacaoFatura(boolean inicioVazio) {
        if (!inicioVazio) {
            setEndereco();
            setCPF();
            setCNPJ();
            setIdLigacao();
        } //if
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return indiceLigacaoCF <i>indiceLigacaoCF</i>
     */
    public static int getIndiceLigacaoCF() {
        return indiceLigacaoCF;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  indiceLigacaoCF <i>indiceLigacaoCF</i>
     */
    public static void setIndiceLigacaoCF(int indiceLigacaoCF) {
        ConsolidacaoFatura.indiceLigacaoCF = indiceLigacaoCF;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return primeiraExecucao <i>primeiraExecucao</i>
     */
    public static boolean isPrimeiraExecucao() {
        return primeiraExecucao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  primeiraExecucao <i>primeiraExecucao</i>
     */
    public static void setPrimeiraExecucao(boolean primeiraExecucao) {
        ConsolidacaoFatura.primeiraExecucao = primeiraExecucao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return idLigacao <i>idLigacao</i>
     */
    public ArrayList<String> getIdLigacao() {
        return idLigacao;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  idLigacao <i>idLigacao</i>
     */
    public void setIdLigacao(ArrayList<String> idLigacao) {
        this.idLigacao = idLigacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return nome <i>nome</i>
     */
    public String getNome() {
        return nome;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  nome <i>nome</i>
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cnpj <i>cnpj</i>
     */
    public String obterCnpj() {
        return cnpj;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cnpj <i>cnpj</i>
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cpf <i>cpf</i>
     */
    public String obterCpf() {
        return cpf;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cpf <i>cpf</i>
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return inscricaoEstadual <i>inscricaoEstadual</i>
     */
    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  inscricaoEstadual <i>inscricaoEstadual</i>
     */
    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return diadeVencimento <i>diadeVencimento</i>
     */
    public String getDiadeVencimento() {
        return diadeVencimento;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  diadeVencimento <i>diadeVencimento</i>
     */
    public void setDiadeVencimento(String diadeVencimento) {
        this.diadeVencimento = diadeVencimento;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return enviarSefaz <i>enviarSefaz</i>
     */
    public String getEnviarSefaz() {
        return enviarSefaz;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  enviarSefaz <i>enviarSefaz</i>
     */
    public void setEnviarSefaz(String enviarSefaz) {
        this.enviarSefaz = enviarSefaz;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return endereco <i>endereco</i>
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  endereco <i>endereco</i>
     */
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     */
    private void setCNPJ() {
        this.cnpj = getCnpj(false);
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     */
    private void setCPF() {
        this.cpf = getCpf(false);
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     */
    private void setEndereco() {
        this.endereco = getDadosCadastro().getEndereco();
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     */
    private void setIdLigacao() {
        this.idLigacao = getDadosCadastro().getConsolidacaoFatura().idLigacao;
    }

    /**
     * <b>metodo que retorna o endereço de consolidaçao fatura</b>
     *
     * @param  campo <i>campo</i>
     */
    public By getXPathConsolidacaoFatura(String campo) {
        switch (campo.toUpperCase()) {
            case ("CIDADE"):
                return ComboInputText.getXPathTextoCombo(campo);
            default:
                return getInputTextXPath(campo);
        } //switch
    } // getXPathConsolidacaoFatura

    /**
     * <b>preenche codigo CF</b>
     *
     * @param  campo <i>campo</i>
     * @param  texto <i>texto</i>
     */
    public void preencherCampoCF(String campo, String texto) {
        switch (campo.toUpperCase()) {
            case ("ENVIAR SEFAZ"):
                clicarCheckBox(campo, getAtributoCF(campo));
                break;

            default:
                if (!isBlank(texto)) {
                    setText(getXPathConsolidacaoFatura(campo), texto);
                }
        } //switch
    } //preencherCampoCF

    /**
     * <b>retorna o valor da variavel CF</b>
     *
     * @return this <i>this</i>
     */
    public String getAtributoCF(String campo) {
        switch (campo.toUpperCase()) {
            case ("ENVIARSEAFAZ"):
                return (this.enviarSefaz);
            default:
                if (campo.toUpperCase().contains("LIGAÇÃO")) {
                    return this.idLigacao.get(indiceLigacaoCF);
                }
        } //switch
        return "";
    }

    /**
     * <b>retorna o valor da variavel CF</b>
     *
     */
    public static void setIndiceCF() {
        indiceLigacaoCF++;
    }
}
