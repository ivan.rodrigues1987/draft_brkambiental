package stepdefinition.Cadastro.ConsolidacaoFatura;

import cucumber.api.DataTable;
import cucumber.api.java.it.E;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.Combos;
import runner.Dados.Cadastro.Endereco;

import static framework.BrowserManager.BrowserActions.clearField;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.Auxiliar.getCampoValidado;
import static main.Utils.ComumScreens.Auxiliar.fecharTela;
import static main.Utils.ComumScreens.ConsultaPersonalizada.LupasConsulta.LUPA_COMUM;
import static main.Utils.ComumScreens.ConsultaPersonalizada.evocarPesquisaPersonalizada;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Combos.ComboInputText.getXPathTextoCombo;
import static main.WebElementManager.Combos.Combos.setComboCidade;


/**
 * <b>Classe para implementar os Steps de Consolidacao de Fatura</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 21/02/2019
 */
public class StepsConsolidacaoFatura {

    private ConsolidacaoFatura consolidacaoFatura = new ConsolidacaoFatura(false);

    @Dado("^a tela Cadastro de Consolidacao de Faturas esta conforme os" +
            " seguintes parametros da entidade \"([^\"]*)\":$")
    public void aTelaCadastroDeConsolidacaoDeFaturasEstaConformeOsSeguintesParametrosDaEntidade(
            String entidade, DataTable dataTable) {
        //PARAMETROS QUE SAO EXIBIDOS NO RELATORIO FINAL
    }

    @Quando("^eu preencher em cidade \"([^\"]*)\" e em status \"([^\"]*)\"$")
    public void euPreencherEmCidadeEEmStatus(String cidade, String status) {
        //CIDADE
        setComboCidade("Cidade", getCampo(cidade, consolidacaoFatura.getEndereco().getCidade()));

        //STATUS
        Combos.setComboClicando("Status", status);
    }

    @Quando("^eu preencher em cidade \"([^\"]*)\"$")
    public void euPreencherEmCidade(String cidade) {
        //CIDADE
        setComboCidade("Cidade", getCampo(cidade, consolidacaoFatura.getEndereco().getCidade()));
    } //euPreencherEmCidade

    @E("^preencher em todas as linhas do Grid de parametros da Consolidacao de Fatura$")
    public void preencherEmTodasAsLinhasDoGridDeParametrosDebitoAutomatico(DataTable table) {
        ConsolidacaoFatura aux;
        aux = getDataTable(table, ConsolidacaoFatura.class);
        aux.setIdLigacao(consolidacaoFatura.getIdLigacao());
        aux.getIdLigacao().add(0, getCampoValidado(aux.getIdLigacao().get(0), consolidacaoFatura.getIdLigacao().
                get(ConsolidacaoFatura.getIndiceLigacaoCF())));
        aux.setEndereco(consolidacaoFatura.getEndereco());

        //VERIFICA SE UTILIZA CPF OU CNPJ
        if (aux.obterCnpj().equals("") && aux.obterCpf().equals("")) {
            consolidacaoFatura = aux;
        } else if (aux.obterCnpj().equals("")) {
            aux.setCpf(consolidacaoFatura.obterCpf());
            consolidacaoFatura = aux;
            consolidacaoFatura.preencherCampoCF("C.P.F.", " " + consolidacaoFatura.obterCpf());
        } else {
            aux.setCnpj(consolidacaoFatura.obterCnpj());
            consolidacaoFatura = aux;
            consolidacaoFatura.preencherCampoCF("C.N.P.J.", " " + consolidacaoFatura.obterCnpj());
        } //else

        //PREENCHE NA TELA OS RESPECTIVOS CAMPOS
        consolidacaoFatura.preencherCampoCF("Nome", consolidacaoFatura.getNome());
        consolidacaoFatura.preencherCampoCF("Inscrição Estadual", consolidacaoFatura.getInscricaoEstadual());
        consolidacaoFatura.preencherCampoCF("Dia de vencimento", consolidacaoFatura.getDiadeVencimento());
        consolidacaoFatura.preencherCampoCF("Enviar SeFaz", consolidacaoFatura.getEnviarSefaz());

    } //preencherEmTodasAsLinhasDoGridDeParametrosDebitoAutomatico

    @E("^em \"([^\"]*)\" Pesquisar por \"([^\"]*)\" em Consolidacao de Fatura$")
    public void emConsultaPersonalizadaPesquisar(String campo, String texto) {
        evocarPesquisaPersonalizada(campo, LUPA_COMUM, getCampo(texto,
                consolidacaoFatura.getAtributoCF(campo)), BTN_OK);
        ConsolidacaoFatura.setPrimeiraExecucao(false);
    } //emConsultaPersonalizadaPesquisar

    @E("^em Endereco preencher os seguintes campos$")
    public void preencherEnderecoTable(DataTable table) {
        Endereco aux = getDataTable(table, Endereco.class);

        //PREENCHE NA TELA OS RESPECTIVOS CAMPOS

        consolidacaoFatura.preencherCampoCF("Logradouro", aux.getLogradouro());
        consolidacaoFatura.preencherCampoCF("Número", aux.getNumero());
        consolidacaoFatura.preencherCampoCF("Complemento", aux.getComplemento());

        consolidacaoFatura.preencherCampoCF("Bairro", aux.getBairro());
        consolidacaoFatura.preencherCampoCF("Quadra", aux.getQuadra());
        consolidacaoFatura.preencherCampoCF("Lote", aux.getLote());

        consolidacaoFatura.preencherCampoCF("Município", aux.getMunicipio());
        consolidacaoFatura.preencherCampoCF("Estado", aux.getEstado());

        consolidacaoFatura.preencherCampoCF("CEP", aux.getCep());
    } //preencherEnderecoTable

    @Entao("^buscar uma nova ligacao para Consolidacao Fatura$")
    public void pedirNovaLigacao() {
        if (!ConsolidacaoFatura.isPrimeiraExecucao()) {
            ConsolidacaoFatura.setIndiceCF();
        }
    } //buscar uma nova ligacao para Consolidacao Fatura

    @E("^preencher a combo \"([^\"]*)\" com \"([^\"]*)\"$")
    public void preencheCombo(String campo, String texto) {
        Combos.setComboClicando(campo, texto);
    }

    @Entao("^limpar a combo cidade na Consolidacao Fatura")
    public void limparCampoCombo() {
        clearField(getXPathTextoCombo("Cidade"));
        fecharTela(2);
    } //limpar a combo


} //StepsConsolidacaoFatura
