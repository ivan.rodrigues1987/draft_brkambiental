package stepdefinition.Cadastro.CadastroMedidor;

import stepdefinition.GenericSteps.ComumSteps;
import cucumber.api.DataTable;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import main.WebElementManager.Combos.ComboInputText;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.InputText.InputText.setInputText;


/**
 * @author Thiago Tolentino P. dos Santos
 * @since 20/03/2019
 */
public class StepsCadastroMedidor {

    private CadastroMedidor cadastroMedidor = new CadastroMedidor();

    @Entao("^preencher os seguintes campos em Cadastro de Medidor$")
    public void preencherEmTodasAsLinhasDoGridDeParametrosCadastroMedidor(DataTable table) {
        CadastroMedidor auxCadMedidor = getDataTable(table, CadastroMedidor.class);
        auxCadMedidor.setNumeroDoMedidor(getCampo(auxCadMedidor.getNumeroDoMedidor(),
                cadastroMedidor.getNumeroDoMedidor()));
        auxCadMedidor.setCidade(getCampo(auxCadMedidor.getCidade(), cadastroMedidor.getCidade()));
        cadastroMedidor = auxCadMedidor;

        //NUMERO DO MEDIDOR
        setInputText("Número do Medidor", cadastroMedidor.getNumeroDoMedidor());

        //MODELO
        ComboInputText.setComboEPressionarEnter("Modelo", cadastroMedidor.getModelo());

        //DATA AQUISICAO
        ComboInputText.setComboEPressionarEnter("Data Aquisição", cadastroMedidor.getDataAquisicao());

        //ANO FABRICACAO
        setInputText("Ano de Fabricação", cadastroMedidor.getAnoFabricacao());

        //SITUACAO
        ComboInputText.setComboEPressionarEnter("Situação", cadastroMedidor.getSituacao());

        //CIDADE
        ComboInputText.setComboEPressionarEnter("Cidade", cadastroMedidor.getCidade());

        //FORMA AQUISICAO
        ComboInputText.setComboEPressionarEnter("Forma Aquisição", cadastroMedidor.getFormaAquisicao());

        //INSTALADO
        clicarCheckBox(" Instalado", cadastroMedidor.getInstalado());

        //EMPRESA
        ComboInputText.setComboEPressionarEnter("Empresa", cadastroMedidor.getEmpresa());
    }

    @E("^alterar o campo \"([^\"]*)\" para \"([^\"]*)\" em Cadastro de Medidor$")
    public void alterarOCampoParaEmCadastroDeMedidor(String campo, String valor)   {
        valor = getCampo(valor, cadastroMedidor.getAtributoCadMedidor(campo));
        new ComumSteps().alterarOCampoPara(campo, valor);
    }
}
