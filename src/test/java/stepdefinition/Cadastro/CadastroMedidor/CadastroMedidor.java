package stepdefinition.Cadastro.CadastroMedidor;

import static main.Utils.Utils.geradorNumberRandom;
import static main.Utils.Utils.isBlank;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Thiago Tolentino P. dos Santos
 * @since 20/03/2019
 */
public class CadastroMedidor {
    private String codigo;
    private String numeroDoMedidor;
    private String modelo;
    private String dataAquisicao;
    private String anoFabricacao;
    private String situacao;
    private String cidade;
    private String formaAquisicao;
    private String instalado;
    private String empresa;
    private static final int TAMANHO = 5;


    /**
     * <b>Construtor</b>
     *
     */
    CadastroMedidor() {
        this.numeroDoMedidor = getNumeroDoMedidor();
        this.cidade = getDadosCadastro().getEndereco().getCidade();
    } //CadastroMedidor

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return codigo <i>codigo</i>
     */
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNumeroDoMedidor(String numeroDoMedidor) {
        this.numeroDoMedidor = numeroDoMedidor;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return modelo <i>modelo</i>
     */
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return dataAquisicao <i>dataAquisicao</i>
     */
    public String getDataAquisicao() {
        return dataAquisicao;
    }

    public void setDataAquisicao(String dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return anoFabricacao <i>anoFabricacao</i>
     */
    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return situacao <i>situacao</i>
     */
    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cidade <i>cidade</i>
     */
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return formaAquisicao <i>formaAquisicao</i>
     */
    public String getFormaAquisicao() {
        return formaAquisicao;
    }

    public void setFormaAquisicao(String formaAquisicao) {
        this.formaAquisicao = formaAquisicao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return instalado <i>instalado</i>
     */
    public String getInstalado() {
        return instalado;
    }

    public void setInstalado(String instalado) {
        this.instalado = instalado;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return empresa <i>empresa</i>
     */
    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return geradorNumberRandom <i>geradorNumberRandom</i>
     */
    public String getNumeroDoMedidor() {
        return geradorNumberRandom(TAMANHO);
    } //getNumeroDoMedidor

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return numeroDoMedidor <i>numeroDoMedidor</i>
     */
    public String getAtributoCadMedidor(String atributo) {
        switch (atributo.toLowerCase()) {
            case ("codigo"):
                return this.codigo;
            case ("número do medidor"):
                if (isBlank(this.numeroDoMedidor)) {
                    this.numeroDoMedidor = getNumeroDoMedidor();
                }
                return this.numeroDoMedidor;
            case ("modelo"):
                return this.modelo;
            case ("data aquisição"):
                return this.dataAquisicao;
            case ("ano de fabricação"):
                return this.anoFabricacao;
            case ("situação"):
                return this.situacao;
            case ("cidade"):
                return this.cidade;
            case ("forma aquisição"):
                return this.formaAquisicao;
            case ("instalado"):
                return this.instalado;
            case ("empresa"):
                return this.empresa;
            default:
                return null;
        }
        //return null;
    }
} //CadastroMedidor
