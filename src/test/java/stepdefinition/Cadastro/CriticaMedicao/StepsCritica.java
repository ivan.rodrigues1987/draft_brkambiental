package stepdefinition.Cadastro.CriticaMedicao;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import main.WebElementManager.Combos.ComboInputText;
import runner.Dados.Cadastro.Endereco;

import java.sql.SQLException;

import static stepdefinition.Cadastro.CriticaMedicao.CriticaMedicao.clicarTextArea;
import static framework.BrowserManager.BrowserActions.clearField;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.preencherComboCidadePesquisaRapida;
import static main.Utils.ComumScreens.PesquisaRapida.pesquisarSimpleBotao;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * @author Juan Castillo
 * @since 21/03/2019
 */
public class StepsCritica {

    private CriticaMedicao critica;
    private Endereco endereco;
    private static final int COLUNA = 3;

    public StepsCritica() {
        endereco = getDadosCadastro().getEndereco();
    }


    @Quando("^eu preencher os seguintes campos de cadastro de critica de medicao$")
    public void euPreencherOsSeguintesCamposDeCadastroDeCriticaDeMedicao(DataTable table) throws SQLException {

        CriticaMedicao criticaBDD = getDataTable(table, CriticaMedicao.class);
        critica = new CriticaMedicao(true, criticaBDD.getCodigocoletor(), criticaBDD.getVolumede());

        //CIDADE
        if (criticaBDD.getCidade().isEmpty()) {
            clearField(ComboInputText.getXPathTextoCombo("Cidade"));
        }

        setComboCidade("Cidade", criticaBDD.getCidade());

        //Sigla
        setInputText("Sigla", getCampo(criticaBDD.getSigla(), critica.getSigla()));

        //Descrição
        setInputText("Descrição", getCampo(criticaBDD.getDescricao(), critica.getDescricao()));

        //Código Coletor
        setInputText("Código Coletor", getCampo(criticaBDD.getCodigocoletor(), critica.getCodigocoletor()));

        //Ativo
        clicarCheckBox("Ativo", getCampo(criticaBDD.getAtivo(), critica.getAtivo()));

        //Volume De
        setInputText("Volume De", getCampo(criticaBDD.getVolumede(), critica.getVolumede()));

        //Volume Até
        setInputText("Volume Até", getCampo(criticaBDD.getVolumeate(), critica.getVolumeate()));

        //Percentual Variação
        setInputText("Percentual Variação", getCampo(criticaBDD.getPercentualvariacao(),
                critica.getPercentualvariacao()));

        //Percentual Ate
        setInputText("Percentual Até", getCampo(criticaBDD.getPercentualate(), critica.getPercentualate()));

        //Consumo Zero
        clicarCheckBox("Consumo Zero", getCampo(criticaBDD.getConsumozero(), critica.getConsumozero()));

        //Leitura Menor Anterior
        clicarCheckBox("Leitura Menor Anterior", getCampo(criticaBDD.getLeituramenor(), critica.getLeituramenor()));

        //Inversão HM
        clicarCheckBox("Inversão HM", getCampo(criticaBDD.getInversaohm(), critica.getInversaohm()));

        //Limite Inferior
        clicarCheckBox("Limite Inferior", getCampo(criticaBDD.getLimiteinferior(), critica.getLimiteinferior()));

        //Ligação Nova
        clicarCheckBox("Ligação Nova", getCampo(criticaBDD.getLigacaonova(), critica.getLigacaonova()));

        //Leitura Não Efetuada
        clicarCheckBox("Leitura Não Efetuada", getCampo(criticaBDD.getLeituranaoefetuada(),
                critica.getLeituranaoefetuada()));

        //Resíduo Inconsistente
        clicarCheckBox("Resíduo Inconsistente", getCampo(criticaBDD.getResiduo(), critica.getResiduo()));

        //Análise Crítica
        clicarCheckBox("Análise Crítica", getCampo(criticaBDD.getAnalisecritica(), critica.getAnalisecritica()));

        //Confirmar Leitura
        clicarCheckBox("Confirmar Leitura", getCampo(criticaBDD.getConfirmarleitura(), critica.getConfirmarleitura()));

        //Repasse
        clicarCheckBox("Repasse", getCampo(criticaBDD.getRepasse(), critica.getRepasse()));

        //Exibir
        clicarCheckBox("Exibir", getCampo(criticaBDD.getExibir(), critica.getExibir()));

        //Repasse Simultânea
        clicarCheckBox("Repasse Simultânea", getCampo(criticaBDD.getRepassesimultanea(),
                critica.getRepassesimultanea()));

        //Variação de Consumo
        clicarCheckBox("Variação de Consumo", getCampo(criticaBDD.getVariacaodeconsumo(),
                critica.getVariacaodeconsumo()));

        //Mensagem Aviso Retenção
        if (!criticaBDD.getAvisoretencao().isEmpty()) {
            clicarTextArea(1, "textarea");
            setInputTextEPressionarEnter(getCampo(criticaBDD.getAvisoretencao(), critica.getAvisoretencao()));
        }


        //Mensagem dispositivo % variação consumo
        if (!criticaBDD.getMensagemdispositivo().isEmpty()) {
            clicarTextArea(COLUNA, "input");
            setInputTextEPressionarEnter(getCampo(criticaBDD.getMensagemdispositivo(),
                    critica.getMensagemdispositivo()));
        }


    }

    @E("^na tela de Pesquisa tem que preencher na cidade \"([^\"]*)\", o campo \"([^\"]*)\" " +
            "e o valor \"([^\"]*)\" para Cadastro de Critica$")
    public void naTelaDePesquisaTemQuePreencherNaCidadeOCampoEOValorParaCadastroDeCritica(
            String cidade, String campo, String valor) throws Throwable {

        preencherComboCidadePesquisaRapida("Cidade", 2, getCampo(
                cidade, getDadosCadastro().getEndereco().getCidade()));
        pesquisarSimpleBotao(campo, getCampo(valor, getCampoPesquisado(valor)));

    }
}
