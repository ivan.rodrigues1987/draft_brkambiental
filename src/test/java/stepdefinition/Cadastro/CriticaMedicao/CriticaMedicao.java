package stepdefinition.Cadastro.CriticaMedicao;

import org.openqa.selenium.By;

import java.sql.SQLException;

import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * @author Juan Castillo
 * @since 21/03/2019
 */
public class CriticaMedicao {

    private String cidade;
    private String sigla;
    private String descricao;
    private String codigocoletor;
    private String ativo;

    private String volumede;
    private String volumeate;
    private String percentualvariacao;
    private String percentualate;
    private String consumozero;
    private String leituramenor;
    private String inversaohm;
    private String limiteinferior;
    private String ligacaonova;
    private String leituranaoefetuada;
    private String residuo;

    private String analisecritica;
    private String confirmarleitura;
    private String repasse;
    private String exibir;
    private String repassesimultanea;
    private String variacaodeconsumo;

    private String avisoretencao;
    private String mensagemdispositivo;

    private static final int TAMANHO_CODIGO_COLETOR = 5;
    private static final int TAMANHO_VOLUMEDE = 4;
    private static final int TAMANHO_VOLUMEATE = 4;
    private static final int TAMANHO_PERCENTUAL_VARIACAO = 1;
    private static final int TAMANHO_PERCENTUAL_ATE = 2;

    /**
     * <b>construtor vazio</b>
     *
     */
    public CriticaMedicao() {

    }

    /**
     * <b>metodo para fazer chamado geral</b>
     *
     * @param  gerarDadosCritica <i>gerarDadosCritica</i>
     * @param  codigocoletor <i>codigocoletor</i>
     * @param  volumede <i>volumede</i>
     */
    public CriticaMedicao(boolean gerarDadosCritica, String codigocoletor, String volumede) throws SQLException {
        if (gerarDadosCritica) {
            gerarDadosCritica(codigocoletor, volumede);
        }
    }


    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigocoletor() {
        return codigocoletor;
    }

    public void setCodigocoletor(String codigocoletor) {
        this.codigocoletor = codigocoletor;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getVolumede() {
        return volumede;
    }

    public void setVolumede(String volumede) {
        this.volumede = volumede;
    }

    public String getVolumeate() {
        return volumeate;
    }

    public void setVolumeate(String volumeate) {
        this.volumeate = volumeate;
    }

    public String getPercentualvariacao() {
        return percentualvariacao;
    }

    public void setPercentualvariacao(String percentualvariacao) {
        this.percentualvariacao = percentualvariacao;
    }

    public String getPercentualate() {
        return percentualate;
    }

    public void setPercentualate(String percentualate) {
        this.percentualate = percentualate;
    }

    public String getConsumozero() {
        return consumozero;
    }

    public void setConsumozero(String consumozero) {
        this.consumozero = consumozero;
    }

    public String getLeituramenor() {
        return leituramenor;
    }

    public void setLeituramenor(String leituramenor) {
        this.leituramenor = leituramenor;
    }

    public String getInversaohm() {
        return inversaohm;
    }

    public void setInversaohm(String inversaohm) {
        this.inversaohm = inversaohm;
    }

    public String getLimiteinferior() {
        return limiteinferior;
    }

    public void setLimiteinferior(String limiteinferior) {
        this.limiteinferior = limiteinferior;
    }

    public String getLigacaonova() {
        return ligacaonova;
    }

    public void setLigacaonova(String ligacaonova) {
        this.ligacaonova = ligacaonova;
    }

    public String getLeituranaoefetuada() {
        return leituranaoefetuada;
    }

    public void setLeituranaoefetuada(String leituranaoefetuada) {
        this.leituranaoefetuada = leituranaoefetuada;
    }

    public String getResiduo() {
        return residuo;
    }

    public void setResiduo(String residuo) {
        this.residuo = residuo;
    }

    public String getAnalisecritica() {
        return analisecritica;
    }

    public void setAnalisecritica(String analisecritica) {
        this.analisecritica = analisecritica;
    }

    public String getConfirmarleitura() {
        return confirmarleitura;
    }

    public void setConfirmarleitura(String confirmarleitura) {
        this.confirmarleitura = confirmarleitura;
    }

    public String getRepasse() {
        return repasse;
    }

    public void setRepasse(String repasse) {
        this.repasse = repasse;
    }

    public String getExibir() {
        return exibir;
    }

    public void setExibir(String exibir) {
        this.exibir = exibir;
    }

    public String getRepassesimultanea() {
        return repassesimultanea;
    }

    public void setRepassesimultanea(String repassesimultanea) {
        this.repassesimultanea = repassesimultanea;
    }

    public String getVariacaodeconsumo() {
        return variacaodeconsumo;
    }

    public void setVariacaodeconsumo(String variacaodeconsumo) {
        this.variacaodeconsumo = variacaodeconsumo;
    }

    public String getAvisoretencao() {
        return avisoretencao;
    }

    public void setAvisoretencao(String avisoretencao) {
        this.avisoretencao = avisoretencao;
    }

    public String getMensagemdispositivo() {
        return mensagemdispositivo;
    }

    public void setMensagemdispositivo(String mensagemdispositivo) {
        this.mensagemdispositivo = mensagemdispositivo;
    }

    /**
     * <b>metodo para fazer chamado geral</b>
     *
     * @param  codigoColetorParametro <i>codigocoletor</i>
     * @param  volumedeParametro <i>volumede</i>
     */
    private void gerarDadosCritica(String codigoColetorParametro, String volumedeParametro)throws SQLException {
        this.codigocoletor = geradorNumberRandom(TAMANHO_CODIGO_COLETOR);
        this.volumede = geradorNumberRandom(TAMANHO_VOLUMEDE);
        this.volumeate = geradorNumberRandom(TAMANHO_VOLUMEATE);
        this.percentualvariacao = geradorNumberRandom(TAMANHO_PERCENTUAL_VARIACAO);
        this.percentualate = geradorNumberRandom(TAMANHO_PERCENTUAL_ATE);

    }

    /**
     * <b>metodo para clicar text area</b>
     *
     * @param  coluna <i>coluna</i>
     * @param  tipo <i>tipo</i>
     */
    public static void clicarTextArea(int coluna, String tipo) {
        clickOnElement(By.xpath("//fieldset[" + coluna + "]/div[1]/div[1]/div[1]/" + tipo + "[1]"));
    }



}
