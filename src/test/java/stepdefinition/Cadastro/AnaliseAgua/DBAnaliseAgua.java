package stepdefinition.Cadastro.AnaliseAgua;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;



public class DBAnaliseAgua extends DBActions {

    /**
     * <b>metodo para conectar com banco de dados</b>
     *
     * @param  environment                    <i>variavel de conexão</i>
     */
    public DBAnaliseAgua(String environment) {
        super(environment);
    }

    /**
     * <b>metodo para conectar com banco de dados</b>
     *
     * @param  dataBase                    <i>nome do database</i>
     */
    public DBAnaliseAgua(DataBase dataBase) {
        super(dataBase);
    }

    /**
     * <b>metodo que obter o FonteAbastecimento</b>
     *
     * @param  cidade <i>cidade a consultar</i>
     */
    public AnaliseAgua getDadosAnaliseAgua(String cidade) {
        AnaliseAgua analiseAgua = new AnaliseAgua();
        String fonteAbastecimento = getColumnInfo("FonteAbastecimento",
                "SELECT fonte.FonteAbastecimento FROM FonteAbastecimento fonte " +
                        "INNER JOIN Cidade cid on fonte.idCidade = cid.id " +
                        "WHERE cid.Cidade = '" + cidade + "'");

        analiseAgua.setCidade(cidade);
        analiseAgua.setFonteDeAbastecimento(fonteAbastecimento);

        return analiseAgua;
    }
}
