package stepdefinition.Cadastro.AnaliseAgua;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import main.WebElementManager.Combos.Combos;

import java.text.ParseException;

import static framework.BrowserManager.BrowserVerifications.waitElementToBeEnable;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;
import static stepdefinition.Cadastro.Cronograma.Cronograma.getPeriodoBDD;

import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.getDataTable;

import static main.WebElementManager.Combos.Combos.setComboCidade;

import static main.WebElementManager.Combos.Combos.setComboClicando;

/**
 * <b>Classe para implementar os Steps de Analise Agua</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public class StepsAnaliseAgua {
    private AnaliseAgua analiseAgua;

    public StepsAnaliseAgua() {
        analiseAgua = getDadosCadastro().getAnaliseAgua();
    }

    @E("^preencher os seguintes campos em Analise de Agua:$")
    public void preencherOsSeguintesCamposEmAnaliseDeAgua(DataTable table) throws ParseException {
        AnaliseAgua analiseDaAguaBDD = getDataTable(table, AnaliseAgua.class);

        //CIDADE
        setComboCidade("Cidade", getCampo(analiseDaAguaBDD.getCidade(), analiseAgua.getCidade()));

        //PERIODO
        waitElementToBeEnable("Período", DEFAULT_TIME_OUT);
        setComboClicando("Período", getCampo(analiseDaAguaBDD.getPeriodo(),
                getPeriodoBDD(analiseDaAguaBDD.getPeriodo())));

        //FONTE DE ABASTECIMENTO
        Combos.setComboClicando("Fonte de Abastecimento", getCampo(analiseDaAguaBDD.getFonteDeAbastecimento(),
                analiseAgua.getFonteDeAbastecimento()));

        //PERIODO DE REFERENCIA
        if (!analiseDaAguaBDD.getPeriodoRef().equals("")) {
            waitElementToBeEnable("Período Ref.", DEFAULT_TIME_OUT);
            setComboClicando("Período Ref.", getCampo(analiseDaAguaBDD.getPeriodoRef(),
                    getPeriodoBDD(analiseDaAguaBDD.getPeriodoRef())));
        }

    }

    @E("^preencher em todas as linhas do Grid de parametros$")
    public void preencherEmTodasAsLinhasDoGridDeParametros(DataTable table) {
        AnaliseAgua analiseAguaG = getDataTable(table, AnaliseAgua.class);
        //PREENCHE PARAMETROS_EXECUCAO
        analiseAguaG.preencherParametros();
    }
}
