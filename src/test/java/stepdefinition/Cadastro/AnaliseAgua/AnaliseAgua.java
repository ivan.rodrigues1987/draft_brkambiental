package stepdefinition.Cadastro.AnaliseAgua;


import main.WebElementManager.InputText.InputText;
import org.openqa.selenium.By;
import org.testng.Assert;


import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.ValMedio;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.ValMin;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.ValMax;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.AmostrasRealizadas;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.AmostrasForaPad;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.TotalAmostras;
import static stepdefinition.Cadastro.AnaliseAgua.AnaliseAgua.ColunaTabelaAnaliseAgua.PercAmostras;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserVerifications.elementExists;


public class AnaliseAgua {

    private static final int MIN_LINHA = 10;
    //Filtros
    private String cidade;
    private String periodo;
    private String fonteDeAbastecimento;
    private String periodoRef;
    //ParametrosExecucao
    private String valMedio;
    private String valMinDetectado;
    private String valMaxDetectado;
    private String amostrasRealizadas;
    private String amostraForaPadrao;
    private String totaldeAmostras;
    private String percAmostrasConfAposRec;

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return cidade <i>cidade</i>
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  cidade <i>cidade</i>
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return periodo <i>periodo</i>
     */
    public String getPeriodo() {
        return periodo;
    }


    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return fonteDeAbastecimento <i>fonteDeAbastecimento</i>
     */
    public String getFonteDeAbastecimento() {
        return fonteDeAbastecimento;
    }

    /**
     * <b>alteral o valor da variavel numero pelo que foi passado pelo parametro</b>
     *
     * @param  fonteDeAbastecimento <i>cidade</i>
     */
    public void setFonteDeAbastecimento(String fonteDeAbastecimento) {
        this.fonteDeAbastecimento = fonteDeAbastecimento;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return periodoRef <i>periodoRef</i>
     */
    public String getPeriodoRef() {
        return periodoRef;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return valMedio <i>valMedio</i>
     */
    public String getValMedio() {
        return valMedio;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return valMinDetectado <i>valMinDetectado</i>
     */
    public String getValMinDetectado() {
        return valMinDetectado;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return valMaxDetectado <i>valMaxDetectado</i>
     */
    public String getValMaxDetectado() {
        return valMaxDetectado;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return amostrasRealizadas <i>amostrasRealizadas</i>
     */
    public String getAmostrasRealizadas() {
        return amostrasRealizadas;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return amostraForaPadrao <i>amostraForaPadrao</i>
     */
    public String getAmostraForaPadrao() {
        return amostraForaPadrao;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return totaldeAmostras <i>totaldeAmostras</i>
     */
    public String getTotaldeAmostras() {
        return totaldeAmostras;
    }

    /**
     * <b>retorna o valor da variavel</b>
     *
     * @return percAmostrasConfAposRec <i>percAmostrasConfAposRec</i>
     */
    public String getPercAmostrasConfAposRec() {
        return percAmostrasConfAposRec;
    }

    /**
     * <b>metodo onde almacena as variavels numericas</b>
     *
     */
    public enum ColunaTabelaAnaliseAgua {
        ValMedio(3), ValMin(4), ValMax(5), AmostrasRealizadas(6), AmostrasForaPad(7), TotalAmostras(8), PercAmostras(9);

        private int valor;

        ColunaTabelaAnaliseAgua(int i) {
            valor = i;
        }

        public int getValor() {
            return valor;
        }
    }

    /**
     * <b>metodo para preencher dados no Grid</b>
     *
     * @param  linha    <i>numero linha</i>
     * @param  coluna   <i>numero coluna</i>
     * @param  valor    <i>valor</i>
     */
    public void preencherGrid(int linha, int coluna, String valor) {
        if (!valor.isEmpty()) {
            clickOnElement(By.xpath("(//div/div/div/table/tbody[2]/tr[" + linha + "]/td[" + coluna + "]/div)[1]"));
            InputText.setInputTextEPressionarEnter(valor);
        }
    }

    /**
     * <b>metodo para preencher dados no Grid</b>
     *
     * @param  analiseAgua    <i>classe AnaliseAgua</i>
     */
    public void preencherParametros(AnaliseAgua analiseAgua) {
        int qtdLinhasGrid = retornarQtdLinhasGrid();
        for (int linha = 1; linha < qtdLinhasGrid; linha++) {
            preencherGrid(linha, ValMedio.getValor(), analiseAgua.getValMedio());
            preencherGrid(linha, ValMin.getValor(), analiseAgua.getValMinDetectado());
            preencherGrid(linha, ValMax.getValor(), analiseAgua.getValMaxDetectado());
            preencherGrid(linha, AmostrasRealizadas.getValor(), analiseAgua.getAmostrasRealizadas());
            preencherGrid(linha, AmostrasForaPad.getValor(), analiseAgua.getAmostraForaPadrao());
            verificarTotalAmostras(linha, TotalAmostras.getValor(), Integer.parseInt(analiseAgua.getTotaldeAmostras()));
            preencherGrid(linha, PercAmostras.getValor(), analiseAgua.getPercAmostrasConfAposRec());
        }
    }

    /**
     * <b>metodo para preencher dados no Grid</b>
     *
     */
    public void preencherParametros() {
        preencherParametros(this);
    }

    /**
     * <b>retorna quantidade de linhas</b>
     *
     * @return linha <i>linha</i>
     */
    private int retornarQtdLinhasGrid() {
        int linha = MIN_LINHA, limiteTentativas = 2;
        while (linha < limiteTentativas) {
            if (!elementExists(By.xpath("(//div/div/div/table/tbody[2]/tr[" + linha + "]/td[1]/div)[1]"))) {
                return linha;
            }
            linha++;
        }
        return linha;
    }

    /**
     * <b>metodo para verificar total de colunas</b>
     *
     * @param  linha                    <i>numero de linha</i>
     * @param  coluna                   <i>numero de coluna</i>
     * @param  totalAmostrasEsperadas    <i>totalAmostrasEsperadas</i>
     */
    private void verificarTotalAmostras(int linha, int coluna, int totalAmostrasEsperadas) {
        int totalAmostras = Integer.parseInt(getText(By.xpath(
                "(//div/div/div/table/tbody[2]/tr[" + linha + "]/td[" + coluna + "]/div)[1]")));
        Assert.assertEquals(totalAmostras, totalAmostrasEsperadas);
    }
}
