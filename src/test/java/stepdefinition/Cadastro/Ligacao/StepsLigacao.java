package stepdefinition.Cadastro.Ligacao;

import stepdefinition.Cadastro.Cliente.Cliente;
import stepdefinition.Cadastro.InstalacaoMedidor.StepsInstalacaoMedidor;
import stepdefinition.Cadastro.Ligacao.Abas.Complemento.Complemento;
import stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais.Categoria;
import stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais.DadosComerciais;
import stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais.LigacaoConsolidadora;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens.Identificacao;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens.Instalacao;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens.RoteirizacaoEntrega;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens.RoteirizacaoLeitura;

import stepdefinition.GenericSteps.PesquisaSteps;
import cucumber.api.DataTable;


import cucumber.api.java.it.E;

import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserActions;
import framework.BrowserManager.BrowserVerifications;
import main.Utils.ComumScreens.Auxiliar;
import main.Utils.ComumScreens.ConsultaPersonalizada;

import main.WebElementManager.Combos.ComboInputText;
import main.WebElementManager.InputText.InputText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import runner.Dados.Cadastro.Endereco;

import java.sql.SQLException;
import java.util.List;

import static stepdefinition.Cadastro.Cliente.Cliente.PESSOA_FISICA;
import static stepdefinition.Cadastro.InstalacaoMedidor.InstalacaoMedidor.removerMedidor;
import static stepdefinition.Cadastro.Ligacao.Abas.Complemento.Complemento.validarCamposInquilino;
import static stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais.LigacaoConsolidadora.pesquisarLigacaoAConsolidar;
import static stepdefinition.Cadastro.Ligacao.Ligacao.clicarComboOpcoes;
import static stepdefinition.Cadastro.Ligacao.Ligacao.fecharTelaLigacao;
import static stepdefinition.Cadastro.Ligacao.Ligacao.clicarMedidor;
import static stepdefinition.Cadastro.Ligacao.Ligacao.getLigacaoCadastrada;
import static stepdefinition.Cadastro.Ligacao.Ligacao.clicarSalvarInstalacaoMedidor;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.Auxiliar.validarComboAlterada;
import static main.Utils.Utils.getDataTable;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.ComboInputText.getXPathTextoCombo;
import static main.WebElementManager.Combos.ComboInputText.setCombo;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.Combos.Combos.setTextoCombo;
import static main.WebElementManager.InputText.InputText.setInputTextEPressionarEnter;

import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;

/**
 * <b>Classe para implementar os Steps de Ligacao</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class StepsLigacao {
    //Lista para armazenar categorias preenchidas
    private static List<Categoria> categorias;

    //Classes
    private Bairro bairro;
    private Logradouro logradouro;
    private Endereco endereco;
    private Grupo grupo;
    private SubCategoria subCategoria;

    public StepsLigacao() {
        endereco = getDadosCadastro().getEndereco();
    }

    @E("^na aba Localizacao, preencher o Endereco de Instalacao:$")
    public void naAbaLocalizacaoPreencherOEnderecoDeInstalacao(DataTable dataTable) {
        Endereco enderecoBDD = getDataTable(dataTable, Endereco.class);
        Endereco enderecoCompleto = getDadosCadastro().getEndereco();
        ConsultaPersonalizada consultar;

        //CIDADE
        BrowserVerifications.waitElementToBeClickable(getXPathTextoCombo("Cidade"), 2);
        setComboCidade("Cidade", getCampo(enderecoBDD.getCidade(), enderecoCompleto.getCidade()));

        //DISTRITO
        setTextoCombo("Distrito", getCampo(enderecoBDD.getDistrito(), enderecoCompleto.getDistrito()), false);

        //BAIRRO
        consultar = new ConsultaPersonalizada("Bairro", ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
        consultar.efetuarPesquisaPersonalizada(getCampo(enderecoBDD.getBairro(), enderecoCompleto.getBairro()), BTN_OK);

        //LOGRADOURO
        consultar = new ConsultaPersonalizada("Logradouro", ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
        consultar.efetuarPesquisaPersonalizada(getCampo(enderecoBDD.getLogradouro(),
                enderecoCompleto.getLogradouro().trim()), BTN_OK);

        //COMPLEMENTO
        InputText.setInputText("Complemento", getCampo(enderecoBDD.getComplemento(),
                enderecoCompleto.getComplemento()));

        //CEP
        InputText.setInputTextEPressionarEnter("CEP", getCampo("  " + enderecoBDD.getCep(),
                "  " + enderecoCompleto.getCep()));

        //NUMERO
        InputText.setInputText("Número", getCampo(enderecoBDD.getNumero(), enderecoCompleto.getNumero()));
    }

    @E("^na aba Localizacao, em \"([^\"]*)\" preencher os seguintes campos:$")
    public void anaAbaLocalizacaoEmPreencherOsSeguintesCampos(String itemTela, DataTable dataTable) {
        //PREENCHE OS CAMPOS DA ABA LOCALIZACAO
        switch (itemTela) {
            case "Instalacao":
                getDataTable(dataTable, Instalacao.class).preencherCampos();
                break;
            case "Identificacao":
                getDataTable(dataTable, Identificacao.class).preencherCampos();
                break;
            case "Roteirizacao de Leitura":
                getDataTable(dataTable, RoteirizacaoLeitura.class).preencherCampos();
                break;
            default:
                break;
        }
    }

    @E("^na aba Localizacao, em Tipo Entrega selecionar \"([^\"]*)\"$")
    public void naAbaLocalizacaoEmTipoEntregaSelecionar(String tipoEntrega) {
        //BrowserVerifications.waitElementToBeEnable("Tipo Entrega",DEFAULT_TIME_OUT);
        //setComboClicando("Tipo Entrega", tipoEntrega);
        setCombo("Tipo Entrega", tipoEntrega, true);
        sendKeys(Keys.ENTER);
        RoteirizacaoEntrega.setTipoEntrega(tipoEntrega);
    }

    @E("^validar o preenchimento automatico dos demais campos de Roteirizacao de Entrega:$")
    public void validarOPreenchimentoAutomaticoDosDemaisCamposDeRoteirizacaoDeEntrega(DataTable dataTable) {
        Endereco enderecoBDD = getDataTable(dataTable, Endereco.class);
        Endereco enderecoParametro = getDadosCadastro().getEndereco();
        //VALIDA PREENCHIMENTO AUTOMATICO DOS CAMPOS DE ROTEIRIZACAO DE ENTREGA
        RoteirizacaoEntrega.validarPreenchimentoAutomatico(enderecoBDD, enderecoParametro);
    }

    @E("^na aba \"([^\"]*)\" preencher os seguintes campos:$")
    public void naAbaDadosComerciaisPreencherOsSeguintesCampos(String aba, DataTable dataTable) {
        //CLICA NA ABA DADOS COMERCIAIS
        clicarAba(aba);

        //PREENCHE OS CAMPOS DA ABA DADOS COMERCIAIS
        getDataTable(dataTable, DadosComerciais.class).preencherCampos();
    }

    @E("^na aba Dados Comerciais, em Categoria preencher os seguintes campos:$")
    public void naAbaDadosComerciaisEmCategoriaPreencherOsSeguintesCampos(DataTable dataTable) {
        categorias = dataTable.asList(Categoria.class);
        //PREENCHE AS LINHAS DO GRID DE CATEGORIA
        for (int i = 0; i < categorias.size(); i++) {
            int a = i + 1;
            categorias.get(i).preencherCampos(i + 1);
        }
    }

    @E("^validar que os seguintes campos de Categoria foram preenchidos corretamente:$")
    public void validarQueOsSeguintesCamposDeCategoriaForamPreenchidosCorretamente(DataTable dataTable) {

        //VALIDA O PREENCHIMENTO DOS CAMPOS DE CATEGORIA
        for (int i = 0; i < categorias.size(); i++) {
            int a = i + 1;
            categorias.get(i).validarCampos(i + 1);
        }
    }

    @E("^na aba \"([^\"]*)\" preencher o campo Tipo de Ligacao com \"([^\"]*)\"$")
    public void naAbaPreencherOCampoTipoDeLigacaoCom(String aba, String tipoLigacao) {
        //CLICA NA ABA MEDICAO
        clicarAba(aba);
        //TIPO DE LIGACAO
        setComboClicando("Tipo de Ligação", tipoLigacao);
    }

    @E("^na aba \"([^\"]*)\", no campo \"([^\"]*)\" clicar em pesquisar e" +
            " filtrar por \"([^\"]*)\" e pesquisar \"([^\"]*)\"$")
    public void naAbaNoCampoClicarEmPesquisarEFiltrarPorEPesquisar(String aba, String campo, String filtrar,
                                                                   String valor)throws SQLException {
        //CLICA NA ABA COMPLEMENTO
        clicarAba(aba);

        //SELECIONA INQUILINO
        if (!valor.isEmpty()) {
            new PesquisaSteps().noCampoClicarEmPesquisarEFiltrarPorEPesquisar(campo, filtrar, valor);
        }
    }

    @E("^na aba Complemento preencher os seguintes campos:$")
    public void naAbaComplementoPreencherOsSeguintesCampos(DataTable dataTable) {
        //PREENCHE OS CAMPOS NA ABA COMPLEMENTO
        getDataTable(dataTable, Complemento.class).preencherCampos();
    }

    @E("^na aba Complemento preencher os seguintes campos de Inquilino:$")
    public void naAbaComplementoPreencherOsSeguintesCamposDeInquilino(DataTable dataTable) throws SQLException {
        Cliente clienteBDD = getDataTable(dataTable, Cliente.class);

        //VERIFICA SE OS CAMPOS DE INQUILINO FORAM PASSADOS PARA SEREM PREENCHIDOS MANUALMENTE
        if (!clienteBDD.getNome().isEmpty()) {
            Cliente cliente = new Cliente(true, (clienteBDD.getTipoCliente()),
                    clienteBDD.obterCpf(), clienteBDD.obterCnpj());

            //NOME
            InputText.setInputText("Nome", getCampo(clienteBDD.getNome(), cliente.getNome()));

            //TELEFONE
            InputText.setInputText("Telefone", getCampo(clienteBDD.getTelefoneFixo(), cliente.getTelefoneFixo()));

            //TELEFONE MOVEL
            InputText.setInputText(2, "Telefone Movel", getCampo(clienteBDD.getTelefoneMovel(),
                    cliente.getTelefoneMovel()));

            //E-MAIL
            InputText.setInputText(2, "E-mail", getCampo(clienteBDD.getEmail(), cliente.getEmail()));

            if (clienteBDD.getTipoCliente().equals(PESSOA_FISICA)) {
                //DATA NASCIMENTO
                ComboInputText.setCombo("Data Nascimento", getCampo(clienteBDD.getDataNascimento(),
                        cliente.getDataNascimento()));

                //RG
                InputText.setInputText(2, "R.G.", getCampo(clienteBDD.getRg(), cliente.getRg()));

                //DATA EXPEDICAO RG
                ComboInputText.setCombo("Data Exp. RG", getCampo(clienteBDD.getDataExpRg(), cliente.getDataExpRg()));

                //CPF
                InputText.setInputText(2, "C.P.F.", getCampo(clienteBDD.obterCpf(), cliente.obterCpf()));
            } else {
                //CNPJ
                InputText.setInputText(2, "C.N.P.J.", getCampo(clienteBDD.obterCnpj(), cliente.obterCnpj()));
            }
        }
    }

    @E("^na aba \"([^\"]*)\" validar que os campos de Inquilino foram preenchidos com os mesmos dados do cliente:$")
    public void naAbaValidarQueOsCamposDeInquilinoForamPreenchidosComOsMesmosDadosDoCliente(String aba,
                                                                                            DataTable dataTable) {
        Cliente cliente = getDataTable(dataTable, Cliente.class);

        //CLICA NA ABA COMPLEMENTO
        clicarAba(aba);

        //VALIDA SE OS CAMPOS ESTAO IGUAIS AOS DO CLIENTE
        validarCamposInquilino(cliente);
    }

    @E("^clicar no botao Medidor e preencher os seguintes campos:$")
    public void clicarNoBotaoMedidorEPreencherOsSeguintesCampos(DataTable dataTable) {
        //CLICA NO BOTAO MEDIDOR
        clicarMedidor();

        //PREENCHE OS CAMPOS DE INSTALACAO DE MEDIDOR
        new StepsInstalacaoMedidor().preencherOsSeguintesCamposEmInstalacaoDeMedidor(dataTable);
    }

    @E("^clicar no botao \"([^\"]*)\" em Instalacao de Medidor$")
    public void clicarNoBotaoEmInstalacaoDeMedidor(String botao) {
        switch (botao.toUpperCase()) {
            case "SALVAR":
                clicarSalvarInstalacaoMedidor();
                break;
            case "REMOVER MEDIDOR":
                removerMedidor(botao);
                break;
            default:
                break;
        }
        //CLICA EM SALVAR NA TELA DE INSTALACAO DE MEDIDOR

    }

    @Quando("^aba \"([^\"]*)\" clicar no botao \"([^\"]*)\"$")
    public void abaClicarNoBotao(String aba, String botao) {
        //CLICA NA ABA DADOS COMERCIAIS
        clicarAba(aba);

        //CLICA BOTAO
        clicarBotao(botao);
    }

    @E("^na tela de DA preencher o campo ligacao com \"([^\"]*)\"$")
    public void naTelaDeDAPreencherOCampoLigacaoCom(String ligacao) {
        final int qtdDivs = 4;
        //PREENCHE O CAMPO LIGACAO
        setInputTextEPressionarEnter("Ligação", getLigacaoCadastrada(ligacao), qtdDivs);
    }

    @E("^na aba Dados Comerciais, em Ligacao Consolidadora preencher os seguintes campos:$")
    public void naAbaDadosComerciaisEmLigacaoConsolidadoraPreencherOsSeguintesCampos(DataTable dataTable) {
        //PREENCHE CAMPOS DE LIGACAO CONSOLIDADORA
        getDataTable(dataTable, LigacaoConsolidadora.class).preencherCampos();
    }

    @E("^na aba Dados Comerciais, em Ligacao a Consolidar, pesquisar a ligacao \"([^\"]*)\"$")
    public void naAbaDadosComerciaisEmLigacaoAConsolidarPesquisarALigacao(String ligacao) {
        //PESQUISA LIGACAO A CONSOLIDAR
        pesquisarLigacaoAConsolidar(ligacao);
    }

    @E("^na aba \"([^\"]*)\", validar que a combo \"([^\"]*)\" foi alterada para \"([^\"]*)\"$")
    public void naAbaMedicaoValidarQueAComboFoiAlteradaPara(String aba, String combo, String valorEsperado) {
        //CLICA NA ABA MEDICAO
        clicarAba(aba);

        //VALIDA QUE A COMBO FOI ALTERADA
        if (combo.equals("Tipo Cobrança")) {
            validarComboAlterada(combo, valorEsperado, false);
        } else {
            validarComboAlterada(combo, valorEsperado, true);
        }
    }

    @E("^na aba Medicao clicar no botao Medidor$")
    public void naAbaMedicaoClicarNoBotaoMedidor() {
        //FECHA TELA DE MENSAGEM DO STEP ANTERIOR
        Auxiliar.fecharTela("Mensagem do Sistema");

        //CLICA NA ABA MEDICAO
        clicarAba("Medição");

        //CLICA BOTAO MEDIDOR
        clicarMedidor();
    }

    @E("^fechar a tela de Cadastro de Ligacao$")
    public void fecharATelaDeCadastroDeLigacao() {
        fecharTelaLigacao();
    }

    @Quando("^preencher os seguintes campos e salvar na tela de Cadastro de bairro$")
    public void preencherOsSeguintesCamposESalvarNaTelaDeCadastroDeBairro(DataTable dataTable) throws SQLException {

        Bairro bairroBDD = getDataTable(dataTable, Bairro.class);
        bairro = new Bairro();

        //CIDADE
        setComboCidade("Cidade", getCampo(bairroBDD.getCidade(), endereco.getCidade()));

        //DISTRITO
        setTextoCombo("Distrito", getCampo(bairroBDD.getDistrito(), endereco.getDistrito()), false);

        //NOME
        InputText.setInputTextEPressionarEnter("Nome", getCampo(bairroBDD.getNome(), bairro.getNome()));
        if (!bairro.getNome().isEmpty()) {
            Ligacao.setNomeBairro(bairro.getNome());
        } else {
            Ligacao.setNomeBairro(bairro.getNome());
        }
    }

    @E("^prencher os seguintes campos no cadastro de Logradouro$")
    public void prencherOsSeguintesCamposNoCadastroDeLogradouro(DataTable dataTable) throws SQLException {


        Logradouro logradouroBDD = getDataTable(dataTable, Logradouro.class);
        logradouro = new Logradouro();

        ConsultaPersonalizada consultar;

        //CIDADE
        setComboCidade("Cidade", getCampo(logradouroBDD.getCidade(), endereco.getCidade()));

        //TIPO
        InputText.setInputTextEPressionarEnter("Tipo", getCampo(logradouroBDD.getTipo(), logradouro.getTipo()));

        //Titulo
        InputText.setInputTextEPressionarEnter("Titulo", getCampo(logradouroBDD.getTitulo(), logradouro.getTitulo()));

        //Nome
        InputText.setInputTextEPressionarEnter("Nome", getCampo(logradouroBDD.getNome(), logradouro.getNome()));
        if (!logradouro.getNome().isEmpty()) {
            Ligacao.setNomeLogradouro(logradouro.getNome());
        } else {
            Ligacao.setNomeLogradouro(logradouroBDD.getNome());
        }

        //Inicio
        consultar = new ConsultaPersonalizada("Início", ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
        consultar.efetuarPesquisaPersonalizada(
                getCampo(logradouroBDD.getInicio(), endereco.getLogradouro().trim()), BTN_OK);

        //Fin
        consultar = new ConsultaPersonalizada("Fin.", ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
        consultar.efetuarPesquisaPersonalizada(
                getCampo(logradouroBDD.getFin(), endereco.getLogradouro().trim()), BTN_OK);

        //Referencia
        InputText.setInputTextEPressionarEnter("Referência", getCampo(logradouroBDD.getReferencia(),
                logradouro.getReferencia()));

        //BAIRRO
        consultar = new ConsultaPersonalizada("Bairro", ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
        consultar.efetuarPesquisaPersonalizada(getCampo(logradouroBDD.getBairro(), Ligacao.getNomeBairro()), BTN_OK);


    }

    @E("^prencher os seguintes campos no cadastro de Grupo$")
    public void prencherOsSeguintesCamposNoCadastroDeGrupo(DataTable dataTable) throws SQLException {

        Grupo grupoBDD = getDataTable(dataTable, Grupo.class);
        grupo = new Grupo();

        ConsultaPersonalizada consultar;

        //CIDADE
        setComboCidade("Cidade", getCampo(grupoBDD.getCidade(), endereco.getCidade()));

        //GRUPO
        InputText.setInputTextEPressionarEnter("Grupo", getCampo(grupoBDD.getGrupo(), grupo.getGrupo()));
        if (!grupo.getGrupo().isEmpty()) {
            Ligacao.setNomeGrupo(grupo.getGrupo());
        } else {
            Ligacao.setNomeGrupo(grupoBDD.getGrupo());
        }

        //ORDEM
        InputText.setInputTextEPressionarEnter("Ordem", getCampo(grupoBDD.getOrdem(), grupo.getOrdem()));

        //VENC
        setCombo("Venc. Padrão", getCampo(grupoBDD.getVencPadrao(), grupo.getVencPadrao()));

        //Tipo Leitura
        setTextoCombo("Tipo Leitura", getCampo(grupoBDD.getTipoLeitura(), grupo.getTipoLeitura()), true);

        //Exige Crítica
        clicarCheckBox("Exige Crítica", getCampo(grupoBDD.getExigeCritica(), grupo.getExigeCritica()));

        //Reaviso de Débito
        clicarCheckBox("Reaviso de Débito", getCampo(grupoBDD.getReavisodeDebito(), grupo.getReavisodeDebito()));

        //Grupo de Ligação Estimada
        clicarCheckBox("Grupo de Ligação Estimada", getCampo(grupoBDD.getGrupodeLigacaoEstimada(),
                grupo.getGrupodeLigacaoEstimada()));

        //Corte Light
        clicarCheckBox("Corte Light", getCampo(grupoBDD.getCorteLight(), grupo.getCorteLight()));

        //Fiscalização de Corte
        clicarCheckBox("Fiscalização de Corte", getCampo(
                grupoBDD.getFiscalizacaodeCorte(), grupo.getFiscalizacaodeCorte()));

        //Estimadas
        clicarCheckBox("Estimadas", getCampo(grupoBDD.getEstimadas(), grupo.getEstimadas()));

        //Ret. do Coletor Automático
        clicarCheckBox("Ret. do Coletor Automático", getCampo(grupoBDD.getRetdoColetorAutomatico(),
                grupo.getRetdoColetorAutomatico()));

        //Cal. de Consumo Automático
        clicarCheckBox("Cal. de Consumo Automático", getCampo(grupoBDD.getCaldeConsumoAutomatico(),
                grupo.getCaldeConsumoAutomatico()));

        //Cal. de Faturamento Automático
        clicarCheckBox("Cal. de Faturamento Automático", getCampo(grupoBDD.getCaldeFaturamentoAutomatico(),
                grupo.getCaldeFaturamentoAutomatico()));

        //Mensagem Canhoto Esgoto
        InputText.setInputText("Mensagem Canhoto Esgoto", getCampo(grupoBDD.getMensagemCanhotoEsgoto(),
                grupo.getMensagemCanhotoEsgoto()));


    }

    @E("^em Vencimentos Alternativos selecionar o dia do Vencimento Alternativo \"([^\"]*)\"$")
    public void emVencimentosAlternativosSelecionarODiaDoVencimentoAlternativo(String valor) throws Throwable {

        //multiopcios
        clicarCheckBox(valor, "S");

    }

    @cucumber.api.java.pt.E("^prencher os seguintes campos no cadastro de SubCategoria$")
    public void prencherOsSeguintesCamposNoCadastroDeSubCategoria(DataTable dataTable) throws SQLException {

        SubCategoria subCategoriaBDD = getDataTable(dataTable, SubCategoria.class);
        subCategoria = new SubCategoria();

        //CIDADE
        setComboCidade("Cidade", getCampo(subCategoriaBDD.getCidade(), endereco.getCidade()));

        //Imovel desabitado
        clicarCheckBox("Imovel desabitado", getCampo(subCategoriaBDD.getImovelDesabitado(),
                subCategoria.getImovelDesabitado()));

        //Baixa Renda
        clicarCheckBox("Baixa Renda", getCampo(subCategoriaBDD.getBaixaRenda(), subCategoria.getBaixaRenda()));

        //Possui Esgoto Dif.
        clicarCheckBox("Possui Esgoto Dif.", getCampo(subCategoriaBDD.getPossuiEsgotoDif(),
                subCategoria.getPossuiEsgotoDif()));

        //Aplicar Desconto
        clicarCheckBox("Aplicar Desconto", getCampo(subCategoriaBDD.getAplicarDesconto(),
                subCategoria.getAplicarDesconto()));

        //Sigla
        InputText.setInputText("Sigla", getCampo(subCategoriaBDD.getSigla(), subCategoria.getSigla()));

        //Descrição
        InputText.setInputText("Descrição", getCampo(subCategoriaBDD.getDescricao(),
                subCategoria.getDescricao()));
        if (!subCategoria.getDescricao().isEmpty()) {
            Ligacao.setNomeSubCategoria(subCategoria.getDescricao());
        } else {
            Ligacao.setNomeSubCategoria(subCategoriaBDD.getDescricao());
        }

        //Unidade de Mensuração
        setTextoCombo("Unidade de Mensuração", getCampo(subCategoriaBDD.getUnidadeDeMensuracao(),
                subCategoria.getUnidadeDeMensuracao()), true);

        // Base de Cálculo
        InputText.setInputText("Base de Cálculo", getCampo(subCategoriaBDD.getBaseDeCalculo(),
                subCategoria.getBaseDeCalculo()));
    }

    @E("^na tela de Cadastro de SubCategoria clicar o botao direito do mouse clicar em \"([^\"]*)\"$")
    public void naTelaDeCadastroDeSubCategoriaClicarOBotaoDireitoDoMouseClicarEm(String botao) throws Throwable {

        switch (botao) {
            case "Adicionar Categoria":
                BrowserActions.selectOptionRightClickOnElement(botao, By.xpath("(//div[@class='GKPSPJYCIUB'])[2]"));
                break;
            default:
                break;
        }

    }

    @cucumber.api.java.pt.E("^adicionar os valores \"([^\"]*)\" para Cadastro de Grupo em Ligacaoes$")
    public void adicionarOsValoresParaCadastroDeGrupoEmLigacaoes(String valor) throws Throwable {
        clicarComboOpcoes(valor);
    }
}
