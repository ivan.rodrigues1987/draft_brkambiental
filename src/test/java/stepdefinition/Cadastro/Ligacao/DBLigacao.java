package stepdefinition.Cadastro.Ligacao;

import framework.DataBase.DataBase;
import main.DBManager.DBActions;

import static org.jsoup.helper.StringUtil.isBlank;

/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class DBLigacao extends DBActions {
    private static final int DEBITO_ATIVO = 4;

    public DBLigacao(String environment) {
        super(environment);
    }

    public DBLigacao(DataBase dataBase) {
        super(dataBase);
    }

    public Ligacao getDadosLigacao(String cidade) {
        DataBase dataBase = getDbOracle();

        Ligacao ligacao = new Ligacao();
        ligacao.setIdLigacao(getLigacaoCidade(cidade, ""));
        ligacao.setIdLigacaoClienteDebito(getLigacaoDebito(cidade, true));
        return ligacao;
    }

    //Retorna um id de uma ligacao cadastrada para a cidade
    public String getLigacaoCidade(String cidade, String sWhereAdicional) {
        String sWhere = " ROWNUM=1 AND ";
        sWhere += "cid.cidade='" + cidade + "' AND ";
        sWhere += "lig.cepEntrega IS NOT NULL AND ";
        sWhere += "NOT EXISTS(SELECT id FROM LigacaoDocumento WHERE lig.id = idligacao) AND ";
        sWhere += "NOT EXISTS(SELECT id FROM MensagemRetencaoForcada WHERE lig.id = idligacao)";
        sWhere += (!isBlank(sWhereAdicional)) ? "AND " + sWhereAdicional : "";

        return getColumnInfo("id",
                "SELECT lig.id FROM Ligacao lig " +
                        "INNER JOIN Cidade cid ON lig.idCidade = cid.id " +
                        "WHERE " + sWhere);
    }

    //Retorna ligacao cliente com debito
    public String getLigacaoDebito(String cidade, boolean debitoAtivo) {
        return getColumnInfo("id",
                "SELECT lig.id FROM Ligacao lig " +
                        "INNER JOIN Cidade cid ON lig.idCidade = cid.id " +
                        "WHERE ROWNUM=1 AND " + "cid.cidade='" + cidade + "' " +
                        "AND lig.idNegativacaoFase" + ((debitoAtivo) ? "=" : "<>") + DEBITO_ATIVO);
    }
}
