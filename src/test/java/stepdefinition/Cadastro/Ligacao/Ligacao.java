package stepdefinition.Cadastro.Ligacao;

import stepdefinition.Cadastro.Cliente.TiposDeCliente;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;
import framework.BrowserManager.BrowserVerifications;
import main.Utils.ComumScreens.Auxiliar;
import main.WebElementManager.InputText.InputText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.ArrayList;

import static framework.BrowserManager.BrowserActions.sendKeys;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.Utils.contemMarcacao;
import static main.WebElementManager.Botoes.getXPathBotoes;
import static main.WebElementManager.Xpath.LI;
import static main.WebElementManager.Xpath.getXPathElementWithText;


/**
 * @author Renan Ferreira dos Santos descontinuar classe e criar metodos por aba
 * @since 01/03/2019
 */
public class Ligacao {
    private String idLigacao;
    private String idLigacaoClienteDebito;
    private static String idLigacaoCadastrada;
    private Localizacao instalacao;
    private Localizacao identificacao;
    private Localizacao roteirizacaoLeitura;
    private Localizacao roteirizacaoEntrega;
    private static ArrayList<TiposDeCliente> listaTiposDeClientes = new ArrayList<>();

    private static String nomeBairro;
    private static String nomeLogradouro;
    private static String nomeGrupo;
    private static String nomeSubCategoria;
    private static int indiceLigacaoCliente = 0;

    public String getIdLigacao() {
        return idLigacao;
    }

    public void setIdLigacao(String idLigacao) {
        this.idLigacao = idLigacao;
    }

    public String getIdLigacaoClienteDebito() {
        return idLigacaoClienteDebito;
    }

    public void setIdLigacaoClienteDebito(String idLigacaoClienteDebito) {
        this.idLigacaoClienteDebito = idLigacaoClienteDebito;
    }

    public static String getIdLigacaoCadastrada() {
        return idLigacaoCadastrada;
    }

    public static void setIdLigacaoCadastrada(String idLigacaoCadastrada) {
        Ligacao.idLigacaoCadastrada = idLigacaoCadastrada;
    }

    public Localizacao getInstalacao() {
        return instalacao;
    }

    public void setInstalacao(Localizacao instalacao) {
        this.instalacao = instalacao;
    }

    public Localizacao getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(Localizacao identificacao) {
        this.identificacao = identificacao;
    }

    public Localizacao getRoteirizacaoLeitura() {
        return roteirizacaoLeitura;
    }

    public void setRoteirizacaoLeitura(Localizacao roteirizacaoLeitura) {
        this.roteirizacaoLeitura = roteirizacaoLeitura;
    }

    public Localizacao getRoteirizacaoEntrega() {
        return roteirizacaoEntrega;
    }

    public void setRoteirizacaoEntrega(Localizacao roteirizacaoEntrega) {
        this.roteirizacaoEntrega = roteirizacaoEntrega;
    }

    public static ArrayList<TiposDeCliente> getListaTiposDeClientes() {
        return listaTiposDeClientes;
    }

    public static void setListaTiposDeClientes(ArrayList<TiposDeCliente> listaTiposDeClientes) {
        Ligacao.listaTiposDeClientes = listaTiposDeClientes;
    }

    public static String getNomeBairro() {
        return nomeBairro;
    }

    public static void setNomeBairro(String nomeBairro) {
        Ligacao.nomeBairro = nomeBairro;
    }

    public static String getNomeLogradouro() {
        return nomeLogradouro;
    }

    public static void setNomeLogradouro(String nomeLogradouro) {
        Ligacao.nomeLogradouro = nomeLogradouro;
    }

    public static String getNomeGrupo() {
        return nomeGrupo;
    }

    public static void setNomeGrupo(String nomeGrupo) {
        Ligacao.nomeGrupo = nomeGrupo;
    }

    public static String getNomeSubCategoria() {
        return nomeSubCategoria;
    }

    public static void setNomeSubCategoria(String nomeSubCategoria) {
        Ligacao.nomeSubCategoria = nomeSubCategoria;
    }

    public static int getIndiceLigacaoCliente() {
        return indiceLigacaoCliente;
    }

    public static void setIndiceLigacaoCliente(int indiceLigacaoCliente) {
        Ligacao.indiceLigacaoCliente = indiceLigacaoCliente;
    }

    public static void clicarMedidor() {
        BrowserVerifications.waitElementToBeClickable(By.xpath("//div[3]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[2]" +
                "/td[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]"), DEFAULT_TIME_OUT);
        clickOnElement(By.xpath("//div[3]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[2]/td[2]/div[1]/div[1]/table[1]" +
                "/tbody[1]/tr[1]/td[1]/img[1]"));
    }

    public static void clicarSalvarInstalacaoMedidor() {
        clickOnElement(getXPathBotoes(2, 2));
    }

    //Retorna ligacao cadastrada anteriormente se houver
    public static String getLigacaoCadastrada(String ligacaoBDD) {
        if (!contemMarcacao(ligacaoBDD)) {
            return ligacaoBDD;
        }

        switch (ligacaoBDD.toUpperCase()) {
            case "[LIGACAO CADASTRADA ACIMA]":
                return getCampo(ligacaoBDD, getTextAttribute(InputText.getInputTextXPath(1, "Ligação"), "value"));
            case "[LIGACAO CADASTRADA ANTERIORMENTE]":
                return idLigacaoCadastrada;
            default:
                break;
        }
        return "";
    }

    //Fecha a tela de ligacao e armazena o id da ligação cadastrada
    public static void fecharTelaLigacao() {
        //Armazena ligacao cadastrada
        idLigacaoCadastrada = getTextAttribute(InputText.getInputTextXPath(1, "Ligação"), "value");

        //Aguarda a mensagem de dados salvos sumir
        BrowserVerifications.waitElementNotExists(getXPathElementWithText(LI, "Dados Salvos com Sucesso!"),
                DEFAULT_TIME_OUT);

        //Fecha tela
        Auxiliar.fecharTela();
    }

    public static void clicarComboOpcoes(String texto) throws InterruptedException {

        //!BrowserVerifications.elementExists(By.xpath(xPath)

        String xPath = "//fieldset[2]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[2]/tr[1]/td[2]/div[1]";

        clickOnElement(By.xpath(xPath));
        sendKeys(texto);
        sendKeys(Keys.ENTER);
        //clickOnElement(getXPathElementWithText(DIV, texto));

    }





}

