package stepdefinition.Cadastro.Ligacao;

import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * @author Juan Castillo
 * @since 04/04/2019
 */
public class Logradouro {

    private static final int TAMANHO_TIPO = 3;
    private static final int TAMANHO_TITULO = 6;
    private static final int TAMANHO_REFERENCIA = 10;
    private static final int TAMANHO_NOME = 7;
    private String cidade;
    private String tipo;
    private String titulo;
    private String nome;
    private String inicio;
    private String fin;
    private String referencia;
    private String bairro;


    public Logradouro() {
        this.tipo = geradorNumberRandom(TAMANHO_TIPO);
        this.titulo = geradorCharRandom(TAMANHO_TITULO);
        this.referencia = geradorCharRandom(TAMANHO_REFERENCIA);
        this.nome = geradorCharRandom(TAMANHO_NOME);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
}
