package stepdefinition.Cadastro.Ligacao;

import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * @author Juan Castillo
 * @since 04/04/2019
 */
public class Grupo {

    private String cidade;
    private String grupo;
    private String ordem;
    private String vencPadrao;
    private String tipoLeitura;
    private String exigeCritica;
    private String reavisodeDebito;
    private String grupodeLigacaoEstimada;
    private String corteLight;
    private String fiscalizacaodeCorte;
    private String estimadas;
    private String retdoColetorAutomatico;
    private String caldeConsumoAutomatico;
    private String caldeFaturamentoAutomatico;
    private String mensagemCanhotoEsgoto;


    public Grupo() {
        final int tamGrupo = 7, tamOrdem = 5, tamMensagem = 3;
        this.grupo = geradorCharRandom(tamGrupo);
        this.ordem = geradorNumberRandom(tamOrdem);
        this.mensagemCanhotoEsgoto = geradorCharRandom(tamMensagem);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getOrdem() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem = ordem;
    }

    public String getVencPadrao() {
        return vencPadrao;
    }

    public void setVencPadrao(String vencPadrao) {
        this.vencPadrao = vencPadrao;
    }

    public String getTipoLeitura() {
        return tipoLeitura;
    }

    public void setTipoLeitura(String tipoLeitura) {
        this.tipoLeitura = tipoLeitura;
    }

    public String getExigeCritica() {
        return exigeCritica;
    }

    public void setExigeCritica(String exigeCritica) {
        this.exigeCritica = exigeCritica;
    }

    public String getReavisodeDebito() {
        return reavisodeDebito;
    }

    public void setReavisodeDebito(String reavisodeDebito) {
        this.reavisodeDebito = reavisodeDebito;
    }

    public String getGrupodeLigacaoEstimada() {
        return grupodeLigacaoEstimada;
    }

    public void setGrupodeLigacaoEstimada(String grupodeLigacaoEstimada) {
        this.grupodeLigacaoEstimada = grupodeLigacaoEstimada;
    }

    public String getCorteLight() {
        return corteLight;
    }

    public void setCorteLight(String corteLight) {
        this.corteLight = corteLight;
    }

    public String getFiscalizacaodeCorte() {
        return fiscalizacaodeCorte;
    }

    public void setFiscalizacaodeCorte(String fiscalizacaodeCorte) {
        this.fiscalizacaodeCorte = fiscalizacaodeCorte;
    }

    public String getEstimadas() {
        return estimadas;
    }

    public void setEstimadas(String estimadas) {
        this.estimadas = estimadas;
    }

    public String getRetdoColetorAutomatico() {
        return retdoColetorAutomatico;
    }

    public void setRetdoColetorAutomatico(String retdoColetorAutomatico) {
        this.retdoColetorAutomatico = retdoColetorAutomatico;
    }

    public String getCaldeConsumoAutomatico() {
        return caldeConsumoAutomatico;
    }

    public void setCaldeConsumoAutomatico(String caldeConsumoAutomatico) {
        this.caldeConsumoAutomatico = caldeConsumoAutomatico;
    }

    public String getCaldeFaturamentoAutomatico() {
        return caldeFaturamentoAutomatico;
    }

    public void setCaldeFaturamentoAutomatico(String caldeFaturamentoAutomatico) {
        this.caldeFaturamentoAutomatico = caldeFaturamentoAutomatico;
    }

    public String getMensagemCanhotoEsgoto() {
        return mensagemCanhotoEsgoto;
    }

    public void setMensagemCanhotoEsgoto(String mensagemCanhotoEsgoto) {
        this.mensagemCanhotoEsgoto = mensagemCanhotoEsgoto;
    }
}
