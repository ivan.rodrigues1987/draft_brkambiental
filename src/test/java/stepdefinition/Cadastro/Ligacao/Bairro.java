package stepdefinition.Cadastro.Ligacao;


import static main.Utils.Utils.geradorCharRandom;

/**
 * @author Juan Castillo
 * @since 04/04/2019
 */
public class Bairro {

    private String cidade;
    private String distrito;
    private String nome;
    private final int tamanhoNome = 6;

    public Bairro() {
        this.nome = geradorCharRandom(tamanhoNome);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getDistrito() {
        return distrito;
    }

    /*public void setDistrito(String distrito) {
        this.distrito = distrito;
    }*/

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}


