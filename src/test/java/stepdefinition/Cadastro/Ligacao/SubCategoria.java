package stepdefinition.Cadastro.Ligacao;

import static main.Utils.Utils.geradorCharRandom;
import static main.Utils.Utils.geradorNumberRandom;

/**
 * @author Juan Castillo
 * @since 04/04/2019
 */
public class SubCategoria {

    private String cidade;
    private String imovelDesabitado;
    private String baixaRenda;
    private String possuiEsgotoDif;
    private String aplicarDesconto;
    private String sigla;
    private String descricao;
    private String unidadeDeMensuracao;
    private String baseDeCalculo;
    private String percentualDeDesconto;

    private static final int TAMANHO_SIGLA = 3;
    private static final int TAMANHO_DESCRICAO = 5;
    private static final int TAMANHO_BASE = 2;

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getImovelDesabitado() {
        return imovelDesabitado;
    }

    public void setImovelDesabitado(String imovelDesabitado) {
        this.imovelDesabitado = imovelDesabitado;
    }

    public String getBaixaRenda() {
        return baixaRenda;
    }

    public void setBaixaRenda(String baixaRenda) {
        this.baixaRenda = baixaRenda;
    }

    public String getPossuiEsgotoDif() {
        return possuiEsgotoDif;
    }

    public void setPossuiEsgotoDif(String possuiEsgotoDif) {
        this.possuiEsgotoDif = possuiEsgotoDif;
    }

    public String getAplicarDesconto() {
        return aplicarDesconto;
    }

    public void setAplicarDesconto(String aplicarDesconto) {
        this.aplicarDesconto = aplicarDesconto;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidadeDeMensuracao() {
        return unidadeDeMensuracao;
    }

    public void setUnidadeDeMensuracao(String unidadeDeMensuracao) {
        this.unidadeDeMensuracao = unidadeDeMensuracao;
    }

    public String getBaseDeCalculo() {
        return baseDeCalculo;
    }

    public void setBaseDeCalculo(String baseDeCalculo) {
        this.baseDeCalculo = baseDeCalculo;
    }

    public String getPercentualDeDesconto() {
        return percentualDeDesconto;
    }

    public void setPercentualDeDesconto(String percentualDeDesconto) {
        this.percentualDeDesconto = percentualDeDesconto;
    }

    public SubCategoria() {
        this.sigla = geradorCharRandom(TAMANHO_SIGLA);
        this.descricao = geradorCharRandom(TAMANHO_DESCRICAO);
        this.baseDeCalculo = geradorNumberRandom(TAMANHO_BASE);


    }
}
