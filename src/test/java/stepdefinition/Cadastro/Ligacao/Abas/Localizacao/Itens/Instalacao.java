package stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens;

import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;
import main.Utils.Utils;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;

import static main.Utils.ComumScreens.Auxiliar.getCampo;


/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class Instalacao extends Localizacao {
    //CAMPOS
    private String zonaDePressao;
    private String bacia;
    private String zonaDeMacro;
    private String quadra;
    private String lote;
    private String inscricao;
    private String nrImovelAntigo;
    private static final int INSCRICAO_TAM = 4;


    @Override
    public void preencherCampos() {
        //ZONA DE PRESSAO
        Combos.setComboClicando("Zona de Pressão", zonaDePressao);

        //BACIA
        Combos.setComboClicando("Bacia", bacia);

        //ZONA DE MACRO
        Combos.setComboClicando("Zona de Macro", zonaDeMacro);

        //QUADRA
        InputText.setInputText("Quadra", quadra);

        //LOTE
        InputText.setInputText("Lote", lote);

        //INSCRICAO
        InputText.setInputText("Inscrição", getCampo(inscricao, Utils.geradorNumberRandom(INSCRICAO_TAM)));

        //NR IMOVEL ANTIGO
        InputText.setInputText("Nr. Imóvel Antigo", nrImovelAntigo);
    }
}
