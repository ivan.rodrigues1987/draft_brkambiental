package stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens;

import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;

import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;


/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class RoteirizacaoLeitura extends Localizacao {
    //CAMPOS
    private String grupoRL;
    private String setorRL;
    private String roteiroRL;
    private String quadraRL;
    private String sequenciaRL;
    private String subUnidadeRL;

    private static final int INDICE_QUADRA = 3;

    @Override
    public void preencherCampos() {
        //GRUPO
        //Combos.setComboClicando("Grupo", 2, grupoRL);
        Combos.setComboClicando("Grupo", 2,  getCampoPesquisado(grupoRL));

        //SETOR
        InputText.setInputText(2, "Setor", setorRL);

        //ROTEIRO
        InputText.setInputText("Roteiro", roteiroRL);

        //QUADRA
        InputText.setInputText(INDICE_QUADRA, "Quadra", quadraRL);

        //LOTE
        InputText.setInputText("Sequência", sequenciaRL);

        //SUB UNIDADE
        InputText.setInputText("Sub Unidade", subUnidadeRL);
    }
}
