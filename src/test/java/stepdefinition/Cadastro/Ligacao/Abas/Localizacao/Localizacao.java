package stepdefinition.Cadastro.Ligacao.Abas.Localizacao;

import stepdefinition.Cadastro.Ligacao.Spans;
import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserVerifications.isElementDisplayed;

/**
 * @author Renan Ferreira dos Santos
 * @since 08/03/2019
 */
public abstract class Localizacao {
    public abstract void preencherCampos();

    protected boolean spanIsDisplayed(Spans spans) {
        return isElementDisplayed(By.xpath("//span[@class='GKPSPJYCPSB']" +
                "[contains(text(),'" + spans.toString() + "')]"));
    }
}
