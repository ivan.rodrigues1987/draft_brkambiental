package stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens;


import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;

import stepdefinition.Cadastro.Ligacao.Spans;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;

import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;


/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class Identificacao extends Localizacao {
    //CAMPOS
    private String grupoID;
    private String setorID;
    private String rotaID;
    private String quadraID;
    private String loteID;
    private String cavaleteID;

    @Override
    public void preencherCampos() {
        if (spanIsDisplayed(Spans.IDENTIFICACAO)) {
            //GRUPO
            //Combos.setComboClicando("Grupo", grupoID);
            Combos.setComboClicando("Grupo", getCampoPesquisado(grupoID));

            //SETOR
            InputText.setInputText("Setor", setorID);

            //ROTA
            InputText.setInputText("Rota", rotaID);

            //QUADRA
            InputText.setInputText(2, "Quadra", quadraID);

            //LOTE
            InputText.setInputText(2, "Lote", loteID);

            //CAVALETE
            InputText.setInputText("Cavalete", cavaleteID);
        }
    }
}
