package stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Itens;

import runner.Dados.Cadastro.Endereco;

import static framework.AssertManager.assertElementAttributeTextEmpty;
import static framework.AssertManager.assertElementAttributeText;
import static framework.AssertManager.assertElementAttributeTextContains;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;


/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class RoteirizacaoEntrega {
    //Tipo de entrega selecionado
    private static String tipoEntrega = "";
    private static final String ENTREGA_NO_LOCAL = "NO LOCAL";

    public static void setTipoEntrega(String tipoEntrega) {
        RoteirizacaoEntrega.tipoEntrega = tipoEntrega;
    }

    public static void validarPreenchimentoAutomatico(Endereco enderecoBDD, Endereco endereco) {
        switch (tipoEntrega.toUpperCase()) {
            case ENTREGA_NO_LOCAL:
                validarPreenchimentoIgualAoEndereco(enderecoBDD, endereco);
                break;
            default:
                validarPreenchimento();
                break;
        }
    }

    //Valida se os campos foram preenchidos iguais aos campos de endereco
    private static void validarPreenchimentoIgualAoEndereco(Endereco enderecoBDD, Endereco endereco) {
        //LOGRADOURO
        assertElementAttributeTextContains(getInputTextXPath("Logradouro"), "value",
                getCampo(enderecoBDD.getLogradouro(), endereco.getLogradouro().trim()));

        //COMPLEMENTO
        assertElementAttributeText(getInputTextXPath(2, "Complemento"), "value",
                getCampo(enderecoBDD.getComplemento(), endereco.getComplemento()));

        //NUMERO
        assertElementAttributeText(getInputTextXPath(2, "Número"), "value",
                getCampo(enderecoBDD.getNumero(), endereco.getNumero()));

        //CEP
        assertElementAttributeText(getInputTextXPath(2, "CEP"), "value",
                getCampo(enderecoBDD.getCep(), endereco.getCep()));

        //BAIRRO
        assertElementAttributeText(getInputTextXPath("Bairro"), "value",
                getCampo(enderecoBDD.getBairro(), endereco.getBairro()));
    }

    //Valida se os campos foram preenchidos
    private static void validarPreenchimento() {
        //LOGRADOURO
        assertElementAttributeTextEmpty(getInputTextXPath("Logradouro"), "value", false);

        //NUMERO
        assertElementAttributeTextEmpty(getInputTextXPath(2, "Número"), "value", false);

        //CEP
        assertElementAttributeTextEmpty(getInputTextXPath(2, "CEP"), "value", false);

        //BAIRRO
        assertElementAttributeTextEmpty(getInputTextXPath("Bairro"), "value", false);
    }
}
