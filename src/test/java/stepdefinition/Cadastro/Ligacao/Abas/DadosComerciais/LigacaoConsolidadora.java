package stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais;

import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;
import main.Utils.ComumScreens.ConsultaPersonalizada;
import org.openqa.selenium.By;


import static framework.BrowserManager.BrowserActions.clickOnElement;
import static main.WebElementManager.Botoes.BTN_OK;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.InputText.InputText.setInputText;

/**
 * @author Renan Ferreira dos Santos
 * @since 18/03/2019
 */
public class LigacaoConsolidadora extends Localizacao {
    private String ligacaoConsolidadora;
    private String economiasLigacaoConsolidadora;
    private String observacaoLigacaoConsolidadora;

    @Override
    public void preencherCampos() {
        //LIGACAO CONSOLIDADORA
        clicarCheckBox("Ligação Consolidadora", ligacaoConsolidadora);

        //ECONOMIAS LIGACAO CONSOLIDADORA
        setInputText("Economias", economiasLigacaoConsolidadora);

        //OBSERVACAO LIGACAO CONSOLIDADORA
        setInputText("Obs", observacaoLigacaoConsolidadora);
    }

    //PESQUISA A LIGACAO A CONSOLIDAR
    public static void pesquisarLigacaoAConsolidar(String ligacao) {
        ConsultaPersonalizada consultaPersonalizada = new ConsultaPersonalizada();

        //CLICA PESQUISAR
        clickOnElement(By.xpath(
                "(//label[text()='Ligação:']/following-sibling::div/div/div/table/tbody/tr/td/div)[4]"));

        //EFETUA A PESQUISA DA LIGACAO
        consultaPersonalizada.efetuarPesquisaPersonalizada(ligacao, BTN_OK);


    }
}
