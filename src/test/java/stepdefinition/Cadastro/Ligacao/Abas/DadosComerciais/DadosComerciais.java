package stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais;

import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;
import main.WebElementManager.Combos.ComboInputText;
import org.openqa.selenium.Keys;


import static framework.BrowserManager.BrowserActions.sendKeys;
import static main.WebElementManager.Combos.Combos.setComboClicando;

/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class DadosComerciais extends Localizacao {
    //CAMPOS
    private String tipoFaturamento;
    private String tipoCobranca;
    private String situacaoDaLigacao;
    private String situacaoDeCobranca;
    private String classConsumo;
    private String irregularidade;
    private String situacaoImovel;
    private String dataInstalacaoAgua;
    private String dataInstalacaoEsgoto;
    private String caracteristicaLigacao;
    private String fonteAlternativa;



    @Override
    public void preencherCampos() {
        //TIPO FATURAMENTO
        setComboClicando("Tipo Faturamento", tipoFaturamento);

        //TIPO COBRANCA
        setComboClicando("Tipo Cobrança", tipoCobranca);

        //SITUACAO DA LIGACAO
        setComboClicando("Situação da Ligação", situacaoDaLigacao);

        //SITUACAO DE COBRANCA
        setComboClicando("Situação de Cobrança", situacaoDeCobranca);

        //CLASS CONSUMO
        setComboClicando("Class. Consumo", classConsumo);

        //IRREGULARIDADE
        setComboClicando("Irregularidade", irregularidade);

        //SITUACAO IMOVEL
        setComboClicando("Situação Imóvel", situacaoImovel);

        //DATA INSTALACAO AGUA
        ComboInputText.setComboEPressionarEnter("Data Instalação Água", " " + dataInstalacaoAgua);

        //DATA INSTALACAO ESGOTO
        ComboInputText.setComboEPressionarEnter("Data Instalação Esgoto", " " + dataInstalacaoEsgoto);

        //CARACTERISTICA LIGACAO
        setComboClicando("Característica Ligação", caracteristicaLigacao);

        //FONTE ALTERNATIVA
        setComboClicando("Fonte Alternativa", fonteAlternativa);

        //CLICA ENTER
        sendKeys(Keys.ENTER);
    }
}
