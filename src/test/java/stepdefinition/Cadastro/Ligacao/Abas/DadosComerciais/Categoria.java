package stepdefinition.Cadastro.Ligacao.Abas.DadosComerciais;


import framework.BrowserManager.BrowserVerifications;
import org.openqa.selenium.By;

import static framework.BrowserManager.BrowserActions.selectOptionRightClickOnElement;
import static framework.BrowserManager.BrowserActions.getText;
import static framework.BrowserManager.BrowserActions.clickOnElement;
import static framework.BrowserManager.BrowserActions.sendKeys;

import static framework.BrowserManager.BrowserVerifications.waitElementValueEquals;
import static framework.BrowserManager.BrowserVerifications.waitPersistentTextSet;
import static framework.ConfigFramework.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.WebElementManager.Xpath.getXPathElementWithText;
import static main.WebElementManager.Xpath.DIV;
import static main.WebElementManager.Xpath.concatenarIndice;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertNotEquals;

/**
 * <b>Classe para manipular o grid de Categoria</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 07/03/2019
 */
public class Categoria {

    //COLUNAS
    private static final int COLUNA_CATEGORIA = 1;
    private static final int COLUNA_SUBCATEGORIA = 2;
    private static final int COLUNA_ECONOMIAS = 3;
    private static final int COLUNA_UNIDADEMENSURACAO = 5;
    private static final int COLUNA_BASECALCULO = 6;
    private static final int COLUNA_VALORMENSURACAO = 7;
    private static final int COLUNA_VOLUMEESTIMADO = 8;
    //NUMERO DIAS PADRAO PARA CALCULO DO VOLUME ESTIMADO
    private static final int NR_DIAS_PADRAO = 30;
    //CAMPOS
    private String categoria;
    private String subCategoria;
    private String economias;
    private String unidadeDeMensuracao;
    private String baseDeCalculo;
    private String valorMensuracao;
    private String volumeEstimado;

 //GETTERS E SETTERS
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(String subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getEconomias() {
        return economias;
    }

    public void setEconomias(String economias) {
        this.economias = economias;
    }

    public String getUnidadeDeMensuracao() {
        return unidadeDeMensuracao;
    }

    public void setUnidadeDeMensuracao(String unidadeDeMensuracao) {
        this.unidadeDeMensuracao = unidadeDeMensuracao;
    }

    public String getBaseDeCalculo() {
        return baseDeCalculo;
    }

    public void setBaseDeCalculo(String baseDeCalculo) {
        this.baseDeCalculo = baseDeCalculo;
    }

    public String getValorMensuracao() {
        return valorMensuracao;
    }

    public void setValorMensuracao(String valorMensuracao) {
        this.valorMensuracao = valorMensuracao;
    }

    public String getVolumeEstimado() {
        return volumeEstimado;
    }

    public void setVolumeEstimado(String volumeEstimado) {
        this.volumeEstimado = volumeEstimado;
    }

    //PREENCHE OS CAMPOS DE CATEGORIA
    public void preencherCampos(int linha) {

        BrowserVerifications.waitElementToBeVisible(By.xpath(
                "(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[2]/tr[1]/td[" + linha + "]/div[1])[1]"),
                DEFAULT_TIME_OUT);

        //ADICIONA MAIS UMA LINHA AO GRID
        if (linha > 1) {
            selectOptionRightClickOnElement("Adicionar", By.xpath("(//tbody[2])[1]"));
        }

        //SELECIONA A LINHA DO GRID
        clicarCampo(linha, COLUNA_CATEGORIA);

        //*CATEGORIA*
        //BrowserVerifications.waitElementToBeClickable(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]
        // /tbody[2]/tr[1]/td[1]/div[1])[1]"),DEFAULT_TIME_OUT);
        BrowserVerifications.waitElementToBeVisible(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]" +
                "/tbody[2]/tr[1]/td[1]/div[1])[1]"), DEFAULT_TIME_OUT);
        preencherCombo(linha, COLUNA_CATEGORIA, categoria);

        //SUBCATEGORIA (preenchimento depois de economias pois possui espeficidades)
        preencherComboSubCategoria(linha);

        //ECONOMIAS
        inserirTexto(linha, COLUNA_ECONOMIAS, economias);

        //VALOR MENSURACAO
        BrowserVerifications.waitElementToBeClickable(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]" +
                "/tbody[2]/tr[1]/td[" + linha + "]/div[1])[1]"), DEFAULT_TIME_OUT);
        inserirTexto(linha, COLUNA_VALORMENSURACAO, valorMensuracao);

        BrowserVerifications.waitElementToBeVisible(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]" +
                "/tbody[2]/tr[1]/td[" + linha + "]/div[1])[1]"), DEFAULT_TIME_OUT);

    }


    //VALIDA O PREENCHIMENTO AUTOMATICO DOS CAMPOS DE CATEGORIA
    public void validarCampos(int linha) {

        BrowserVerifications.waitElementToBeVisible(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]" +
                "/tbody[2]/tr[1]/td[" + linha + "]/div[1])[1]"), DEFAULT_TIME_OUT);

        By byVolumeEstimado = retornarXPathCampo(linha, COLUNA_VOLUMEESTIMADO);
        if (!valorMensuracao.isEmpty()) {
            //UNIDADE DE MENSURACAO
            assertTrue(!getText(retornarXPathCampo(linha, COLUNA_UNIDADEMENSURACAO)).isEmpty());

            //BASE DE CALCULO
            baseDeCalculo = getText(retornarXPathCampo(linha, COLUNA_BASECALCULO));
            assertNotEquals("0", baseDeCalculo);

            //VOLUME ESTIMADO - aguardar ser igual ao calculado e diferente de zero
            waitElementValueEquals(byVolumeEstimado, calcularVolumeEstimado() + "", DEFAULT_TIME_OUT);
        }
        BrowserVerifications.waitElementToBeVisible(By.xpath("(//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]" +
                "/tbody[2]/tr[1]/td[" + linha + "]/div[1])[1]"), DEFAULT_TIME_OUT);
    }

    //Calculo volume estimado - sempre arredonda para mais
    public int calcularVolumeEstimado() {
        final int mil = 1000;
        return (int) Math.ceil(
                (Integer.parseInt(economias) * Double.parseDouble(valorMensuracao.replace(
                        ",", ".")) * NR_DIAS_PADRAO * Double.parseDouble(baseDeCalculo) / mil));
    }

    /**
     * <b>Clica no campo do grid</b>
     *
     * @param linha  <i>linha do campo</i>
     * @param coluna <i>coluna do campo</i>
     */
    private void clicarCampo(int linha, int coluna) {
        clickOnElement(retornarXPathCampo(linha, coluna));
    }

    /**
     * <b>Preenche combo do grid</b>
     *
     * @param linha  <i>linha do campo</i>
     * @param coluna <i>coluna do campo</i>
     * @param valor  <i>valor a ser selecionado</i>
     */
    private void preencherCombo(int linha, int coluna, String valor) {
        if (!valor.isEmpty()) {
            //CLICA NO CAMPO
            clicarCampo(linha, coluna);

            //INSERE VALOR
            sendKeys(valor);

            //CLICA NO ITEM
            clickOnElement(getXPathElementWithText(DIV, valor));

            //CLICA FORA PARA CARREGAR A INFORMACAO
            clicarAba("Dados Comerciais");
        }
    }

    /**
     * <b>Preenche combo de SubCategoria do grid</b>
     *
     * @param linha <i>linha do campo</i>
     */
    private void preencherComboSubCategoria(int linha) {
        if (!subCategoria.isEmpty()) {
            By bySubCategoria = retornarXPathCampo(linha, COLUNA_SUBCATEGORIA);
            //CLICA NO CAMPO DE SUBCATEGORIA
            BrowserVerifications.waitElementToBeClickable(bySubCategoria, DEFAULT_TIME_OUT);
            clickOnElement(bySubCategoria);

            //CLICA FORA PARA CARREGAR A INFORMACAO
            clicarAba("Dados Comerciais");

            //PREENCHE A COMBO COM A INFORMACAO
            //preencherCombo(linha, COLUNA_SUBCATEGORIA, subCategoria);
            BrowserVerifications.waitElementToBeVisible(bySubCategoria, DEFAULT_TIME_OUT);
            preencherCombo(linha, COLUNA_SUBCATEGORIA, getCampoPesquisado(subCategoria));
            //BrowserVerifications.waitElementAttributeValue(bySubCategoria,"value",getCampoPesquisado(subCategoria),2);
        }
    }

    /**
     * <b>Insere texto no grid</b>
     *
     * @param linha  <i>linha do campo</i>
     * @param coluna <i>coluna do campo</i>
     * @param valor  <i>valor a ser inserido</i>
     */
    private void inserirTexto(int linha, int coluna, String valor) {
        if (!valor.isEmpty()) {
            //CLICA NO CAMPO
            clicarCampo(linha, coluna);

            //INSERE VALOR
            sendKeys(valor);

            //CLICA FORA PARA CARREGAR A INFORMACAO
            clicarAba("Dados Comerciais");
        }
    }

    /**
     * <b>Preenche combo do grid</b>
     *
     * @param linha  <i>linha do campo</i>
     * @param coluna <i>coluna do campo</i>
     * @param valor  <i>valor a ser selecionado</i>
     */
    private void setComboCategoria(int linha, int coluna, String valor) {
        if (!valor.isEmpty()) {
            //Clica na linha a ser preenchida
            clickOnElement(retornarXPathCategoria(linha, coluna));
            //Preenche e valida a insersão dos valores
            waitPersistentTextSet(retornarXPathInputTextCategoria(1), valor, DEFAULT_TIME_OUT);
            //CLICA FORA PARA CARREGAR A INFORMACAO
            clicarAba("Dados Comerciais");
        }
    }

    private By retornarXPathCategoria(int linha, int coluna) {
        return By.xpath("//fieldset[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[2]" +
                "/tr[" + linha + "]/td[" + coluna + "]/div[1]");
    }

    private By retornarXPathInputTextCategoria(int linha) {
        return By.xpath("//div[3]/div[1]/table[1]/tbody[1]/tr[" + linha + "]/td[1]/input[1]");
    }
    /**
     * <b>Retorna xPath do campo do grid</b>
     *
     * @param linha  <i>linha do campo</i>
     * @param coluna <i>coluna do campo</i>
     * @return By-   <i>retorna xpath</i>
     *
     */
    private By retornarXPathCampo(int linha, int coluna) {
        return concatenarIndice("//tbody[2]/tr[" + linha + "]/td[" + coluna + "]", 1);
    }


}
