package stepdefinition.Cadastro.Ligacao.Abas.Complemento;

import stepdefinition.Cadastro.Cliente.Cliente;
import stepdefinition.Cadastro.Ligacao.Abas.Localizacao.Localizacao;


import main.Utils.ComumScreens.ConsultaPersonalizada;

import main.WebElementManager.Botoes;
import main.WebElementManager.Combos.ComboInputText;



import static framework.AssertManager.assertElementAttributeText;

import static framework.BrowserManager.BrowserActions.getTextAttribute;
import static main.Utils.ComumScreens.ConsultaPersonalizada.getLupaAzulInputTextXPath;
import static main.Utils.Utils.getSomenteNumeros;
import static main.WebElementManager.Checkbox.clicarCheckBox;
import static main.WebElementManager.Combos.Combos.setComboClicando;
import static main.WebElementManager.InputText.InputText.setInputText;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static org.testng.AssertJUnit.assertEquals;

/**
 * @author Renan Ferreira dos Santos
 * @since 01/03/2019
 */
public class Complemento extends Localizacao {
    //CAMPOS
    private String codInquilino;
    private String numeroDeMoradores;
    private String tipoDePavimento;
    private String tipoDeRede;
    private String padraoDeInstalacao;
    private String posicaoDaRede;
    private String volPiscina;
    private String piscina;
    private String tipoDeCavalete;
    private String situacaoDoCavalete;
    private String tipoDeEsgotamento;
    private String fonteDeAbastecimento;
    private String capacidadeReservatorio;
    private String poco;
    private String reservatorio;
    private String nrQuartos;
    private String areaConstruidam2;
    private String areaVerde;
    private String ligacaoTemp;
    private String bloqOsClienteCallCenter;
    private String posicaoRedeDeEsgoto;
    private String orgaoPublico;
    private String distanciaRamalAgua;
    private String distanciaRamalEsgoto;

    @Override
    public void preencherCampos() {
        //NUMERO DE MORADORES
        setInputText("Número de Moradores", numeroDeMoradores);

        //TIPO DE PAVIMENTO
        setComboClicando("Tipo de Pavimento", tipoDePavimento);

        //TIPO DE REDE
        setComboClicando("Tipo de Rede", tipoDeRede);

        //PADRAO DE INSTALACAO
        setComboClicando("Padrão de instalação", padraoDeInstalacao);

        //POSICAO DA REDE
        setComboClicando("Posição da Rede", posicaoDaRede);

        //PISCINA
        clicarCheckBox("Piscina", piscina);

        //VOL PISCINA
        if (piscina.equals("S")) {
            setInputText("Vol. Piscina", volPiscina);
        }

        //TIPO DE CAVALETE
        setComboClicando("Tipo de Cavalete", tipoDeCavalete);

        //SITUACAO DO CAVALETE
        setComboClicando("Situação do Cavalete", situacaoDoCavalete);

        //TIPO DE ESGOTAMENTO
        setComboClicando("Tipo de Esgotamento", tipoDeEsgotamento);

        //FONTE DE ABASTECIMENTO
        setComboClicando("Fonte de Abastecimento", fonteDeAbastecimento);

        //CAPACIDADE RESERVATORIO
        setInputText("Capacidade Reservatório", capacidadeReservatorio);

        //POCO
        clicarCheckBox("Poço", poco);

        //RESERVATORIO
        if (!reservatorio.isEmpty()) {
            ConsultaPersonalizada consultaPersonalizada = new ConsultaPersonalizada("Reservatório",
                    ConsultaPersonalizada.LupasConsulta.LUPA_COMUM);
            consultaPersonalizada.efetuarPesquisaPersonalizada("Reservatorio", reservatorio, Botoes.BTN_OK);
        }

        //NUMERO QUARTOS
        setInputText("Nr. Quartos", nrQuartos);

        //AREA CONSTRUIDA
        setInputText("Área Construída [m2]", areaConstruidam2);

        //AREA VERDE
        setInputText("Área Verde", areaVerde);

        //LIGACAO TEMP
        clicarCheckBox("Ligação Temp.", ligacaoTemp);

        //BLOQ OS CLIENTE CALL CENTER
        clicarCheckBox("Bloq. OS Cliente Call Center", bloqOsClienteCallCenter);

        //POSICAO REDE DE ESGOTO
        setComboClicando("Posição Rede de Esgoto", posicaoRedeDeEsgoto);

        //ORGAO PUBLICO
        setComboClicando("Órgão Público", orgaoPublico);

        //DISTANCIA RAMAL AGUA
        setInputText("Distância Ramal Água", distanciaRamalAgua);

        //DISTANCIA RAMAL ESGOTO
        setInputText("Distância Ramal Esgoto", distanciaRamalEsgoto);
    }

    //VALIDA SE OS CAMPOS DO INQUILINO ESTAO IGUAIS AOS DO CLIENTE
    public static void validarCamposInquilino(Cliente inquilino) {
        //INQUILINO
        if (!inquilino.getCodigo().isEmpty()) {
            assertElementAttributeText(getLupaAzulInputTextXPath("Inquilino"),
                    getLupaAzulInputTextXPath("Cliente"), "value");
        }

        //NOME
        assertElementAttributeText(getInputTextXPath("Nome"), getInputTextXPath("Nome Cliente"), "value");

        //DATA DE NASCIMENTO
        assertElementAttributeText(ComboInputText.getXPathTextoCombo("Data Nascimento"),
                getInputTextXPath("Data Nascimento"), "value");

        //TELEFONE
        assertElementAttributeText(getInputTextXPath("Telefone"), getInputTextXPath("Telefone Fixo"), "value");

        //TELEFONE MOVEL
        assertElementAttributeText(getInputTextXPath(2, "Telefone Movel"),
                getInputTextXPath(1, "Telefone Movel"), "value");

        //E-MAIL
        assertElementAttributeText(getInputTextXPath(2, "E-mail"), getInputTextXPath(1, "E-mail"), "value");

        //RG
        String rgCliente = getSomenteNumeros(getTextAttribute(getInputTextXPath(1, "R.G."), "value"));
        String rgInquilino = getSomenteNumeros(getTextAttribute(getInputTextXPath(2, "R.G."), "value"));
        assertEquals(rgInquilino, rgCliente);

        //DATA EXPEDICAO RG
        assertElementAttributeText(ComboInputText.getXPathTextoCombo("Data Exp. RG"),
                getInputTextXPath("Data Exp. RG"), "value");

        //CPF
        String cpfCliente = getSomenteNumeros(getTextAttribute(getInputTextXPath(1, "C.P.F."), "value"));
        String cpfInquilino = getSomenteNumeros(getTextAttribute(getInputTextXPath(2, "C.P.F."), "value"));
        assertEquals(cpfInquilino, cpfCliente);

        //CNPJ
        String cnpjCliente = getSomenteNumeros(getTextAttribute(getInputTextXPath(1, "C.N.P.J."), "value"));
        String cnpjInquilino = getSomenteNumeros(getTextAttribute(getInputTextXPath(2, "C.N.P.J."), "value"));
        assertEquals(cnpjInquilino, cnpjCliente);
    }


}
