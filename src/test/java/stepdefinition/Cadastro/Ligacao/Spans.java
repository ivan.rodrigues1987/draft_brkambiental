package stepdefinition.Cadastro.Ligacao;

/**
 * @author Renan Ferreira dos Santos
 * @since 06/03/2019
 */
public enum Spans {
    INSTALACAO("Instalação"),
    IDENTIFICACAO("Identificação"),
    ROTEIRIZACAO_LEITURA("Roteirização de Leitura"),
    ROTEIRIZACAO_ENTREGA("Roteirização de Entrega");

    private final String span;

    /**
     * @param text <i>valor que precisa-se ter</i>
     */
    Spans(final String text) {
        this.span = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return span;
    }
}


