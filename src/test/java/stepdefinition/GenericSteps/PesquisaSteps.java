package stepdefinition.GenericSteps;


import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import main.Utils.ComumScreens.ConsultaPersonalizada;
import main.Utils.ComumScreens.PesquisaRapida;

import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.PesquisaRapida.getCampoPesquisado;
import static main.Utils.ComumScreens.PesquisaRapida.clicarResultadoPesquisa;
import static main.Utils.ComumScreens.PesquisaRapida.verificarResultadoGridPesquisa;
import static main.Utils.ComumScreens.PesquisaRapida.preencherConsultaCompleta;
import static main.Utils.ComumScreens.PesquisaRapida.verificarSeExisteResultadoGridPesquisa;
import static main.WebElementManager.Botoes.BTN_OK;

/**
 * <b>Classe para implementar os Steps em comum das telas de pesquisa do sistema</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 13/03/2019
 */
public class PesquisaSteps {

    @Entao("^o resultado da pesquisa sera exibido em tela$")
    public void oResultadoDaPesquisaSeraExibidoEmTela() {
        verificarSeExisteResultadoGridPesquisa();
    }

    @E("^selecionar no resultado da pesquisa \"([^\"]*)\" e clicar em \"([^\"]*)\"$")
    public void selecionarNoResultadoDaPesquisaEClicarEm(String valor, String clicar) {
        clicarResultadoPesquisa(getCampoPesquisado(valor), clicar);
    }

    @Quando("^no campo \"([^\"]*)\" clicar em pesquisar e filtrar por \"([^\"]*)\" e pesquisar \"([^\"]*)\"$")
    public void noCampoClicarEmPesquisarEFiltrarPorEPesquisar(String campo, String filtrar, String valor) {
        ConsultaPersonalizada consultar = new ConsultaPersonalizada(campo,
                ConsultaPersonalizada.LupasConsulta.LUPA_AZUL);
        consultar.efetuarPesquisaPersonalizada(filtrar, valor, BTN_OK);
    }

    @Quando("^na tela de Pesquisa eu preencher Cidade \"([^\"]*)\", Campo \"([^\"]*)\" e Valor \"([^\"]*)\"$")
    public void naTelaDePesquisaEuPreencherCidadeCampoEValor(String cidade, String campo, String valor) {
        PesquisaRapida.pesquisar(cidade, campo, getCampoPesquisado(valor));
    }

    @E("^selecionar em campo \"([^\"]*)\", em operador \"([^\"]*)\" e em valor \"([^\"]*)\"$")
    public void selecionarEmCampoEmOperadorEEmValor(String campo, String operador, String valor) {
        preencherConsultaCompleta(campo, operador, valor);
    }

    @E("^na aba \"([^\"]*)\" selecionar em campo \"([^\"]*)\", em operador \"([^\"]*)\" e em valor \"([^\"]*)\"$")
    public void naAbaSelecionarEmCampoEmOperadorEEmValor(String aba, String campo, String operador, String valor) {
        clicarAba(aba);
        selecionarEmCampoEmOperadorEEmValor(campo, operador, valor);
    }

    @E("^na tela de pesquisa rapida pesquisar \"([^\"]*)\"$")
    public void naTelaDePesquisaRapidaPesquisar(String valor) {
        PesquisaRapida.pesquisar(getCampo(valor, getCampoPesquisado(valor)));
    }

    @Entao("^o resultado da pesquisa \"([^\"]*)\" sera exibido em tela$")
    public void oResultadoDaPesquisaSeraExibidoEmTela(String valor) {
        verificarResultadoGridPesquisa(getCampo(valor, getCampoPesquisado(valor)));
    }

    @E("^em pesquisa rapida selecionar em campo \"([^\"]*)\" e preencher em valor \"([^\"]*)\"$")
    public void emPesquisaRapidaSelecionarEmCampoEPreencherEmValor(String campo, String valor) {
        PesquisaRapida.pesquisar(campo, getCampo(valor, getCampoPesquisado(valor)));
    }

    @E("^na consulta personalizada \"([^\"]*)\" clicar em pesquisar e pesquisar \"([^\"]*)\"$")
    public void naConsultaPersonalizadaClicarEmPesquisarEPesquisar(String campo, String valor) {
        ConsultaPersonalizada.evocarPesquisaPersonalizada(campo,
                ConsultaPersonalizada.LupasConsulta.LUPA_COMUM, valor, BTN_OK);
    }

    @E("^em pesquisa rapida selecionar em campo \"([^\"]*)\" e preencher em valor \"([^\"]*)\" e clicar no resultado$")
    public void emPesquisaRapidaSelecionarEmCampoEPreencherEmValorEClicarNoResultado(String campo, String valor) {
        PesquisaRapida.pesquisar(campo, getCampo(valor, getCampoPesquisado(valor)));
        clicarResultadoPesquisa(valor, "OK");
    }
}
