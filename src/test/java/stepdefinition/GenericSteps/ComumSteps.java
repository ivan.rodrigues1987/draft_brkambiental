package stepdefinition.GenericSteps;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import framework.BrowserManager.BrowserVerifications;
import main.Utils.ComumScreens.Auxiliar;
import main.WebElementManager.Botoes;
import main.WebElementManager.Checkbox;
import main.WebElementManager.Combos.Combos;
import main.WebElementManager.InputText.InputText;
import main.WebElementManager.TextArea;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static Reports.ExtentReports.appendToReport;
import static framework.BrowserManager.BrowserActions.clearField;
import static framework.BrowserManager.BrowserActions.sendKeys;
import static main.Utils.ComumScreens.Auxiliar.getDriver;
import static main.Utils.ComumScreens.Auxiliar.efetuarLogin;
import static main.Utils.ComumScreens.Auxiliar.acessarMenu;
import static main.Utils.ComumScreens.Auxiliar.verificarMensagemJanela;
import static main.Utils.ComumScreens.Auxiliar.verificarMensagemTela;
import static main.Utils.ComumScreens.Auxiliar.verificarTelaFechada;
import static main.Utils.ComumScreens.Auxiliar.DEFAULT_TIME_OUT;
import static main.Utils.ComumScreens.Auxiliar.getCampo;
import static main.Utils.ComumScreens.Auxiliar.clicarAba;
import static main.Utils.ComumScreens.Auxiliar.validarComboAlterada;
import static main.WebElementManager.Botoes.clicarBotao;
import static main.WebElementManager.Botoes.clicarBotaoPesquisarConsultaCompleta;
import static main.WebElementManager.Botoes.clicarBotaoPesquisar;
import static main.WebElementManager.Botoes.clicarBotaoPesquisaAzul;
import static main.WebElementManager.Combos.ComboInputText.getXPathTextoCombo;
import static main.WebElementManager.Combos.Combos.setComboCidade;
import static main.WebElementManager.InputText.InputTextXPath.getInputTextXPath;
import static runner.Setup.Json.DadosDinamicos.getDadosCadastro;


/**
 * <b>Classe para implementar os Steps em comum das telas do sistema</b>
 *
 * @author Renan Ferreira dos Santos
 * @since 01/02/2019
 */
public class ComumSteps {

    private WebDriver browser = getDriver();

    @Dado("^a tela \"([^\"]*)\" esta conforme os seguintes parametros da entidade \"([^\"]*)\":$")
    public void aTelaEstaConformeOsSeguintesParametrosDaEntidade(String tela, String entidade, DataTable dataTable) {
        //PARAMETROS QUE SAO EXIBIDOS NO RELATORIO FINAL
    }

    @Dado("^Eu acesse a unidade \"([^\"]*)\" e navegue pelo menu \"([^\"]*)\" e \"([^\"]*)\"$")
    public void euAcesseAUnidadeENaveguePeloMenuE(String unidade, String menu1, String menu2) {
        //Efetua login
        efetuarLogin(unidade);
        //Acessa menu
        acessarMenu(menu1, menu2);
    }

    @E("^navegar pelo menu \"([^\"]*)\" e \"([^\"]*)\"$")
    public void navegarPeloMenuE(String menu1, String menu2) {
        acessarMenu(menu1, menu2);
    }

    @E("^fechar Tela$")
    public void fecharTela() {
        Auxiliar.fecharTela(1);
    }

    @E("^clicar no botao \"([^\"]*)\"$")
    public void clicarNoBotao(String botao) {
        clicarBotao(botao);
    }

    @Entao("^a janela de mensagem \"([^\"]*)\" sera: \"([^\"]*)\"$")
    public void aJanelaDeMensagemSera(String janela, String mensagem) {
        verificarMensagemJanela(janela, mensagem, false);
    }

    @Entao("^sera exibida a mensagem em tela: \"([^\"]*)\"$")
    public void seraExibidaAMensagemEmTela(String mensagem) {
        verificarMensagemTela(mensagem);
        appendToReport(getDriver());
    }

    @Entao("^a tela \"([^\"]*)\" estara fechada$")
    public void aTelaEstaraFechada(String tela) {
        verificarTelaFechada(tela);
    }

    @E("^clicar nos botoes \"([^\"]*)\" e \"([^\"]*)\"$")
    public void clicarNosBotoesE(String botao1, String botao2) {
        clicarBotao(botao1);
        clicarBotao(botao2);
    }

    @E("^clicar nos botoes \"([^\"]*)\" e pesquisar$")
    public void clicarNosBotoesEPesquisar(String botao) {
        clicarNoBotao(botao);
        clicarBotaoPesquisarConsultaCompleta();
    }

    @E("^no campo \"([^\"]*)\" clicar em pesquisar$")
    public void noCampoClicarEmPesquisar(String campo) {
        clicarBotaoPesquisar(campo);
    }

    @Quando("^preencher o campo \"([^\"]*)\" e pressionar Enter$")
    public void preencherCampo(String valor) {
        InputText.setInputTextEPressionarEnter(valor);
    }

    @E("^na janela \"([^\"]*)\" clicar em \"([^\"]*)\"$")
    public void naJanelaClicarEm(String janela, String botao) {
        clicarNoBotao(botao);
    }

    @E("^alterar a combo \"([^\"]*)\" para \"([^\"]*)\"$")
    public void alterarAComboPara(String combo, String valor) {
        BrowserVerifications.waitElementToBeEnable(combo, DEFAULT_TIME_OUT);
        Combos.setComboClicando(combo, valor);
    }

    @E("^alterar o campo \"([^\"]*)\" para \"([^\"]*)\"$")
    public void alterarOCampoPara(String nomecampo, String valor) {
        BrowserVerifications.waitElementToBeEnable(nomecampo, DEFAULT_TIME_OUT);
        InputText.setInputText(nomecampo, valor, true);
    }

    @Quando("^preencher a combo de Cidade \"([^\"]*)\"$")
    public void preencherAComboDeCidade(String cidade) {
        BrowserVerifications.waitElementToBeEnable("Cidade", DEFAULT_TIME_OUT);
        setComboCidade("Cidade", getCampo(cidade, getDadosCadastro().getEndereco().getCidade()));
    }

    @Entao("^limpar o campo \"([^\"]*)\"$")
    public void limparCampoInputText(String campo) {
        clearField(getInputTextXPath(campo));
    }

    @Entao("^limpar a combo \"([^\"]*)\"$")
    public void limparCampoCombo(String campo) {
        clearField(getXPathTextoCombo(campo));
    } //limpar a combo

    @E("^clicar no botao \"([^\"]*)\" na segunda tela$")
    public void clicarNoBotaoNaSegundaTela(String botao) {
        Botoes.clicarBotao(botao, 2);
    }

    @E("^selecionar a aba \"([^\"]*)\"$")
    public void selecionarAAba(String aba) {
        clicarAba(aba);
    }

    @E("^validar que a combo \"([^\"]*)\" foi alterada para \"([^\"]*)\"$")
    public void validarQueAComboFoiAlteradaPara(String combo, String valorEsperado) {
        validarComboAlterada(combo, valorEsperado, true);
    }

    @E("^fechar Tela \"([^\"]*)\"$")
    public void fecharTela(String tela) {
        Auxiliar.fecharTela(tela);
    }

    @Entao("^sera exibida a mensagem de sucesso em tela$")
    public void seraExibidaAMensagemDeSucessoEmTela() {
        verificarMensagemTela();
    }

    @E("^alterar o TextArea \"([^\"]*)\" para \"([^\"]*)\"$")
    public void alterarOTextAreaPara(String textArea, String texto) {
        TextArea.setTextArea(textArea, texto, true);

    }

    @E("^pressionar \"([^\"]*)\"$")
    public void pressionar(String key) {
        switch (key.toUpperCase()) {
            case "ENTER":
                sendKeys(Keys.ENTER);
                break;
            default:
                break;
        }
    }

    @Quando("^no campo \"([^\"]*)\" clicar no icone lupa$")
    public void noCampoClicarNoIconeLupa(String campo) {
        clicarBotaoPesquisaAzul(campo);
    }

    @E("^clicar no checkbox \"([^\"]*)\"$")
    public void clicarNoCheckBox(String checkbox) {
        Checkbox.clicarCheckBox(checkbox, "S");
    }


}