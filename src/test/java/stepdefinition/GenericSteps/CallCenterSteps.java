package stepdefinition.GenericSteps;


import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import main.Utils.ComumScreens.CentralAtendimento;

import static main.Utils.ComumScreens.Auxiliar.verificarMensagemJanela;
import static main.Utils.ComumScreens.CentralAtendimento.trocarIframe;


/**
 * @author Renan Ferreira dos Santos
 * @since 20/03/2019
 */
public class CallCenterSteps {


    @E("^pesquisar a ligacao \"([^\"]*)\" em Central de Atendimento$")
    public void pesquisarALigacaoEmCentralDeAtendimento(String ligacao) {
        CentralAtendimento.pesquisarLigacao(ligacao);
    }

    @E("^pesquisar a ligacao \"([^\"]*)\" em Central de Atendimento e validar que a \"" +
            "([^\"]*)\" esta igual a \"([^\"]*)\"$")
    public void pesquisarALigacaoEmCentralDeAtendimentoEValidarQueAEstaIgualA(String ligacao,
                                                                              String campo, String valor) {
        //PESQUISA LIGACAO
        CentralAtendimento.pesquisarLigacao(ligacao);

        //VALIDA VALOR DO CAMPO NA PARTE DE INFORMACOES DA LIGACAO
        CentralAtendimento.validarInfoLigacao(campo, valor);
    }


    @Entao("^a janela de mensagem callcenter \"([^\"]*)\" sera: \"([^\"]*)\"$")
    public void aJanelaDeMensagemCallcenterSera(String janela, String mensagem)  {

        //MUDA PARA O FRAME CENTRAL DE ATENDIMENTO
        trocarIframe(true);

        verificarMensagemJanela(janela, mensagem, true);

        //MUDA PARA FRAME PRINCIPAL
        trocarIframe(false);

    }
}
