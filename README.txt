﻿-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
�Requisitos para execucao:
	-JDK 11 INSTALADO
	-GRADLE INSTALADO

�EXECUCAO (para facilitar e possivel executar pelo arquivo executar-features.vbs):
	comando gradle: gradle clean test -Dcucumber.options="--tags @<tagExecucao> --tags @<idUnidade>"
	ex: gradle clean test -Dcucumber.options="--tags @Cadastro_AnaliseAgua --tags @2"

�TAGS DISPONIVEIS(Homologadas nas unidades ARAGUAIA e CACHOEIRO DE ITAPEMIRIM):
		@Cadastro_AnaliseAgua
		@Cadastro_Arrecadador
		@Cadastro_CodigoLeitura
		@Cadastro_DebitoAutomatico
		@Cadastro_Documentos
		@Cadastro_Grupo


(As execucoes poderao ser feitas por modulo, ex: @Cadastro)

Ao final da execucao os relatorios ficam armazenados na pasta relatorios.
				
				