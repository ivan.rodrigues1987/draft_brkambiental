Feature: C|A Web Services
  Assists when working with API calls in back-end


  Scenario: TC001 - Recurring Jobs: Autopost - Daily
    Given I need to access "Recurring Jobs" page
    When I check "Autopost - Daily" job and hit "Trigger Now" button
    Then confirm if the job "Succeeded"


  Scenario: TC002 - Recurring Jobs: CalculateSeriesExpectedDateRules
    Given I need to access "Recurring Jobs" page
    When I check "CalculateSeriesExpectedDateRules" job and hit "Trigger Now" button
    Then confirm if the job "Succeeded"


  Scenario: TC003 - Recurring Jobs: Autopost - Monthly
    Given I need to access "Recurring Jobs" page
    When I check "Autopost - Monthly" job and hit "Trigger Now" button
    Then confirm if the job "Succeeded"


    Scenario: TC004 - Recurring Jobs: NotifyJobExecutionsHistory
    Given I need to access "Recurring Jobs" page
    When I check "NotifyJobExecutionsHistory" job and hit "Trigger Now" button
    Then confirm if the job "Succeeded"