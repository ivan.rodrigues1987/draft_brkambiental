@CA
Feature: C|A Web Services
  Assists when working with API calls in back-end


  Scenario: [CMD-XXXX] - [Positive] - Succeded Exposure Data job run: AASX
    Given I have the following uri "/datastream/Api/Jobs/RunExposureData"
    When benchmarkSet "AASX" and asOfDate "2019-10-02" are requested
    When the HTTP Status 200 is returned and the message is "Job enqueued Successfully" to the "Exposure Data"
    Then "Exposure Data" job will be "Succeeded"
    And the jobId should be get from the execution above
    Then the JobId is provided the expected Status is: "SUCCESS"



  Scenario: [CMD-1324] - [Positive] - Database setting validation to the job: HFRI
    Given I have a new "Exposure Data" job into CMDB database
    When the "HFRI" job is set at "BenchmarkSetConfig"
    Then results should match on "RunasPreviousMonthLastBusinessDay" and "FIVEDAYS" and "US"


  Scenario: [CMD-1324] - [Positive] - Padding validation for: HFRI
    Given I have a new "Exposure Data" job into CMDB database
    When the "'HFRIEHI', 'HFRIFOF', 'HFRIFOFD'" job is set at "Benchmark"


  Scenario: [CMD-XXXX] - [Positive] - Constituent File - Roll Up to 100%:
    Given I have a new "Exposure Data" job into CMDB database
    When the BenchmarkId is "'160364', '160389', 'ERGLXXXX'", FamilyId is "EPRA", Currency is "USD" and Date is "2019-10-16"
    Then the benchmarkId into the Constituent file should roll up to 100

  Scenario: [CMD-XXXX] - [Positive] - Succeded Index Rates job: DailyDelayed
    Given I have the following uri "/datastream/Api/Jobs/Run"
    When jobType "DailyDelayed" and asOfDate "2019-10-02" are requested
    And the HTTP Status 200 is returned and the message is "Job enqueued Successfully" to the "Index Rate"
    And "Index Rate" job will be "Succeeded"
    And the jobId should be get from the execution above
    Then the JobId is provided the expected Status is: "SUCCESS"

@tom
  Scenario: [CMD-XXXX] - [Negative] - Failed Index Rate job: DailyDelayed
    Given I have the following uri "/datastream/Api/Jobs/Run"
    When jobType "DailyDelayed" and asOfDate "2019-24-24" are requested
    Then the HTTP Status 400 is returned and the message is "Job enqueued Successfully" to the "Index Rate"


  @tom
  Scenario: [CMD-XXXX] - [Negative] - Failed Exposure Data job: EPRA
    Given I have the following uri "/datastream/Api/Jobs/RunExposureData"
    When benchmarkSet "EPRA" and asOfDate "2019-22-22" are requested
    Then the HTTP Status 400 is returned and the message is "The request is invalid." to the "Exposure Data"